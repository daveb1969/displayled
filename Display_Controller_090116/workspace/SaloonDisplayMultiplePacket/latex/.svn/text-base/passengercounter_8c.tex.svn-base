\hypertarget{passengercounter_8c}{
\section{src/utils/passengercounter.c File Reference}
\label{passengercounter_8c}\index{src/utils/passengercounter.c@{src/utils/passengercounter.c}}
}


SNPserialport.c SNP Serial Port.  


{\ttfamily \#include \char`\"{}stdlib.h\char`\"{}}\par
{\ttfamily \#include \char`\"{}string.h\char`\"{}}\par
{\ttfamily \#include \char`\"{}../utils/tfx\_\-assert.h\char`\"{}}\par
{\ttfamily \#include \char`\"{}passengercounter.h\char`\"{}}\par
{\ttfamily \#include \char`\"{}usart.h\char`\"{}}\par
{\ttfamily \#include \char`\"{}gpio.h\char`\"{}}\par
{\ttfamily \#include \char`\"{}user\_\-board.h\char`\"{}}\par
\subsection*{Functions}
\begin{DoxyCompactItemize}
\item 
\hyperlink{structpassengercounter__str}{PassengerCounter} \hyperlink{passengercounter_8c_a909203f48944845dfc9b95a8e72648a7}{PassengerCounter\_\-construct} ()
\begin{DoxyCompactList}\small\item\em Passenger Counter constructor. \item\end{DoxyCompactList}\item 
void \hyperlink{passengercounter_8c_a1f673ea4bc68e62d8e074460041fbfa1}{PassengerCounter\_\-destroy} (\hyperlink{structpassengercounter__str}{PassengerCounter} passengerCounter)
\begin{DoxyCompactList}\small\item\em Passenger Counter destructor. \item\end{DoxyCompactList}\end{DoxyCompactItemize}


\subsection{Detailed Description}
SNPserialport.c SNP Serial Port. This file creates and provides services for the RS485 link to the Passenger Counter found above each doorway in the saloon carriage.


\begin{DoxyItemize}
\item Compiler: GNU GCC for AVR32
\item Supported devices: All AVR32UC3B0256.
\item AppNote:
\end{DoxyItemize}

The services provided allow the sending and receiving of commands and data from the PCN-\/1001 Passenger Counter mounted above each doorway in the saloon carriages on the train

\begin{DoxyAuthor}{Author}
TrainFX Limited: \href{http://www.trainfx.com}{\tt http://www.trainfx.com}\par
 Unit 15, Melbourne Business Court, Millennium Way, Pride Park, Derby, DE24 8HZ, United Kingdom. Tel: 01332 366 175 
\end{DoxyAuthor}


\subsection{Function Documentation}
\hypertarget{passengercounter_8c_a909203f48944845dfc9b95a8e72648a7}{
\index{passengercounter.c@{passengercounter.c}!PassengerCounter\_\-construct@{PassengerCounter\_\-construct}}
\index{PassengerCounter\_\-construct@{PassengerCounter\_\-construct}!passengercounter.c@{passengercounter.c}}
\subsubsection[{PassengerCounter\_\-construct}]{\setlength{\rightskip}{0pt plus 5cm}{\bf PassengerCounter} PassengerCounter\_\-construct (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}
\label{passengercounter_8c_a909203f48944845dfc9b95a8e72648a7}


Passenger Counter constructor. 

Create Passenger Counter data structure off the heap.

\begin{DoxyReturn}{Returns}
Return PassengerCounter$\ast$ 
\end{DoxyReturn}
\hypertarget{passengercounter_8c_a1f673ea4bc68e62d8e074460041fbfa1}{
\index{passengercounter.c@{passengercounter.c}!PassengerCounter\_\-destroy@{PassengerCounter\_\-destroy}}
\index{PassengerCounter\_\-destroy@{PassengerCounter\_\-destroy}!passengercounter.c@{passengercounter.c}}
\subsubsection[{PassengerCounter\_\-destroy}]{\setlength{\rightskip}{0pt plus 5cm}void PassengerCounter\_\-destroy (
\begin{DoxyParamCaption}
\item[{{\bf PassengerCounter}}]{ passengerCounter}
\end{DoxyParamCaption}
)}}
\label{passengercounter_8c_a1f673ea4bc68e62d8e074460041fbfa1}


Passenger Counter destructor. 

Remove Passenger Counter data structure off the heap. 