// Application framework.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#include "fwork.h"

// Software framework includes.
#include "compiler.h"
#include "board.h"
#include "gpio.h"
#include "pm.h"
#include "intc.h"
#include "tc.h"
#include "flashc.h"


#include "usart.h"
#include "wdt.h"
#include "user_board.h"
#include "adc.h"
#include "compiler.h"
#include "preprocessor.h"
#include "spi.h"
#include "conf_echelon.h"

#include "pdca.h"
#include "echelon.h"
#include "TFX-SPI.h"
#include "debug.h"
#include "delay.h"



// Standard C library includes.
#include "stdio.h"
#include "stdlib.h"
#include <string.h>

// Project includes.
#include "../utils/dotslist.h"
#include "../utils/displayExtern.h"
#include "../utils/message.h"
#include "../utils/circularbuffer.h"
#include "../utils/packet.h"
#include "../utils/network.h"
#include "../utils/slaveMasterSM.h"
#include "../utils/visualtest.h"
#include "../utils/passengercount.h"
#include "../utils/optoinputs.h"
#include "../utils/NVmemory.h"
#include "../utils/passengercount.h"
#include "../utils/ecatest.h"


//-----------------------------------------------------------------------------
// Macros.

#define TC_CHANNEL0    0
#define TC_CHANNEL1    1

// Min value of 1s
#define WDT_MIN_VALUE_US   1000000
// Min value of 4s
#define WDT_MAX_VALUE_US   9000000
// Step of 1s
#define WDT_CTRL_STEP_US   1000000

//-----------------------------------------------------------------------------
// Globals.

char semaphore = 0;
char clocked = 0;

unsigned short writeByte1;
unsigned short writeByte2;

unsigned short receivedMessageArraySPI[256];
unsigned short sendMessageArraySPI[256];
volatile char clkTick = 0;

unsigned short readByte1;
unsigned short readByte2;

unsigned short readStatus;
unsigned char  readData;

int MSState = 0;
int SMState = 0;

unsigned long readAnalogueLightChannel();
void changeLine2(char lineMode,char messageNumber);

volatile char changeBufferLine1 = 0;
volatile char changeBufferLine2 = 0;
volatile char changeBufferLine3 = 0;

volatile unsigned short line1MessageBufferToUse = 1;
volatile unsigned short line2MessageBufferToUse = 2;
volatile unsigned short line3MessageBufferToUse = 3;

unsigned short gBrightnessLevel = 0;
char singleLineTest = 0;
unsigned short gcommsHeartBeat = 0;
unsigned short gcurrentHeartBeat = 0;
volatile avr32_adc_t *adc = &AVR32_ADC; // ADC IP registers address
unsigned long adc_value_light = 0;

volatile unsigned int timerCounter = 0;

unsigned char displayType;

unsigned char serviceRS485Read = 0;


//! The channel instance for SPI TX, here PDCA channel 0 (highest priority).
//#define PDCA_CHANNEL_SPI_RX 0
//! The channel instance for SPI TX, here PDCA channel 1.
//#define PDCA_CHANNEL_SPI_TX 1

void run_main_loop();

static const tc_waveform_opt_t WAVEFORM_OPT1 = {
  .channel = TC_CHANNEL0,        // Channel selection.

  .bswtrg  = TC_EVT_EFFECT_NOOP, // Software trigger effect on TIOB
  .beevt   = TC_EVT_EFFECT_NOOP, // External event effect on TIOB
  .bcpc    = TC_EVT_EFFECT_NOOP, // RC compare effect on TIOB
  .bcpb    = TC_EVT_EFFECT_NOOP, // RB compare effect on TIOB

  .aswtrg  = TC_EVT_EFFECT_NOOP, // Software trigger effect on TIOA
  .aeevt   = TC_EVT_EFFECT_NOOP, // External event effect on TIOA
  .acpc    = TC_EVT_EFFECT_NOOP, // RC compare effect on TIOA
  .acpa    = TC_EVT_EFFECT_NOOP, // RA compare effect on TIOA

  .wavsel  = TC_WAVEFORM_SEL_UP_MODE_RC_TRIGGER, // Waveform mode
  .enetrg  = FALSE,              // External event trigger enable
  .eevt    = 0,                  // External event selection
  .eevtedg = TC_SEL_NO_EDGE,     // External event edge selection
  .cpcdis  = FALSE,              // Counter disable when RC compare
  .cpcstop = FALSE,              // Counter clock stopped with RC compare

  .burst   = FALSE,              // Burst signal selection
  .clki    = FALSE,              // Clock inversion
  .tcclks  = TC_CLOCK_SOURCE_TC2 // Internal source clock 3 :
};

static const tc_waveform_opt_t WAVEFORM_OPT2 = {
  .channel = TC_CHANNEL1,        // Channel selection.

  .bswtrg  = TC_EVT_EFFECT_NOOP, // Software trigger effect on TIOB
  .beevt   = TC_EVT_EFFECT_NOOP, // External event effect on TIOB
  .bcpc    = TC_EVT_EFFECT_NOOP, // RC compare effect on TIOB
  .bcpb    = TC_EVT_EFFECT_NOOP, // RB compare effect on TIOB

  .aswtrg  = TC_EVT_EFFECT_NOOP, // Software trigger effect on TIOA
  .aeevt   = TC_EVT_EFFECT_NOOP, // External event effect on TIOA
  .acpc    = TC_EVT_EFFECT_NOOP, // RC compare effect on TIOA
  .acpa    = TC_EVT_EFFECT_NOOP, // RA compare effect on TIOA

  .wavsel  = TC_WAVEFORM_SEL_UP_MODE_RC_TRIGGER, // Waveform mode
  .enetrg  = FALSE,              // External event trigger enable
  .eevt    = 0,                  // External event selection
  .eevtedg = TC_SEL_NO_EDGE,     // External event edge selection
  .cpcdis  = FALSE,              // Counter disable when RC compare
  .cpcstop = FALSE,              // Counter clock stopped with RC compare

  .burst   = FALSE,              // Burst signal selection
  .clki    = FALSE,              // Clock inversion
  .tcclks  = TC_CLOCK_SOURCE_TC5 // Internal source clock 5 :
};


static const tc_interrupt_t TC_INTERRUPT0 = {
  .etrgs = 0,
  .ldrbs = 0,
  .ldras = 0,
  .cpcs  = 1,
  .cpbs  = 0,
  .cpas  = 0,
  .lovrs = 0,
  .covfs = 0
};


static const tc_interrupt_t TC_INTERRUPT1 = {
  .etrgs = 0,
  .ldrbs = 0,
  .ldras = 0,
  .cpcs  = 1,
  .cpbs  = 0,
  .cpas  = 0,
  .lovrs = 0,
  .covfs = 0
};

// Set-up power management to run CPU at 60MHz with Peripheral bus at 30MHz
static pm_freq_param_t FREQ_PARAM = {
  .cpu_f        = 48000000,
  .pba_f        = 12000000,
  .osc0_f       = FOSC0,
  .osc0_startup = OSC0_STARTUP
};

//! \Brief Initialise USART 1 to communicate
static const gpio_map_t USART_GPIO_MAP =
{
  {RS485_USART_RX_PIN, RS485_USART_RX_FUNCTION},
  {RS485_USART_TX_PIN, RS485_USART_TX_FUNCTION}
};

// USART options.
static const usart_options_t USART_OPTIONS =
{
  .baudrate     = 9600,
  .charlength   = 8,
  .paritytype   = USART_NO_PARITY,
  .stopbits     = USART_1_STOPBIT,
  .channelmode  = USART_NORMAL_CHMODE
};



//-----------------------------------------------------------------------------
// Globals.

int displayptr = 0;
int scrollrate = 0;
int blank      = 0;
#if defined(OLDCODE)
  char rowcount  = 0;
#else
  char renderRow  =0;
#endif
int message_repeat_counter = 0;

int message_index      = 1; // Index of the message currently being played.
int next_message_index = 1; // Index of the next message to play.
int message_changed    = 0; // A flag that goes up when the message changes.


// Other storage.
DotsList dots_list = NULL;
//Message custom_message = NULL;

//-----------------------------------------------------------------------------
// Debugging stuff.
#if defined(DEBUG)
  #define DEBUG_ECHO
  #define USART_PRINT(MSG) \
    usart_write_line(EXAMPLE_USART, MSG)
#else // DEBUG
  #define USART_PRINT(MSG)
#endif // DEBUG


//-----------------------------------------------------------------------------
// Initialise storage.
void initialiseStorage(void) {

}

/*! \brief The PDCA interrupt handler.
 *
 */

__attribute__((__interrupt__)) void pdca_int_handler(void)
{
	volatile avr32_pdca_channel_t *pdca_channeltx = pdca_get_handler(PDCA_CHANNEL_SPI_TX);
	volatile avr32_pdca_channel_t *pdca_channelrx = pdca_get_handler(PDCA_CHANNEL_SPI_RX);

	//if ( (pdca_channeltx->ISR.trc == 1) && (pdca_channelrx->ISR.trc == 1)
	//		&&  ( (pdca_channeltx->ISR.terr > 0) ||  (pdca_channelrx->ISR.terr > 0) ) )

	if ( (pdca_channelrx->ISR.terr > 0) )
	{
		pdca_disable_interrupt_transfer_error( PDCA_CHANNEL_SPI_RX );
	}

	if ( (pdca_channeltx->ISR.terr > 0) )
	{
		pdca_disable_interrupt_transfer_error( PDCA_CHANNEL_SPI_TX );
	}

	if ( (pdca_channelrx->ISR.trc == 1) )
	{
		pdca_disable_interrupt_transfer_complete( PDCA_CHANNEL_SPI_RX );
	}

	if ( (pdca_channeltx->ISR.trc == 1) )
	{
		pdca_disable_interrupt_transfer_complete( PDCA_CHANNEL_SPI_TX );
	}

	//Have both the DMA SPI_RX or DMA SPI_TX transfers completed?
	//If the DMA Transfers have completed successfully two interrupts will be asserted -
	//one for DMA SPI_RX complete and one for DMA SPI_TX complete.
	//Each DMA transfer may take place at differing times and the code accounts
	//for this be disabling the applicable DMA channel interrupt after completion.
	if ( (pdca_channeltx->ISR.trc == 1) && (pdca_channelrx->ISR.trc == 1)
			&& (pdca_channeltx->ISR.terr == 0) &&  (pdca_channelrx->ISR.terr == 0) )
	{
		thisDisplay->network->numberSuccessfulDMATransfers++;
		thisDisplay->network->tryDMATransfer = 0;
		thisDisplay->network->failedDMATransfer = 0;
	}

	//Have either of the DMA SPI_RX or DMA SPI_TX transfers failed?
	//If either DMA Transfer has failed an interrupt will be asserted and be caught in this 'if' construct.
	//The tryDMATransfer flag will be reset to zero.
	//The code that is waiting for the DMA transfers to complete will stop waiting in a tight while loop
	//for the DMA transfers to complete.
	//After dropping out of the tight While loop; the code waiting for the DMA transfers to complete
	//disables both DMA SPI_RX and DMA SPI_TX transfers that may still be continuing to take place.
	if ( (pdca_channeltx->ISR.trc == 1) && (pdca_channelrx->ISR.trc == 1)
			&& ( (pdca_channeltx->ISR.terr > 0) || (pdca_channelrx->ISR.terr > 0) ) )
	{
		thisDisplay->network->numberFailedDMATransfers++;
		thisDisplay->network->tryDMATransfer = 0;
		thisDisplay->network->failedDMATransfer = 1;
	}
}



#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
//-----------------------------------------------------------------------------
// Interrupt handler.
__attribute__((__interrupt__)) void usart_int_handler(void) {

static int rxCharacter = 65;
static unsigned char rxChar = 65;
static int counter = 0;

if ( usart_test_hit( RS485_USART ) )
	{
    	  rxCharacter = usart_getchar(RS485_USART);
    	  if (rxCharacter == -1)
    	  {
    		  counter++;
    		  thisDisplay->passengerCount->passengerCountComms->triggerRXInterruptCounter++;

        	  if ( counter > 50  )
    		  {
    		  //Disable interrupts globally.
    		  Disable_global_interrupt();

    		  // Initialize USART in RS232 mode.
    		  usart_init_rs232(RS485_USART, &USART_OPTIONS, FREQ_PARAM.pba_f);

    		  //Disable RS485 RX Interrupt
    		  RS485_USART->idr = AVR32_USART_IER_RXRDY_MASK;

    		  // Enable interrupts globally.
			  Enable_global_interrupt();

    		  //Enable RS485 RX Interrupt
    		  RS485_USART->ier = AVR32_USART_IER_RXRDY_MASK;
    		  }
    		  return;
    	  }
    	  counter = 0;
    	  rxChar = (char) rxCharacter;
    	  cb_push_back(thisDisplay->passengerCount->passengerCountComms->receiveCircularBuffer, &rxChar );
	}
}
#endif



#if ( DISPLAY_TYPE == SALOON_DISPLAY) && (DISPLAY_FUNCTIONALITY == ECAON_PCON || DISPLAY_FUNCTIONALITY == ECAON_PCOFF || DISPLAY_FUNCTIONALITY == CREW_COMMS)
//-----------------------------------------------------------------------------
// Interrupt handler.
__attribute__((__interrupt__)) void optoIOScanServiceInterrupt(void) {

   AVR32_TC.channel[TC_CHANNEL1].sr;

   static unsigned char lastStateOpto1;
   static unsigned char lastStateOpto2;
   static unsigned char lastStateOpto3;
   static unsigned char firstTime = 0;


if (thisDisplay->operationalMode == OperationalModeTestECACyclicDisplayed )
{
	   thisDisplay->optoinputs->opto_Input1->optoInput = thisDisplay->ecaTest->opto_Input1;
	   thisDisplay->optoinputs->opto_Input2->optoInput = thisDisplay->ecaTest->opto_Input2;
	   thisDisplay->optoinputs->opto_Input3->optoInput = thisDisplay->ecaTest->opto_Input3;
}
else
{
	   thisDisplay->optoinputs->opto_Input1->optoInput = gpio_get_pin_value(OPTO_01_GPIO);
	   thisDisplay->optoinputs->opto_Input2->optoInput = gpio_get_pin_value(OPTO_02_GPIO);
	   thisDisplay->optoinputs->opto_Input3->optoInput = gpio_get_pin_value(OPTO_03_GPIO);
}

   if (firstTime == 0)
   {
	   lastStateOpto1 = thisDisplay->optoinputs->opto_Input1->optoInput;
	   lastStateOpto2 = thisDisplay->optoinputs->opto_Input2->optoInput;
	   lastStateOpto3 = thisDisplay->optoinputs->opto_Input3->optoInput;
	   firstTime = 1;
   }


   if ( thisDisplay->optoinputs->opto_Input1->optoInput != lastStateOpto1)
   {
	   thisDisplay->optoinputs->opto_Input1->edge_detect = 1;
   }
   else
   {
	   thisDisplay->optoinputs->opto_Input1->edge_detect = 0;
   }

   if ( thisDisplay->optoinputs->opto_Input2->optoInput != lastStateOpto2)
   {
	   thisDisplay->optoinputs->opto_Input2->edge_detect = 1;
   }
   else
   {
	   thisDisplay->optoinputs->opto_Input2->edge_detect = 0;
   }

   if ( thisDisplay->optoinputs->opto_Input3->optoInput != lastStateOpto3)
   {
	   thisDisplay->optoinputs->opto_Input3->edge_detect = 1;
   }
   else
   {
	   thisDisplay->optoinputs->opto_Input3->edge_detect = 0;
   }

   if ( thisDisplay->optoinputs->opto_Input1->optoInput != lastStateOpto1 )
   {

	   thisDisplay->optoinputs->opto_Input1->optoChangedCounter++;
   }

   if ( thisDisplay->optoinputs->opto_Input2->optoInput != lastStateOpto2 )
   {

	   thisDisplay->optoinputs->opto_Input2->optoChangedCounter++;
   }

   if ( thisDisplay->optoinputs->opto_Input3->optoInput != lastStateOpto3 )
   {

	   thisDisplay->optoinputs->opto_Input3->optoChangedCounter++;
   }

   if ( thisDisplay->optoinputs->opto_Input1->optoInput != lastStateOpto1 \
		   || thisDisplay->optoinputs->opto_Input2->optoInput != lastStateOpto2 \
		      || thisDisplay->optoinputs->opto_Input3->optoInput != lastStateOpto3 )
   {

	   thisDisplay->optoinputs->changedState = 1;
   }

   lastStateOpto1 = thisDisplay->optoinputs->opto_Input1->optoInput;
   lastStateOpto2 = thisDisplay->optoinputs->opto_Input2->optoInput;
   lastStateOpto3 = thisDisplay->optoinputs->opto_Input3->optoInput;

}
#endif


//-----------------------------------------------------------------------------
// Interrupt handler.
__attribute__((__interrupt__)) void panelScanServiceInterrupt(void) {

	AVR32_TC.channel[TC_CHANNEL0].sr;


	//SCANS EVERY 4.16ms - 240Hz

	timerCounter++;
	if (timerCounter < 200)
	{
	  clkTick = 0;
	}

	if (timerCounter > 200)
	{
	  clkTick = 1;
	}
	if (timerCounter > 2*200)
	{
	  timerCounter = 0;
	}
#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif



static int currentRenderRow = 0;
static int cycleCounter = 0;

#if DISPLAY_TYPE == SALOON_DISPLAY

    if ( currentRenderRow <= thisDisplay->numberRows )
    {
    	if ( thisDisplay->enablePanelScan == 0x01)
    	{
        Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
    	}
    }

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY

    if ( currentRenderRow <= thisDisplay->numberRows )
    {
		if (thisDisplay->operationalMode == OperationalModeTestVisual)
		{
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
		}
		else
		{
#if	END_CLIENT == ARRIVA
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
#elif END_CLIENT == SOUTHERN
		Displayline_renderLine(thisDisplay->displayLine3, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine2, currentRenderRow);
#elif END_CLIENT == FGW
		Displayline_renderLine(thisDisplay->displayLine3, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine2, currentRenderRow);
#elif END_CLIENT == CLASS150
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
#else
  #error No DISPLAY_TYPE defined
#endif
		}
    }
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY

    if ( currentRenderRow <= thisDisplay->numberRows )
    {
		if (thisDisplay->operationalMode == OperationalModeTestVisual)
		{
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
		}
		else
		{
#if	END_CLIENT == ARRIVA
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
#elif END_CLIENT == SOUTHERN
		Displayline_renderLine(thisDisplay->displayLine3, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine2, currentRenderRow);
#elif END_CLIENT == FGW
		Displayline_renderLine(thisDisplay->displayLine3, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
		Displayline_renderLine(thisDisplay->displayLine2, currentRenderRow);
#elif END_CLIENT == CLASS150
		Displayline_renderLine(thisDisplay->displayLine1, currentRenderRow);
#else
  #error No DISPLAY_TYPE defined
#endif
		}
    }
#else
  #error No DISPLAY_TYPE defined
#endif

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif


	Display_displayRow( currentRenderRow );
	currentRenderRow++;

	//If the currentRenderRow is the last row to render - that is the number of
	//physical rows on the display + 1.
	//Set the brightness level for the LED's upon the display
	if (  ( thisDisplay->enablePanelScan == 0x01) )
	{
		if ( currentRenderRow == ( thisDisplay->numberRows + 1) )
		{
			Display_set_dotCorrection(FULL_BRIGHTNESS, LED_DRIVERS_IN_CHAIN);
		}
	}

	if ( currentRenderRow > ( thisDisplay->numberRows + 1) )
	{
	    currentRenderRow = 0;
	    cycleCounter++;
	}

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif
}



/*! \brief Initializes Echelon SPI Channel Comms resources.
 */
static void echelon_resources_init(void)
{
  static const gpio_map_t ECHELON_SPI_GPIO_MAP =
  {
    {ECHELON_SPI_SCK_PIN,          ECHELON_SPI_SCK_FUNCTION         },  // SPI Clock.
    {ECHELON_SPI_MISO_PIN,         ECHELON_SPI_MISO_FUNCTION        },  // MISO.
    {ECHELON_SPI_MOSI_PIN,         ECHELON_SPI_MOSI_FUNCTION        },  // MOSI.
    {ECHELON_SPI_NPCS_PIN,         ECHELON_SPI_NPCS_FUNCTION}       };  // Chip Select NPCS.

  spi_options_t spiOptions =
  {
    .reg          = ECHELON_SPI_CHANNEL,      // Defined in conf_echelon.h.
    .baudrate     = ECHELON_SPI_MASTER_SPEED, // Defined in conf_echelon.h.
    .bits         = ECHELON_SPI_BITS,         // Defined in conf_echelon.h.
    .spck_delay   = 0,
    .trans_delay  = 150,
    .stay_act     = 1,
    .spi_mode     = 3,
    .modfdis      = 1
  };


  //! \brief Assign I/O to SPI channel.
  //!
  gpio_enable_module(ECHELON_SPI_GPIO_MAP,
                     sizeof(ECHELON_SPI_GPIO_MAP) / sizeof(ECHELON_SPI_GPIO_MAP[0]));

  //! \brief Initialise the SPI Peripheral as Master.
  //!
  // Initialize as master.
  spi_initMaster(ECHELON_SPI, &spiOptions);

  //! \brief Set selection mode: variable_ps, pcs_decode, delay.
  //!
  spi_selectionMode(ECHELON_SPI, 0, 0, 0);

  //! \brief Enable the SPI peripheral.
  //!
  spi_enable(ECHELON_SPI);

  //! \brief Initialize SPI channel used to communicate to Echelon at 12MHz.
  //!
  echelon_init(spiOptions, 12000000);
}


//-----------------------------------------------------------------------------
// Initialise the software subsystems.
void initialiseSubsystems(void) {



  // GPIO pin/adc-function map.

  /*DB REMOVED
  static const gpio_map_t ADC_GPIO_MAP =
  {
	{ADC_LIGHT_PIN, ADC_LIGHT_FUNCTION},
	{ADC_TEMPERATURE_PIN, ADC_TEMPERATURE_FUNCTION}
  };
  */

  // Set-up power management to run CPU and peripheral bus at required speeds.
  pm_configure_clocks(&FREQ_PARAM);

  delay_init( FREQ_PARAM.cpu_f );
  // Switch to external oscillator 0.
  //pm_switch_to_osc0(&AVR32_PM, FOSC0, OSC0_STARTUP);

  /*DB REMOVED
  // Assign and enable GPIO pins to the ADC function.
  gpio_enable_module(ADC_GPIO_MAP, sizeof(ADC_GPIO_MAP) / sizeof(ADC_GPIO_MAP[0]));

  // configure ADC
  // Lower the ADC clock to match the ADC characteristics (because we configured
  // the CPU clock to 12MHz * 5 = 60MHz, and the ADC clock characteristics are usually lower;
  // cf. the ADC Characteristic section in the datasheet).
  // Set the ADC clock = CLK_ADC/ ((PRESCALE+1)*2)
  // Current ADC clock = 60MHz / (( 9 + 1) * 2) = 60 / 12 = 3 Mhz
  AVR32_ADC.mr |= 0x09 << AVR32_ADC_MR_PRESCAL_OFFSET;
  adc_configure(adc);
  */


  //Allow re-arbitration to take place of the connection between the
  //CPU Instruction Master and the Slave it is connected to at one beat intervals.
  //The undefined length burst for which the CPU instruction Master uses the HMATRIX
  //bus can be broken and re-arbitration take place in ONE beat intervals.
  AVR32_HMATRIX.mcfg[AVR32_HMATRIX_MASTER_CPU_INSN] = 0x1;

  //Allow re-arbitration to take place of the connection between the
  //PDCA Master and the Slave it is connected to at ONE beat intervals.
  //The undefined length burst for which the PDCA Master uses the HMATRIX
  //bus can be broken and re-arbitration take place in SIXTEEN beat intervals.
  AVR32_HMATRIX.mcfg[AVR32_HMATRIX_MASTER_PDCA] = 0x1;

  //avr32_hmatrix_scfg_t scfg1 = AVR32_HMATRIX.SCFG[AVR32_HMATRIX_SLAVE_FLASH];
  //scfg1.defmstr_type = AVR32_HMATRIX_DEFMSTR_TYPE_LAST_DEFAULT;
  //AVR32_HMATRIX.SCFG[AVR32_HMATRIX_SLAVE_FLASH] = scfg1;

  //avr32_hmatrix_scfg_t scfg2 = AVR32_HMATRIX.SCFG[AVR32_HMATRIX_SLAVE_SRAM];
  //scfg2.defmstr_type = AVR32_HMATRIX_DEFMSTR_TYPE_LAST_DEFAULT;
  //AVR32_HMATRIX.SCFG[AVR32_HMATRIX_SLAVE_SRAM] = scfg2;



#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
{
	//opto_io_scan_rate = 24000 timer ticks.
	//We are using TIMER_CLOCK5 = PBA Clock/128 = 12000000/128 = 93750 Hz
	//The resolution of the timer counter = 1/93750 = 11 uSec per timer tick.
	//opto_io_scan_rate every 24000 x 11 uSec =  256 ms
	//The TC counter counts to 2400 before asserting complete and
	//triggering the interrupt routine for the opto_io_scan.
	//The interrupt routine designated as associated to the opto_io_scan_rate TC
	//completing is setup further below.
	//const int opto_io_scan_rate = 24000;
	const int opto_io_scan_rate = 24000;
	tc_init_waveform(&AVR32_TC, &WAVEFORM_OPT2);
	tc_write_rc(&AVR32_TC, TC_CHANNEL1, opto_io_scan_rate);
	tc_start(&AVR32_TC, TC_CHANNEL1);
}
#endif

// Set-up timer counter to trigger ISR for row display:
//
#if DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT != CLASS319 && END_CLIENT !=CLASS150
{
	//row_trigger_value = 48000000/1000/200 =  240 timer ticks
    //We are using TIMER_CLOCK5 = PBA Clock/128 = 12000000/2 = 6000000 Hz
	//The resolution of the timer counter = 1/6000000 = 0.16 uSec per timer tick.
	//row_trigger_value every (240*64 + 129) x 0.16 uSec =  2.21 ms
	//The TC counter counts to (240*64 + 129) before asserting complete and
	//triggering the interrupt routine for the display row scan.
	//The interrupt routine designated as associated to the TC
	//completing is setup below.
	//Time to scan a 8 row display = 8 * 2.21 ms = 16.68 ms
	//const int row_trigger_value = 240;

	//const int row_trigger_value = (212*64);
	const int row_trigger_value = (215*64);
	//const int row_trigger_value = (214*64 + 129);
	//const int row_trigger_value = (214*57);

	tc_init_waveform(&AVR32_TC, &WAVEFORM_OPT1);
	tc_write_rc(&AVR32_TC, TC_CHANNEL0, row_trigger_value);
	tc_start(&AVR32_TC, TC_CHANNEL0);
}
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT == CLASS319 || END_CLIENT == CLASS150
{
	//row_trigger_value = 48000000/1000/200 =  240 timer ticks
    //We are using TIMER_CLOCK5 = PBA Clock/128 = 12000000/2 = 6000000 Hz
	//The resolution of the timer counter = 1/6000000 = 0.16 uSec per timer tick.
	//row_trigger_value every (240*64 + 129) x 0.16 uSec =  2.21 ms
	//The TC counter counts to (240*64 + 129) before asserting complete and
	//triggering the interrupt routine for the display row scan.
	//The interrupt routine designated as associated to the TC
	//completing is setup below.
	//Time to scan a 8 row display = 8 * 2.21 ms = 16.68 ms
	//const int row_trigger_value = 240;


	const int row_trigger_value = (214*57);

	tc_init_waveform(&AVR32_TC, &WAVEFORM_OPT1);
	tc_write_rc(&AVR32_TC, TC_CHANNEL0, row_trigger_value);
	tc_start(&AVR32_TC, TC_CHANNEL0);
}
#endif

#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
{
	//row_trigger_value = 87 timer ticks
    //We are using TIMER_CLOCK5 = PBA Clock/128 = 12000000/2 = 6000000 Hz
	//The resolution of the timer counter = 1/6000000 = 0.16 uSec per timer tick.
	//row_trigger_value every (82 x 64) x 0.16 uSec =  840uSec, approx. 1 ms.
	//The TC counter counts to 82 before asserting complete and
	//triggering the interrupt routine for the display row scan.
	//The interrupt routine designated as associated to the TC
	//completing is setup below.
	//Time to scan a 25 row display 25 * 840 uSec = 21 ms
	const int row_trigger_value = 82*64;
	tc_init_waveform(&AVR32_TC, &WAVEFORM_OPT1);
	tc_write_rc(&AVR32_TC, TC_CHANNEL0, row_trigger_value);
	tc_start(&AVR32_TC, TC_CHANNEL0);
}
#endif

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
{
	//row_trigger_value = 87 timer clicks
    //We are using TIMER_CLOCK5 = PBA Clock/128 = 12000000/2 = 6000000 Hz
	//The resolution of the timer counter = 1/6000000 = 0.16 uSec per timer tick.
	//row_trigger_value every (82 x 64) x 0.16 uSec =  840uSec, approx. 1 ms.
	//The TC counter counts to 82 before asserting complete and
	//triggering the interrupt routine for the display row scan.
	//The interrupt routine designated as associated to the TC
	//completing is setup below.
	//const int row_trigger_value = 87;
	const int row_trigger_value = 82*64;
	tc_init_waveform(&AVR32_TC, &WAVEFORM_OPT1);
	tc_write_rc(&AVR32_TC, TC_CHANNEL0, row_trigger_value);
	tc_start(&AVR32_TC, TC_CHANNEL0);
}
#endif



  // Initialise LED Driver Dot Correction
  {
	  Display_set_dotCorrection(DIMMED_BRIGHTNESS, LED_DRIVERS_IN_CHAIN);
  }

  // Clear the display
  {
	  Display_clearDisplay( thisDisplay->numberCols );
  }
  // Initialize resources: GPIO, SPI.
  {
	echelon_resources_init();
  }

  static const pdca_channel_options_t PDCA_SPI_RX_OPTIONS =
  {
    .addr = (void *)receivedMessageArraySPI,               			// memory address
    .size = sizeof(receivedMessageArraySPI),               			// transfer counter
    .r_addr = NULL,                          						// next memory address
    .r_size = 0,                             						// next transfer counter
    .pid = PDCA_PID_SPI_RX,                  						// select peripheral
    .transfer_size = PDCA_TRANSFER_SIZE_HALF_WORD                   // select size of the transfer
  };

  // PDCA channel options
  static const pdca_channel_options_t PDCA_SPI_TX_OPTIONS =
  {
    .addr = (void *)sendMessageArraySPI,               				// memory address
    .size = sizeof(sendMessageArraySPI),               				// transfer counter
    .r_addr = NULL,                           						// next memory address
    .r_size = 0,                              						// next transfer counter
    .pid = PDCA_PID_SPI_TX,                   						// select peripheral
    .transfer_size = PDCA_TRANSFER_SIZE_HALF_WORD      				// select size of the transfer
  };

  // Init PDCA channel with the pdca_options.
  pdca_init_channel(PDCA_CHANNEL_SPI_TX, &PDCA_SPI_TX_OPTIONS); // init PDCA channel with options.
  pdca_init_channel(PDCA_CHANNEL_SPI_RX, &PDCA_SPI_RX_OPTIONS); // init PDCA channel with options.

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
  // To specify which current Watchdog value
  volatile U32 current_wdt_value = WDT_MAX_VALUE_US;

  //Enable watchdog timer
  wdt_enable(current_wdt_value);
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
  //Enable glitch filtering on required GPIO
  gpio_enable_pin_glitch_filter(OPTO_01_GPIO);
  gpio_enable_pin_glitch_filter(OPTO_02_GPIO);
  gpio_enable_pin_glitch_filter(OPTO_03_GPIO);

  //Enable the internal pull resistor present at GPIO input.
  //Prevents the Opto Input Floating around.
  //DB Changed
  gpio_enable_pin_pull_up(OPTO_01_GPIO);
  gpio_enable_pin_pull_up(OPTO_02_GPIO);
  gpio_enable_pin_pull_up(OPTO_03_GPIO);
  //DB Changed

#endif

  //Enable glitch filtering on required GPIO
  gpio_enable_pin_glitch_filter(HREQA_GPIO);
  gpio_enable_pin_glitch_filter(SREQA_GPIO);
  gpio_enable_pin_glitch_filter(SRDYA_GPIO);

  gpio_enable_pin_glitch_filter(HREQB_GPIO);
  gpio_enable_pin_glitch_filter(SREQB_GPIO);
  gpio_enable_pin_glitch_filter(SRDYB_GPIO);

  gpio_enable_pin_glitch_filter(ECHELON_SPI_MISO_PIN);
  gpio_enable_pin_glitch_filter(ECHELON_SPI_MOSI_PIN);
  gpio_enable_pin_glitch_filter(ECHELON_SPI_NPCS_PIN);

  //Enable the internal pull resistor present at GPIO input.
  //Prevents the GPIO Input Floating around.
  //DB Changed
  gpio_enable_pin_pull_up(HREQA_GPIO);
  gpio_enable_pin_pull_up(SREQA_GPIO);
  gpio_enable_pin_pull_up(SRDYA_GPIO);

  gpio_enable_pin_pull_up(HREQB_GPIO);
  gpio_enable_pin_pull_up(SREQB_GPIO);
  gpio_enable_pin_pull_up(SRDYB_GPIO);

  gpio_enable_pin_pull_up(ECHELON_SPI_MISO_PIN);
  gpio_enable_pin_pull_up(ECHELON_SPI_MOSI_PIN);
  gpio_enable_pin_pull_up(ECHELON_SPI_NPCS_PIN);


#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
  // Assign GPIO to USART.
  gpio_enable_module(USART_GPIO_MAP,  sizeof(USART_GPIO_MAP) / sizeof(USART_GPIO_MAP[0]) );

  //Enable the internal pull resistor present at GPIO input.
  //Prevents the RS485_USART_RX_PIN Input Floating around.
  //DB Changed
  gpio_enable_pin_pull_up(RS485_USART_RX_PIN);

  // Initialize USART in RS232 mode.
  usart_init_rs232(RS485_USART, &USART_OPTIONS, FREQ_PARAM.pba_f);

#endif

  // Initialise interrupts.
  {
    // Initialize interrupt vectors.
    INTC_init_interrupts();

    // Register the compare interrupt handler to the interrupt controller
    // and enable the compare interrupt.
    // (__int_handler) &pdca_int_handler The handler function to register.
    // AVR32_PDCA_IRQ_0 The interrupt line to register to.
    // AVR32_INTC_INT0  The priority level to set for this interrupt line.
    // INTC_register_interrupt(__int_handler handler, int line, int priority);


    INTC_register_interrupt( (__int_handler) &pdca_int_handler, AVR32_PDCA_IRQ_0, AVR32_INTC_INT2);

    INTC_register_interrupt(&panelScanServiceInterrupt, AVR32_TC_IRQ0, AVR32_INTC_INT1);
    tc_configure_interrupts(&AVR32_TC, TC_CHANNEL0, &TC_INTERRUPT0);

#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
    INTC_register_interrupt(&optoIOScanServiceInterrupt, AVR32_TC_IRQ1, AVR32_INTC_INT0);
    tc_configure_interrupts(&AVR32_TC, TC_CHANNEL1, &TC_INTERRUPT1);
#endif

#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
    // Register the USART interrupt handler to the interrupt controller.
    // usart_int_handler is the interrupt handler to register.
    // RS485_USART_IRQ is the IRQ of the interrupt handler to register.
    // AVR32_INTC_INT1 is the interrupt priority level to assign to the group of
    // this IRQ.
    INTC_register_interrupt(&usart_int_handler, RS485_USART_IRQ, AVR32_INTC_INT3);

#endif

    // Enable interrupts globally.
    Enable_global_interrupt();

#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
    Display_initialisePasssengerCounters();
#endif
  }
}


unsigned long readAnalogueLightChannel()
{
	static unsigned long measuredLightIntensity;
	static signed char highIntensityCounter;
	static unsigned char highState = 0;

	  // Get Analogue Reading from Light Sensor
	  // found upon Front Destination Display
	  adc_enable(adc,ADC_LIGHT_CHANNEL);
	  if (adc_get_status ( adc, ADC_LIGHT_CHANNEL) )
	  {
		  adc_start(adc);
		  if ( adc_check_eoc(adc, ADC_LIGHT_CHANNEL) )
		  {
		  adc_value_light = adc_get_value(adc, ADC_LIGHT_CHANNEL);
		  adc_disable(adc,ADC_LIGHT_CHANNEL);
		  }
	  }

	  measuredLightIntensity = (adc_value_light * 100 /1023);

	  if ( measuredLightIntensity > 80 && highIntensityCounter < 10 )
	  {
		  highIntensityCounter += 1;
	  }

	  if (measuredLightIntensity < 80 && highIntensityCounter > -10)
	  {
		  highIntensityCounter -= 1;
	  }

	  if (highIntensityCounter > 5 && (highState == 0 ))
	  {
		  highState = 1;
		  tc_stop(&AVR32_TC, TC_CHANNEL0);
		  Display_set_dotCorrection(FULL_BRIGHTNESS, LED_DRIVERS_IN_CHAIN);
		  tc_start(&AVR32_TC, TC_CHANNEL0);
	  }
	  if (highIntensityCounter < -5 && (highState == 1) )
	  {
		  highState = 0;
		  tc_stop(&AVR32_TC, TC_CHANNEL0);
		  Display_set_dotCorrection(DIMMED_BRIGHTNESS, LED_DRIVERS_IN_CHAIN);
		  tc_start(&AVR32_TC, TC_CHANNEL0);
	  }

	  return measuredLightIntensity;

}

void delay(unsigned long count){

	long delay1 = 0;
	long delay2 = 0;
	long delay3 = 0;
	while (delay1 < count){
		delay1++;
		while (delay2 < count){
				delay2++;
				while (delay3 < count){
						delay3++;
					}
			}
	}
}


//-----------------------------------------------------------------------------
// Run the main loop of the application.
void runMainLoop() {


  thisDisplay->network->disableAutoTestMode = 1;
#if CENTER_ENABLE == ENABLED
  thisDisplay->operationalMode = OperationalModeStandardCentered;
#elif CENTER_ENABLE == DISABLED
  thisDisplay->operationalMode = OperationalModeStandard;
#else
#error No CENTER_ENABLE defined
#endif
  while(1)
  {

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif

	if ( (thisDisplay->nvmemory->readNeuronState != 5) )
	{
     thisDisplay->operationalMode = OperationalModeStandard;
	 NVMemory_readNeuron( thisDisplay->displayLine1, thisDisplay->network, thisDisplay->nvmemory );
	}

	Display_scanOperationalMode( thisDisplay );


#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif

#if TEST_MODE == ENABLE_NVMTEST_WRITE_PL_TRIGGERED || TEST_MODE == ENABLE_NVMTEST_WRITE_INTERNAL && CENTER_ENABLE == DISABLED
  thisDisplay->network->disableAutoTestMode = 1;
  if ( thisDisplay->nvmemory->readNeuronState == 5 )
  {
	  if ( thisDisplay->network->disableAutoTestMode == 0)
	  {
	  thisDisplay->operationalMode = OperationalModeStandard;
	  Display_operationalModeAutoChange( thisDisplay );
	  }
  }
#elif TEST_MODE != ENABLE_NVMTEST_WRITE_PL_TRIGGERED && TEST_MODE != ENABLE_NVMTEST_WRITE_INTERNAL && CENTER_ENABLE == ENABLED
  if ( thisDisplay->nvmemory->readNeuronState == 5 )
  {
	  if ( thisDisplay->network->disableAutoTestMode == 0)
	  {
	  Display_operationalModeAutoChange( thisDisplay );
	  }
  }
#elif TEST_MODE != ENABLE_NVMTEST_WRITE_PL_TRIGGERED && TEST_MODE != ENABLE_NVMTEST_WRITE_INTERNAL && CENTER_ENABLE == DISABLED
  if ( thisDisplay->nvmemory->readNeuronState == 5 )
  {
	  if ( thisDisplay->network->disableAutoTestMode == 0)
	  {
	  thisDisplay->operationalMode = OperationalModeStandard;
	  Display_operationalModeAutoChange( thisDisplay );
	  }
  }
#else
#error No CENTER_ENABLE defined
#endif

//If the APIS controller connected to the network does not have
//CRC Enabled Network Transactions - this also means it does not send
//Maintenance mode selection through a Network Transaction.
//So upon disabling the Auto Test Mode place the display in
//Standard Operational mode.
//#if CENTER_ENABLE == ENABLED
//      if ( thisDisplay->network->disableAutoTestMode == 1 &&
//	  		  thisDisplay->network->crcEnabledNetworkTransactions == 0 &&
//	  				thisDisplay->nvmemory->readNeuronState == 5 &&
//	  		            thisDisplay->operationalMode != OperationalModeStandardCentered )
//	  {
//	  	  thisDisplay->operationalMode = OperationalModeStandardCentered;
//	  }
//#elif CENTER_ENABLE == DISABLED
//      if ( thisDisplay->network->disableAutoTestMode == 1 &&
//	  		  thisDisplay->network->crcEnabledNetworkTransactions == 0 &&
//	  		    thisDisplay->operationalMode != OperationalModeStandard )
//	  {
//	  	  thisDisplay->operationalMode = OperationalModeStandard;
//	  }
//#else
//#error No CENTER_ENABLE defined
//#endif

//Power Cycled and Neuron ID Found in Flash?
// If Destination Display - Send Power Cycled Message Over PL Channel.
// Set the Default Operation Mode of the Display upon first power on.

#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY && CENTER_ENABLE == ENABLED
      if ( (thisDisplay->nvmemory->readNeuronState == 5)
    		 && ( thisDisplay->displaytype == FrontDestinationDisplay )
    	       && ( thisDisplay->network->completedPowerOnCycle == 0) )
      {
    	  //Set Default Power on Mode of Display
	      thisDisplay->operationalMode = OperationalModeStandardCentered;
	      //Send Power Cycled Message over PL.
    	  Network_PowerCycleMessage( thisDisplay->network );
          thisDisplay->network->completedPowerOnCycle = 1;
      }
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY && CENTER_ENABLE == DISABLED
      if ( (thisDisplay->nvmemory->readNeuronState == 5)
    		 && ( thisDisplay->displaytype == FrontDestinationDisplay )
    	       && ( thisDisplay->network->completedPowerOnCycle == 0) )
      {
    	  //Set Default Power on Mode of Display
	      thisDisplay->operationalMode = OperationalModeStandard;
	      //Send Power Cycled Message over PL.
    	  Network_PowerCycleMessage( thisDisplay->network );
          thisDisplay->network->completedPowerOnCycle = 1;
      }

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY  && CENTER_ENABLE == ENABLED

      if ( (thisDisplay->nvmemory->readNeuronState == 5)
    		  &&( thisDisplay->displaytype == FrontDestinationDisplay )
    	        && ( thisDisplay->network->completedPowerOnCycle == 0) )
      {
    	  //Set Default Power on Mode of Display
    	  thisDisplay->operationalMode = OperationalModeStandardCentered;
    	  //Send Power Cycled Message over PL.
    	  Network_PowerCycleMessage( thisDisplay->network );
          thisDisplay->network->completedPowerOnCycle = 1;
      }
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY  && CENTER_ENABLE == DISABLED

      if ( (thisDisplay->nvmemory->readNeuronState == 5)
    		  &&( thisDisplay->displaytype == FrontDestinationDisplay )
    	        && ( thisDisplay->network->completedPowerOnCycle == 0) )
      {
    	  //Set Default Power on Mode of Display
    	  thisDisplay->operationalMode = OperationalModeStandard;
    	  //Send Power Cycled Message over PL.
    	  Network_PowerCycleMessage( thisDisplay->network );
          thisDisplay->network->completedPowerOnCycle = 1;
      }
#elif DISPLAY_TYPE == SALOON_DISPLAY

      if ( (thisDisplay->nvmemory->readNeuronState == 5)
    		  &&( thisDisplay->displaytype == SaloonDisplay )
    	        && ( thisDisplay->network->completedPowerOnCycle == 0) )
      {
    	  //Set Default Power on Mode of Display
    	  thisDisplay->operationalMode = OperationalModeStandard;
          thisDisplay->network->completedPowerOnCycle = 1;
      }
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
	  Display_serviceOptoInputs(thisDisplay->network, thisDisplay->optoinputs );
	  Display_serviceECA( thisDisplay->network );
#endif


#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif

#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
	  Display_servicePassengerCounters( thisDisplay, thisDisplay->network, thisDisplay->passengerCount );
#endif

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif

   slaveMasterSM( thisDisplay->network );

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif


  //Visualtest_counterDisplay( thisDisplay->displayLine1 );

  Display_updateDisplay( thisDisplay );

  //Visualtest_WriteNVMemory( thisDisplay->displayLine1 );

#if WATCHDOG_FUNCTION == WATCHDOG_ENABLED
  {
	//reset the watchdog timer
	wdt_clear();
  }
#elif WATCHDOG_FUNCTION == WATCHDOG_DISABLED
  {

  }
#else
#error No WATCHDOG_FUNCTION defined
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY
  thisDisplay->numberTextMessages = thisDisplay->displayLine1->numberTextMessages;
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
  thisDisplay->numberTextMessages = thisDisplay->displayLine1->numberTextMessages + \
		                                thisDisplay->displayLine2->numberTextMessages + \
		                                    thisDisplay->displayLine3->numberTextMessages;
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  thisDisplay->numberTextMessages = thisDisplay->displayLine1->numberTextMessages + \
		                                thisDisplay->displayLine2->numberTextMessages + \
		                                    thisDisplay->displayLine3->numberTextMessages;
#else
  #error No known DISPLAY_TYPE defined
#endif

#if TEST_MODE == ENABLE_DEFAULT_PL_MESSAGE_SENDER
      if ( thisDisplay->nvmemory->readNeuronState == 5
    		 && thisDisplay->network->neuron->storedInFlash == 1   )
      {
    	  //Visualtest_SendDefaultMessagePacket( thisDisplay->displayLine1 );
    	  Visualtest_Class319TestSuite( thisDisplay->displayLine1 );
      }
#endif

#if TEST_MODE == ENABLE_NVMTEST_DISPLAY_MESSAGES
      if ( thisDisplay->nvmemory->readNeuronState == 5
    		 && thisDisplay->network->neuron->storedInFlash == 1   )
      {
    	  Visualtest_TestNVMemory( thisDisplay->displayLine1 );
      }
#endif


#if TEST_MODE == ENABLE_SALOON_TEXT_PL_MESSAGE_SENDER
      if ( thisDisplay->nvmemory->readNeuronState == 5
    		 && thisDisplay->network->neuron->storedInFlash == 1   )
      {
    	  Visualtest_SendTextMessage();
      }
#endif

  }

	// Disable the ADC channels.
	adc_disable(adc,ADC_TEMPERATURE_CHANNEL);
	adc_disable(adc,ADC_LIGHT_CHANNEL);
 }

//-----------------------------------------------------------------------------
// Initialise the application framework.
void frameWorkInit()
{
  //Create the display
  Display_construct();

  // Initialise the software subsystems.
  initialiseSubsystems();

}

//-----------------------------------------------------------------------------
// Run the application framework.
void frameWorkRun(void) {
  // Run the main loop of the application.
  runMainLoop();

}

//-----------------------------------------------------------------------------
// Clean up.
void frameWorkCleanUp(void) {
  // Disable interrupts globally.
  Disable_global_interrupt();
  DotsList_destroy(dots_list);
  dots_list = NULL;

}


