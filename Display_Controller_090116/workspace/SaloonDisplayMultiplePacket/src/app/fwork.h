// Application framework.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef FWORK_H_
#define FWORK_H_

// Standard C library includes.
#include "stddef.h"


// Initialise the application framework.
void frameWorkInit();

// Run the application framework.
void frameWorkRun(void);

// Clean up.
void frameWorkCleanUp(void);


#endif /* FWORK_H_ */
