/*
 * network.h
 *
 *  Created on: 27 Aug 2010
 *      Author: DAVE
 */

#ifndef NETWORK_H_
#define NETWORK_H_

//! The channel instance for SPI TX, here PDCA channel 0 (highest priority).
#define PDCA_CHANNEL_SPI_RX 0
//! The channel instance for SPI TX, here PDCA channel 1.
#define PDCA_CHANNEL_SPI_TX 1

#include "callforaid.h"
#include "optoinputs.h"
#include "circularbuffer.h"
#include "packetlist.h"
#include "spi.h"
#include "TFX-SPI.h"
#include "neuron.h"
#include "visualtest.h"
#include "crc.h"


enum AcknowledgeType
{
	AckNull = 0,
    AckPassengerCount,
	AckReportFeatureSet,
	AckTalkBackAmp,
	AckECA,
	AckEcho
};

enum FeedbackType
{
	FeedbackNull = 0,
	FeedbackPassengerCounter = 1,
	FeedbackReportFeatureSet = 2,
	FeedbackECAMessage = 4,
	FeedbackEchoMessage = 5,
	FeedbackSaloonRXAmpECAMessage = 6,
	FeedbackPowerCycled = 7
};

struct network_str{
    Circularbuffer  readBuffer;						// Circular buffer from which data is read from SPI channel
    Circularbuffer  writeBuffer;    				// Write Circular buffer
    unsigned char	smState;						// spiSlaveMasterTransfer State machine state
    unsigned char   msState;                        // spiMasterSlaveTransfer State machine state
    unsigned short  receivedMessageArraySPI[256];   // Array into which received data taken from SPI channel
    unsigned short  sendMessageArraySPI[256];       // Array from which data written into SPI channel
    Datagram 		receivedPacket0;				// Received Packet from echelon
    Datagram 		receivedPacket1;				// Received Packet from echelon
    Datagram 		receivedPacket2;				// Received Packet from echelon
    Datagram 		receivedPacket3;				// Received Packet from echelon
    Datagram 		receivedPacket4;				// Received Packet from echelon
    /* DB CHANGED
    Datagram 		receivedPacket5;				// Received Packet from echelon
    Datagram 		receivedPacket6;				// Received Packet from echelon
    Datagram 		receivedPacket7;				// Received Packet from echelon
    Datagram 		receivedPacket8;				// Received Packet from echelon
    Datagram 		receivedPacket9;				// Received Packet from echelon
    */
    Datagram        workPacket;						// Work packet
    volatile Spibuf sendMessageBuffer;				// SPI Send Message Buffer
    unsigned char   numberBytesInTransfer;			// Number bytes in SPI transfer
    volatile Neuron neuron;							// Echelon Neuron Info
    volatile Neuron neuronACK;                      // Echelon Neuron Acknowledge
    volatile Neuron neuronReply;					// Echelon Neuron Reply
    unsigned char   disableAutoTestMode;            // Disable Auto Test Mode Cycle upon Network Traffic
    volatile unsigned char   crcEnabledNetworkTransactions;  // Flag to determine if CRC used in packet
                                                    // Also whether maintenance mode operation present
                                                    // in Network Packets.
    unsigned int   numberCRCFailures;              // Number of CRC packet failures
    unsigned int   numberCRCPasses;                // Number of CRC packet passes
    volatile unsigned char numberStateMCCalls;    // Number of calls to comms state machine
    volatile unsigned char feedbackFeatureSet;    // Device Feedback Feature Set Requested
    volatile unsigned char feedbackFeatureSet1;    // Device Feedback Feature Set Requested
    volatile unsigned char feedbackFeatureSet2;    // Device Feedback Feature Set Requested
    volatile unsigned char feedbackFeatureSet3;    // Device Feedback Feature Set Requested


    volatile unsigned char ECARequested;              // ECA Requested
    volatile unsigned char tryDMATransfer;        // DMA failed to TX or RX flag
    volatile unsigned char failedDMATransfer;
    volatile unsigned int  numberFailedDMATransfers; // Number of failed DMA transfers counter
    volatile unsigned int  numberSuccessfulDMATransfers; // Number of succesful DMA transfers
	volatile unsigned char acknowledgePCResetReceived;     // Acknowledge Passenger Count Message Received
	volatile unsigned char acknowledgeFeatureResetReceived;// Acknowledge Feature Set Message Received
	volatile unsigned char acknowledgeECAResetReceived;    // Acknowledge ECA Message Received
	volatile unsigned char acknowledgeEchoResetReceived;   // Acknowledge Echo Message Received
	volatile short CRCResult;
	volatile short CRCResult_Received;
	volatile short CRCResult_Calculated;
	volatile enum AcknowledgeType messageAcknowledgeType; 	//Acknowledge Message Type
	volatile unsigned int  numberECARequests;        		// Number ECA Requests
	volatile unsigned int  numberECAAck;             		// Number ECA Acknowledsges
	volatile unsigned char  numberServiceECAPacketsSent;    // Number ECA Packets Sent for Current ECA
	volatile unsigned char  EchoMessageRequested;			// Echo Message Requested
	volatile unsigned char  numberServiceEchoPacketsSent;   // Number Echo Packets Sent
	volatile unsigned char  echoModeEnabled;                // Echo Feedback Mode enabled
	volatile unsigned char  completedPowerOnCycle;          // Completed Powered On Cycle Flag
};

typedef struct network_str* Network;


// Constructor.
//
//
Network Network_construct();

// Destructor.
//
//
void Network_destroy();

void Network_decodePacketInformation(Network network);

int Network_convertCharBufferToShortBuffer( const char* charMessageBuffer, const int charBufferSize, unsigned short* ushortMessageBuffer);

void Network_storeReceivedMessagePacket(Network network);

int Network_enableMessageTransferOverSPI( Network network, char* charSendMessageBuffer, const int charBufferSize );

void Network_spiSlaveMasterTransfer(Network network, char *message, int numberBytesInTransfer);

void Network_decodeRingBufferMessages(Network network);

void Network_createMessageBuffer(Network network, char* messageBuffer, unsigned char messageLength );

void Network_clearBuffer(int bufferSize, char * buffer);

void Network_readNeuronID(Network network);

void Network_storeNeuronID(Network network);

unsigned char* Network_getNeuronID();

void Network_CFAPressed(Network network, Callforaid callforaid);

unsigned char Network_neuronCompare( Network network );

void Network_mergeReceivedPacketFrames(Network network);

void Network_processLongMessages(Network network, char *longMessageBuffer, int* messageLength);

void Network_processMessages( Network network );

void Network_OptoMessage(Network network, Optoinputs optoinputs );

void Network_PassengerCounterMessage(Network network, PassengerCount passengerCount );

void Network_ReportFeatureSetMessageLP( Network network );

void Network_ReportFeatureSetMessageHP( Network network );

void Network_TriggerDiagnosticMessage( Network network );

unsigned char Network_globalCompare( Neuron neuron );

void Network_ECAMessage( Network network );

void Network_EchoMessage( Network network );

void Network_DefaultSaloonLine1Message( Network network, char* messageToSend );

void Network_DefaultSaloonLine2Message( Network network, char* messageToSend );

void Network_DefaultSaloonLine3Message( Network network, char* messageToSend );


void Network_SendSaloonLine1TextMessage( Network network, char* messageToSend  );


unsigned char  Network_DefaultDisplayLineMessage( Network network, char* messageToSend, char displayType, char displayLine );

void Network_PowerCycleMessage( Network network );

void Network_createMessageBufferSaloon(Network network, char* messageBuffer, unsigned char messageLength );

void Network_createMessageBufferDestination(Network network, char* messageBuffer, unsigned char messageLength );
void Network_sendMessageFrame( Network network, char* messageToSend, int frameNumber, int totalFrameNumber  );


void Network_SendDestinationLine1TextMessage( Network network, char* messageToSend  );
void Network_SendDestinationLine2TextMessage( Network network, char* messageToSend  );
#endif /* NETWORK_H_ */
