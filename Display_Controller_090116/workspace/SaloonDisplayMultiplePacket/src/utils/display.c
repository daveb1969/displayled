
#include "display.h"
#include "displayline.h"
#include "gpio.h"
#include "board.h"
#include "tc.h"
#include "tfxDisplayTestMode.h"
#include "visualtest.h"
#include "dots.h"
#include "delay.h"
#include "time.h"
#include "tc.h"

#include <stdlib.h>

extern char clkTick;

#define TC_CHANNEL0    0
#define TC_CHANNEL1    1

// Constructor.
//
//
void Display_construct() {

  thisDisplay = ( struct display_str* ) malloc(sizeof(struct display_str));
  TFX_ASSERT(thisDisplay, "Failed to allocate memory.");
  thisDisplay->defaultMessage = DefaultMessage_construct();
  thisDisplay->nvmemory = NVmemory_construct();
  thisDisplay->numberTextMessages = 0;
  thisDisplay->numberMaintenanceMessages = 0;

#if DISPLAY_TYPE == SALOON_DISPLAY
  Display_saloonDisplayConstruct( thisDisplay );
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
  Display_destinationDisplayConstruct( thisDisplay );
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Display_destinationDisplayConstruct( thisDisplay );
#else
  #error No known DISPLAY_TYPE defined
#endif


#if DISPLAY_FUNCTIONALITY == STANDARD
  thisDisplay->functionality = Standard;

#elif DISPLAY_FUNCTIONALITY == CREW_COMMS
  thisDisplay->functionality = Crew_Comms;

#elif DISPLAY_FUNCTIONALITY == ECAON_PCOFF
  thisDisplay->functionality = ECAon_PCoff;

#elif DISPLAY_FUNCTIONALITY == ECAON_PCON
  thisDisplay->functionality = ECAon_PCon;

#elif DISPLAY_FUNCTIONALITY == ECAOFF_PCON
  thisDisplay->functionality = ECAoff_PCon;

#else
  #error No DISPLAY_FUNCTIONALITY defined
#endif

  thisDisplay->enablePanelScan = 0x01;

#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
  thisDisplay->callforaid = Callforaid_construct();
  thisDisplay->optoinputs = Optoinputs_construct();

#endif

#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
  thisDisplay->ecaTest = ECAtest_construct();
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
  thisDisplay->passengerCount = PassengerCount_construct();
#endif

  thisDisplay->network = Network_construct();
  thisDisplay->visualtest = Visualtest_construct();
}

// Destructor.
//
//
void Display_destroy() {

  Visualtest_destroy(thisDisplay->visualtest);
  NVmemory_destroy(thisDisplay->nvmemory);
  PassengerCount_destroy(thisDisplay->passengerCount);
  Network_destroy(thisDisplay->network);

#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
  Callforaid_destroy(thisDisplay->callforaid);
  Optoinputs_destroy(thisDisplay->optoinputs);
  ECAtest_destroy( thisDisplay->ecaTest );
#endif
  Displayline_destroy(thisDisplay->displayLine1);
#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
  Displayline_destroy(thisDisplay->displayLine2);
  Displayline_destroy(thisDisplay->displayLine3);
#endif
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_destroy(thisDisplay->displayLine2);
  Displayline_destroy(thisDisplay->displayLine3);
#endif

  //DefaultMessage_destroy(thisDisplay->defaultMessage );
  free(thisDisplay);
}


//////////////////////////////////////////////////////////////////////////////////
// Initialise the LED Driver LED Individual Brightness Levels
// Setting Dot Correction.
// Each LED Driver TLC5923 requires dot correction data loading at start up.
// Dot correction data is loaded for all LED channels at the same time.
// The complete dot correction data format consists of 16x7-bit words,
// which forms a 112bit wide serial data packet.
// To input data into the Dot Correction Register:
// 			MODE - PB04 must be set high -
//				   this sets the internal input shift register to 112bit width.
//          XLAT - PB03 set low
//	while(data to send)
//	{
//			SIN  - PB09 has data placed upon it.
//			SCLK - PB10 is toggled to clock data into 112bit wide register
//  }
//          Toggle XLAT to latch the data to the dot correction register.
//
// The current display has N x TLC5923 chained together in series.
// The SOUT (Serial out) of one TLC5923 is chained to the SIN (Serial In) of the next.
//
// The display will therefore initialise as follows:
//
// 	MODE - PB04 must be set high -
//		   this sets the internal input shift register to 112bit width.
//  XLAT - PB03 set low
//	while(Another TLC in chain)
//  {
//      Set Data to desired brightness level
//		while(Brightness data to send)
//		{
//				SIN  - PB09 has data placed upon it.
//				SCLK - PB10 is toggled to clock data into 112bit wide register
//  	}
//  }
//  Toggle XLAT - PB03
//                to latch the data into the dot correction register.
/////////////////////////////////////////////////////////////////////////////////////

/*************************************************************************************
* 		void set_Dot_Correction(int nBrightness, int nNumInChain)                    *
**************************************************************************************
* 		nBrightness 	- The LED Brightness level - range 0 to 127                  *
*		nNumInChain  	- The Number of LED Drivers chained together in the display  *
*                                                                                    *
**************************************************************************************/


void Display_set_dotCorrection(unsigned short nBrightness, int nNumInChain){

	int numInChainLoop = 0;
	int dotCorrectLoop = 0;

	// Range Check Brightness level to be set
	if (nBrightness > MAX_BRIGHTNESS ){
		nBrightness = MAX_BRIGHTNESS;
	}

	// Range Check NumInChain
	if (nNumInChain > 30){
		nNumInChain = 30;
	}

	// Set clock signal low
	gpio_clr_gpio_pin(SCLK);

	// Set MODE - PB04 high - enable internal input shift
	// register to 112bit width upon all TLC5923 in chain.
	// Place driver(s) in Dot Correction Data Input Mode
	gpio_set_gpio_pin(MODE);

	//  XLAT - PB03 set low - allow data to be clocked into device
	gpio_clr_gpio_pin(XLAT);


	while(numInChainLoop < nNumInChain){

		while (dotCorrectLoop < DOT_CORRECT_NUMBER_WORDS)
		{
				unsigned short i =0;
				unsigned short temp = 0x40;
				for (i = 0; i < 7; i++)
				{
					if (nBrightness & (temp>>i))
					{
						gpio_set_gpio_pin(SIN);
					}
					else
					{
						gpio_clr_gpio_pin(SIN);
					}
					// Clock the data bit into the Dot Correction Register
					gpio_set_gpio_pin(SCLK);
					//delay( 100 );
					gpio_clr_gpio_pin(SCLK);
					//delay( 100 );
				}
                //Increment loop count
				dotCorrectLoop++;
		}
		//Increment loop count
		numInChainLoop++;
		dotCorrectLoop = 0;
	}
	// Toggle XLAT - PB03 to latch the data to the dot correction register(s).
	gpio_set_gpio_pin(XLAT);

	// Place the LED Driver device(s) in ON/OFF mode after completion
	// of DOT Correction Initialisation
	gpio_clr_gpio_pin(MODE);


}

void Display_displayRow( int rowToSelect){


#if DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT == CLASS319

	int displayMap[26] = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 19, 16, 16, 16 };
	//int displayMap[26] = { 0, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,7,7,7,7,7,7,7,7,7,7,7,7,7 };
#elif DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT == CLASS150
	int displayMap[26] = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 19, 16, 16, 16 };

#elif 	DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT != CLASS319 && END_CLIENT != CLASS150
	int displayMap[26] = {7, 6, 5, 4, 3, 2, 1, 0, 15, 14, 13, 12, 11, 10, 9, 9, 24, 23, 22, 21, 20, 19, 19, 17, 16 };

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	int displayMap[26] = {24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25};
	//int displayMap[26] = {16, 17, 18, 19, 20, 21, 22, 23, 24, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7 };

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
	int displayMap[26] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
	//int displayMap[26] = {7, 6, 5, 4, 3, 2, 1, 0, 15, 14, 13, 12, 11, 10, 9, 8, 24, 23, 22, 21, 20, 19, 18, 17, 16 };
#else
  #error No DISPLAY_TYPE defined
#endif

	int rowSelect;

	if (rowToSelect <= 26 ){
    rowSelect = displayMap[rowToSelect];
	}
	else
	{
		// Disable the Row Enable Output Driver IC -
		// allows for selection of row to enable with 3 bit code.
	    gpio_clr_gpio_pin(ROW_REN);
	    gpio_clr_gpio_pin(BLANK);
		return;
	}
	// Disable the Row Enable Output Driver IC -
	// allows for selection of row to enable with 3 bit code.
    gpio_set_gpio_pin(ROW_REN);
    // Enable blanking the display
    gpio_clr_gpio_pin(BLANK);

    // For the the row of LEDS to be driven from the LED driver circuit
    //
    // Select Row of LED(s) to be enabled for display.

    if ( rowSelect & 1)
    {
    	gpio_set_gpio_pin(ROWADD0);
    }
    else
    {
    	gpio_clr_gpio_pin(ROWADD0);
    }

    if ( rowSelect & 2)
    {
        gpio_set_gpio_pin(ROWADD1);
    }
    else
    {
    	gpio_clr_gpio_pin(ROWADD1);
    }

    if ( rowSelect & 4)
    {
        gpio_set_gpio_pin(ROWADD2);
    }
    else
    {
        gpio_clr_gpio_pin(ROWADD2);
    }

    if ( rowSelect & 8)
    {
        gpio_set_gpio_pin(ROWADD3);
    }
    else
    {
    	gpio_clr_gpio_pin(ROWADD3);
    }

    if ( rowSelect & 16)
    {
        gpio_set_gpio_pin(ROWADD4);
    }
    else
    {
    	gpio_clr_gpio_pin(ROWADD4);
    }


    //Enable Output driver so as to enable
    //row selected with 3 bit code to be turned off
    gpio_clr_gpio_pin(ROW_REN);

    // Latch the current data in the LED Driver(s) On/Off Input Shift Register(s)
    // into the ON/OFF register(s) that drive the LED(s).
    // This data will remain in the ON/OFF register(s) -
    // driving the LED display - for the period of time
    // it takes the interrupt routine to be triggered again.

    gpio_clr_gpio_pin(XLAT);
    gpio_set_gpio_pin(XLAT);
    gpio_clr_gpio_pin(XLAT);

    //Disable blanking the display
    gpio_set_gpio_pin(BLANK);
}

void Display_clearDisplay(int numberColumns){

	int row = 1;
	Display_displayRow(row);

	// Set the BLANK signal to be enabled
	// All output channels operational.
	// Note the BLANK signal is inverted before passing onto the LED Driver IC

	gpio_clr_gpio_pin(BLANK);

	//Clear the LED ON/OFF Register(s) (that drive the LED's)  - LATCH signal
	gpio_clr_gpio_pin(XLAT);

	int loop = 0;
	// Place a high at the input to the LED Driver Shift register to be clocked in.
	// Clock in 112 ones into the shift register
	while (loop < numberColumns)
	{
		gpio_clr_gpio_pin(SIN);

		// Clock data into On/OFF Input Shift Register on rising edge.
		//delay( 100 );
		gpio_clr_gpio_pin(SCLK);
		//delay( 100 );
		gpio_set_gpio_pin(SCLK);
		loop++;
	}
	// Latch the current data in the LED Driver(s) On/Off Input Shift Register(s)
	// into the ON/OFF register(s) that drive the LED(s).
	gpio_set_gpio_pin(XLAT);
}


void Display_writeRow(int rowLength){

	int loop = 0;

	//Place the LED Driver IC in the correct MODE
	gpio_clr_gpio_pin(MODE);

	// Set the BLANK signal to be disabled
	// All output channels operational.
	// Note the BLANK signal is inverted before passing onto the LED Driver IC

	gpio_set_gpio_pin(BLANK);

    //Clear the LED ON/OFF Register(s) that drive the LED's LATCH signal
    gpio_clr_gpio_pin(XLAT);


	// Place a high at the input to the LED Driver Shift register to be clocked in.
    // Clock in 112 ones into the shift register
    while (loop < rowLength)
	{
		gpio_set_gpio_pin(SIN);

		// Clock data into On/OFF Input Shift Register on rising edge.
		gpio_clr_gpio_pin(SCLK);
		gpio_set_gpio_pin(SCLK);
		loop++;
	}

    // Latch the current data in the LED Driver(s) On/Off Input Shift Register(s)
    // into the ON/OFF register(s) that drive the LED(s).
    gpio_set_gpio_pin(XLAT);

	// Set the BLANK signal to be disabled - will blank the display
	// All output channels operational.
	// Note the BLANK signal is inverted before passing onto the LED Driver IC

	gpio_set_gpio_pin(BLANK);

}


void Display_set_displayType(Display display, unsigned char displayType)
{
	TFX_ASSERT(display, "Expected a non-null pointer6.");
	display->displaytype = displayType;
}

void Display_set_numberRows(Display display, unsigned char numberRows)
{
	TFX_ASSERT(display, "Expected a non-null pointer7.");
	display->numberRows = numberRows;
}

void Display_set_numberCols(Display display, unsigned char numberCols)
{
	TFX_ASSERT(display, "Expected a non-null pointer8.");
	display->numberCols = numberCols;
}



unsigned char Display_get_numberRows(Display display )
{
	TFX_ASSERT(display, "Expected a non-null pointer9.");
	return display->numberRows;
}

void Display_saloonDisplayConstruct( Display display )
{
	  Display_set_displayType( display, SaloonDisplay );
#if   END_CLIENT == CLASS319 || END_CLIENT == CLASS150
	  Display_set_numberRows( display, 10 );
#elif END_CLIENT != CLASS319 && END_CLIENT != CLASS150
	  Display_set_numberRows( display, 8 );
#endif
	  Display_set_numberCols( display, 112);
	  Display_set_currentRenderRow( display, 0 );
	  display->displayLine1 = Displayline_construct();
	  Display_saloonDisplayInitialise( display );
	  display->operationalMode = OperationalModeStandard;
}

void Display_saloonDisplayInitialise(Display display)
{
	  display->displayLine1->startRow = 0;
#if   END_CLIENT == CLASS319 || END_CLIENT == CLASS150
	  display->displayLine1->stopRow =display->displayLine1->startRow + 10;
	  display->displayLine1->scrollBufferIncrement = 2;
	  display->displayLine1->displayNumberRows = 10;
	  display->displayLine1->charSet = 5;
#elif END_CLIENT != CLASS319 && END_CLIENT != CLASS150
	  display->displayLine1->stopRow =display->displayLine1->startRow + 8;
	  display->displayLine1->scrollBufferIncrement = 1;
	  display->displayLine1->charSet = 1;
	  display->displayLine1->displayNumberRows = 8;
#endif
	  display->displayLine1->scrollEnable = 1;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;

#if END_CLIENT == SOUTHERN

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = southern;
	  const char* SOUTHERN_LINE1 = "Welcome aboard this Southern Service                                    ";

	  Displayline_set_messageBuffer(display->displayLine1, thisDisplay->nvmemory->nvram->DM1Text1);
#elif END_CLIENT == FGW

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = fgw;
	  const char* FGW_LINE1 = "Welcome aboard this First Great Western Service                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, FGW_LINE1);
#elif END_CLIENT == ARRIVA

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = arriva;
	  const char* ARRIVA_LINE1 = "Welcome aboard this Arriva Trains Wales Service                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, ARRIVA_LINE1);
#elif END_CLIENT == CLASS319

	  display->displayLine1->displayNumberRows = 10;
	  display->displayLine1->charSet = 5;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 10;
	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = class319;

	  const char* LINE1 = "Display Line 1";
	  if ( thisDisplay->nvmemory->nvram->DM1storedInFlash != 1 )
	  {
		  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
		  Displayline_set_messageBuffer(display->displayLine1, LINE1);
	  }
	  else
	  {
		  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
		  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
		  Displayline_set_messageBuffer(display->displayLine1, thisDisplay->nvmemory->nvram->DM1Text1);
	  }
#elif END_CLIENT == CLASS150

	  display->displayLine1->displayNumberRows = 10;
	  display->displayLine1->charSet = 5;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 10;
	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client =class150;

	  const char* LINE1 = "Display Line 1";
	  if ( thisDisplay->nvmemory->nvram->DM1storedInFlash != 1 )
	  {
		  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
		  Displayline_set_messageBuffer(display->displayLine1, LINE1);
	  }
	  else
	  {
		  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
		  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
		  Displayline_set_messageBuffer(display->displayLine1, thisDisplay->nvmemory->nvram->DM1Text1);
	  }
#else
#error No END_CLIENT defined
#endif
	  Displayline_createDots(display->displayLine1);
	  display->displayLine1->messageChanged = 1;
	  display->displayLine1->displayLineDotsCreated = 1;
}

void Display_saloonDisplayInitialiseCounterComms(Display display)
{
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 8;
	  display->displayLine1->scrollEnable = 1;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->scrollBufferIncrement = 1;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->charSet = 1;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayNumberRows = 8;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;

#if PASSENGER_COUNTER_FUNCTION==PC_FUNCTION_ON
	  Display_disablePanelScan();
	  PassengerCount_disableAutoLEDBrightness(display->passengerCount);
	  Display_enablePanelScan();
#endif

#if END_CLIENT == SOUTHERN

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = southern;
	  const char* SOUTHERN_LINE1 = "Passenger Counters Under Test                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, SOUTHERN_LINE1);
#elif END_CLIENT == FGW

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = fgw;
	  const char* FGW_LINE1 = "Passenger Counters Under Test                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, FGW_LINE1);
#elif END_CLIENT == ARRIVA

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = arriva;
	  const char* ARRIVA_LINE1 = "Passenger Counters Under Test                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, ARRIVA_LINE1);
#elif END_CLIENT == CLASS319

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = class319;
	  const char* CLASS319_LINE1 = "Passenger Counters Under Test                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, CLASS319_LINE1);
#elif END_CLIENT == CLASS150

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = class150;
	  const char* CLASS150_LINE1 = "Passenger Counters Under Test                                    ";
	  Displayline_set_messageBuffer(display->displayLine1, CLASS150_LINE1);

#else
#error No END_CLIENT defined
#endif
	  Displayline_createDots(display->displayLine1);
	  display->displayLine1->messageChanged = 1;
	  display->displayLine1->displayLineDotsCreated = 1;
}
void Display_saloonDisplayInitialiseL1C1Static(Display display)
{
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 8;
	  display->displayLine1->scrollBufferIncrement = 1;
	  display->displayLine1->displayNumberRows = 8;
	  display->displayLine1->scrollEnable = 0;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->charSet = 1;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;
}


void Display_saloonDisplayInitialiseL1C5Static(Display display)
{
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 10;
	  display->displayLine1->scrollBufferIncrement = 2;
	  display->displayLine1->displayNumberRows = 10;
	  display->displayLine1->scrollEnable = 0;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->charSet = 5;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;
}


void Display_saloonDisplayInitialiseL1C1Scroll(Display display)
{
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 8;
	  display->displayLine1->scrollBufferIncrement = 1;
	  display->displayLine1->displayNumberRows = 8;
	  display->displayLine1->charSet = 1;
	  display->displayLine1->scrollEnable = 1;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;
}


void Display_saloonDisplayInitialiseL1C5Scroll(Display display)
{
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 10;
	  display->displayLine1->scrollBufferIncrement = 2;
	  display->displayLine1->displayNumberRows = 10;
	  display->displayLine1->charSet = 5;
	  display->displayLine1->scrollEnable = 1;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;
}


void Display_saloonDisplayInitialiseL1C2Scroll(Display display)
{
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow =display->displayLine1->startRow + 10;
	  display->displayLine1->scrollEnable = 1;
	  display->displayLine1->scrollRate = 0;
	  display->displayLine1->scrollBufferIncrement = 2;
	  display->displayLine1->lineNumber = 1;
	  display->displayLine1->charSet = 2;
	  display->displayLine1->prePend = 112;
	  display->displayLine1->displayPtr = 0;
	  display->displayLine1->displayNumberRows = 10;
	  display->displayLine1->displayLineNumberColumns = 112;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimeout = 0;
	  display->displayLine1->displayLineTimerCount = 0;
	  display->displayLine1->displayLineTimerEnable = 0;
	  display->displayLine1->scrollEnableChanged = 0;
	  display->displayLine1->scrollEnableReceived = 0;
	  display->displayLine1->messageComplete = 0;
	  display->displayLine1->dotBufferSemaphore = 0;
}
void Display_destinationDisplayConstruct(Display display)
{
	Display_set_displayType(display, FrontDestinationDisplay);
	Display_set_numberRows(display, 25 );
	Display_set_numberCols(display, 96);
	Display_set_currentRenderRow( display, 0 );
	display->displayLine1 = Displayline_construct();
	display->displayLine2 = Displayline_construct();
	display->displayLine3 = Displayline_construct();
	Display_destinationDisplayInitialise( display );
#if CENTER_ENABLE == ENABLED
	display->operationalMode = OperationalModeStandardCentered;
#elif CENTER_ENABLE == DISABLED
	display->operationalMode = OperationalModeStandard;
#else
#error No CENTER_ENABLE Declared
#endif
}


void Display_destinationDisplayInitialise(Display display)
{
#if END_CLIENT == FGW

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = fgw;
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
	  display->displayLine1->displayLineNumberColumns = 96;
#elif END_CLIENT == SOUTHERN

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = southern;
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
	  display->displayLine1->displayLineNumberColumns = 96;
#elif END_CLIENT == ARRIVA

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = arriva;
	  display->displayLine1->startRow = 7;
	  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
	  display->displayLine1->displayLineNumberColumns = 85;
#elif END_CLIENT == CLASS319

	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = class319;
	  display->displayLine1->startRow = 0;
	  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
	  display->displayLine1->displayLineNumberColumns = 85;
#elif END_CLIENT == CLASS150
	  thisDisplay->serialNumber[0] = 0x00;
	  thisDisplay->serialNumber[1] = 0x09;
	  thisDisplay->serialNumber[2] = 0x01;
	  thisDisplay->serialNumber[3] = 0x10;
	  thisDisplay->client = class150;
	  display->displayLine1->startRow = 0+10;
	  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
	  display->displayLine1->displayLineNumberColumns = 96;


#else
#error No END_CLIENT defined
#endif

		  display->displayLine1->rowShift = 0;
		  display->displayLine1->lineNumber = 1;
		  display->displayLine1->charSet = 2;
		  display->displayLine1->displayPtr = 0;
		  display->displayLine1->displayNumberRows = 25;
		  display->displayLine1->displayLineNumberColumns = 96;
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimeout = 0;
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimerEnable = 0;
		  display->displayLine1->scrollEnableChanged = 0;
		  display->displayLine1->scrollEnableReceived = 0;
		  display->displayLine1->messageComplete = 0;
		  display->displayLine1->messageChanged = 0;
		  display->displayLine1->dotBufferSemaphore = 0;
#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
		  display->displayLine1->scrollEnable = 0;
		  //
		  display->displayLine1->scrollRate = 0;
		  display->displayLine1->scrollBufferIncrement = 2;
		  display->displayLine1->prePend = 0;
		  const char* LINE1 = "Display Line 1";
		  if ( thisDisplay->nvmemory->nvram->DM1storedInFlash != 1 )
		  {
			  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
			  Displayline_set_reversedMessageBuffer(display->displayLine1, LINE1);
		  }
		  else
		  {
			  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
			  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
			  Displayline_set_reversedMessageBuffer(display->displayLine1, thisDisplay->nvmemory->nvram->DM1Text1);
		  }

#if END_CLIENT == FGW
		  display->displayLine1->prePend = 96;
		  const char* FGW_LINE1 = "   ";
		  Displayline_set_messageBuffer(display->displayLine1, FGW_LINE1);
#elif END_CLIENT == SOUTHERN
		  display->displayLine1->prePend = 96;
		  const char* SOUTHERN_LINE1 = "   ";
		  Displayline_set_messageBuffer(display->displayLine1, SOUTHERN_LINE1);
#elif END_CLIENT == ARRIVA
		  display->displayLine1->prePend = 85;
		  const char* ARRIVA_LINE1 = "http://www.arrivatrainswales.co.uk        ";
		  Displayline_set_messageBuffer(display->displayLine1, ARRIVA_LINE1);
#elif END_CLIENT == CLASS150
		  display->displayLine1->scrollEnable = 0;
		  display->displayLine1->scrollRate = 0;
		  display->displayLine1->scrollBufferIncrement = 2;
		  display->displayLine1->prePend = 0;
		  const char* CLASS150_LINE1 = "Class150 Display Line 1 ";
		  Displayline_set_messageBuffer(display->displayLine1, CLASS150_LINE1);
		  if ( thisDisplay->nvmemory->nvram->DM1storedInFlash != 1 )
		  {
			  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
			  Displayline_set_messageBuffer(display->displayLine1, CLASS150_LINE1);
		  }
		  else
		  {
			  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
			  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
		  	  Displayline_set_messageBuffer(display->displayLine1, thisDisplay->nvmemory->nvram->DM1Text1);
		  }
#else
#error No END_CLIENT defined
#endif

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
		  //DB Changed for centered display
		  display->displayLine1->scrollEnable = 0;
		  //
		  display->displayLine1->scrollRate = 0;
		  display->displayLine1->scrollBufferIncrement = 2;
		  display->displayLine1->prePend = 0;
		  const char* LINE1 = "Display Line 1";
		  if ( thisDisplay->nvmemory->nvram->DM1storedInFlash != 1 )
		  {
			  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
			  Displayline_set_reversedMessageBuffer(display->displayLine1, LINE1);
		  }
		  else
		  {
			  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
			  TFX_ASSERT( (display->displayLine1 != NULL) , "displayLine1 not allocated" );
			  Displayline_set_reversedMessageBuffer(display->displayLine1, thisDisplay->nvmemory->nvram->DM1Text1);
		  }

#elif DISPLAY_TYPE == SALOON_DISPLAY
#else
#error No DISPLAY_TYPE defined
#endif
		  display->displayLine1->displayLineDotsCreated = 0;
		  Displayline_createDots(display->displayLine1);
		  display->displayLine1->messageChanged = 1;

		  display->displayLine2->startRow = 13;
		  display->displayLine2->stopRow = display->displayLine2->startRow + 12;
		  display->displayLine2->rowShift = 0;
		  display->displayLine2->lineNumber = 2;
		  display->displayLine2->charSet = 2;
		  display->displayLine2->displayPtr = 0;
		  display->displayLine2->displayNumberRows = 25;
		  display->displayLine2->displayLineNumberColumns = 96;
		  display->displayLine2->displayLineTimerCount = 0;
		  display->displayLine2->displayLineTimeout = 0;
		  display->displayLine2->displayLineTimerCount = 0;
		  display->displayLine2->displayLineTimerEnable = 0;
		  display->displayLine2->scrollEnableChanged = 0;
		  display->displayLine2->scrollEnableReceived = 0;
		  display->displayLine2->messageComplete = 0;
		  display->displayLine2->messageChanged = 0;
		  display->displayLine2->dotBufferSemaphore = 0;

#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
		  display->displayLine2->scrollEnable = 1;
		  display->displayLine2->scrollRate = 0;
		  display->displayLine2->scrollBufferIncrement = 2;
		  display->displayLine2->prePend = 96;

#if END_CLIENT == FGW
		  const char* FGW_LINE2 = "First Great Western        ";
		  Displayline_set_messageBuffer(display->displayLine2, FGW_LINE2);
#elif END_CLIENT == SOUTHERN
		  const char* SOUTHERN_LINE2 = "First Capital Connect       ";
		  Displayline_set_messageBuffer(display->displayLine2, SOUTHERN_LINE2);
#elif END_CLIENT == ARRIVA
		  const char* ARRIVA_LINE2 = "   ";
		  Displayline_set_messageBuffer(display->displayLine2, ARRIVA_LINE2);
#elif END_CLIENT == CLASS150
		  const char* CLASS150_LINE2 = "Class150 Display Line 2";
		  if ( thisDisplay->nvmemory->nvram->DM2storedInFlash != 1 )
		  {
			  TFX_ASSERT( (display->displayLine2 != NULL) , "displayLine2 not allocated" );
			  Displayline_set_messageBuffer(display->displayLine2, CLASS150_LINE2);
		  }
		  else
		  {
			  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
			  TFX_ASSERT( (display->displayLine2 != NULL) , "displayLine2 not allocated" );
		  	  Displayline_set_messageBuffer(display->displayLine2, thisDisplay->nvmemory->nvram->DM2Text2);
		  }
#else
#error No END_CLIENT defined
#endif

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
		  //DB Changed for centered display
		  display->displayLine2->scrollEnable = 0;
		  display->displayLine2->scrollRate = 0;
		  display->displayLine2->scrollBufferIncrement = 2;
		  display->displayLine2->prePend = 96;
#if END_CLIENT == SOUTHERN
		  const char* SOUTHERN_LINE2 = "First Capital Connect        ";
		  Displayline_set_reversedMessageBuffer(display->displayLine2, SOUTHERN_LINE2);
#elif END_CLIENT == FGW
		  const char* FGW_LINE2 = "First Great Western        ";
		  Displayline_set_reversedMessageBuffer(display->displayLine2, FGW_LINE2);
#elif END_CLIENT == ARRIVA
		  const char* ARRIVA_LINE2 = "   ";
		  Displayline_set_reversedMessageBuffer(display->displayLine2, ARRIVA_LINE2);
#elif END_CLIENT == CLASS319
		  const char* LINE2 = "Display Line 2";
		  if ( thisDisplay->nvmemory->nvram->DM2storedInFlash != 1 )
		  {
			  TFX_ASSERT( (display->displayLine2 != NULL) , "displayLine2 not allocated" );
			  Displayline_set_reversedMessageBuffer(display->displayLine2, LINE2);
		  }
		  else
		  {
			  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
			  TFX_ASSERT( (display->displayLine2 != NULL) , "displayLine2 not allocated" );
		  	  Displayline_set_reversedMessageBuffer(display->displayLine2, thisDisplay->nvmemory->nvram->DM2Text2);
		  }
#elif END_CLIENT == CLASS150
		  const char* CLASS150_LINE2 = "Class150 Display Line 2";
		  if ( thisDisplay->nvmemory->nvram->DM2storedInFlash != 1 )
		  {
			  TFX_ASSERT( (display->displayLine2 != NULL) , "displayLine2 not allocated" );
			  Displayline_set_messageBuffer(display->displayLine2, CLASS150_LINE2);
		  }
		  else
		  {
			  TFX_ASSERT( (thisDisplay->nvmemory->nvram != NULL) , "NVMemory not allocated" );
			  TFX_ASSERT( (display->displayLine2 != NULL) , "displayLine2 not allocated" );
		  	  Displayline_set_messageBuffer(display->displayLine2, thisDisplay->nvmemory->nvram->DM2Text2);
		  }
#else
#error No END_CLIENT defined
#endif

#elif DISPLAY_TYPE == SALOON_DISPLAY
#else
#error No DISPLAY_TYPE defined
#endif
		  if ( display->operationalMode != OperationalModeTestVisual)
		  {
		  display->displayLine2->displayLineDotsCreated = 0;
		  Displayline_createDots(display->displayLine2);
		  display->displayLine2->messageChanged = 1;
		  }
		  display->displayLine3->startRow = 0;
		  display->displayLine3->stopRow = thisDisplay->displayLine3->startRow + 18;
		  display->displayLine3->scrollEnable = 0;
		  display->displayLine3->scrollRate = 0;
		  display->displayLine3->scrollBufferIncrement = 2;
		  display->displayLine3->rowShift = 0;
		  display->displayLine3->lineNumber = 3;
		  display->displayLine3->charSet = 3;
		  display->displayLine3->prePend = 0;
		  display->displayLine3->displayPtr = 0;
		  display->displayLine3->displayNumberRows = 20;
		  display->displayLine3->displayLineNumberColumns = 24;
		  display->displayLine3->displayLineTimerCount = 0;
		  display->displayLine3->displayLineTimeout = 0;
		  display->displayLine3->displayLineTimerCount = 0;
		  display->displayLine3->displayLineTimerEnable = 0;
		  display->displayLine3->scrollEnableChanged = 0;
		  display->displayLine3->scrollEnableReceived = 0;
		  display->displayLine3->messageComplete = 0;
		  display->displayLine3->dotBufferSemaphore = 0;
		  display->displayLine3->prePend = 3;


		  const char* LINE3 = "  ";
		  Displayline_set_messageBuffer(display->displayLine3, LINE3);

		  if ( display->operationalMode != OperationalModeTestVisual)
		  {
		  Displayline_createDots(display->displayLine3);
		  display->displayLine3->messageChanged = 1;
		  }
		  display->displayLine3->displayLineDotsCreated = 1;

}

void Display_destinationDisplayInitialiseL1C2ScrollL2BlankL3Blank(Display display)
{
#if END_CLIENT == FGW || END_CLIENT == SOUTHERN || END_CLIENT == CLASS319 || END_CLIENT == CLASS150
		  display->displayLine1->startRow = 0;
		  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
		  display->displayLine1->displayLineNumberColumns = 96;
#elif END_CLIENT == ARRIVA
		  display->displayLine1->startRow = 7;
		  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
		  display->displayLine1->displayLineNumberColumns = 85;
#else
#error No END_CLIENT defined
#endif
		  display->displayLine1->rowShift = 0;
		  display->displayLine1->lineNumber = 1;
		  display->displayLine1->charSet = 2;
		  display->displayLine1->displayPtr = 0;
		  display->displayLine1->displayNumberRows = 25;
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimeout = 0;
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimerEnable = 0;
		  display->displayLine1->scrollEnableChanged = 0;
		  display->displayLine1->scrollEnableReceived = 0;
		  display->displayLine1->messageComplete = 0;
		  display->displayLine1->messageChanged = 0;
		  display->displayLine1->dotBufferSemaphore = 0;

		  display->displayLine1->scrollEnable = 1;
		  display->displayLine1->scrollRate = 0;
		  display->displayLine1->scrollBufferIncrement = 2;
		  display->displayLine1->prePend = 96;

		  display->displayLine2->startRow = 13;
		  display->displayLine2->stopRow = display->displayLine2->startRow + 12;
		  display->displayLine2->rowShift = 0;
		  display->displayLine2->lineNumber = 2;
		  display->displayLine2->charSet = 2;
		  display->displayLine2->displayPtr = 0;
		  display->displayLine2->displayNumberRows = 25;
		  display->displayLine2->displayLineNumberColumns = 96;
		  display->displayLine2->displayLineTimerCount = 0;
		  display->displayLine2->displayLineTimeout = 0;
		  display->displayLine2->displayLineTimerCount = 0;
		  display->displayLine2->displayLineTimerEnable = 0;
		  display->displayLine2->scrollEnableChanged = 0;
		  display->displayLine2->scrollEnableReceived = 0;
		  display->displayLine2->messageComplete = 0;
		  display->displayLine2->messageChanged = 0;
		  display->displayLine2->dotBufferSemaphore = 0;

		  display->displayLine2->scrollEnable = 1;
		  display->displayLine2->scrollRate = 0;
		  display->displayLine2->scrollBufferIncrement = 2;
		  display->displayLine2->prePend = 96;

		  const char* BLANK_LINE2 = " ";
		  Displayline_set_messageBuffer(display->displayLine2, BLANK_LINE2);
		  Displayline_createDots(display->displayLine2);
		  display->displayLine2->messageChanged = 1;

		  display->displayLine3->startRow = 0;
		  display->displayLine3->stopRow = thisDisplay->displayLine3->startRow + 18;
		  display->displayLine3->scrollEnable = 0;
		  display->displayLine3->scrollRate = 0;
		  display->displayLine3->scrollBufferIncrement = 2;
		  display->displayLine3->rowShift = 0;
		  display->displayLine3->lineNumber = 3;
		  display->displayLine3->charSet = 3;
		  display->displayLine3->prePend = 0;
		  display->displayLine3->displayPtr = 0;
		  display->displayLine3->displayNumberRows = 20;
		  display->displayLine3->displayLineNumberColumns = 24;
		  display->displayLine3->displayLineTimerCount = 0;
		  display->displayLine3->displayLineTimeout = 0;
		  display->displayLine3->displayLineTimerCount = 0;
		  display->displayLine3->displayLineTimerEnable = 0;
		  display->displayLine3->scrollEnableChanged = 0;
		  display->displayLine3->scrollEnableReceived = 0;
		  display->displayLine3->messageComplete = 0;
		  display->displayLine3->dotBufferSemaphore = 0;
		  display->displayLine3->prePend = 3;

		  const char* BLANK_LINE3 = "  ";
		  Displayline_set_messageBuffer(display->displayLine3, BLANK_LINE3);
		  Displayline_createDots(display->displayLine3);
		  display->displayLine3->messageChanged = 1;
}

void Display_destinationDisplayInitialiseL1C2StaticL2BlankL3Blank(Display display)
{
#if END_CLIENT == FGW || END_CLIENT == SOUTHERN || END_CLIENT == CLASS319 || END_CLIENT == CLASS150
		  display->displayLine1->startRow = 0;
		  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
		  display->displayLine1->displayLineNumberColumns = 96;
#elif END_CLIENT == ARRIVA
		  display->displayLine1->startRow = 7;
		  display->displayLine1->stopRow = display->displayLine1->startRow + 12;
		  display->displayLine1->displayLineNumberColumns = 85;
#else
#error No END_CLIENT defined
#endif
		  display->displayLine1->rowShift = 0;
		  display->displayLine1->lineNumber = 1;
		  display->displayLine1->charSet = 2;
		  display->displayLine1->displayPtr = 0;
		  display->displayLine1->displayNumberRows = 25;

		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimeout = 0;
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimerEnable = 0;
		  display->displayLine1->scrollEnableChanged = 0;
		  display->displayLine1->scrollEnableReceived = 0;
		  display->displayLine1->messageComplete = 0;
		  display->displayLine1->messageChanged = 0;
		  display->displayLine1->dotBufferSemaphore = 0;

		  display->displayLine1->scrollEnable = 0;
		  display->displayLine1->scrollRate = 0;
		  display->displayLine1->scrollBufferIncrement = 2;
		  display->displayLine1->prePend = 96;

		  display->displayLine2->startRow = 13;
		  display->displayLine2->stopRow = display->displayLine2->startRow + 12;
		  display->displayLine2->rowShift = 0;
		  display->displayLine2->lineNumber = 2;
		  display->displayLine2->charSet = 2;
		  display->displayLine2->displayPtr = 0;
		  display->displayLine2->displayNumberRows = 25;
		  display->displayLine2->displayLineNumberColumns = 96;
		  display->displayLine2->displayLineTimerCount = 0;
		  display->displayLine2->displayLineTimeout = 0;
		  display->displayLine2->displayLineTimerCount = 0;
		  display->displayLine2->displayLineTimerEnable = 0;
		  display->displayLine2->scrollEnableChanged = 0;
		  display->displayLine2->scrollEnableReceived = 0;
		  display->displayLine2->messageComplete = 0;
		  display->displayLine2->messageChanged = 0;
		  display->displayLine2->dotBufferSemaphore = 0;

		  display->displayLine2->scrollEnable = 1;
		  display->displayLine2->scrollRate = 0;
		  display->displayLine2->scrollBufferIncrement = 2;
		  display->displayLine2->prePend = 96;

		  const char* BLANK_LINE2 = " ";
		  Displayline_set_messageBuffer(display->displayLine2, BLANK_LINE2);
		  Displayline_createDots(display->displayLine2);
		  display->displayLine2->messageChanged = 1;

		  display->displayLine3->startRow = 0;
		  display->displayLine3->stopRow = thisDisplay->displayLine3->startRow + 18;
		  display->displayLine3->scrollEnable = 0;
		  display->displayLine3->scrollRate = 0;
		  display->displayLine3->scrollBufferIncrement = 2;
		  display->displayLine3->rowShift = 0;
		  display->displayLine3->lineNumber = 3;
		  display->displayLine3->charSet = 3;
		  display->displayLine3->prePend = 0;
		  display->displayLine3->displayPtr = 0;
		  display->displayLine3->displayNumberRows = 20;
		  display->displayLine3->displayLineNumberColumns = 24;
		  display->displayLine3->displayLineTimerCount = 0;
		  display->displayLine3->displayLineTimeout = 0;
		  display->displayLine3->displayLineTimerCount = 0;
		  display->displayLine3->displayLineTimerEnable = 0;
		  display->displayLine3->scrollEnableChanged = 0;
		  display->displayLine3->scrollEnableReceived = 0;
		  display->displayLine3->messageComplete = 0;
		  display->displayLine3->dotBufferSemaphore = 0;
		  display->displayLine3->prePend = 3;

		  const char* BLANK_LINE3 = "  ";
		  Displayline_set_messageBuffer(display->displayLine3, BLANK_LINE3);
		  Displayline_createDots(display->displayLine3);
		  display->displayLine3->messageChanged = 1;
}


void Display_destinationDisplayInitialiseL1C4ScrollL2OffL3Off(Display display)
{

		  display->displayLine1->rowShift = 0;
		  display->displayLine1->lineNumber = 1;
		  display->displayLine1->displayPtr = 0;
		  display->displayLine1->displayNumberRows = 25;
#if END_CLIENT == SOUTHERN || END_CLIENT == CLASS319 || END_CLIENT == CLASS150
		  display->displayLine1->displayLineNumberColumns = 96;
#elif END_CLIENT == FGW
		  display->displayLine1->displayLineNumberColumns = 120;
#elif END_CLIENT == ARRIVA
		  display->displayLine1->displayLineNumberColumns = 85;
#else
  #error No DISPLAY_TYPE defined
#endif
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimeout = 0;
		  display->displayLine1->displayLineTimerCount = 0;
		  display->displayLine1->displayLineTimerEnable = 0;
		  display->displayLine1->scrollEnableChanged = 0;
		  display->displayLine1->scrollEnableReceived = 0;
		  display->displayLine1->messageComplete = 0;
		  display->displayLine1->messageChanged = 0;
		  display->displayLine1->dotBufferSemaphore = 0;

		  display->displayLine1->scrollEnable = 1;
		  display->displayLine1->scrollRate = 0;
		  display->displayLine1->scrollBufferIncrement = 4;
#if END_CLIENT == SOUTHERN || END_CLIENT == CLASS319 || END_CLIENT == CLASS150
		  display->displayLine1->prePend = 96;
#elif END_CLIENT == FGW
		  display->displayLine1->prePend = 120;
#elif END_CLIENT == ARRIVA
		  display->displayLine1->prePend = 85;
#else
  #error No DISPLAY_TYPE defined
#endif
		  display->displayLine1->charSet = 4;
		  display->displayLine1->startRow = 0;
		  display->displayLine1->stopRow = display->displayLine1->startRow + 25;

		  //Turn off the Get_char_dots function for
		  //displayLine2 and displayLine3 while in
		  //OperationalModeTestVisual
		  display->displayLine2->messageChanged = 0;
		  display->displayLine3->messageChanged = 0;
}




#if DISPLAY_TYPE == SALOON_DISPLAY && (DISPLAY_FUNCTIONALITY == ECAON_PCON || DISPLAY_FUNCTIONALITY == ECAON_PCOFF)

void Display_serviceOptoInputs(Network network, Optoinputs optoinputs )
{
   if (optoinputs->changedState == 1 && network->ECARequested == 0 )
   {
	   optoinputs->changedState = 0;
	   //Has either of the ECA buttons been pressed?
	   if ( optoinputs->opto_Input1->optoInput == 0 ||
			   optoinputs->opto_Input2->optoInput == 0 )
	   {
		   network->ECARequested = 1;
	   }
   }
}

#endif


#if DISPLAY_TYPE == SALOON_DISPLAY && DISPLAY_FUNCTIONALITY == CREW_COMMS

void Display_serviceOptoInputs(Network network, Optoinputs optoinputs )
{
   if (optoinputs->changedState == 1 )
   {
	   optoinputs->changedState = 0;
	   Network_OptoMessage(network, optoinputs );
   }
}

#endif



void Display_serviceFeedbackFeatureSetHP( Network network  )
{
	static unsigned char lastTick = 0;
	static unsigned char counter = 0;
	static unsigned char countStepOnValue = 0;
	static unsigned char step = 0;
	static unsigned long numberFeedbackPacketsSent = 0;

	//Has an acknowledge packet been received from the APIS controller?
	if ( network->acknowledgeFeatureResetReceived == 1 )
	{
		step = 0;
		network->acknowledgeFeatureResetReceived = 0;
		network->feedbackFeatureSet1 = 0;
		numberFeedbackPacketsSent = 0;
	}

	//Have we sent more than 10 Feedback Packets?
	//If we have stop sending Feedback Packets.
	//if ( numberFeedbackPacketsSent >= 1000  )
	//{
	//	step =0;
	//	network->feedbackFeatureSet1 = 0;
		//numberFeedbackPacketsSent = 0;
	//}

	if ( step == 0 )
	{
		counter = 0;
		if (network->feedbackFeatureSet1 == 1)
		{
			step = 1;
		}
	}

	if ( step == 1)
	{
		Network_ReportFeatureSetMessageHP( network );
		counter = 0;
		countStepOnValue = 9;
		numberFeedbackPacketsSent++;
		step = 2;
	}

	if ( step == 2 && (counter > countStepOnValue) )
	{
		counter = 0;
		step = 1;
	}


	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}

}


void Display_serviceFeedbackFeatureSetLP( Network network  )
{
	static unsigned char lastTick = 0;
	static unsigned char counter = 0;
	static unsigned char countStepOnValue = 0;
	static unsigned char step = 0;
	static unsigned long numberFeedbackPacketsSent = 0;

	//Has an acknowledge packet been received from the APIS controller?
	if ( network->acknowledgeFeatureResetReceived == 1 )
	{
		step = 0;
		network->acknowledgeFeatureResetReceived = 0;
		network->feedbackFeatureSet2 = 0;
		numberFeedbackPacketsSent = 0;
	}

	//Have we sent more than 10 Feedback Packets?
	//If we have stop sending Feedback Packets.
	//if ( numberFeedbackPacketsSent >= 1000  )
	//{
	//	step =0;
	//	network->feedbackFeatureSet2 = 0;
		//numberFeedbackPacketsSent = 0;
	//}

	if ( step == 0 )
	{
		counter = 0;
		if (network->feedbackFeatureSet2 == 1)
		{
			step = 1;
		}
	}

	if ( step == 1)
	{
		Network_ReportFeatureSetMessageLP( network );
		counter = 0;
		countStepOnValue = 9;
		numberFeedbackPacketsSent++;
		step = 2;
	}

	if ( step == 2 && (counter > countStepOnValue) )
	{
		counter = 0;
		step = 1;
	}


	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}

}

void Display_updateDisplay(Display display)
{

#if DISPLAY_TYPE == SALOON_DISPLAY
/*
	if ( display->displayLine1->messageChanged && \
    	    ( display->displayLine1->messageComplete || \
    	           display->displayLine1->scrollEnable == 0 ) )
*/
	if ( display->displayLine1->messageChanged )
    {
    	if (display->displayLine1->scrollEnableChanged)
    	{
    		if ( display->displayLine1->scrollEnableReceived == 1)
    		{
    			display->displayLine1->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine1->scrollEnable = 0;
    		}
			display->displayLine1->scrollEnableChanged = 0;
    	}
    	//DB Changed for southern saloon
    	//if ( display->displayLine1->dotBufferSemaphore == 0)
    	{
    	display->displayLine1->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine1->DotBuffer);
    	Displayline_createDots(display->displayLine1);
    	Displayline_setScrollIncrement( display->displayLine1 );
    	display->displayLine1->dotBufferSemaphore = 0;
    	}
    }
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
/*
    if ( display->displayLine1->messageChanged && \
    	    ( display->displayLine1->messageComplete || \
    	           display->displayLine1->scrollEnable == 0 ) )
*/
	if ( display->displayLine1->messageChanged )
    {

    	if (display->displayLine1->scrollEnableChanged)
    	{
    		if ( display->displayLine1->scrollEnableReceived == 1)
    		{
    			display->displayLine1->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine1->scrollEnable = 0;
    		}
			display->displayLine1->scrollEnableChanged = 0;
    	}
    	if ( display->displayLine1->dotBufferSemaphore == 0)
    	{
    	display->displayLine1->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine1->DotBuffer);
    	Displayline_createDots(display->displayLine1);
    	Displayline_setScrollIncrement( display->displayLine1 );
    	display->displayLine1->dotBufferSemaphore = 0;
    	}
    }

    if ( display->displayLine2->messageChanged )
    {

    	if (display->displayLine2->scrollEnableChanged)
    	{
    		if ( display->displayLine2->scrollEnableReceived == 1)
    		{
    			display->displayLine2->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine2->scrollEnable = 0;
    		}
			display->displayLine2->scrollEnableChanged = 0;
    	}
    	if ( display->displayLine2->dotBufferSemaphore == 0)
    	{
    	display->displayLine2->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine2->DotBuffer);
    	Displayline_createDots(display->displayLine2);
    	Displayline_setScrollIncrement( display->displayLine2 );
    	display->displayLine2->dotBufferSemaphore = 0;
    	}
    }

    if ( display->displayLine3->messageChanged )
    {

    	if (display->displayLine3->scrollEnableChanged)
    	{
    		if ( display->displayLine3->scrollEnableReceived == 1)
    		{
    			display->displayLine3->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine3->scrollEnable = 0;
    		}
			display->displayLine3->scrollEnableChanged = 0;
    	}
    	if ( display->displayLine3->dotBufferSemaphore == 0)
    	{
    	display->displayLine3->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine3->DotBuffer);
    	Displayline_createDots(display->displayLine3);
    	Displayline_setScrollIncrement( display->displayLine3 );
    	display->displayLine3->dotBufferSemaphore = 0;
    	}
    }
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
    if ( display->displayLine1->messageChanged == 1 )
    {
    	if (display->displayLine1->scrollEnableChanged)
    	{
    		if ( display->displayLine1->scrollEnableReceived == 1)
    		{
    			display->displayLine1->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine1->scrollEnable = 0;
    		}
			display->displayLine1->scrollEnableChanged = 0;
    	}
    	if ( display->displayLine1->dotBufferSemaphore == 0)
    	{
    	display->displayLine1->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine1->DotBuffer);
    	Displayline_createDots(display->displayLine1);
    	Displayline_setScrollIncrement( display->displayLine1 );
    	display->displayLine1->dotBufferSemaphore = 0;
    	}
    }

    if ( display->displayLine2->messageChanged == 1)
    {

    	if (display->displayLine2->scrollEnableChanged)
    	{
    		if ( display->displayLine2->scrollEnableReceived == 1)
    		{
    			display->displayLine2->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine2->scrollEnable = 0;
    		}
			display->displayLine2->scrollEnableChanged = 0;
    	}
    	if ( display->displayLine2->dotBufferSemaphore == 0)
    	{
    	display->displayLine2->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine2->DotBuffer);
    	Displayline_createDots(display->displayLine2);
    	Displayline_setScrollIncrement( display->displayLine2 );
    	display->displayLine2->dotBufferSemaphore = 0;
    	}
    }

    if ( display->displayLine3->messageChanged == 1)
    {

    	if (display->displayLine3->scrollEnableChanged)
    	{
    		if ( display->displayLine3->scrollEnableReceived == 1)
    		{
    			display->displayLine3->scrollEnable = 1;
    		}
    		else
    		{
    			display->displayLine3->scrollEnable = 0;
    		}
			display->displayLine3->scrollEnableChanged = 0;
    	}
    	if ( display->displayLine3->dotBufferSemaphore == 0)
    	{
    	display->displayLine3->dotBufferSemaphore = 1;
    	Dots_clear(display->displayLine3->DotBuffer);
    	Displayline_createDots(display->displayLine3);
    	Displayline_setScrollIncrement( display->displayLine3 );
    	display->displayLine3->dotBufferSemaphore = 0;
    	}
    }
#else
  #error No DISPLAY_TYPE defined
#endif
}


void Display_activateTest( char displayTestMode )
{
	Display_activateTestMode( thisDisplay, displayTestMode);
}

void Display_activateTestMode( Display display, char displayTestMode )
{
	static unsigned char lastTestMode = 0;

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_REPORT_FEATURE_SET)
	{
		display->network->feedbackFeatureSet = 1;
		display->operationalMode = OperationalModeStandard;
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_MODE_OFF)
	{
#if CENTER_ENABLE == DISABLED
		display->operationalMode = OperationalModeStandard;
#elif CENTER_ENABLE == ENABLED
		display->operationalMode = OperationalModeStandardCentered;
#else
#error CENTER_ENABLE not specified
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_IDCODES)
	{
		display->operationalMode = OperationalModeTestIDCodes;
	}

	if ( displayTestMode == tfxDISPLAY_TEST_OPTO )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		display->operationalMode = OperationalModeTestOptos;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_VISUAL)
	{
		display->operationalMode = OperationalModeTestVisual;
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_NEURON_DISCOVERY)
	{
		display->operationalMode = OperationalModeNeuronDiscovery;
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_MODE_OPTO_COUNT)
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		display->operationalMode = OperationalModeTestOptoCount;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_PASSENGER_COUNTER )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		display->operationalMode = OperationalModeTestPassengerCounter;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDISPLAY_TEST_PASSENGER_COUNTER_COMMS )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		display->operationalMode = OperationalModeTestPassengerCounterComms;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_ECA_CYCLIC)
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && (DISPLAY_FUNCTIONALITY == ECAON_PCON || DISPLAY_FUNCTIONALITY == ECAON_PCOFF)
		display->operationalMode = OperationalModeTestECACyclicDisplayed;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_PASSENGER_COUNT_FEEDBACK)
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		display->operationalMode = OperationalModeTestPassengerCountFeedback;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_DISPLAY_ECA)
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		display->operationalMode = OperationalModeTestECADisplayed;
#else
		display->operationalMode = OperationalModeStandard;
#endif
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_DISPLAY_MESSAGE_COUNT)
	{
		display->operationalMode = OperationalModeTestMessageCount;
	}


	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_ECHO_MESSAGE_ON)
	{
		display->operationalMode = OperationalModeTestEchoMessage;
		display->network->echoModeEnabled = 1;

		//Re-Write the Flash with new value for echoModeEnabled
		//NVmemory_corruptNVMData( display->nvmemory );
		//display->nvmemory->readNeuronState = 0;
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_ECHO_MESSAGE_OFF)
	{
		display->operationalMode = OperationalModeTestEchoMessage;
		display->network->echoModeEnabled = 0;
		//Re-Write the Flash with new value for echoModeDisabled
		//NVmemory_corruptNVMData( display->nvmemory );
		//display->nvmemory->readNeuronState = 0;
	}

#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_PC_EOL_MESSAGE_ON)
	{
		display->passengerCount->pcEOLMessageModeEnabled = 1;
		//Re-Write the Flash with new value for pcEOLMessageModeEnabled
		NVmemory_corruptNVMData( display->nvmemory );
		display->nvmemory->readNeuronState = 0;
	}

	if (displayTestMode != lastTestMode && displayTestMode == tfxDisplay_TEST_PC_EOL_MESSAGE_OFF)
	{
		display->passengerCount->pcEOLMessageModeEnabled = 0;
		//Re-Write the Flash with new value for pcEOLMessageModeDisabled
		NVmemory_corruptNVMData( display->nvmemory );
		display->nvmemory->readNeuronState = 0;
	}
#endif

	lastTestMode = displayTestMode;

}

void Display_setModeNeuronDiscovery( Display display)
{
	Display_switchOperationalMode( display );
	NVmemory_corruptNVMData( display->nvmemory );
	display->nvmemory->readNeuronState = 0;
}

void Display_setModeNormal( Display display )
{
	Display_switchOperationalMode( display );
}

void Display_setModeTestVisual(Display display)
{
	Display_switchOperationalMode( display );
#if DISPLAY_TYPE == SALOON_DISPLAY
	Visualtest_movingBlock( display->displayLine1 );
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
	Visualtest_movingBlock( display->displayLine1 );
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	Visualtest_movingBlock( display->displayLine1 );
#endif
}

#if DISPLAY_TYPE == SALOON_DISPLAY
void Display_setModeTestOptos( Display display )
{
	Display_switchOperationalMode( display );
	Visualtest_opto( display->displayLine1,  display->optoinputs, display->visualtest );
}
#endif


void Display_setModeTestIDCodes( Display display )
{
	Display_switchOperationalMode( display );
	Visualtest_idCodes( display->displayLine1, display->network->neuron );
}

#if DISPLAY_TYPE == SALOON_DISPLAY
void Display_setModeTestOptoCount( Display display )
{
	Display_switchOperationalMode( display );
	Visualtest_optoCount( display->displayLine1, display->optoinputs, display->visualtest );
}
#endif

void Display_setModeTestPassengerCounter( Display display)
{
	Display_switchOperationalMode( display );
	Visualtest_passengerCounter( display->displayLine1, display->passengerCount );
}


void Display_setModeTestPassengerCounterComms( Display display)
{
	Display_switchOperationalMode( display );
	Visualtest_TestPassengerCounterComms( display->displayLine1, display->passengerCount );
}

void Display_setModeTestECACyclic( Display display)
{
	Display_switchOperationalMode( display );

    if (display->network->ECARequested != 1 )
    {
    //DB Changed
    ECAtest_OptoIOStateMachine( display->ecaTest );
    }
	Display_ecaCyclicTest( display->displayLine1, display->visualtest );
}


void Display_setModeDisplayECA( Display display)
{
	Display_switchOperationalMode( display );
	Display_ecaTest( display->displayLine1, display->visualtest );
	//Display_messageCountTest( display->displayLine1, display->visualtest );
}

void Display_setModeNVMemory( Display display)
{
	Display_switchOperationalMode( display );
#if TEST_MODE == ENABLE_NVMTEST_WRITE_PL_TRIGGERED
	Visualtest_TestNVMemory( display->displayLine1 );
#elif TEST_MODE == ENABLE_NVMTEST_WRITE_INTERNAL
	Visualtest_WriteNVMemory( display->displayLine1 );
#endif

	//Display_ecaTest( display->displayLine1, display->visualtest );
	//Display_messageCountTest( display->displayLine1, display->visualtest );
}

void Display_setModeMessageCount( Display display)
{
	Display_switchOperationalMode( display );
	Display_messageCountTest( display->displayLine1, display->visualtest );
}

void Display_setModePassengerCountFeedback( Display display)
{
	Display_switchOperationalMode( display );
	Display_passengerCountFeedback( display->displayLine1, display->visualtest );
}

void Display_setModeEchoMessage( Display display)
{
	Display_switchOperationalMode( display );
}


void Display_switchOperationalMode(Display display)
{
	static volatile enum OperationalMode lastOperationalMode = OperationalModeNull;
	//lastOperationalMode = OperationalModeNull;

#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
    if ( lastOperationalMode == OperationalModeTestPassengerCounterComms
    		&& display->operationalMode != lastOperationalMode )
    {
    	Display_disablePanelScan();
    	PassengerCount_enableAutoLEDBrightness( display->passengerCount );
    	Display_enablePanelScan();
    }
#endif

	if ( display->operationalMode != lastOperationalMode )
	{
		#if DISPLAY_TYPE == SALOON_DISPLAY
				switch ( display->operationalMode)
				{
				case OperationalModeNull:

					break;
				case OperationalModeStandard:
					Display_saloonDisplayInitialise( display );
					break;
				case OperationalModeTestIDCodes:
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					break;
				case OperationalModeTestOptos:
					display->visualtest->updateDisplayAtleastOnce = 1;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Static( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Static( display );
#endif
					break;
				case OperationalModeTestVisual:
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS150
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					break;
				case OperationalModeNeuronDiscovery:
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					break;
				case OperationalModeTestOptoCount:
					display->visualtest->updateDisplayAtleastOnce = 1;
					display->optoinputs->opto_Input1->optoChangedCounter = 0;
					display->optoinputs->opto_Input2->optoChangedCounter = 0;
					display->optoinputs->opto_Input3->optoChangedCounter = 0;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Static( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Static( display );
#endif
					break;
				case OperationalModeTestPassengerCounter:
					display->visualtest->updateDisplayAtleastOnce = 1;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					break;
				case OperationalModeTestPassengerCounterComms:
					Display_saloonDisplayInitialiseCounterComms( display );
					break;
				case OperationalModeTestECACyclicDisplayed:
					display->visualtest->updateDisplayAtleastOnce = 1;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Static( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Static( display );
#endif
					break;
				case OperationalModeTestPassengerCountFeedback:
					display->visualtest->updateDisplayAtleastOnce = 1;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif

					break;
				case OperationalModeTestECADisplayed:
					display->visualtest->updateDisplayAtleastOnce = 1;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					break;
				case OperationalModeTestMessageCount:
					display->visualtest->updateDisplayAtleastOnce = 1;
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Static( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Static( display );
#endif
					break;
				case OperationalModeTestEchoMessage:
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					display->visualtest->updateDisplayAtleastOnce = 1;
					Visualtest_idCodes( display->displayLine1, display->network->neuron );
					break;
				case OperationalModeTestNVMemory:
#if END_CLIENT == CLASS319
					Display_saloonDisplayInitialiseL1C5Scroll( display );
#elif END_CLIENT != CLASS319
					Display_saloonDisplayInitialiseL1C1Scroll( display );
#endif
					display->visualtest->updateDisplayAtleastOnce = 1;
					break;
				default:
					Display_saloonDisplayInitialise( display );
					break;
				}
		#endif


		#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY || DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
				switch ( display->operationalMode)
				{
				case OperationalModeNull:

					break;
				case OperationalModeStandard:
					Display_destinationDisplayInitialise( display );
					break;
				case OperationalModeTestIDCodes:
					Display_destinationDisplayInitialiseL1C2ScrollL2BlankL3Blank( display );
					break;
				case OperationalModeTestOptos:
					break;
				case OperationalModeTestVisual:
					Display_destinationDisplayInitialiseL1C4ScrollL2OffL3Off( display );
					break;
				case OperationalModeNeuronDiscovery:
					Display_destinationDisplayInitialiseL1C2ScrollL2BlankL3Blank( display );
					break;
				case OperationalModeTestOptoCount:
					break;
				case OperationalModeTestMessageCount:
					display->visualtest->updateDisplayAtleastOnce = 1;
					Display_destinationDisplayInitialiseL1C2StaticL2BlankL3Blank( display );
					break;
				case OperationalModeTestEchoMessage:
					Display_destinationDisplayInitialiseL1C2ScrollL2BlankL3Blank( display );
					display->visualtest->updateDisplayAtleastOnce = 1;
					Visualtest_idCodes( display->displayLine1, display->network->neuron );
					break;
				case OperationalModeStandardCentered:
					Display_destinationDisplayInitialise( display );
					break;
				default:
					Display_destinationDisplayInitialise( display );
					break;
				}
		#endif
	};
	lastOperationalMode = thisDisplay->operationalMode;
}



void Display_scanOperationalMode(Display display)
{
	static volatile unsigned char lastState = 0;

	if (display->operationalMode == OperationalModeStandard && lastState != 1  )
	{
		Display_setModeNormal( display );
		lastState = 1;
	}

	if (display->operationalMode == OperationalModeTestVisual && lastState != 2 )
	{
		Display_setModeTestVisual( display );
		lastState = 2;
	}

	if (display->operationalMode == OperationalModeTestIDCodes  && lastState != 3)
	{
		Display_setModeTestIDCodes( display );
		lastState = 3;
	}

	if (display->operationalMode == OperationalModeTestOptoCount )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		Display_setModeTestOptoCount( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 4;
	}

	if (display->operationalMode == OperationalModeNeuronDiscovery  )
	{
		Display_setModeNeuronDiscovery( display);
		lastState = 5;
	}

	if (display->operationalMode == OperationalModeTestOptos )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		Display_setModeTestOptos( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 6;
	}
	if (display->operationalMode == OperationalModeTestPassengerCounter && lastState != 7 )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION==PC_FUNCTION_ON

		Display_setModeTestPassengerCounter( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 7;
	}

	if (display->operationalMode == OperationalModeTestPassengerCounterComms  )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION==PC_FUNCTION_ON
		Display_setModeTestPassengerCounterComms( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 8;
	}

	if (display->operationalMode == OperationalModeTestECACyclicDisplayed  )
	{
#if  DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		Display_setModeTestECACyclic( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 9;
	}

	if (display->operationalMode == OperationalModeTestPassengerCountFeedback  )
	{
#if  DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		Display_setModePassengerCountFeedback( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 10;
	}

	if (display->operationalMode == OperationalModeTestECADisplayed  )
	{
#if  DISPLAY_TYPE == SALOON_DISPLAY &&  CFA_FUNCTION == CFA_FUNCTION_ON
		Display_setModeDisplayECA( display );
#else
		Display_setModeNormal( display );
#endif
		lastState = 11;
	}

	if (display->operationalMode == OperationalModeTestMessageCount  )
	{
		Display_setModeMessageCount( display );
		lastState = 12;
	}
	if (display->operationalMode == OperationalModeTestEchoMessage  )
	{
		Display_setModeEchoMessage( display );
		lastState = 13;
	}

	if (display->operationalMode == OperationalModeTestNVMemory  )
	{
		Display_setModeNVMemory( display );
		lastState = 14;
	}


	if (display->operationalMode == OperationalModeStandardCentered && lastState != 15 )
	{
		Display_setModeNormal( display );
		lastState = 15;
	}

}


void Display_operationalModeAutoChange( Display display )
{
	static unsigned char lastTick = 0;
	static unsigned short counter = 0;
	static unsigned short countStepOnValue = 0;
	static unsigned char step = 0;
	static unsigned char stepOneShot = 0;

	static unsigned char defaultStepOnValue = 40;

	if ( step == 0 && stepOneShot == 0 )
	{
		display->operationalMode = OperationalModeStandard;
		stepOneShot = 1;
		countStepOnValue = defaultStepOnValue;
	}
	if ( step == 1 && stepOneShot == 0 )
	{
		display->operationalMode = OperationalModeTestVisual;
		stepOneShot = 1;
		countStepOnValue = defaultStepOnValue;
	}

	if ( step == 2 && stepOneShot == 0)
	{
		display->operationalMode = OperationalModeTestIDCodes;
		stepOneShot = 1;
		countStepOnValue = defaultStepOnValue*20;
	}

	if ( step == 3 )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		display->operationalMode = OperationalModeTestOptos;
		countStepOnValue = 5;
#else
		countStepOnValue = 1;
#endif
	}


	if ( step == 4 )
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && CFA_FUNCTION == CFA_FUNCTION_ON
		display->operationalMode = OperationalModeTestOptoCount;
		countStepOnValue = 5;
#else
		countStepOnValue = 1;
#endif
	}

	if ( step == 5 && stepOneShot == 0)
	{

#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		display->operationalMode = OperationalModeTestPassengerCounter;
		stepOneShot = 1;
		countStepOnValue = 5;
#else
		countStepOnValue = 1;
#endif
	}

	if ( step == 6 && stepOneShot == 0)
	{
#if DISPLAY_TYPE == SALOON_DISPLAY && PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
		display->operationalMode = OperationalModeTestPassengerCounterComms;
		stepOneShot = 1;
		countStepOnValue = 320;
#else
		countStepOnValue = 1;
#endif
	}

	if ( step == 7 && stepOneShot == 0)
	{
		display->operationalMode = OperationalModeTestNVMemory;
		stepOneShot = 1;
		countStepOnValue = defaultStepOnValue*10;
	}

	if (counter > countStepOnValue )
	{
		counter = 0;
		step++;
		if (step > 7)
		{
			step = 0;
		}
		stepOneShot = 0;
	}

//	if ( step == 8 && stepOneShot == 0)
//	{
//		display->operationalMode = OperationalModeNeuronDiscovery;
//		stepOneShot = 1;
//	}



	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}
}



void Display_startPassengerCounters()
{
	Display_disablePanelScan();
	PassengerCount_startCounters( thisDisplay->passengerCount);
	thisDisplay->passengerCount->startMessageSent = 1;
	Display_enablePanelScan();
}

void Display_stopPassengerCounters()
{
	Display_disablePanelScan();
	thisDisplay->passengerCount->startMessageSent = 0;
	PassengerCount_stopCounters( thisDisplay->passengerCount);
    Display_enablePanelScan();

#if PASSENGER_COUNT_MESSAGE == PASSENGER_COUNT_MESSAGE_ENABLED
   //Network_PassengerCounterMessage( thisDisplay->network , thisDisplay->passengerCount );
#endif

   if (thisDisplay->operationalMode == OperationalModeTestPassengerCounter )
   {
   PassengerCount_updateCounterVisualTest( thisDisplay->displayLine1,
   	    		                                 thisDisplay->passengerCount);
   }
}

void Display_passengerCountTest(Display display)
{
	  static unsigned char lastTick = 0;
	  static unsigned char counter = 0;

	  if (counter == 30)
	  {
	    display->passengerCount->sendStartPassengerCounters = 1;
	    counter++;
	  }


	  if (counter == 60)
	  {
		display->passengerCount->sendStopPassengerCounters = 1;
		counter++;
	  }

	  if (counter == 120)
	  {
	      counter = 0;
	  }
	 if (lastTick !=clkTick)
	 {
		 counter++;
		 lastTick = clkTick;
	 }
}

void Display_set_currentRenderRow( Display display, unsigned char currentRenderRow )
{
	if (currentRenderRow >=0 && currentRenderRow <= 26)
	{
	display->currentRenderRow = currentRenderRow;
	}
}


void Display_enablePanelScan()
{
	thisDisplay->enablePanelScan = 1;
}

void Display_disablePanelScan()
{
	thisDisplay->enablePanelScan = 0;
}


void Display_servicePassengerCounters(Display display, Network network, PassengerCount passengerCount )
{
   if ( passengerCount->sendStartPassengerCounters == 1 )
   {
	   passengerCount->sendStartPassengerCounters = 0;
	   Display_startPassengerCounters();
	   return;
   }

   Display_serviceFeedbackPassengerCountValues( display, network, passengerCount  );

}

void Display_serviceFeedbackPassengerCountValues( Display display, Network network, PassengerCount passengerCount  )
{
	static unsigned char lastTick = 0;
	static unsigned char counter = 0;
	static unsigned char countStepOnValue = 0;
	static unsigned char step = 0;

	//Has an acknowledge packet been received from the APIS controller?
	if ( network->acknowledgePCResetReceived == 1)
	{
	   network->acknowledgePCResetReceived = 0;
	   if ( Network_neuronCompare( network ) )
	   {
		 step = 0;
		 passengerCount->sendStopPassengerCounters = 0;
		 passengerCount->numberPassengerCounterPacketsSent = 0;
		 passengerCount->numberPassengerCounterAcknowledgesReceived++;

	   }
	}

	//Have 20 passenger Count feed back messages been sent?
	//If so, stop sending passenger counter feedback messages.
	if ( passengerCount->numberPassengerCounterPacketsSent >= 20 )
	{
		 step = 0;
		 passengerCount->sendStopPassengerCounters = 0;
		 passengerCount->numberPassengerCounterPacketsSent = 0;

	}

	//Has a startPassengerCounters message been received over the power line?
	//Has a APIS reboot message been received over the power line?
	//If so, stop sending passenger counter feedback messages.
	if ( passengerCount->resetPassengerCounterMessageFeedback == 1)
	{
		step = 0;
		passengerCount->numberPassengerCounterPacketsSent = 0;
		passengerCount->resetPassengerCounterMessageFeedback = 0;
	}

if ( passengerCount->pcEOLMessageModeEnabled == 0 )
{
	if ( step == 0 )
	{
		counter = 0;
		if ( passengerCount->sendStopPassengerCounters == 1 )
		{
			Display_stopPassengerCounters();
			passengerCount->totalNumberPassengerCounterReadCycles++;
			step = 1;
		}
	}
}
else
{
	if ( step == 0 )
	{
		counter = 0;
		if ( passengerCount->sendStopPassengerCounters == 1
				  && display->displayLine1->scrollEnable == 1
				     && display->displayLine1->messageComplete == 1)
		{
			Display_stopPassengerCounters();
			passengerCount->totalNumberPassengerCounterReadCycles++;
			step = 1;
		}
	}

	if ( step == 0 )
	{
		counter = 0;
		if ( passengerCount->sendStopPassengerCounters == 1
				&& display->displayLine1->scrollEnable == 0 )
		{
			Display_stopPassengerCounters();
			passengerCount->totalNumberPassengerCounterReadCycles++;
			step = 1;
		}
	}
}
	if ( step == 1 )
	{
		counter = 0;
		countStepOnValue = ( rand()%5 + 5) ;
		step = 2;
	}

	if ( step == 2 && (counter > countStepOnValue) )
	{
		passengerCount->numberPassengerCounterPacketsSent++;

		Network_PassengerCounterMessage( network , passengerCount );
		counter = 0;
		step = 1;
	}

	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}
}

void Display_serviceECA( Network network )
{
	static unsigned char lastTick = 0;
	static unsigned char counter = 0;
	static unsigned char countStepOnValue = 0;
	static unsigned char step = 0;

   if ( network->acknowledgeECAResetReceived == 1 )
   {
	   step = 0;
	   network->acknowledgeECAResetReceived = 0;
	   network->numberECAAck++;
	   network->ECARequested = 0;
	   network->numberServiceECAPacketsSent = 0;
   }

	if ( network->numberServiceECAPacketsSent >= 15 )
	{
		 step = 0;
		 network->ECARequested = 0;
		 network->numberServiceECAPacketsSent = 0;
	}

   if (step == 0)
   {

	   if ( network->ECARequested == 1 )
	   {
		   step = 1;
		   network->numberECARequests++;
	   }
   }

	if ( step == 1)
	{
		Network_ECAMessage( network );
		counter = 0;
		countStepOnValue = 10 + rand()%5;
		network->numberServiceECAPacketsSent++;
		step = 2;
	}

	if ( step == 2 && (counter > countStepOnValue) )
	{
		counter = 0;
		step = 1;
	}


	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}
}

void Display_initialisePasssengerCounters()
{
	  Display_disablePanelScan();
	  PassengerCount_startCounters( thisDisplay->passengerCount);
	  PassengerCount_enableAutoLEDBrightness(thisDisplay->passengerCount);
	  Display_enablePanelScan();
}


void Display_ecaCyclicTest( Displayline displayLine, Visualtest visualtest )
{
  int index = 0;
  static int numberECARequestLast = -1;
  static int NumberECAACKLast = -1;
  char characterBuffer[224];

  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

	  if ( thisDisplay->network->numberECARequests == numberECARequestLast && \
			  thisDisplay->network->numberECAAck == NumberECAACKLast &&
			    		  visualtest->updateDisplayAtleastOnce == 0 )
	  {
		  return;
	  }

  index  = sprintf( characterBuffer, "ECA:%d", thisDisplay->network->numberECARequests);
  index += sprintf( characterBuffer + index, " ACK:%d ", thisDisplay->network->numberECAAck );
  displayLine->messageComplete = 0x00;


#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
  displayLine->displayLineDotsCreated = 0;
  displayLine->messageChanged = 1;

  numberECARequestLast = thisDisplay->network->numberECARequests;
  NumberECAACKLast = thisDisplay->network->numberECAAck;

  visualtest->updateDisplayAtleastOnce = 0;
}



void Display_passengerCountFeedback(  Displayline displayLine, Visualtest visualtest )
{
  int index = 0;
  static int totalNumberPassengerCounterReadCyclesLast = -1;
  static int numberPassengerCounterAcknowledgesReceivedLast = -1;
  static int totalNumberAPISRebootMessagesLast = -1;
  char characterBuffer[224];

  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

	  if ( thisDisplay->passengerCount->totalNumberPassengerCounterReadCycles == totalNumberPassengerCounterReadCyclesLast && \
			  thisDisplay->passengerCount->numberPassengerCounterAcknowledgesReceived == numberPassengerCounterAcknowledgesReceivedLast &&
					  thisDisplay->passengerCount->totalNumberAPISRebootMessages == totalNumberAPISRebootMessagesLast &&
			    		  visualtest->updateDisplayAtleastOnce == 0 )
	  {
		  return;
	  }

  index  = sprintf( characterBuffer, "Close:%d", thisDisplay->passengerCount->totalNumberPassengerCounterReadCycles);
  index += sprintf( characterBuffer + index, " Ack:%d", thisDisplay->passengerCount->numberPassengerCounterAcknowledgesReceived );
  index += sprintf( characterBuffer + index, " Rebt:%d", thisDisplay->passengerCount->totalNumberAPISRebootMessages );
  displayLine->messageComplete = 0x00;


#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
  displayLine->displayLineDotsCreated = 0;
  displayLine->messageChanged = 1;

  totalNumberPassengerCounterReadCyclesLast = thisDisplay->passengerCount->totalNumberPassengerCounterReadCycles;
  numberPassengerCounterAcknowledgesReceivedLast = thisDisplay->passengerCount->numberPassengerCounterAcknowledgesReceived;
  totalNumberAPISRebootMessagesLast = thisDisplay->passengerCount->totalNumberAPISRebootMessages;

  visualtest->updateDisplayAtleastOnce = 0;
}


void Display_ecaTest( Displayline displayLine, Visualtest visualtest )
{
  int index = 0;
  static unsigned int NumberECARequestLast = -1;
  static unsigned int NumberECAACKLast = -1;
  static unsigned int NumberTextMessagesLast = -1;
  char characterBuffer[224];

  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;



	  if ( thisDisplay->network->numberECARequests == NumberECARequestLast && \
			  thisDisplay->network->numberECAAck == NumberECAACKLast && \
					  thisDisplay->numberTextMessages == NumberTextMessagesLast && \
			    		  visualtest->updateDisplayAtleastOnce == 0 )
	  {
		  return;
	  }


  index = sprintf( characterBuffer, "MSG:%d ", thisDisplay->numberTextMessages );
  index += sprintf( characterBuffer + index, "ECA:%d", thisDisplay->network->numberECARequests);
  index += sprintf( characterBuffer + index, " ACK:%d ", thisDisplay->network->numberECAAck );

  displayLine->messageComplete = 0x00;


#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
  displayLine->displayLineDotsCreated = 0;
  displayLine->messageChanged = 1;

  NumberECARequestLast = thisDisplay->network->numberECARequests;
  NumberECAACKLast = thisDisplay->network->numberECAAck;
  NumberTextMessagesLast = thisDisplay->numberTextMessages;

  visualtest->updateDisplayAtleastOnce = 0;
}


void Display_messageCountTest( Displayline displayLine, Visualtest visualtest )
{
  int index = 0;
  static unsigned int NumberTextMessagesLast = -1;
  char characterBuffer[224];

  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

  if ( thisDisplay->numberTextMessages == NumberTextMessagesLast &&
					  visualtest->updateDisplayAtleastOnce == 0 )
  {
	  return;
  }

  index = sprintf( characterBuffer, "MSG:%d ", thisDisplay->numberTextMessages );

  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif

  displayLine->displayLineDotsCreated = 0;
  displayLine->messageChanged = 1;

  NumberTextMessagesLast = thisDisplay->numberTextMessages;
  visualtest->updateDisplayAtleastOnce = 0;

}



void Display_serviceEchoMessage( Network network )
{
	static unsigned char lastTick = 0;
	static unsigned char counter = 0;
	static unsigned char countStepOnValue = 0;
	static unsigned char step = 0;

   if ( network->acknowledgeEchoResetReceived == 1 )
   {
	   step = 0;
	   network->acknowledgeEchoResetReceived = 0;
	   if ( network->EchoMessageRequested > 0)
	   {
	   network->EchoMessageRequested--;
	   }
	   network->numberServiceEchoPacketsSent = 0;
   }

	if ( network->numberServiceEchoPacketsSent >= 5 )
	{
		 step = 0;

		 network->EchoMessageRequested = 0;
		 network->numberServiceEchoPacketsSent = 0;
	}

   if (step == 0)
   {

	   if ( network->EchoMessageRequested > 0 )
	   {
		   step = 1;
	   }
	   else
	   {
			 step = 0;
			 network->EchoMessageRequested = 0;
			 network->numberServiceEchoPacketsSent = 0;
	   }
   }

	if ( step == 1)
	{
		Network_EchoMessage( network );
		counter = 0;
		countStepOnValue = 5 + rand()%5;
		network->numberServiceEchoPacketsSent++;
		step = 2;
	}

	if ( step == 2 && (counter > countStepOnValue) )
	{
		counter = 0;
		step = 1;
	}


	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}
}



void Display_serviceDiagnosticPLMessage( Network network  )
{
	static unsigned char lastTick = 0;
	static unsigned long counter = 0;
	static unsigned long countStepOnValue = 0;
	static unsigned char step = 0;
	static unsigned char numberFeedbackPacketsSent = 0;

	//Has an acknowledge packet been received from the APIS controller?
	if ( network->acknowledgeFeatureResetReceived == 1 )
	{
		step = 0;
		network->acknowledgeFeatureResetReceived = 0;
		network->feedbackFeatureSet3 = 0;
		numberFeedbackPacketsSent = 0;
	}

	//Have we sent more than 10 Feedback Packets?
	//If we have stop sending Feedback Packets.
	if ( numberFeedbackPacketsSent >= 5  )
	{
		step =0;
		network->feedbackFeatureSet3 = 0;
		numberFeedbackPacketsSent = 0;
	}

	if ( step == 0 )
	{
		counter = 0;
		if (network->feedbackFeatureSet3 == 1)
		{
			step = 1;
		}
	}

	if ( step == 1)
	{
		Network_TriggerDiagnosticMessage( network );
		counter = 0;
		countStepOnValue = 50;
		numberFeedbackPacketsSent++;
		step = 2;
	}

	if ( step == 2 && (counter > countStepOnValue) )
	{
		counter = 0;
		step = 1;
	}


	if (lastTick !=clkTick)
	{
		counter++;
		lastTick = clkTick;
	}

}
