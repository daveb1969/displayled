/*
 * display.h
 *
 *  Created on: 12 Aug 2010
 *      Author: DAVE
 */


#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "displayline.h"
#include "network.h"
//#include "callforaid.h"
#include "optoinputs.h"
#include "passengercount.h"
#include "NVmemory.h"
#include "visualtest.h"
#include "ecatest.h"
#include "defaultmessage.h"
#include "board.h"



enum DisplayType
{
    DisplayNull = 0,
	FrontDestinationDisplay,
	SaloonDisplay
};

enum Functionality
{
    FunctionalityNull = 0,
    Standard,
    Crew_Comms,
    ECAon_PCoff,
    ECAon_PCon,
    ECAoff_PCon
};

enum Client
{
	ClientNull = 0,
	fgw,
	southern,
	arriva,
	class319,
	class16x,
	class150
};



enum OperationalMode
{
    OperationalModeNull = 0,
    OperationalModeStandard,
	OperationalModeTestIDCodes,
	OperationalModeTestOptos,
	OperationalModeTestVisual,
	OperationalModeNeuronDiscovery,
	OperationalModeTestOptoCount,
	OperationalModeTestPassengerCounter,
	OperationalModeTestPassengerCounterComms,
	OperationalModeTestECACyclicDisplayed,
	OperationalModeTestPassengerCountFeedback,
	OperationalModeTestECADisplayed,
	OperationalModeTestMessageCount,
	OperationalModeTestEchoMessage,
	OperationalModeTestNVMemory,
	OperationalModeStandardCentered
};



struct display_str
{
    volatile unsigned char			displaytype;
    volatile unsigned char			numberRows;
    volatile unsigned char			numberCols;
    volatile unsigned char          currentRenderRow;
    volatile unsigned char          enablePanelScan;
    volatile unsigned int           numberTextMessages;
	volatile unsigned int           numberMaintenanceMessages;
    volatile enum OperationalMode 	operationalMode;
    volatile enum Functionality     functionality;
    volatile enum Client            client;
    volatile Displayline     		displayLine1;
    volatile Displayline     		displayLine2;
    volatile Displayline     		displayLine3;
    volatile Callforaid      		callforaid;
    volatile Optoinputs      		optoinputs;
    Network  						network;
    volatile PassengerCount  		passengerCount;
    volatile NVmemory        		nvmemory;
    volatile Visualtest      		visualtest;
    volatile unsigned char          serialNumber[4];
    volatile ECAtest				ecaTest;
    volatile DefaultMessage         defaultMessage;
};

typedef struct display_str* Display;

volatile Display thisDisplay = NULL;

//TLC5923 LED Driver control signals
#	define XERR           AVR32_PIN_PB02 //
#   define SCLK           AVR32_PIN_PB10
#   define XLAT           AVR32_PIN_PB03
#   define MODE           AVR32_PIN_PA05
#   define SIN            AVR32_PIN_PB09 //
#   define BLANK          AVR32_PIN_PA29 // LOW - Normal, HIGH Disabled
#   define SOUT           AVR32_PIN_PB11 //

// LED Row Enables
// Dealing with row 1 to row 25.
// Used to select which of the possible 25 rows of LED's to turn on.
// The required row for display is selected by use of a 4 bit code
// The 4 bit code placed into ROWADD0, ROWADD1, ROWADD2, ROWADD4.
// Toggling the ROW_REN output clocks the required row
// select code into the Row Selector IC.

#   define ROWADD0        AVR32_PIN_PB08
#   define ROWADD1        AVR32_PIN_PB07
#   define ROWADD2        AVR32_PIN_PB06

#if END_CLIENT == CLASS319 && DISPLAY_TYPE == SALOON_DISPLAY
#   define ROWADD3        AVR32_PIN_PB05
#elif END_CLIENT == CLASS150 && DISPLAY_TYPE == SALOON_DISPLAY
#   define ROWADD3        AVR32_PIN_PB05
#elif END_CLIENT == CLASS319 && DISPLAY_TYPE != SALOON_DISPLAY
#   define ROWADD3        AVR32_PIN_PA07
#elif END_CLIENT == CLASS150 && DISPLAY_TYPE != SALOON_DISPLAY
#   define ROWADD3        AVR32_PIN_PA07
#elif END_CLIENT != CLASS319 || END_CLIENT != CLASS150
#   define ROWADD3        AVR32_PIN_PA07
#endif

#   define ROWADD4        AVR32_PIN_PA13
#   define ROW_REN        AVR32_PIN_PA06

// LED Dot Correction Brightness parameters
#   define MAX_BRIGHTNESS 			127
#   define DOT_CORRECT_NUMBER_WORDS	16
#   define BRIGHTNESS_LEVEL 	    10
#   define LED_DRIVERS_IN_CHAIN 	10
#   define ROW_OFFSET				127
#   define DIMMED_BRIGHTNESS		127
#   define FULL_BRIGHTNESS			127



void Display_construct();

void Display_destroy();

void Display_set_dotCorrection(unsigned short nBrightness, int nNumInChain);

void Display_displayRow( int rowToSelect);

void Display_clearDisplay(int numberColumns);

void Display_writeRow(int rowLength);

void Display_set_displayType(Display display, unsigned char displaytype);

void Display_set_numberRows(Display display, unsigned char numberRows);

unsigned char Display_get_numberRows( Display display );

void Display_set_numberCols(Display display, unsigned char numberCols);

void Display_updateDisplay(Display display);

void Display_saloonDisplayConstruct(Display display);

void Display_saloonDisplayInitialise(Display display);

void Display_destinationDisplayConstruct(Display display);

void Display_destinationDisplayInitialise(Display display);

void Display_serviceOptoInputs(Network network, Optoinputs optoinputs );

void Display_switchOperationalMode(Display display);

void Display_activateTestMode( Display display, char displayTestMode );

void Display_activateTest( char displayTestMode );

void Display_setModeNormal( Display display );

void Display_setModeTestVisual(Display display);

#if DISPLAY_TYPE == SALOON_DISPLAY
void Display_setModeTestOptos( Display display );

void Display_setModeTestOptoCount( Display display );
#endif

void Display_setModeTestIDCodes( Display display );

void Display_setModeTestECACyclic( Display display);

void Display_setModeDisplayECA( Display display);

void Display_setModeMessageCount( Display display);

void Display_scanOperationalMode(Display display);

void Display_operationalModeAutoChange( Display display );

void Display_setModeNeuronDiscovery( Display display);

void Display_startPassengerCounters();

void Display_stopPassengerCounters();

void Display_passengerCountTest(Display display);

void Display_set_currentRenderRow( Display display, unsigned char currentRenderRow );

void Display_enablePanelScan();

void Display_disablePanelScan();

void Display_servicePassengerCounters(Display display, Network network, PassengerCount passengerCount );

void Display_serviceFeedbackPassengerCountValues( Display display, Network network, PassengerCount passengerCount  );

void Display_serviceFeedbackFeatureSetHP( Network network  );

void Display_serviceFeedbackFeatureSetLP( Network network  );

void Display_serviceECA( Network network );

void Display_serviceEchoMessage( Network network );

void Display_initialisePasssengerCounters();

void Display_ecaCyclicTest( Displayline displayLine, Visualtest visualtest );

void Display_ecaTest( Displayline displayLine, Visualtest visualtest );

void Display_passengerCountFeedback(  Displayline displayLine, Visualtest visualtest );

void Display_setModePassengerCountFeedback( Display display);

void Display_messageCountTest( Displayline displayLine, Visualtest visualtest );

void Display_serviceDiagnosticPLMessage( Network network  );

#endif /* DISPLAY_H_ */
