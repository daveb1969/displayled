/*
 * dotmessage.h
 *
 *  Created on: 27 Aug 2010
 *      Author: DAVE
 */

#ifndef DOTMESSAGE_H_
#define DOTMESSAGE_H_

#include "displayline.h"


unsigned char GetDotRepresentation( Displayline displayline );

#endif /* DOTMESSAGE_H_ */
