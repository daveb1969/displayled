/*! \file ******************************************************************
 *
 * \brief SNPserialport.c 		SNP Serial Port.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carriages on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

// Standard C library includes.
#include "stdlib.h"
#include "string.h"

// Project includes.
#include "../utils/tfx_assert.h"
#include "passengercounter.h"
#include "usart.h"
#include "gpio.h"
#include "user_board.h"


//! \brief Passenger Counter constructor
//!
//! Create Passenger Counter data structure off the heap.
//!
//! \return Return PassengerCounter*
//!
PassengerCounter PassengerCounter_construct() {
	PassengerCounter passengerCounter = ( struct passengercounter_str* ) malloc(sizeof(struct passengercounter_str));

	passengerCounter->SNP_passengerCounterAddress = 0x00;
	passengerCounter->countIn = 0;
	passengerCounter->countOut = 0;
	passengerCounter->passengerCounterEnabled = 0;
	passengerCounter->crcPasses = 0;
	passengerCounter->crcFails = 0;
	TFX_ASSERT(passengerCounter, "Failed to allocate memory.");
	return passengerCounter;
}

//! \brief Passenger Counter destructor
//!
//!
//! Remove Passenger Counter data structure off the heap.
//!
void PassengerCounter_destroy(PassengerCounter passengerCounter) {
	TFX_ASSERT(passengerCounter, "Expected a non-null pointer.");
	free(passengerCounter);
}



