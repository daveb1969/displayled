

#ifndef NVMEMORY_H_
#define NVMEMORY_H_


#include "network.h"
#include "displayline.h"
#include "neuron.h"
#include "defaultmessage.h"
#include "debug.h"


//! Structure type containing variables to store in NVRAM using a specific
//! memory map.
typedef const struct
{
	unsigned char id[6];
	unsigned char prog_id[8];
	unsigned char idFound;
	//////////////////////////
	//Default Message 1 Data
	/////////////////////////
	unsigned char DM1CharSet;
	unsigned char DM1PrePend;
	unsigned char DM1ScrollEnable;
	char          DM1Text1[defaultMessageSize];
	unsigned char DM1storedInFlash;
	//////////////////////////
	//Default Message 2 Data
	/////////////////////////
	unsigned char DM2CharSet;
	unsigned char DM2PrePend;
	unsigned char DM2ScrollEnable;
	char          DM2Text2[defaultMessageSize];
	unsigned char DM2storedInFlash;
	/////////////////////////
	//Default Message 3 Data
	/////////////////////////
	unsigned char DM3CharSet;
	unsigned char DM3PrePend;
	unsigned char DM3ScrollEnable;
	char          DM3Text3[defaultMessageSize];
	unsigned char DM3storedInFlash;
	//////////////////////////
	short         CRC16;
} NVMData_str;

struct NVmemory_str {
	char readNeuronState;			//State of Neuron ID NVM storage
	NVMData_str * nvram;			//Pointer to NV Memory into which data stored.
	char CRC_checkPassed;
	short NV_crcCalculated;
	short SRAM_crcCalculated;
};


typedef struct NVmemory_str* NVmemory;


// Constructor.
//
//
NVmemory NVmemory_construct();

// Destructor.
//
//
void NVmemory_destroy();

void NVmemory_delay(unsigned long count);

/*! \brief Prints the variables stored in NVRAM.
 *
 * \param nvram_data  Pointer to the NVRAM data structure to print.
 */

void NVmemory_displayData( Displayline displayLine, NVmemory nvmemory );

/*! \brief This is an example demonstrating flash read / write data accesses
 *         using the FLASHC driver.
 *
 * \param caption     Caption to print before running the example.
 * \param nvram_data  Pointer to the NVRAM data structure to use in the example.
 */

void NVMemory_readNeuron(Displayline displayLine, Network network, NVmemory nvmemory);

void NVmemory_corruptNVMData(NVmemory nvmemory);

void NVmemory_corruptTest( Displayline displayLine, NVmemory nvmemory );

void NVmemory_displayCorruptTest(Displayline displayLine, NVmemory nvmemory);

void NVmemory_readDataTable( Displayline displayLine, Neuron neuron, NVmemory nvmemory );

char NVMemory_getNeuronIDStatus();

void NVMemory_readNeuron(Displayline displayLine, Network network, NVmemory nvmemory);

void NVmemory_displayNeuronData( Displayline displayLine, NVmemory nvmemory );

void NVmemory_displayMessage1( Displayline displayLine, NVmemory nvmemory  );
void NVmemory_displayMessage2( Displayline displayLine, NVmemory nvmemory  );
void NVmemory_displayMessage3( Displayline displayLine, NVmemory nvmemory  );

void NVmemory_writeDataTable(Displayline displayLine, Neuron neuron, NVmemory nvmemory  );

#endif /* NVMEMORY_H_ */
