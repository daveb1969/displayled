// A list of dot representations of messages.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#include "board.h"
#include "dotslist.h"

#if DISPLAY_TYPE == SALOON_DISPLAY
GENERIC_LIST_IMPLEMENT(Dots, 2);
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
GENERIC_LIST_IMPLEMENT(Dots, 4);
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
GENERIC_LIST_IMPLEMENT(Dots, 4);
#else
  #error No DISPLAY_TYPE defined
#endif


// Standard C library includes.
#include "stdlib.h"

// Project includes.
#include "tfx_assert.h"

//int Counter = 0;

  /* Constructor. */
  //DotsList DotsList_construct(void);
  /* Destructor. */
  //void DotsList_destroy(DotsList list);

  /* Get the length of the list. */
  //size_t DotsList_get_length(const DotsList list);

  /* Get the item at the specified position in the list. */
  //Dots DotsList_get_item(const DotsList list, int position);

  /* Append the specified item to the list. */
  /* */
  /* N.B. The list assumes ownership of the item. */
  //void DotsList_append(DotsList list, const Dots item);


  /* "Object" structure definition. */
  //struct DotsList_str {
  //  Dots m_items[4];
  //  size_t m_length;
  //};

  /* Constructor. */ \
  //DotsList DotsList_construct(void) {
  //  DotsList list = ( struct DotsList_str* )malloc(sizeof(struct DotsList_str));
  //  TFX_ASSERT(list, "Failed to allocate memory.");
  //  list->m_length = 0;
  //  return list;
  //}
  /* Destructor. */
  //void DotsList_destroy(DotsList list) {
  //  TFX_ASSERT(list, "Expected a non-null pointer.");
    /* Delete our items. */
  //  {
  //    int i = 0;
  //    for (i = 0; i < 4; i++) {
  //      Dots item_i = DotsList_get_item(list, i);
  //      if (item_i != NULL) {
  //        Dots_destroy(item_i);
  //        list->m_items[i] = NULL;
  //      }
  //    }
  //  }
  //
    /* Delete ourself. */
  //  list->m_length = 0;
  //  free(list);
  //}

  /* Get the length of the list. */
  //size_t DotsList_get_length(const DotsList list)
  //{
  //  TFX_ASSERT(list, "Expected a non-null pointer.");
  //  return list->m_length;
  //}

  /* Get the item at the specified position in the list. */
  //Dots DotsList_get_item(const DotsList list, int position) {
  //  TFX_ASSERT(list, "Expected a non-null pointer.");
  //  return (0 <= position && position < list->m_length) ?
  //    list->m_items[position] : NULL;
  //}

  /* Append the specified item to the list. */
  /* */
  /* N.B. The list assumes ownership of the item. */
  //void DotsList_append(DotsList list, const Dots item)
  //{
  //  TFX_ASSERT(list, "Expected a non-null pointer.");

  //  list->m_items[list->m_length++] = item;
  //  TFX_ASSERT(list->m_length < 5, "Insufficient space in buffer.");
  //  Counter++;
  //}
