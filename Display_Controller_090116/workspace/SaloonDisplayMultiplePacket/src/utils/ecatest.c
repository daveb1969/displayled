/*
 * ecatest.c
 *
 *  Created on: 6 Sep 2010
 *      Author: DAVE
 */

#include "ecatest.h"
#include "optoinputs.h"
#include "tfx_assert.h"
#include "malloc.h"
#include "board.h"
#include "gpio.h"
#include "displayExtern.h"

extern char clkTick;

//! \brief ECAtest constructor
//!
//! Create ECAtest data structure off the heap.
//!
//! \return Return ecatest*
//!
ECAtest ECAtest_construct() {
	ECAtest ecatest = ( struct ecatest_str* ) malloc(sizeof(struct ecatest_str));
	TFX_ASSERT(ecatest, "Failed to allocate memory.");
	ecatest->opto_Input1 = 0;
	ecatest->opto_Input2 = 0;
	ecatest->opto_Input3 = 0;
	return ecatest;
}

//! \brief ECAtest destructor
//!
//!
//! Destroy ECAtest structure
//!
void ECAtest_destroy( ECAtest ecatest ) {
	free(ecatest);
}

void ECAtest_OptoIOStateMachine( ECAtest ecatest)
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;

	if (counter == 10)
	{
	counter++;
	ecatest->opto_Input1 = 1;
	}

	if (counter == 20)
	{
	counter++;
	ecatest->opto_Input1 = 0;
	}

	if (counter == 30)
	{
	counter++;
	ecatest->opto_Input2 = 1;
	}

	if (counter == 40)
	{
	counter++;
	ecatest->opto_Input2 = 0;
	}

	if (counter == 50)
	{
	counter++;
	counter = 0;
	}

	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}









