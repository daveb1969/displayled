/*
 * displayline.h
 *
 *  Created on: 26 Aug 2010
 *      Author: DAVE
 */

#ifndef DISPLAYLINE_H_
#define DISPLAYLINE_H_

#include "Dots.h"
#include "message.h"
#include "tfx_assert.h"
#include "gpio.h"
#include "neuron.h"

struct displayline_str
{
	unsigned char 			lineNumber;          		// Line to render on display
	volatile unsigned char	startRow;					// LED row to start rendering line y
	volatile unsigned char	stopRow;     				// LEDrow to stop rendering line on display
	volatile unsigned char	charSet;     				// character set to be used
	volatile unsigned char	prePend;					// Number of characters to prepend infront of message
	volatile unsigned char 	scrollEnable;				// Enable the display line to scroll
	volatile unsigned char 	scrollBufferIncrement;		// Increment step into buffer for scroll operation
	volatile unsigned char 	scrollRateCount;			// Scroll rate counter
	volatile unsigned char 	scrollRate;					// Scroll rate of display line
	volatile unsigned char 	rowShift;                  	// Bit map used to select correct row from column data
	volatile unsigned char 	messageComplete;	        // Flag that the text to display has completed
	volatile unsigned char 	scrollEnableChanged;      	// Change scroll enable received in packet
	volatile unsigned char 	scrollEnableReceived;     	// Scroll enable received in packet
	volatile unsigned char	displayNumberRows;        	// Number of physical rows in the display
	volatile unsigned char	displayLineNumberColumns; 	// Nnumber columns in the display
	unsigned int  			displayLineTimeout;       	// Number of seconds to display line on screen
	unsigned int  			displayLineTimerCount;    	// Timer Tick counter
	unsigned char 			displayLineTimerEnable;   	// Enable timer control of display line
	unsigned char           displayLineTimerComplete;   // Timer Complete
	volatile unsigned char 	displayLineDotsCreated;   	// Has the dots buffer be created from the current text
	unsigned char			timerTick;                	// Timer tick
	volatile int           	messageLength;            	// Message length
	volatile unsigned char 	messageChanged;           	// Has the message changed?
	volatile int           	displayPtr;               	// Pointer into Dot buffer being displayed on line
	volatile unsigned char  dotBufferSemaphore;         // Locking Semaphore
	Dots		  			DotBuffer;                	// Dot buffer used to render on display line
	Message       			TextMessageBuffer;        	// Text message buffer used for display line
	volatile unsigned int   numberTextMessages;         // Number of Text Messages Received for display line
	volatile unsigned int   indent;                     // Number of columns to indent text to be rendered
	volatile unsigned int   dotBufferOffset;            // Number of bytes to offset into Dot Buffer
};

typedef struct displayline_str* Displayline;

// Constructor.
//
//
Displayline Displayline_construct();

// Destructor.
//
//
void Displayline_destroy(Displayline displayLine);


// Get the display line number for which line will be rendered
unsigned char Displayline_get_startRow(Displayline displayline);

// Get the start render row on the display for display line
unsigned char Displayline_get_startRow(Displayline displayline);

// Get the stop render row on the display for display line
unsigned char Displayline_get_stopRow(Displayline displayline);

//Get character set to be sed on the display for display line
unsigned char Displayline_get_charSet(Displayline displayline);

//Get number of columns to prepend on display for display line
unsigned char Displayline_get_prePend(Displayline displayline);

//Get display message complete
unsigned char Displayline_get_messageComplete(Displayline displayline);

//Get the pointer to the Dot buffer used to render dots on the displayline
Dots Displayline_get_dotBuffer(Displayline displayline);


//Get the pointer to the Text Message Buffer used to store the text message to be
//rendered onto the displayline
Message Displayline_get_textMessage(Displayline displayline);

// Set the display line number for which line will be rendered
void Displayline_set_lineNumber(Displayline displayline, unsigned char lineNumber);

// Set the start render row on the display for display line
void Displayline_set_startRow(Displayline displayline, unsigned char startRow);

// Set the stop render row on the display for display line
void Displayline_set_stopRow(Displayline displayline, unsigned char stopRow );

//Set character set to be sed on the display for display line
void Displayline_set_charSet(Displayline displayline, unsigned char charSet);

//Set number of columns to prepend on display for display line
void Displayline_set_prePend(Displayline displayline, unsigned char prePend);

//Set display message complete
void Displayline_set_messageComplete(Displayline displayline, unsigned messageComplete);

//Set text message buffer
void Displayline_set_messageBuffer(Displayline displayline, const char* textMessage);

void Displayline_set_reversedMessageBuffer(Displayline displayline, const char* textMessage);

//Set scroll enable
void Displayline_set_scrollEnable(Displayline displayline, unsigned char* scrollEnable);

void Displayline_setScrollIncrement( Displayline displayline );

// Create dot representation of  text message
void Displayline_createDots(Displayline displayline);

void Displayline_renderLine(Displayline displayline, unsigned char currentRenderRow );

void Displayline_renderBlankLine(Displayline displayline, unsigned char currentRenderRow );

void Display_changeLine( Displayline displayLine, unsigned char lineMode, unsigned char messageNumber, Neuron neuron );




#endif /* DISPLAYLINE_H_ */
