// A "class" to represent messages.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#include "message.h"
#include "board.h"

// Standard C library includes.
#include "stdlib.h"
#include "string.h"
#include "compiler.h"
#include "malloc.h"

// Project includes.
#include "characterutils.h"
#include "tfx_assert.h"


// Constructor, taking message text as input.
//
// If message_text is NULL, then an empty message is created.
Message Message_construct(const char* message_text) {
  Message message = ( struct message_str* ) malloc(sizeof(struct message_str));
  TFX_ASSERT(message, "Failed to allocate memory.");
  Message_set(message, message_text);
  return message;
}

// Destructor.
void Message_destroy(Message message) {
  TFX_ASSERT(message, "Expected a non-null pointer.");
  free(message);
}

// Get the text of the message.
const char* Message_get_message_text(const Message message) {
  TFX_ASSERT(message, "Expected a non-null pointer.");
  return message->m_message_text;
}

// Get the length of the message, in characters.
size_t Message_get_length(const Message message)
{
  TFX_ASSERT(message, "Expected a non-null pointer.");
  return message->m_length;
}

// Clear the message.
void Message_clear(Message message)
{
  TFX_ASSERT(message, "Expected a non-null pointer.");
  // Don't really need to clear the memory --- we can just anull the length.
  message->m_length = 0;
}

// Set the message to the specified text.
void Message_set(Message message, const char* message_text)
{
  int length = 0;
  TFX_ASSERT(message, "Expected a non-null pointer.");
  if (message_text != NULL) {
    length = strlen(message_text);
    if (length > MESSAGE_BUFFER_SIZE)
    {
    TFX_ASSERT(length < MESSAGE_BUFFER_SIZE, "Insufficient space in buffer - MESSAGE SET.");
    }
    if ( length < MESSAGE_BUFFER_SIZE )
    {
    strcpy(message->m_message_text, message_text);
    message->m_length = length;
    }
  } else {
    Message_clear(message);
  }
}

// Append the specified character to the message.
void Message_append_char(Message message, char c) {
  TFX_ASSERT(message, "Expected a non-null pointer.");
  message->m_message_text[message->m_length++] = c;
  TFX_ASSERT(message->m_length < MESSAGE_BUFFER_SIZE,
             "Insufficient space in buffer - APPENDING CHARACTER.");
}

// Append the specified text to the message.
void Message_append_text(Message message, const char* more_text)
{
  TFX_ASSERT(message, "Expected a non-null pointer.");
  TFX_ASSERT(more_text, "Expected a non-null pointer.");
  const size_t more_text_length = strlen(more_text);
  TFX_ASSERT(message->m_length + more_text_length < MESSAGE_BUFFER_SIZE,
             "Insufficient space in buffer - APPENDING TEXT.");
  {
    int i = 0;
    for (i = 0; i < more_text_length; i++) {
      const char char_i = more_text[i];
      Message_append_char(message, char_i);
    }
  }
}

long getHeapFreeSize()
{
  long high_mark= AVR32_SRAM_SIZE;
  long low_mark = 0;
  long size ;
  void* p_mem;

  size = (high_mark + low_mark)/2;

  do
  {
    p_mem = malloc(size);
    if( p_mem != NULL)
    { // Can allocate memory
      free(p_mem);
      low_mark = size;
    }
    else
    { // Can not allocate memory
      high_mark = size;
    }

    size = (high_mark + low_mark)/2;
  }
  while( (high_mark-low_mark) >1 );

  return size;
}


