/*
 * dotmessage.c
 *
 *  Created on: 27 Aug 2010
 *      Author: DAVE
 */

#include "dotmessage.h"
#include "characterutils.h"
#include "tfx_assert.h"
#include "compiler.h"
#include "malloc.h"
#include "board.h"


// Get a dot representation of the message.
//
// The specified number of blank columns will be prepended to the message;
// use zero if this behaviour is not appropriate.
unsigned char GetDotRepresentation( Displayline displayline )
{


  TFX_ASSERT(displayline->TextMessageBuffer, "Expected a non-null pointer32.");
  TFX_ASSERT(displayline->DotBuffer, "Expected a non-null pointer33.");

  // Clear the output list.
  Dots_clear( displayline->DotBuffer );

  const size_t message_length = Message_get_length( displayline->TextMessageBuffer );

  // Prepend blank columns, as appropriate.
  // C1 SmallText   - Character set 1 selected - prepend with column of 8 rows on the display
  // C2 SmallNumber - Character set 2 selected - prepend with column of 16 rows onto the display
  // C3 LargeText   - Character set 3 selected - prepend with column of 24 rows onto the display
  // Work over the message string, one character at a time.
  {
    // Create a working buffer --- storage for the dot representation of a
    // single character.
//    Dots working_space = NULL;
//    if (working_space == NULL) {
//      working_space = Dots_construct();
//    } else {
//      Dots_clear(working_space);
//    }

    Dots_working_clear( displayline->DotBuffer );

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
    {
        volatile unsigned char charSetHeightInBytes;

        int numberBlankCols;
        {
          switch ( displayline->charSet)
          {
          case 1:
        	  charSetHeightInBytes = 1;
        	  break;
          case 2:
        	  charSetHeightInBytes = 2;
        	  break;
          case 3:
        	  charSetHeightInBytes = 3;
        	  break;
          case 4:
        	  charSetHeightInBytes = 4;
        	  break;
          default:
        	  charSetHeightInBytes = 0;
        	  break;
          }

          numberBlankCols = 96;
          //volatile int numberBlankColsToAdd;
          //numberBlankColsToAdd = displayline->displayLineNumberColumns;
          int i = 0;
          while ( i++ < numberBlankCols )
          {
            switch ( charSetHeightInBytes ){
            case 1:
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  break;
            case 2:
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  break;
            case 3:
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  break;
            case 4:
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  Dots_append_value(displayline->DotBuffer, 0x00);
          	  break;
            default:
          	  break;
            }
          }
        }


    }
#else
  {
    int i = 0;
    for (i = 0; i < displayline->prePend; i++) {
      switch ( displayline->charSet ){
      case 1:
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  break;
      case 2:
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  break;
      case 3:
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  break;
      case 4:
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  break;
      case 5:
    	  Dots_append_value(displayline->DotBuffer, 0x00);
    	  Dots_append_value(displayline->DotBuffer, 0x00);;
    	  break;
      default:
    	  break;
      }
    }
  }
#endif

#if DISPLAY_TYPE != SALOON_DISPLAY
    {
      int i;
      for (i = 0 ; i < message_length; i++) {
    	const char char_i = Message_get_message_text( displayline->TextMessageBuffer)[i];
    	//Select the correct character set to create the dot buffer from.
    	switch ( displayline->charSet )
    	{
    	case 1:
        // Get the dot representation of the current character.
    	CharUtils_get_char_dots_8_high(char_i, displayline->DotBuffer);
        break;
    	case 2:
        // Get the dot representation of the current character.
    	CharUtils_get_char_dots_12_high(char_i, displayline->DotBuffer);
        break;
    	case 3:
        // Get the dot representation of the current character.
    	CharUtils_get_char_dots_16_high(char_i, displayline->DotBuffer);
        break;
    	case 4:
        // Get the dot representation of the current character.
    	CharUtils_get_char_dots_25_high(char_i, displayline->DotBuffer);
        break;
    	default:
		// Get the dot representation of the current character.
		CharUtils_get_char_dots_12_high(char_i, displayline->DotBuffer);
		break;
        }

        Dots_append_dots(displayline->DotBuffer);
        Dots_working_clear(displayline->DotBuffer);

      }
    }
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY
    {
      int i;
      for (i = 0 ; i < message_length; i++) {
    	const char char_i = Message_get_message_text( displayline->TextMessageBuffer)[i];
    	if ( char_i > 127 )
    	{
    	continue;
    	}
    	//Select the correct character set to create the dot buffer from.
    	switch ( displayline->charSet )
    	{
    	case 1:
        // Get the dot representation of the current character.
    	CharUtils_get_char_dots_8_high(char_i, displayline->DotBuffer);
        break;
    	case 5:
        // Get the dot representation of the current character.
    	CharUtils_get_char_dots_10_high(char_i, displayline->DotBuffer);
        break;

    	default:
		// Get the dot representation of the current character.
    	CharUtils_get_char_dots_8_high(char_i, displayline->DotBuffer);
		break;
        }
        Dots_append_dots(displayline->DotBuffer);
        Dots_working_clear(displayline->DotBuffer);
      }
    }
#endif




#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
    {
      int i = 0;
      for (i = 0; i < displayline->prePend; i++) {
        switch ( displayline->charSet ){
        case 1:
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  break;
        case 2:
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  break;
        case 3:
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  break;
        case 4:
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  Dots_append_value(displayline->DotBuffer, 0x00);
      	  break;
        default:
      	  break;
        }
      }
    }
#endif
    displayline->messageLength = Dots_get_length (displayline->DotBuffer);
    // Clean up.
    //Dots_destroy(working_space);
  }
  return 1;
}

