/*
 * neuron.c
 *
 *  Created on: 1 Sep 2010
 *      Author: DAVE
 */

// Standard C library includes.
#include "stdlib.h"
#include "string.h"
#include "compiler.h"
#include "malloc.h"


// Project includes.
#include "neuron.h"
#include "tfx_assert.h"

// Constructor
//
//
Neuron Neuron_construct() {
  Neuron neuron = ( struct neuron_str* ) malloc(sizeof(struct neuron_str));
  memset( neuron, 0x00, sizeof(struct neuron_str) );
  TFX_ASSERT( neuron , "Failed to allocate memory.");

  return neuron;
}

// Destructor.
//
//
void Neuron_destroy(Neuron neuron) {
  TFX_ASSERT( neuron , "Expected a non-null pointer.");
  free(neuron);
}


