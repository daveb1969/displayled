/*
 * passengercountcomms.h
 *
 *  Created on: 10 Oct 2010
 *      Author: DAVE
 */

#ifndef PASSENGERCOUNTCOMMS_H_
#define PASSENGERCOUNTCOMMS_H_


#include "passengercounter.h"
#include "crc.h"
#include "circularbuffer.h"

#define MAX_DATA_LEN			50
#define MAX_PACKET_LEN			5+1+1+1+1+1+2+MAX_DATA_LEN+2+1
#define MAX_CRC_DIGITS       	256
#define BYTE_WIDTH            	8
#define CRC_WIDTH             	16
#define CRC_MASK              	0xFF

struct passengercountcomms_str
{
	unsigned char  SNP_displayControllerAddress;	    // SNP Address of Display Controller
	unsigned char  SNP_destinationAddress;			    // SNP Address of Recipient
	unsigned char  SNP_sourceAddress;				    // SNP Address of Sender
	unsigned short SNP_dataLength;					    // Length of message in packet
	char           SNP_totalPacketNumber;			    // Total Number of packets in transaction
	char           SNP_packetNumber;                    // Current Packet Number
	char           dataPayload[MAX_PACKET_LEN];         // Payload Data to place into the Packet
	char           receiveBuffer[MAX_PACKET_LEN]; 	    // Receive data Buffer
	char           buffer[MAX_PACKET_LEN];              // Send data Buffer
	int            sendMessageLength;                   // Send Message Length
	int            receivedMessageLength;               // Received Message Length
	Circularbuffer receiveCircularBuffer;               // Receive Circular Buffer
	volatile unsigned int  triggerRXInterruptCounter;   // Number times RX interrupt triggered
	short          calculatedCRCValue;                  // CRC calculated from SNP Packet
	short          SNP_CRCValue;                        // CRC value found in SNP Packet
};

typedef struct passengercountcomms_str* PassengerCountComms;

// Constructor.
//
//
PassengerCountComms PassengerCountComms_construct();

// Destructor.
//
//
void PassengerCountComms_destroy(PassengerCountComms passengerCountComms);

int PassengerCountComms_messageDecode(PassengerCountComms passengerCountComms, PassengerCounter passengerCounter );

int PassengerCountComms_reply(unsigned char dest,unsigned char recipient,char *buffer,char *args,unsigned int args_size);

unsigned char PassengerCountComms_send(char* buffer, unsigned char src, unsigned char dest, char *command,char *arg,int arg_size);


//int PassengerCountComms_receive(PassengerCountComms passengerCountComms);

int PassengerCountComms_buildCommand(char *buf,unsigned char src,unsigned char dest,char tpn,char pn,short datalen,char *data);

unsigned char PassengerCountComms_enableCounter(PassengerCountComms passengerCountComms, PassengerCounter passengerCounter);

unsigned char PassengerCountComms_disableCounter(PassengerCountComms passengerCountComms, PassengerCounter passengerCounter);

unsigned int PassengerCountComms_getArg(char *arg,int arg_size,int pos,int size);

int PassengerCountComms_readRS485Port(char *buf,int len);

int PassengerCountComms_writeRS485Port(char *buf,int len);

void sled_example( PassengerCountComms passengerCountComms, unsigned char ledLightLevel);

signed char PassengerCountComms_receiveCircular(PassengerCountComms passengerCountComms);


#endif /* PASSENGERCOUNTCOMMS_H_ */
