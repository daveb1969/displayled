/*! \file ******************************************************************
 *
 * \brief circularbuffer.c 		Circular Buffer.
 *
 * This file creates and provides services for a Circular Ring Buffer object.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The Circular buffer data structure is used as a data storage object.
 *	In the APIS display code it is used in the process of sending and in receiving
 *	data over the SPI channel between the Echelon Neuron and UC3B0256 uController
 *	upon the display controller board.
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/


#include "circularbuffer.h"
#include "tfx_assert.h"

// Standard C library includes.
#include "stdlib.h"
#include "string.h"



//! \brief Circularbuffer constructor
//!
//! Create Circularbuffer data structure off the heap.
//!
//! Initialise Circular Buffer - Number of Elements Circular buffer will hold.
//!
//! Initialise Circular Buffer - Size of individual Element - Unit Byte(s)
//!
//! \param[in]  capacity - Number of storage elements in Circular Buffer
//! \param[in]  sz		- Size in bytes of an element in Circular Buffer
//!
//! \return Return Circularbuffer*
//!

Circularbuffer cb_construct(size_t capacity, size_t sz)
{
	Circularbuffer circularbuffer = ( struct circular_buffer* ) malloc(sizeof(struct circular_buffer));
	TFX_ASSERT(circularbuffer, "Failed to allocate memory.");
	circularbuffer->buffer = malloc(capacity * sz);
	TFX_ASSERT(circularbuffer->buffer, "Failed to allocate memory.");
    circularbuffer->buffer_end = (char *)circularbuffer->buffer + capacity * sz;
    circularbuffer->capacity = capacity;
    circularbuffer->count = 0;
    circularbuffer->sz = sz;
    circularbuffer->head = circularbuffer->buffer;
    circularbuffer->tail = circularbuffer->buffer;
    return circularbuffer;
}

/*
Circularbuffer cb_construct()
{
	Circularbuffer circularbuffer = ( struct circular_buffer* ) malloc(sizeof(struct circular_buffer));
	TFX_ASSERT(circularbuffer, "Failed to allocate memory.");
	return circularbuffer;
}
*/

/*
int cb_init(Circularbuffer cb, size_t capacity, size_t sz)
{
    cb->buffer = malloc(capacity * sz);
    if(cb->buffer == NULL)
    {
      //print_dbg("Failed to allocate memory for circular buffer.\n");
      return 0;
    }
    cb->buffer_end = (char *)cb->buffer + capacity * sz;
    cb->capacity = capacity;
    cb->count = 0;
    cb->sz = sz;
    cb->head = cb->buffer;
    cb->tail = cb->buffer;
    return 1;
}
*/



//! \brief Circularbuffer destructor
//!
//! Removes Circularbuffer data structure off the heap.
//!
//! \param[in] cb	- address of Circularbuffer structure to destroy
void cb_free(Circularbuffer cb)
{
    free(cb->buffer);
}



//! \brief Push Item onto the Circular Buffer
//!
//! \param[in] cb		- address of Circularbuffer structure
//! \param[in] item		- address of item to push onto Circular Buffer
//!
//! \return Returns 1 if successfully push item onto Circular buffer, else return 0.
int cb_push_back(Circularbuffer cb, const void *item)
{
    if(cb->count == cb->capacity)
    {
      //print_dbg("Reached capacity of circular buffer.\n");
      return 0;
    }
    memcpy(cb->head, item, cb->sz);
    cb->head = (char*)cb->head + cb->sz;
    if(cb->head == cb->buffer_end)
        cb->head = cb->buffer;
    cb->count++;
    return 1;
}


//! \brief Pop Item off the Circular Buffer
//!
//!
//! \param[in] cb		- address of Circularbuffer structure
//! \param[in] item		- address of where to store item popped from Circular Buffer
//!
//! \return Returns 1 if successfully pushed item onto Circular buffer, else return 0.

int  cb_pop_front(Circularbuffer cb, void *item)
{
    if(cb->count == 0)
    {
      //print_dbg("Buffer empty of data.\n");
      return 0 ;
    }
    memcpy(item, cb->tail, cb->sz);
    cb->tail = (char*)cb->tail + cb->sz;
    if(cb->tail == cb->buffer_end)
        cb->tail = cb->buffer;
    cb->count--;
    return 1;
}
