/*
 * opto.c
 *
 *  Created on: 8 Sep 2010
 *      Author: DAVE
 */

#include "opto.h"
#include "gpio.h"
#include "tfx_assert.h"
#include "board.h"



// Constructor.
//
//
Opto Opto_construct() {
	Opto opto = ( struct opto_str* ) malloc(sizeof(struct opto_str));
	TFX_ASSERT(opto, "Failed to allocate memory.");
	opto->optoInput = 0;
	opto->optoTimeout = 0;
	opto->optoTimerCount =0;
	opto->optoTimerEnable = 0;
	opto->optoTimerTick = 0;
	opto->edge_detect = 0;
	opto->optoChangedCounter = 0;
	return opto;
}

// Destructor.
void Opto_destroy(Opto opto) {
	TFX_ASSERT(opto, "Expected a non-null pointer.");
	free(opto);
}


