/*
 * packet.h
 *
 *  Created on: 26 Aug 2010
 *      Author: DAVE
 */

#ifndef PACKET_H_
#define PACKET_H_

#include "TFX-SPI.h"

// "Object" structure definition.
struct datagram_str
{
	unsigned char  messageLength;			// Length of message
	unsigned char  messageType;     		// Message Type - 0x03 SlaveMasterTransfer
	unsigned char  messageSubType;          // Message Sub Type - 0x02 Saloon Display,
	                                        //                  - 0x01 Destination Display
	unsigned char  transactionID;     		// Unique packet identifier
	unsigned char  numberOfFrames;			// Number of frames in transaction
	unsigned char  frameNumber;				// Number of this frame
	char  		   dataContent[MAX_SPI_LEN];// Data content in this frame
	unsigned short crc;						// CRC for this frame
};


typedef struct datagram_str* Datagram;

// Constructor.
//
//
Datagram Datagram_construct();

// Destructor.
//
//
void Datagram_destroy(Datagram datagram);

// Get the text of the message.
const char* Datagram_get_payload(const Datagram datagram);

// Get the transactionID.
unsigned char  Datagram_get_transactionID(const Datagram datagram);


// Get the numberOfFrames.
unsigned char  Datagram_get_numberOfFrames(const Datagram datagram);


// Get the frameNumber.
unsigned char  Datagram_get_frameNumber(const Datagram datagram);


// Set the Datagram payload to the specified text.
void Datagram_set_payload(Datagram datagram, const char* payload, unsigned char length);

// Set the transactionID.
void Datagram_set_transactionID(Datagram datagram, unsigned char transactionID);


// Set the numberOfFrames.
void Datagram_set_numberOfFrames(Datagram datagram, unsigned char numberOfFrames);


// Set the frameNumber.
void Datagram_set_frameNumber(Datagram datagram, unsigned char frameNumber);

// Set the messageSubType
void Datagram_set_messageSubType(Datagram datagram, unsigned char messageSubType);

// Set the messageLength.
void Datagram_set_messageLength(Datagram datagram, unsigned char messageLength);

// Set the messageType.
void Datagram_set_messageType(Datagram datagram, unsigned char messageType);

// Clear Datagram
void Datagram_clear( Datagram datagram );

//Make a copy of one datagram into another
void Datagram_copy( Datagram datagram1, Datagram datagram2,
		             unsigned char transactionIDIndex, unsigned char crcEnabledPacket);


#endif /* PACKET_H_ */
