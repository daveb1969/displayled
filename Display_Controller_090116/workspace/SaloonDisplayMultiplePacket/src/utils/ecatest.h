/*
 * ecatest.h
 *
 *  Created on: 6 Sep 2010
 *      Author: DAVE
 */

#ifndef ECATEST_H_
#define ECATEST_H_

#include "displayline.h"
#include "neuron.h"
#include "debug.h"


struct ecatest_str
{
	volatile int opto_Input1;
	volatile int opto_Input2;
	volatile int opto_Input3;
};

typedef struct ecatest_str* ECAtest;

// Constructor.
//
//
ECAtest ECAtest_construct();

// Destructor.
//
//
void ECAtest_destroy( ECAtest ecatest );

void ECAtest_OptoIOStateMachine( ECAtest ecatest);


#endif /* ECATEST_H_ */
