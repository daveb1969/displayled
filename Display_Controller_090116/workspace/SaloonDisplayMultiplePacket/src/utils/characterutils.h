// Utilities for working with characters and their dot representations.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef CHARACTERUTILS_H_
#define CHARACTERUTILS_H_

// Project includes.
#include "message.h"

enum CharSet{
SmallText = 1,
LargeText,
SmallNumber
} characterSet;


// Get a dot representation of the specified character.
void CharUtils_get_char_dots_16_high(
  int input_char,
  Dots char_dots
);

// Get a dot representation of the specified character.
void CharUtils_get_char_dots_12_high(
  int input_char,
  Dots char_dots
);


// Get a dot representation of the specified character.
void CharUtils_get_char_dots_8_high(
  int input_char,
  Dots char_dots
);


// Get a dot representation of the specified character.
void CharUtils_get_char_dots_25_high(
  int input_char,
  Dots char_dots
);


// Get a dot representation of the specified character.
void CharUtils_get_char_dots_10_high(
  int input_char,
  Dots char_dots
);


// Trim leading and trailing "blanks" from the specified dot representation.
void CharUtils_trim_dots(
  Dots char_dots,
  enum CharSet
);





#endif /* CHARACTERUTILS_H_ */
