/*! \file ******************************************************************
 *
 * \brief displayline.c 		Display Line.
 *
 * This file creates and provides services for a Display Line object.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The display line object provides an interface to the controlling display
 *	object to be able to render a particular line onto the LED display.
 *	The display line structure contains lower level data structures that
 *	contain all the necessary data required to place a message onto to
 *	the LED display.
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#include "displayline.h"
#include "dotmessage.h"
#include "tfx_assert.h"
#include "debug.h"
#include "gpio.h"
#include "board.h"
#include "string.h"
#include "network.h"
#include "tc.h"
#include "passengercount.h"
#include "displayExtern.h"


extern char clkTick;

//TLC5923 LED Driver control signals
#	define XERR           AVR32_PIN_PB02 //
#   define SCLK           AVR32_PIN_PB10
#   define XLAT           AVR32_PIN_PB03
#   define MODE           AVR32_PIN_PA05
#   define SIN            AVR32_PIN_PB09 //
#   define BLANK          AVR32_PIN_PA29 // LOW - Normal, HIGH Disabled
#   define SOUT           AVR32_PIN_PB11 //

// LED Row Enables
// Dealing with row 1 to row 25.
// Used to select which of the possible 25 rows of LED's to turn on.
// The required row for display is selected by use of a 4 bit code
// The 4 bit code placed into ROWADD0, ROWADD1, ROWADD2, ROWADD4.
// Toggling the ROW_REN output clocks the required row
// select code into the Row Selector IC.

//#   define ROWADD0        AVR32_PIN_PB08
//#   define ROWADD1        AVR32_PIN_PB07
//#   define ROWADD2        AVR32_PIN_PB06
//#   define ROWADD3        AVR32_PIN_PA07
//#   define ROWADD4        AVR32_PIN_PA13
//#   define ROW_REN        AVR32_PIN_PA06

// LED Dot Correction Brightness parameters
//#   define MAX_BRIGHTNESS 			5
//#   define DOT_CORRECT_NUMBER_WORDS	16
//#   define BRIGHTNESS_LEVEL 	    5
//#   define LED_DRIVERS_IN_CHAIN 	10
//#   define ROW_OFFSET				5
//#   define DIMMED_BRIGHTNESS		5
//#   define FULL_BRIGHTNESS			5

#define TC_CHANNEL0    0
#define TC_CHANNEL1    1

//! \brief Displayline constructor
//!
//! Create Displayline data structure off the heap.
//!
//! Construct a new DotBuffer
//! Construct a new TextMessage Buffer.
//!
//! \return Return Displayline*
//!


Displayline Displayline_construct() {
	Displayline displayline = ( struct displayline_str* ) malloc(sizeof(struct displayline_str));
	TFX_ASSERT(displayline, "Failed to allocate memory.");

	displayline->DotBuffer = Dots_construct();
	displayline->TextMessageBuffer = Message_construct(NULL);
	displayline->numberTextMessages = 0;
	displayline->indent = 0;
	//Displayline_clear( displayline );
	return displayline;
}

//! \brief Displayline destructor
//!
//!
//! Destroy DotBuffer
//! Destroy TextMessage Buffer.
//! Remove Displayline data structure off the heap.
//!
void Displayline_destroy(Displayline displayline) {
	TFX_ASSERT(displayline, "Expected a non-null pointer10.");
	Dots_destroy(displayline->DotBuffer);
	Message_destroy(displayline->TextMessageBuffer);
	free(displayline);
}




// Get the display line number for which line will be rendered
unsigned char Displayline_get_lineNumber(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer11.");
	return displayline->lineNumber;
}

// Get the start render row on the display for display line
unsigned char Displayline_get_startRow(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer12.");
	return displayline->startRow;
}

// Get the stop render row on the display for display line
unsigned char Displayline_get_stopRow(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer13.");
	return displayline->stopRow;
}

//Get character set to be sed on the display for display line
unsigned char Displayline_get_charSet(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer14.");
	return displayline->charSet;
}

//Get number of columns to prepend on display for display line
unsigned char Displayline_get_prePend(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer15.");
	return displayline->prePend;
}

//Get display message complete
unsigned char Displayline_get_messageComplete(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer16.");
	return displayline->messageComplete;
}

//Get the pointer to the Dot buffer used to render dots on the displayline
Dots Displayline_get_dotBuffer(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer17.");
	return displayline->DotBuffer;
}


//Get the pointer to the Text Message Buffer used to store the text message to be
//rendered onto the displayline
Message Displayline_get_textMessage(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer18.");
	return displayline->TextMessageBuffer;
}


// Set the display line number for which line will be rendered
void Displayline_set_lineNumber(Displayline displayline, unsigned char lineNumber)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer19.");
	displayline->lineNumber = lineNumber;
}

// Set the start render row on the display for display line
void Displayline_set_startRow(Displayline displayline, unsigned char startRow)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer20.");
	displayline->startRow = startRow;
}

// Set the stop render row on the display for display line
void Displayline_set_stopRow(Displayline displayline, unsigned char stopRow )
{
	TFX_ASSERT(displayline, "Expected a non-null pointer21.");
	displayline->stopRow = stopRow;
}

//Set character set to be sed on the display for display line
void Displayline_set_charSet(Displayline displayline, unsigned char charSet)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer22.");
	displayline->charSet = charSet;
}

//Set number of columns to prepend on display for display line
void Displayline_set_prePend(Displayline displayline, unsigned char prePend)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer23.");
	displayline->prePend = prePend;
}

//Set display message complete
void Displayline_set_messageComplete(Displayline displayline, unsigned messageComplete)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer24.");
	displayline->messageComplete = messageComplete;
}

//Set text message buffer
void Displayline_set_messageBuffer(Displayline displayline, const char* textMessage)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer25.");
	TFX_ASSERT(textMessage, "Expected a non-null pointer26.");
	Message_clear(displayline->TextMessageBuffer);
	Message_set( displayline->TextMessageBuffer, textMessage );
}

//Set text message buffer
void Displayline_set_reversedMessageBuffer(Displayline displayline, const char* textMessage)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer27.");
	TFX_ASSERT(textMessage, "Expected a non-null pointer28.");
	char reversedTextMessage[224];
	int i1 = 0;
	int i2 = 0;
	unsigned char endIndexOfTextContent = strlen(textMessage);
	//TFX_ASSERT( (endIndexOfTextContent < 140) , "Text Message too long for buffer space");
	if ( endIndexOfTextContent > 140)
	{
		endIndexOfTextContent = 140;
	}
	//Reversing the text message before sending for the dot information
	//Insert a zero at the end of the string obtained from the message packet received.
	for(i1 = (endIndexOfTextContent-1) , i2 = 0 ; i1 >= 0 ; i1--, i2++)
	{
		reversedTextMessage[i2] = textMessage[i1];
	}
	reversedTextMessage[i2] = 0x00;
	Message_clear(displayline->TextMessageBuffer);
	Message_set( displayline->TextMessageBuffer, reversedTextMessage );
}


//Set scroll enable
void Displayline_set_scrollEnable(Displayline displayline, unsigned char* scrollEnable)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer29.");
	TFX_ASSERT(scrollEnable, "Expected a non-null pointer30.");
	if ( *scrollEnable == 1 )
	{
		displayline->scrollEnableChanged = 1;
		displayline->scrollEnableReceived = 1;
		//displayline->scrollEnable = 1;
		return;
	}
	else
	{
		displayline->scrollEnableChanged = 1;
		displayline->scrollEnableReceived = 0;
		//displayline->scrollEnable = 0;
	}
 return;
}

// Create dot representation of  text message
// Added functionality to test for the '~' character to change operational mode.
void Displayline_createDots(Displayline displayline)
{
	TFX_ASSERT(displayline, "Expected a non-null pointer31.");
	GetDotRepresentation( displayline );
	displayline->displayLineDotsCreated = 1;
}


#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY

void Displayline_centerIndent(Displayline displayline)
{
	  int numberDotColumnsInDotBuffer = 0;

	  displayline->messageLength = Dots_get_length( displayline->DotBuffer);

	  if ( displayline->charSet == 1 )
	  {
		  numberDotColumnsInDotBuffer = displayline->messageLength / 1;

	  }
	  if ( displayline->charSet == 2 )
	  {
		  //For a reversed display the dot buffer holds 96 columns of blank columns as well as the text to be rendered.
		  //The dot buffer thus holds 96 * number of bytes required per vertical column for font.
		  //As it so happens the number of bytes required to store one vertical column of dot data for a
		  //character is the same number as was used to represent the charset number.
		  //IE we require 2 bytes of column data for each vertical for characterset 2.
		  //We want to know what part of the buffer created is actual text.
		  //Actual text data found within the buffer is present for :
		  // displayline->messageLength - number bytes of additional blank data
		  // Actual Text Buffer usage = displayline->messageLength - (96 * displayLine->charSet) bytes
		  numberDotColumnsInDotBuffer = ( displayline->messageLength - (displayline->displayLineNumberColumns * displayline->charSet ) ) / 2;

	  }
	  if ( displayline->charSet == 3 )
	  {
		  numberDotColumnsInDotBuffer = displayline->messageLength / 3;

	  }
	  if ( displayline->charSet == 4 )
	  {
		  numberDotColumnsInDotBuffer = displayline->messageLength / 4;

	  }

	  //Center of the display is at column displayline->displayLineNumberColumns/2.
	  //We thus require an indent of number cols to center of screen less half number cols to render the text
	  //to be rendered.
	  displayline->indent = displayline->displayLineNumberColumns/2 - numberDotColumnsInDotBuffer/2;

	  if ( displayline->indent < 0 )
	  {
		  displayline->indent = 10;
	  }
}

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY

void Displayline_centerIndent(Displayline displayline)
{
	  int numberDotColumnsInDotBuffer = 0;

	  if ( displayline->charSet == 1 )
	  {
		  numberDotColumnsInDotBuffer = displayline->messageLength / 1;

	  }
	  if ( displayline->charSet == 2 )
	  {
		  numberDotColumnsInDotBuffer = ( displayline->messageLength ) / 2;

	  }
	  if ( displayline->charSet == 3 )
	  {
		  numberDotColumnsInDotBuffer = displayline->messageLength / 3;

	  }
	  if ( displayline->charSet == 4 )
	  {
		  numberDotColumnsInDotBuffer = displayline->messageLength / 4;

	  }

	  //Center of the display is at column displayline->displayLineNumberColumns/2.
	  //We thus require an indent of number cols to center of screen less half number cols to render the text
	  //to be rendered.
	  if (  numberDotColumnsInDotBuffer > displayline->displayLineNumberColumns )
	  {
		  displayline->indent = 0;
		  displayline->dotBufferOffset = ( numberDotColumnsInDotBuffer / 2 -
				                            displayline->displayLineNumberColumns/2 ) * displayline->charSet;

	  }
	  else
	  {
		  displayline->indent = displayline->displayLineNumberColumns/2 - numberDotColumnsInDotBuffer/2;
		  displayline->dotBufferOffset = 0;
	  }

}
#else
#error DISPLAY_TYPE not specified
#endif

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY

void Displayline_renderLine(Displayline displayline, unsigned char currentRenderRow )
{
	  static int fontRowToRender;
	  int dataByte1Index;
	  int dataByte2Index;
	  int dataByte3Index;
	  int dataByte4Index;
	  char dotRenderByte= 0x00;
	  int displayColumn;

	  if (displayline->dotBufferSemaphore == 0 )
	  {
		  displayline->dotBufferSemaphore = 1;

	  displayline->messageLength = Dots_get_length( displayline->DotBuffer);


	  if ( displayline->messageChanged && displayline->displayLineDotsCreated == 1)
	  {
		  //displayline->displayPtr = (displayline->messageLength - 96 * displayline->charSet);
		  //displayline->displayPtr = (displayline->messageLength - 192);
		  displayline->messageComplete = 0;
		  displayline->messageChanged = 0;
		  displayline->displayLineDotsCreated = 0;
		  displayline->displayLineTimerCount = 0;
		  if ( thisDisplay->operationalMode == OperationalModeStandardCentered )
		  {
		  Displayline_centerIndent( displayline );
		  }
		  else
		  {
			  displayline->indent = 0;
		  }
		  displayline->displayPtr = (displayline->messageLength - ( 96 - displayline->indent) * displayline->charSet);

	  }

	  //displayline->messageLength = Dots_get_length( displayline->DotBuffer );
	  // Before we render a row of dots onto the display
	  // reset the pointers to the data bytes to be retrieved from
	  // the dot buffer

	  dataByte1Index = 0;
	  dataByte2Index = 1;
	  dataByte3Index = 2;
	  dataByte4Index = 3;

	  if (currentRenderRow <= displayline->startRow || currentRenderRow > displayline->stopRow)
	  {
	  //When not rendering a character from the Dot Buffer Reset the rowShift counter.
	  displayline->rowShift = 0;
	  dotRenderByte = 0;
	 }

	  if (currentRenderRow >= displayline->startRow && currentRenderRow <= displayline->stopRow)
	  {
		  fontRowToRender = currentRenderRow - displayline->startRow;

		  // Large Text Display Area.
		  // Clock out 96 columns of dot information for the current renderRow.

		  for (displayColumn = 0; displayColumn < displayline->displayLineNumberColumns; displayColumn++ ){

		  if ( displayline->charSet == 1 )
		  {
              if (displayColumn > (displayline->displayLineNumberColumns - displayline->indent ) )
              {
             	  dotRenderByte = 0x00;
              }
              else
              {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = Dots_get_value(displayline->DotBuffer,
														  (dataByte1Index) + displayline->displayPtr);
				  }
              }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				  if ( displayColumn <= (displayline->displayLineNumberColumns - displayline->indent) )
				  {
				   //Increment the Dots Buffer Bytes Indices to point to the next
				   //byte of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 1;
				  }
			  }

			  if ( displayline->charSet == 2 )
			  {
	              if (displayColumn > (displayline->displayLineNumberColumns - displayline->indent) )
	              {
	             	  dotRenderByte = 0x00;
	              }
	              else
	              {

					  // if fontRowToRender is between 1 and 8 then retrieve
					  // the dots information from the buffer for the first
					  // 8 rows of the character to be rendered for the current column
					  if (fontRowToRender >= 0 && fontRowToRender < 8){

					  //Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte1Index) + displayline->displayPtr);
					  }

					  // if the fontRowToRender is between 9 and 16
					  if (fontRowToRender >= 8 && fontRowToRender < 12)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte2Index) + displayline->displayPtr);
					  }
	              }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				  if ( displayColumn <= ( displayline->displayLineNumberColumns - displayline->indent) )
				  {
				   //Increment the Dots Buffer Bytes Indices to point to the next two
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 2;
				   dataByte2Index  = dataByte2Index + 2;
				  }
			  }
			  if ( displayline->charSet == 3 )
			  {
	              if (displayColumn > (displayline->displayLineNumberColumns - displayline->indent) )
	              {
	             	  dotRenderByte = 0x00;
	              }
	              else
	              {
					  // if fontRowToRender is between 1 and 8 then retrieve
					  // the dots information from the buffer for the first
					  // 8 rows of the character to be rendered for the current column
					  if (fontRowToRender >= 0 && fontRowToRender < 8){

					  //Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte1Index) + displayline->displayPtr);
					  }

					  // if the fontRowToRender is between 9 and 16
					  if (fontRowToRender >= 8 && fontRowToRender < 16)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte2Index) + displayline->displayPtr);
					  }
					  // if the fontRowToRender is between 9 and 16
					  if (fontRowToRender >= 16 && fontRowToRender < 20)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte3Index) + displayline->displayPtr);
					  }

	              }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);


				  if ( displayColumn <= (displayline->displayLineNumberColumns - displayline->indent) )
				  {
				   //Increment the Dots Buffer Bytes Indices to point to the next three
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 3;
				   dataByte2Index  = dataByte2Index + 3;
				   dataByte3Index  = dataByte3Index + 3;
				  }
			   }
			  if ( displayline->charSet == 4 )
			  {
	              if (displayColumn > (displayline->displayLineNumberColumns - displayline->indent) )
	              {
	             	  dotRenderByte = 0x00;
	              }
	              else
	              {

					  // if fontRowToRender is between 1 and 8 then retrieve
					  // the dots information from the buffer for the first
					  // 8 rows of the character to be rendered for the current column
					  if (fontRowToRender >= 0 && fontRowToRender < 8){

					  //Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte1Index) + displayline->displayPtr);
					  }

					  // if the fontRowToRender is between 8 and 15
					  if (fontRowToRender >= 8 && fontRowToRender < 16)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte2Index) + displayline->displayPtr);
					  }
					  // if the fontRowToRender is between 16 and 23
					  if (fontRowToRender >= 16 && fontRowToRender < 24)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte3Index) + displayline->displayPtr);
					  }
					  // if the fontRowToRender is between 24 and 25
					  if (fontRowToRender >= 24 && fontRowToRender < 25 )
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte4Index) + displayline->displayPtr);
					  }
	              }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);


				  if ( displayColumn <= (displayline->displayLineNumberColumns - displayline->indent) )
				  {
				   //Increment the Dots Buffer Bytes Indices to point to the next four
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 4;
				   dataByte2Index  = dataByte2Index + 4;
				   dataByte3Index  = dataByte3Index + 4;
				   dataByte4Index  = dataByte4Index + 4;
				  }
			   }
		      }// end of for loop

		      // Increment the rowShift count so as to plot the next
		      // row of dots when the render routine is next called
			  displayline->rowShift++;
			  if (displayline->rowShift > 7)
			  {
				  displayline->rowShift = 0;
			  }

	  }

	  if ( currentRenderRow >= displayline->displayNumberRows && displayline->scrollEnable ) {
	     displayline->scrollRateCount++;
	    // Speed control of display.
	    if (displayline->scrollRateCount > displayline->scrollRate) {
	          displayline->displayPtr = displayline->displayPtr - displayline->scrollBufferIncrement;
	          displayline->scrollRateCount = 0;
	    	  }
		  if (displayline->displayPtr == 0 ) {
			  //displayline->displayPtr = (displayline->messageLength - 192);
			  displayline->displayPtr = (displayline->messageLength - 96 * displayline->charSet);
			  if (displayline->messageChanged == 1)
			  {
			  Dots_clear(displayline->DotBuffer);
			  displayline->messageComplete = 1;
			  }
	      }
	    }

	  if ( displayline->scrollEnable == 0 && displayline->displayLineTimerEnable == 1)
	  {
		  if (displayline->displayLineTimerCount < displayline->displayLineTimeout \
				  && displayline->timerTick != clkTick )
		  {
		  displayline->displayLineTimerCount++;
		  displayline->timerTick = clkTick;
		  }

		  if ( displayline->displayLineTimerCount >= displayline->displayLineTimeout \
				  && currentRenderRow >= displayline->displayNumberRows)
		  {
			  //displayline->displayPtr = (displayline->messageLength - 192);
			  displayline->displayPtr = (displayline->messageLength - 96 * displayline->charSet);
			  displayline->displayLineTimerCount = 0;
		  }
	  }
	  displayline->dotBufferSemaphore = 0;
	  }

	  if (displayline->dotBufferSemaphore == 1)
	  {
		  Displayline_renderBlankLine( displayline, currentRenderRow);
	  }

}


#else

void Displayline_renderLine( Displayline displayline, unsigned char currentRenderRow )
{
	  static int fontRowToRender;
	  int dataByte1Index;
	  int dataByte2Index;
	  int dataByte3Index;
	  int dataByte4Index;
	  char dotRenderByte= 0x00;
	  int displayColumn;

	  //displayline->messageLength = Dots_get_length( displayline->DotBuffer);

	  if ( (displayline->messageChanged == 1) && (displayline->displayLineDotsCreated == 1) )
	  {
		  displayline->displayPtr = 0;
		  displayline->messageComplete = 0;
		  displayline->messageChanged = 0;
		  displayline->displayLineDotsCreated = 0;
		  displayline->displayLineTimerCount = 0;
		  displayline->dotBufferOffset = 0;
		  if ( thisDisplay->operationalMode == OperationalModeStandardCentered )
		  {
		      Displayline_centerIndent( displayline );
		  }
		  else
		  {
			  displayline->indent = 0;
		  }
	  }

	  // Before we render a row of dots onto the display
	  // reset the pointers to the data bytes to be retrieved from
	  // the dot buffer

	  dataByte1Index = 0 + displayline->dotBufferOffset;
	  dataByte2Index = 1 + displayline->dotBufferOffset;
	  dataByte3Index = 2 + displayline->dotBufferOffset;
	  dataByte4Index = 3 + displayline->dotBufferOffset;

	  if (currentRenderRow <= displayline->startRow || currentRenderRow > displayline->stopRow)
	  {
	  //When not rendering a character from the Dot Buffer Reset the rowShift counter.
	  displayline->rowShift = 0;
	  dotRenderByte = 0;
	  }

	  if (currentRenderRow >= displayline->startRow && currentRenderRow <= displayline->stopRow)
	  {
		  fontRowToRender = currentRenderRow - displayline->startRow;

		  // Large Text Display Area.
		  // Clock out 96 columns of dot information for the current renderRow.

		  for (displayColumn = 0; displayColumn < displayline->displayLineNumberColumns; displayColumn++ ){
		  if ( displayline->charSet == 1 )
		  {
              if (displayColumn < displayline->indent )
              {
             	  dotRenderByte = 0x00;
              }
              else
              {
				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = Dots_get_value(displayline->DotBuffer,
														  (dataByte1Index) + displayline->displayPtr);
				  }
              }
			   // Render to the screen the current row information from the column
			   // byte I have just retrieved from the buffer.
			   dotRenderByte <<= (displayline->rowShift);
			   if (dotRenderByte & 0x80) {
				 gpio_set_gpio_pin(SIN);
			   } else {
				 gpio_clr_gpio_pin(SIN);
			   }

			   // Clock data into On/OFF Input Shift Register on rising edge.
			   gpio_clr_gpio_pin(SCLK);
			   gpio_set_gpio_pin(SCLK);
			   gpio_clr_gpio_pin(SCLK);

			   if (displayColumn >= displayline->indent )
			   {
			   //Increment the Dots Buffer Bytes Indices to point to the next two
			   //bytes of Dot information for the next column
			   dataByte1Index  = dataByte1Index + 1;
			   }
		}

		if ( displayline->charSet == 2 )
		{
                  if (displayColumn < displayline->indent )
                  {
                	  dotRenderByte = 0x00;
                  }
                  else
                  {
					  // if fontRowToRender is between 1 and 8 then retrieve
					  // the dots information from the buffer for the first
					  // 8 rows of the character to be rendered for the current column
					  if (fontRowToRender >= 0 && fontRowToRender < 8){

					  //Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte1Index) + displayline->displayPtr);
					  }

					  // if the fontRowToRender is between 9 and 16
					  if (fontRowToRender >= 8 && fontRowToRender < 12)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte2Index) + displayline->displayPtr);
					  }
                  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   if ( displayColumn >= displayline->indent )
				   {
				   //Increment the Dots Buffer Bytes Indices to point to the next two
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 2;
				   dataByte2Index  = dataByte2Index + 2;
				   }
		}
		if ( displayline->charSet == 3 )
		{
                  if ( displayColumn < displayline->indent )
                  {
                	  dotRenderByte = 0x00;
                  }
                  else
                  {

					  // if fontRowToRender is between 1 and 8 then retrieve
					  // the dots information from the buffer for the first
					  // 8 rows of the character to be rendered for the current column
					  if (fontRowToRender >= 0 && fontRowToRender < 8){

					  //Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte1Index) + displayline->displayPtr);
					  }

					  // if the fontRowToRender is between 9 and 16
					  if (fontRowToRender >= 8 && fontRowToRender < 16)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte2Index) + displayline->displayPtr);
					  }
					  // if the fontRowToRender is between 9 and 16
					  if (fontRowToRender >= 16 && fontRowToRender < 20)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte3Index) + displayline->displayPtr);
					  }
                  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);


				   if ( displayColumn >= displayline->indent )
				   {
				   //Increment the Dots Buffer Bytes Indices to point to the next three
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 3;
				   dataByte2Index  = dataByte2Index + 3;
				   dataByte3Index  = dataByte3Index + 3;
				   }

		}

		if ( displayline->charSet == 4 )
		{
                  if ( displayColumn < displayline->indent )
                  {
                	  dotRenderByte = 0x00;
                  }
                  else
					  {
					  // if fontRowToRender is between 1 and 8 then retrieve
					  // the dots information from the buffer for the first
					  // 8 rows of the character to be rendered for the current column
					  if (fontRowToRender >= 0 && fontRowToRender < 8){

					  //Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte1Index) + displayline->displayPtr);
					  }

					  // if the fontRowToRender is between 8 and 15
					  if (fontRowToRender >= 8 && fontRowToRender < 16)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte2Index) + displayline->displayPtr);
					  }
					  // if the fontRowToRender is between 16 and 23
					  if (fontRowToRender >= 16 && fontRowToRender < 24)
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte3Index) + displayline->displayPtr);
					  }
					  // if the fontRowToRender is between 24 and 25
					  if (fontRowToRender >= 24 && fontRowToRender < 25 )
					  {
					  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = Dots_get_value(displayline->DotBuffer,
															  (dataByte4Index) + displayline->displayPtr);
					  }
                  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }


				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   if ( displayColumn >= displayline->indent )
				   {
					   //Increment the Dots Buffer Bytes Indices to point to the next four
					   //bytes of Dot information for the next column
					   dataByte1Index  = dataByte1Index + 4;
					   dataByte2Index  = dataByte2Index + 4;
					   dataByte3Index  = dataByte3Index + 4;
					   dataByte4Index  = dataByte4Index + 4;
				   }
		}

		if ( displayline->charSet == 5 )
		{
            if ( displayColumn < displayline->indent )
            {
          	  dotRenderByte = 0x00;
            }
            else
            {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = Dots_get_value(displayline->DotBuffer,
														  (dataByte1Index) + displayline->displayPtr);
				  }

				  // if the fontRowToRender is between 9 and 10
				  if (fontRowToRender >= 8 && fontRowToRender < 10)
				  {
				  // Retrieve 8 rows of dot information for the current column
				  dotRenderByte = Dots_get_value(displayline->DotBuffer,
														  (dataByte2Index) + displayline->displayPtr);
				  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   if ( displayColumn >= displayline->indent )
				   {
				   //Increment the Dots Buffer Bytes Indices to point to the next two
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 2;
				   dataByte2Index  = dataByte2Index + 2;
				   }
			  }
		   }
		}// end of for loop

		      // Increment the rowShift count so as to plot the next
		      // row of dots when the render routine is next called
			  displayline->rowShift++;
			  if (displayline->rowShift > 7)
			  {
				  displayline->rowShift = 0;
			  }
	  }

	  if ( currentRenderRow >= displayline->displayNumberRows && displayline->scrollEnable ) {
	     displayline->scrollRateCount++;
	    // Speed control of display.
	    if (displayline->scrollRateCount > displayline->scrollRate) {
	          displayline->displayPtr = displayline->displayPtr + displayline->scrollBufferIncrement;
	          displayline->scrollRateCount = 0;
	          displayline->messageComplete = 0;
	    	  }
		  if (displayline->displayPtr > displayline->messageLength) {
			  displayline->displayPtr = 0;
			  //DB added
			  displayline->messageComplete = 1;
			  if (displayline->messageChanged == 1)
			  {
			  Dots_clear(displayline->DotBuffer);
			  displayline->messageComplete = 1;
			  }
	      }

	  }

	  if ( displayline->scrollEnable == 0 && displayline->displayLineTimerEnable == 1)
	  {
		  if (displayline->displayLineTimerCount < displayline->displayLineTimeout \
				  && displayline->timerTick != clkTick )
		  {
		  displayline->displayLineTimerCount++;
		  displayline->timerTick = clkTick;
		  }

		  if ( displayline->displayLineTimerCount >= displayline->displayLineTimeout \
				  && currentRenderRow >= displayline->displayNumberRows)
		  {
			  displayline->displayPtr = 0;
			  displayline->messageComplete = 1;
			  displayline->displayLineTimerCount = 0;
		  }
	  }

}
#endif


void Displayline_renderBlankLine(Displayline displayline, unsigned char currentRenderRow )
{
	  static int fontRowToRender;
	  int dataByte1Index;
	  int dataByte2Index;
	  int dataByte3Index;
	  int dataByte4Index;
	  char dotRenderByte= 0x00;
	  int displayColumn;

	  // Before we render a row of dots onto the display
	  // reset the pointers to the data bytes to be retrieved from
	  // the dot buffer

	  dataByte1Index = 0;
	  dataByte2Index = 1;
	  dataByte3Index = 2;
	  dataByte4Index = 3;

	  if (currentRenderRow <= displayline->startRow || currentRenderRow > displayline->stopRow)
	  {
	  //When not rendering a character from the Dot Buffer Reset the rowShift counter.
	  displayline->rowShift = 0;
	  dotRenderByte = 0;
	 }

	  if (currentRenderRow >= displayline->startRow && currentRenderRow <= displayline->stopRow)
	  {
		  fontRowToRender = currentRenderRow - displayline->startRow;

		  // Large Text Display Area.
		  // Clock out 96 columns of dot information for the current renderRow.

		  for (displayColumn = 0; displayColumn < displayline->displayLineNumberColumns; displayColumn++ ){
		  if ( displayline->charSet == 1 )
		  {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   //Increment the Dots Buffer Bytes Indices to point to the next
				   //byte of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 1;
			  }

			  if ( displayline->charSet == 2 )
			  {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }

				  // if the fontRowToRender is between 9 and 16
				  if (fontRowToRender >= 8 && fontRowToRender < 12)
				  {
				  // Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   //Increment the Dots Buffer Bytes Indices to point to the next two
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 2;
				   dataByte2Index  = dataByte2Index + 2;
			  }

			  if ( displayline->charSet == 3 )
			  {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }

				  // if the fontRowToRender is between 9 and 16
				  if (fontRowToRender >= 8 && fontRowToRender < 16)
				  {
				  // Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				  // if the fontRowToRender is between 9 and 16
				  if (fontRowToRender >= 16 && fontRowToRender < 20)
				  {
				  // Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   //Increment the Dots Buffer Bytes Indices to point to the next three
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 3;
				   dataByte2Index  = dataByte2Index + 3;
				   dataByte3Index  = dataByte3Index + 3;
			   }
			  if ( displayline->charSet == 4 )
			  {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){
				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				  // if the fontRowToRender is between 9 and 16
				  if (fontRowToRender >= 8 && fontRowToRender < 16)
				  {
				  // Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				  // if the fontRowToRender is between 16 and 23
				  if (fontRowToRender >= 16 && fontRowToRender < 24)
				  {
				  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = 0x00;
				  }
				  // if the fontRowToRender is between 24 and 25
				  if (fontRowToRender >= 24 && fontRowToRender < 25 )
				  {
				  // Retrieve 8 rows of dot information for the current column
					  dotRenderByte = 0x00;
				  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   //Increment the Dots Buffer Bytes Indices to point to the next three
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 4;
				   dataByte2Index  = dataByte2Index + 4;
				   dataByte3Index  = dataByte3Index + 4;
				   dataByte4Index  = dataByte3Index + 4;
			   }


			  if ( displayline->charSet == 5 )
			  {

				  // if fontRowToRender is between 1 and 8 then retrieve
				  // the dots information from the buffer for the first
				  // 8 rows of the character to be rendered for the current column
				  if (fontRowToRender >= 0 && fontRowToRender < 8){

				  //Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }

				  // if the fontRowToRender is between 9 and 10
				  if (fontRowToRender >= 8 && fontRowToRender < 10)
				  {
				  // Retrieve 8 rows of dot information for the current column
				  dotRenderByte = 0x00;
				  }
				   // Render to the screen the current row information from the column
				   // byte I have just retrieved from the buffer.
				   dotRenderByte <<= (displayline->rowShift);
				   if (dotRenderByte & 0x80) {
					 gpio_set_gpio_pin(SIN);
				   } else {
					 gpio_clr_gpio_pin(SIN);
				   }

				   // Clock data into On/OFF Input Shift Register on rising edge.
				   gpio_clr_gpio_pin(SCLK);
				   gpio_set_gpio_pin(SCLK);
				   gpio_clr_gpio_pin(SCLK);

				   //Increment the Dots Buffer Bytes Indices to point to the next two
				   //bytes of Dot information for the next column
				   dataByte1Index  = dataByte1Index + 2;
				   dataByte2Index  = dataByte2Index + 2;
			  }
		      }// end of for loop

		      // Increment the rowShift count so as to plot the next
		      // row of dots when the render routine is next called
			  displayline->rowShift++;
			  if (displayline->rowShift > 7)
			  {
				  displayline->rowShift = 0;
			  }
	  }
}


//Update the scrollBufferIncrement property of the displayLine dependant upon the
//character set to be rendered onto the line.
//The scrollBufferIncrement needs to be updated upon the changing of a character set
//It is updated at a point where the new render buffer has been created and is about
//to start rendering onto the display.

void Displayline_setScrollIncrement( Displayline displayline )
{
	switch (displayline->charSet)
	{
	case 1:
		displayline->scrollBufferIncrement = 1;
	break;
	case 2:
		displayline->scrollBufferIncrement = 2;
	break;
	case 3:
		displayline->scrollBufferIncrement = 2;
	break;
	case 4:
		displayline->scrollBufferIncrement = 4;
	break;
	case 5:
		displayline->scrollBufferIncrement = 2;
	break;

	default:
		displayline->scrollBufferIncrement = 0;
	break;
	}
}
