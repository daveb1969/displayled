/*! \file ******************************************************************
 *
 * \brief optoinputs.c 		Opto Input Service.
 *
 * This file creates the data structure used within the Opto Input service.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#include "optoinputs.h"
#include "tfx_assert.h"
#include "malloc.h"
#include "board.h"
#include "gpio.h"

extern char clkTick;

//! \brief Optoinputs data structure constructor
//!
//! Create Optoinputs data structure off the heap.
//!
//! The Optoinputs data structure is used to store the GPIO information
//! retrieved from the de-bounced call for aid buttons, found within the train
//! carriage, wired as inputs to the display using opto-isolated inputs.
//!
//!
//! \return Return Optoinputs*
//!
Optoinputs Optoinputs_construct() {
	Optoinputs optoinputs = ( struct optoinputs_str* ) malloc(sizeof(struct optoinputs_str));
	TFX_ASSERT( optoinputs , "Failed to allocate memory.");
	optoinputs->opto_Input1 = Opto_construct();
    optoinputs->opto_Input2 = Opto_construct();
    optoinputs->opto_Input3 = Opto_construct();
    optoinputs->changedState = 0;
	return optoinputs;
}

//! \brief Optoinputs data structure destructor
//!
//! Optoinputs data structure off the heap.
//!
//! \param[in] optoinputs address of Optoinputs data structure to destroy
void Optoinputs_destroy(Optoinputs optoinputs) {
	TFX_ASSERT( optoinputs , "Expected a non-null pointer.");
	free( optoinputs );
}

