/*
 * opto.h
 *
 *  Created on: 8 Sep 2010
 *      Author: DAVE
 */

#ifndef OPTO_H_
#define OPTO_H_

struct opto_str
{
    volatile int  optoInput;		 		// GPIO debounced input from button
    volatile unsigned char edge_detect;  	// GPIO edge transition has been detected flag
	unsigned int  optoTimeout;       		// Timer Timeout in seconds
	unsigned int  optoTimerCount;    		// Timer Tick counter
	unsigned char optoTimerEnable;   		// Enable timer
	unsigned char optoTimerTick;     		// Timer tick
	unsigned int optoChangedCounter; 		// Count of Opto Input Changing.
};

typedef struct opto_str* Opto;


// Constructor.
//
//
Opto Opto_construct();

// Destructor.
void Opto_destroy(Opto opto);


#endif /* OPTO_H_ */
