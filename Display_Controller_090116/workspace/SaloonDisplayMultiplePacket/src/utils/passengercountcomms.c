/*! \file ******************************************************************
 *
 * \brief passengercountcomms.c 		Passenger Count Comms.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carriages on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

// Standard C library includes.
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

// Project includes.
#include "tfx_assert.h"
#include "passengercountcomms.h"
#include "usart.h"
#include "gpio.h"
#include "user_board.h"
#include "delay.h"


//! \brief Passenger Counter constructor
//!
//! Create Passenger Counter data structure off the heap.
//!
//! \return Return PassengerCounter*
//!
PassengerCountComms PassengerCountComms_construct() {
	PassengerCountComms passengerCountComms = ( struct passengercountcomms_str* ) malloc(sizeof(struct passengercountcomms_str));
	TFX_ASSERT( passengerCountComms , "Failed to allocate memory.");

	passengerCountComms->receiveCircularBuffer = NULL;
	passengerCountComms->receiveCircularBuffer = cb_construct(100, 1);
	passengerCountComms->triggerRXInterruptCounter = 0;

	return passengerCountComms;
}

//! \brief Passenger Counter destructor
//!
//!
//! Remove Passenger Counter data structure off the heap.
//!
void PassengerCountComms_destroy(PassengerCountComms passengerCountComms) {
	TFX_ASSERT(passengerCountComms, "Expected a non-null pointer.");
	free(passengerCountComms);
}


int PassengerCountComms_messageDecode(PassengerCountComms passengerCountComms, PassengerCounter passengerCounter )
{
	  int ret = -1;
	  if(strcmp(&passengerCountComms->buffer[8],"gcounters")==0)
	  {
		passengerCounter->countIn = passengerCountComms->buffer[18];
		passengerCounter->countOut = passengerCountComms->buffer[18+4];
	    return 0;
	  }

	  if(strcmp(passengerCountComms->buffer,"enable_pc")==0)
	  {
	    return 0;
	  }
	  return ret;
}


unsigned char PassengerCountComms_send(char* buffer, unsigned char src, unsigned char dest, char *command,char *arg,int arg_size)
{
  int len;
  short cmd_len;
  short total_len;
  char *tmp_buf;
  int numberCyclesTimeout = 10000;

  memset(buffer,0,MAX_PACKET_LEN);
  cmd_len = strlen(command)+1;
  total_len = arg_size+cmd_len;

  if(total_len > MAX_DATA_LEN) return -1;
  tmp_buf = ( char* ) malloc(sizeof(char [total_len]));
  TFX_ASSERT(tmp_buf, "Failed to allocate memory.");

  strcpy(tmp_buf,command);
  memcpy(tmp_buf+cmd_len,arg,arg_size);
  //Create the command for the particular Passenger Counter
  len = PassengerCountComms_buildCommand(buffer, src,
		                                      dest, 1, 1, total_len, tmp_buf);

  // Disable USART Rx interrupt.
  RS485_USART->idr = AVR32_USART_IER_RXRDY_MASK;

  //Place RS485 driver into tri-state for 5 ms.
  gpio_set_gpio_pin(RS485_USART_CTS_PIN);  // /RX Input Enable - High - Disable RX Input
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN);  // TX Input Enable - Low - Enable TX Output

  delay_ms(5);

  //Enable the output driver upon the RS485 port
  gpio_set_gpio_pin(RS485_USART_CTS_PIN);  // /RX Input Enable - High - Disable RX Input
  gpio_set_gpio_pin(RS485_USART_RTS_PIN);  // TX Input Enable - High - Enable TX Output


  //Write the NULL terminated string buffer to the RS485 serial line
  usart_write_number_chars(RS485_USART, buffer, len);

  while( !usart_tx_empty(RS485_USART) &&  ( numberCyclesTimeout > 0) )
	  {
	  numberCyclesTimeout--;
	  }

  //Place RS485 driver into tri-state for 5 ms.
  gpio_set_gpio_pin(RS485_USART_CTS_PIN);  // /RX Input Enable - High - Disable RX Input
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN);  // TX Input Enable - High - Enable TX Output

  delay_ms(5);

  //Disable the output driver upon the RS485 port, thus enabling the receiver
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN); // TX Input Enable - Low - Disable TX Output
  gpio_clr_gpio_pin(RS485_USART_CTS_PIN); // /RX Input Enable - Low - Enable RX Input


  // Enable USART Rx interrupt.
  RS485_USART->ier = AVR32_USART_IER_RXRDY_MASK;

  delay_ms(100);

  // Disable USART Rx interrupt.
  RS485_USART->idr = AVR32_USART_IER_RXRDY_MASK;

  //Place RS485 driver into tri-state.
  gpio_set_gpio_pin(RS485_USART_CTS_PIN);  // /RX Input Enable - High - Disable RX Input
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN);  // TX Input Enable - Low - Enable TX Output

  free(tmp_buf);

  if ( numberCyclesTimeout > 0 )
  {
	  return 1;
  }
  else
  {
	  return 0;
  }
}


unsigned char PassengerCountComms_enableCounter(PassengerCountComms passengerCountComms, PassengerCounter passengerCounter)
{
    char value=1;
    static unsigned char success;
    success = 0;

    //Enable passenger counter hardware.
    success = PassengerCountComms_send(passengerCountComms->buffer, passengerCountComms->SNP_displayControllerAddress, \
    		              passengerCounter->SNP_passengerCounterAddress,"enable_pc", &value, sizeof(value));
    if (success == 1)
    {
    	passengerCounter->passengerCounterEnabled = 0x01;
    }
    else
    {
    	passengerCounter->passengerCounterEnabled = 0x00;
    }
    return success;
}

unsigned char PassengerCountComms_disableCounter(PassengerCountComms passengerCountComms, PassengerCounter passengerCounter)
{
    char value=0;
    static unsigned char success = 0;
    //Disable passenger counter hardware.
    success = PassengerCountComms_send(passengerCountComms->buffer, passengerCountComms->SNP_displayControllerAddress, \
    		                passengerCounter->SNP_passengerCounterAddress,"enable_pc", &value, sizeof(value));
    if (success == 1)
    {
    passengerCounter->passengerCounterEnabled = 0x00;
    }
    return success;
}


/***********************************************************************/
/**************** building the SNP message *****************************/
/***********************************************************************/
int PassengerCountComms_buildCommand(char *buf,unsigned char src,unsigned char dest,char tpn,char pn,short datalen,char *data)
{
  short crc;
  int index = 0;
  int data_start;

  buf[index++] = 0xFF;                //Preample
  buf[index++] = 0xFF;                //Preample
  buf[index++] = 0xFF;                //Preample
  buf[index++] = 0xFF;                //Preample
  buf[index++] = 0xFF;                //Preample

  buf[index++] = 0x01;                //SOH
  data_start = index;

  buf[index++] = src;                 //source address
  buf[index++] = dest;                //destination address
  buf[index++] = tpn;                 //Total Packet Number
  buf[index++] = pn;                  //Number of this packet
  buf[index++] = datalen & 0x00FF;    //Data Length LSB
  buf[index++] = datalen >> 8;        //Data Length MSB

  if(data) memcpy(&buf[index],data,datalen);

  index += datalen;

  CRC_calcCrc16Block(&buf[data_start],6+datalen,&crc);
  buf[index++] = crc & 0x00FF;
  buf[index++] = crc >> 8;
  buf[index++] = 0xFF;

  return index;
}



unsigned int PassengerCountComms_getArg(char *arg,int arg_size,int pos,int size)
{
  int i;
  char *ptr;
  unsigned int ret;

  if((pos+1)*size > arg_size) return 0;
  else ptr = arg + pos*size;

  ret = 0;
  for(i=0;i<size;i++)
  {
    ret |= ptr[i] << i*8;
  }
  return ret;
}


int PassengerCountComms_readRS485Port(char *buf,int len)
{
  int sum;
  int buf1;
  int bytesleft;
  char usartStatus = 9;
  sum = 0;
  bytesleft = len;
  //Enable the RS485 receiver
  //gpio_clr_gpio_pin(RS485_USART_CTS_PIN);
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN);
  while(sum < len && usartStatus != USART_FAILURE )
  {
	usartStatus = usart_read_char(RS485_USART, &buf1 );
	if (usartStatus == USART_SUCCESS )
    {
		buf[sum] = (char )buf1;
    	sum ++;

    }
  }
  if (usartStatus == USART_SUCCESS)
  {
      return sum;
  }
  else
  {
	  return -1;
  }
}


int PassengerCountComms_writeRS485Port(char *buf,int len)
{
  int counter = 0;
  int usartStatus = 9;

  //Enable the output driver upon the RS485 port
  //gpio_set_gpio_pin(RS485_USART_CTS_PIN);
  gpio_set_gpio_pin(RS485_USART_RTS_PIN);

  while(counter < len && usartStatus != USART_FAILURE )
  {
	int character = (int)buf[counter];
	usartStatus = usart_putchar( RS485_USART, character );
	if ( usartStatus == USART_SUCCESS )
	{
		counter++;
	}
  }

  //Disable the output driver upon the RS485 port, thus enabling the receiver
  //gpio_clr_gpio_pin(RS485_USART_CTS_PIN);
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN);

  if (usartStatus == USART_SUCCESS)
  {
      return counter;
  }
  else
  {
	  return -1;
  }
}




signed char PassengerCountComms_receiveCircular(PassengerCountComms passengerCountComms)
{
  char tmp;
  char tail[3];
  passengerCountComms->calculatedCRCValue = 0;
  passengerCountComms->SNP_CRCValue = 0;
  passengerCountComms->buffer[0] = passengerCountComms->buffer[1] = 0;

  //Keep repeating the code in the Do while loop
  //while there is data being received from the
  //serial port over the RS485 link
  //Look at the data received to find the message type

  //Keep repeating the code in the Do While loop
  //until the PreAmble data has been received and
  //parsed and the Start Character has been received
  //and parsed.

  do
  {
	//Read one byte of data from the configured
	//Serial port

	//Receive one byte of data from the RS485 serial port
	cb_pop_front( passengerCountComms->receiveCircularBuffer, &tmp);


    //What has been received over the serial port
    //in the one byte of data

		switch(tmp)
		{
		  case 0xFF: passengerCountComms->buffer[0] = tmp; break;
		  case 0x01: passengerCountComms->buffer[1] = tmp; break;
		  default: passengerCountComms->buffer[0] = passengerCountComms->buffer[1] = 0;
		}
  }
  while( (passengerCountComms->buffer[0] == 0 || passengerCountComms->buffer[1] == 0) && \
		  passengerCountComms->receiveCircularBuffer->count > 0 );

  if ( passengerCountComms->receiveCircularBuffer->count == 0)
  {
	  return -1 ;
  }

  //At this point in the code the PreAmble has been found
  //together with the Start Character.


  //Read 6 bytes of data from the RS485 port and place them into
  //the passengerCounter.receiveBuffer[] array.
  //Store in passengerCounter.receiveBuffer[2] through passengerCounter.receiveBuffer[8] inclusive.

  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &passengerCountComms->buffer[2]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &passengerCountComms->buffer[3]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &passengerCountComms->buffer[4]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &passengerCountComms->buffer[5]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &passengerCountComms->buffer[6]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &passengerCountComms->buffer[7]);

  //Is the packet received for this SNP_displayControllerAddress address?
  //Or is the packet a global packet 0xFF?
  //If neither of the above then return from the SNP_Recv function.
  if((passengerCountComms->buffer[3] != passengerCountComms->SNP_displayControllerAddress) \
		  && (passengerCountComms->buffer[3] != 0xFF))
  {
    #ifdef DEBUG
    //printf("message not for me but for %d \n", buf[3]);
    #endif
    return -1; //it's not my address
  }

  //Encode all the information that has been received in the network packet
  //and placed into the buf[] array in to the correct local variables

  //Address of the recipient of the data on the RS485 network
  passengerCountComms->SNP_destinationAddress = passengerCountComms->buffer[3];
  //Address of the sender on the data on the RS485 network
  passengerCountComms->SNP_sourceAddress = passengerCountComms->buffer[2];
  //The length of the payload to be sent in this transaction.
  passengerCountComms->SNP_dataLength = passengerCountComms->buffer[6] | (passengerCountComms->buffer[7] <<8);

  //If the number of bytes to be passed in the payload is greater than
  //that allowed then return from function SNP_Recv
  if(passengerCountComms->SNP_dataLength > MAX_DATA_LEN) return -1;

  //Read into the serial port the Payload of the transaction
  //If this does not take place correcty then return from function SNP_Recv
  //Read datalen bytes of data from the RS485 port and place them into
  //the buf[] array.
  //Store in buf[8] through buf[datalen + 8] inclusive.
  //If this DOES NOT take place successfully return from the function.
  //if(PassengerCountComms_readRS485Port(passengerCountComms->buffer+8, passengerCountComms->SNP_dataLength ) < 0) return -1;
  int counter = 0;
  int index = 8;
  while (counter < passengerCountComms->SNP_dataLength )
  {
	  cb_pop_front( passengerCountComms->receiveCircularBuffer,
	  			                     &passengerCountComms->buffer[index]);
	  index++;
	  counter++;
  }

  //Read into the serial port the Tail of the transaction
  //This includes the CRC 2 bytes that are used in CRC checking of the data
  //transaction and the one byte post amble.
  //If the Tail is not read into the Serial port successfully
  //then return from function SNP_Recv
  //if(PassengerCountComms_readRS485Port(tail,sizeof(tail)) < 0) return -1;

  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &tail[0]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &tail[1]);
  cb_pop_front( passengerCountComms->receiveCircularBuffer,
  			                     &tail[2]);

  //From the Tail data parse the CRC data and place in Short variable CRC
  passengerCountComms->SNP_CRCValue = ( tail[0] | (tail[1] << 8) );


  //Call function PassengerCounter_calcCrc16Block to calculate the CRC
  //of the data passed and placed in to passengerCounter.receiveBuffer[]
  //Place the result of the CRC calculation in to variable Short crctest
  //CRC block includes :
  //					1 byte 			Source Address
  //					1 byte 			Destination Address
  //					1 byte 			TotalPacketNumber
  //					1 byte 			PacketNumber
  //					2 bytes 		DataLength
  //					datalen bytes 	Data
  //
  CRC_calcCrc16Block(&passengerCountComms->buffer[2],
		                  ( 6+passengerCountComms->SNP_dataLength ),
		                          &passengerCountComms->calculatedCRCValue);

  //Has the CRC check passed or failed?
  //Data transaction has taken place successfully
  //Return true
  if( passengerCountComms->SNP_CRCValue == passengerCountComms->calculatedCRCValue )
  {
  return 1;  // crc passed!
  }
  //Upon crc failure return -1
  return -1;

}

