/*
 * visualtest.h
 *
 *  Created on: 6 Sep 2010
 *      Author: DAVE
 */

#ifndef VISUALTEST_H_
#define VISUALTEST_H_

#include "displayline.h"
#include "optoinputs.h"
#include "neuron.h"
#include "debug.h"
#include "passengercount.h"



struct visualtest_str
{
	volatile unsigned char 	updateDisplayAtleastOnce;    // Update the display once
	volatile unsigned char  disableTestCycle;            // Disable Test Cycle
};

typedef struct visualtest_str* Visualtest;

// Constructor.
//
//
Visualtest Visualtest_construct();

// Destructor.
//
//
void Visualtest_destroy( Visualtest visualtest );

void Visualtest_idCodes( Displayline displayLine, Neuron neuron );

void Visualtest_opto( Displayline displayLine, Optoinputs optoinputs, Visualtest visualtest  );

//void Visualtest_optoCount( Displayline displayLine, Optoinputs optoinputs, Visualtest visualtest );

void Visualtest_heap( Displayline displayLine, Visualtest visualtest );

void Visualtest_rs485( Displayline displayLine );

void Visualtest_singleLineRender();

void Visualtest_movingBlock( Displayline displayLine );

void Visualtest_passengerCounter( Displayline displayLine, PassengerCount passengerCount );

void Visualtest_loopCount( Displayline displayLine );

void Visualtest_RXTriggerCounter( Displayline displayLine, PassengerCount passengerCount );

void Visualtest_DMACounter( Displayline displayLine );

void Visualtest_TestPassengerCounterComms( Displayline displayLine, PassengerCount passengerCount );

void Visualtest_CRCCounter( Displayline displayLine );

void Visualtest_TestPassengerCounterCRCTest( Displayline displayLine, PassengerCount passengerCount );

void Visualtest_PowerLineCRC( Displayline displayLine );

void Visualtest_PowerLineCRCResult( Displayline displayLine );

void Visualtest_PowerLineCRCApisCompare( Displayline displayLine );

void Visualtest_WriteNVMemory( Displayline displayLine );

void Visualtest_optoCount( Displayline displayLine, Optoinputs optoinputs, Visualtest visualtest );

void Visualtest_TestNVMemory( Displayline displayLine );

void Visualtest_TestDefaultMessagePacket( Displayline displayLine );

void Visualtest_TestSendABCMessagePacket ( Displayline displayLine );

void Visualtest_SendDefaultMessagePacket ( Displayline displayLine);

void Visualtest_SendTextMessage();

void Visualtest_Class319TestSuite( Displayline displayLine );

void Visualtest_counterDisplay( Displayline displayLine );

#endif /* VISUALTEST_H_ */
