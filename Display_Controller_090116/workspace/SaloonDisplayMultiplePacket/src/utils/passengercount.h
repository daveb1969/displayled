/*
 * passengercount.h
 *
 *  Created on: 8 Oct 2010
 *      Author: DAVE
 */

#ifndef PASSENGERCOUNT_H_
#define PASSENGERCOUNT_H_

#include "passengercounter.h"
#include "passengercountComms.h"
#include "passengercounterbuffer.h"
#include "passengercounterdataset.h"
#include "displayline.h"


struct passengercount_str
{
	volatile PassengerCounter passengerCounter1;
	volatile PassengerCounter passengerCounter2;
	volatile PassengerCounter passengerCounter3;
	volatile PassengerCounter passengerCounter4;
	volatile PassengerCounterDataSet passengerCounterDataSet1;
	volatile PassengerCounterDataSet passengerCounterDataSet2;
	volatile PassengerCounterDataSet passengerCounterDataSet3;
	volatile PassengerCounterDataSet passengerCounterDataSet4;
	volatile PassengerCounterDataSet passengerCounterDataSet5;
	volatile PassengerCountComms passengerCountComms;
	volatile unsigned char sendStartPassengerCounters;
	volatile unsigned char sendStopPassengerCounters;
	volatile unsigned char resetPassengerCounterMessageFeedback;
	volatile unsigned int CRCFails;
	volatile unsigned int CRCPasses;
	volatile unsigned char startMessageSent;
    volatile unsigned char numberPassengerCounterPacketsSent;
    volatile unsigned char doorClosedCounter;
    volatile unsigned short closedDoorID;
    volatile unsigned int  numberPassengerCounterAcknowledgesReceived;
    volatile unsigned int  totalNumberPassengerCounterReadCycles;
    volatile unsigned int  totalNumberAPISRebootMessages;
    volatile unsigned char pcEOLMessageModeEnabled;
};

typedef struct passengercount_str* PassengerCount;

// Constructor.
//
//
PassengerCount PassengerCount_construct();

// Destructor.
//
//
void PassengerCount_destroy(PassengerCount passengerCount);

unsigned char PassengerCount_sendReadCounterMessage(PassengerCount passengerCount, int passengerCounter);

unsigned char PassengerCount_resetCounters(PassengerCount passengerCount);

unsigned char PassengerCount_setLedBrightness(PassengerCount passengerCount,
											    PassengerCounter passengerCounter,
												  char ledBrightnessLevel );

signed char PassengerCount_readCountValue( PassengerCount passengerCount,
		                                  unsigned char passengerCounter,
		                                       unsigned char numberRetries);

void PassengerCount_readCounters(PassengerCount passengerCount);

void PassengerCount_ledTest(PassengerCount passengerCount);

void PassengerCount_updateCounterVisualTest( Displayline displayLine, PassengerCount passengerCount );

void PassengerCount_startCounters(PassengerCount passengerCount);

void PassengerCount_stopCounters(PassengerCount passengerCount);

unsigned char PassengerCount_enableCounters(PassengerCount passengerCount);

void PassengerCount_disableAutoLEDBrightness(PassengerCount passengerCount);

void PassengerCount_enableAutoLEDBrightness(PassengerCount passengerCount);

void PassengerCounter_ledsON( PassengerCount passengerCount, PassengerCounter passengerCounter );

void PassengerCounter_ledsOFF( PassengerCount passengerCount, PassengerCounter passengerCounter );

unsigned char PassengerCount_disableCounters(PassengerCount passengerCount);

void PassengerCount_UpdateCounterBuffers(PassengerCount passengerCount);


#endif /* PASSENGERCOUNT_H_ */
