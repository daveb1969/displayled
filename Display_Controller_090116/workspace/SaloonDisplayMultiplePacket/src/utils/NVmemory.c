/*! \file *********************************************************************
 *
 * \brief Example of flash access using the FLASHC driver.
 *
 * - Compiler:           IAR EWAVR32 and GNU GCC for AVR32
 * - Supported devices:  All AVR32 devices with a FLASHC module can be used.
 * - AppNote:
 *
 * \author               Atmel Corporation: http://www.atmel.com \n
 *                       Support and FAQ: http://support.atmel.no/
 *
 ******************************************************************************/

/*! \page License
 * Copyright (c) 2009 Atmel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an Atmel
 * AVR product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */
/*! \mainpage
 * \section intro Introduction
 * This is the documentation for the data structures, functions, variables,
 * defines, enums, and typedefs for the FLASHC software driver.
 *
 * It also comes bundled with an example. This example demonstrates flash read /
 * write data accesses, using a flash block as an NVRAM, located either in the
 * flash array or in the User page.
 *
 * Operating mode of the example:
 *   -# After reset, the NVRAM variables are displayed on the USART link.
 *   -# The NVRAM is cleared (all bytes are set to 0x00).
 *   -# All NVRAM variables are written with incrementing nibbles, starting from
 *      0x0.
 *   -# The user can reset or power-cycle the board to check the
 *      non-volatileness of the NVRAM.
 *
 * \section files Main Files
 *   - flashc.c: FLASHC driver;
 *   - flashc.h: FLASHC driver header file;
 *   - flash_example.c: flash access example application.
 *
 * \section compilinfo Compilation Information
 * This software is written for GNU GCC for AVR32 and for IAR Embedded Workbench
 * for Atmel AVR32. Other compilers may or may not work.
 *
 * \section deviceinfo Device Information
 * All AVR32 devices with a FLASHC module can be used.
 *
 * \section configinfo Configuration Information
 * This example has been tested with the following configuration:
 *   - EVK1100, EVK1101, UC3C_EK or EVK1104 or EVK1105 evalutation kit;
 *   - CPU clock:
 *        -- 12 MHz : EVK1100, EVK1101, EVK1104, EVK1105 evaluation kits
 *        -- 16 Mhz : UC3C_EK
 *   - USART1 (on EVK1100 or EVK1101) connected to a PC serial port via a
 *     standard RS232 DB9 cable, or USART0 (on EVK1105) or USART1 (on EVK1104)
 *     or USART2 (on UC3C_EK) abstracted with a USB CDC connection to a PC;
 *   - PC terminal settings:
 *     - 57600 bps,
 *     - 8 data bits,
 *     - no parity bit,
 *     - 1 stop bit,
 *     - no flow control.
 *
 * \section contactinfo Contact Information
 * For further information, visit
 * <A href="http://www.atmel.com/products/AVR32/">Atmel AVR32</A>.\n
 * Support and FAQ: http://support.atmel.no/
 */

#include "stdio.h"
#include "stdlib.h"
#include <string.h>

#include "compiler.h"
#include "board.h"
#include "power_clocks_lib.h"
#include "flashc.h"
#include "NVmemory.h"
#include "crc.h"
#include "displayExtern.h"
#include "defaultmessage.h"


//! NVRAM data structure located in the flash array.
#if defined (__GNUC__)
__attribute__((__section__(".flash_nvram")))
#endif
static NVMData_str flash_nvram_data
#if defined (__ICCAVR32__)
@ "FLASH_NVRAM"
#endif
;

//! NVRAM data structure located in the User page.

#if defined (__GNUC__)
__attribute__((__section__(".userpage")))
#endif
static NVMData_str user_nvram_data
#if defined (__ICCAVR32__)
@ "USERDATA32_C"
#endif
;


// Constructor.
//
//
NVmemory NVmemory_construct() {

	NVmemory nvmemory = ( struct NVmemory_str* ) malloc(sizeof(struct NVmemory_str));
	TFX_ASSERT(nvmemory, "Failed to allocate memory.");
	nvmemory->readNeuronState = 0;
	//TFX_ASSERT(flash_nvram_data, "Expected Non Null value.");
	nvmemory->nvram = &flash_nvram_data;
	nvmemory->CRC_checkPassed = 0;
	nvmemory->NV_crcCalculated = 0;
	nvmemory->SRAM_crcCalculated = 0;
	return nvmemory;
}

// Destructor.
//
//
void NVmemory_destroy(NVmemory nvmemory) {
	TFX_ASSERT(nvmemory, "Expected a non-null pointer.");
	free(nvmemory);
}

void NVmemory_delay(unsigned long count){

	long delay1 = 0;
	long delay2 = 0;
	long delay3 = 0;
	long delay4 = 0;
	while(delay4 < 6){
		delay4++;
		while (delay1 < count){
			delay1++;
			while (delay2 < count){
					delay2++;
					while (delay3 < count){
							delay3++;
					}
			}
		}
	}
}

void NVmemory_delayPeriod()
{
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
}

/*! \brief Display the Neuron ID variables stored in NVRAM.
 *
 * \param nvram_data pointer to the NVRAM data structure to print.
 */
void NVmemory_displayNeuronData( Displayline displayLine, NVmemory nvmemory )
{

  char val[224];
  int index;
#if DISPLAY_TYPE == SALOON_DISPLAY
  displayLine->charSet = 5;
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY || DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  displayLine->charSet = 2;
  displayLine->prePend = 96;
  displayLine->scrollEnable = 1;
  displayLine->scrollEnableChanged = 1;

#endif
  index  = sprintf( val, "Neuron %02x:", nvmemory->nvram->id[0]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->id[1]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->id[2]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->id[3]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->id[4]);
  index += sprintf( val + index, "%02x", nvmemory->nvram->id[5]);
  index += sprintf( val + index ," ProgID %02x:", nvmemory->nvram->prog_id[0]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->prog_id[1]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->prog_id[2]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->prog_id[3]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->prog_id[4]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->prog_id[5]);
  index += sprintf( val + index, "%02x:", nvmemory->nvram->prog_id[6]);
  index += sprintf( val + index, "%02x", nvmemory->nvram->prog_id[7]);

  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, val);
#else
  Displayline_set_messageBuffer(displayLine, val);
#endif
  Displayline_createDots(displayLine);
  displayLine->displayLineDotsCreated = 1;
  displayLine->messageChanged = 1;

}


/*! \brief Display Default Message 1 upon Display.
 *
 * \param nvmemory->nvram->line1DefaultMessage[] Array within the NVRAM data structure that contains default text string.
 */
void NVmemory_displayMessage1( Displayline displayLine, NVmemory nvmemory  )
{

  char val[defaultMessageSize];
  unsigned char defaultMessageIndex = 0;
  unsigned char index = 0;

	displayLine->charSet = nvmemory->nvram->DM1CharSet;
	displayLine->prePend = nvmemory->nvram->DM1PrePend;
	displayLine->scrollEnable = nvmemory->nvram->DM1ScrollEnable ;
	while ( defaultMessageIndex < defaultMessageSize  )
	{
	  val[index++] = nvmemory->nvram->DM1Text1[defaultMessageIndex++];
	}
  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, val);
#else
  Displayline_set_messageBuffer(displayLine, val);
#endif
  displayLine->displayLineDotsCreated = 1;
  Displayline_createDots(displayLine);
  displayLine->messageChanged = 1;

}


/*! \brief Display Default Message 1 upon Display.
 *
 * \param nvmemory->nvram->line1DefaultMessage[] Array within the NVRAM data structure that contains default text string.
 */
void NVmemory_displayMessage2( Displayline displayLine, NVmemory nvmemory  )
{

  char val[defaultMessageSize];
  unsigned char defaultMessageIndex = 0;
  unsigned char index = 0;

	displayLine->charSet = nvmemory->nvram->DM2CharSet;
	displayLine->prePend = nvmemory->nvram->DM2PrePend;
	displayLine->scrollEnable = nvmemory->nvram->DM2ScrollEnable ;
	while ( defaultMessageIndex < defaultMessageSize  )
	{
	  val[index++] = nvmemory->nvram->DM2Text2[defaultMessageIndex++];
	}
  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, val);
#else
  Displayline_set_messageBuffer(displayLine, val);
#endif
  displayLine->displayLineDotsCreated = 1;
  Displayline_createDots(displayLine);
  displayLine->messageChanged = 1;

}

void NVmemory_displayMessage3( Displayline displayLine, NVmemory nvmemory  )
{

  char val[defaultMessageSize];
  unsigned char defaultMessageIndex = 0;
  unsigned char index = 0;

	displayLine->charSet = nvmemory->nvram->DM3CharSet;
	displayLine->prePend = nvmemory->nvram->DM3PrePend;
	displayLine->scrollEnable = nvmemory->nvram->DM3ScrollEnable ;
	while ( defaultMessageIndex < defaultMessageSize  )
	{
	  val[index++] = nvmemory->nvram->DM3Text3[defaultMessageIndex++];
	}
  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, val);
#else
  Displayline_set_messageBuffer(displayLine, val);
#endif
  displayLine->displayLineDotsCreated = 1;
  Displayline_createDots(displayLine);
  displayLine->messageChanged = 1;

}


/*! \brief Store the Data Table structure into the NV Memory
 *
 *
 * \param caption     Caption to print before running the example.
 * \param nvram_data  Pointer to the NVRAM data structure.
 */
void NVmemory_writeDataTable(Displayline displayLine, Neuron neuron, NVmemory nvmemory  )
{
	short calcCRC16;
	// Clearing NVRAM variables
	flashc_memset((void *)nvmemory->nvram, 0x00, 8, sizeof(*nvmemory->nvram), TRUE);
	NVmemory_displayNeuronData( displayLine, nvmemory );

	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);


	// Write Data Table Structure into flash memory
	flashc_memcpy((void *)&nvmemory->nvram->id, &neuron->id, sizeof(nvmemory->nvram->id), TRUE  );
	flashc_memcpy((void *)&nvmemory->nvram->prog_id, &neuron->prog_id, sizeof(nvmemory->nvram->prog_id), TRUE  );
	flashc_memcpy((void *)&nvmemory->nvram->idFound, &neuron->idFound, sizeof(nvmemory->nvram->idFound), TRUE  );


	//Write Default Message 1 Information into the Flash memory
	flashc_memcpy((void *)&nvmemory->nvram->DM1CharSet , \
			                &thisDisplay->defaultMessage->textMessage1->charSet , \
			                     sizeof(nvmemory->nvram->DM1CharSet ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM1PrePend , \
			                &thisDisplay->defaultMessage->textMessage1->prePend , \
			                     sizeof(nvmemory->nvram->DM1PrePend ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM1ScrollEnable , \
			                &thisDisplay->defaultMessage->textMessage1->modeDisplayLine , \
			                     sizeof(nvmemory->nvram->DM1ScrollEnable ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM1Text1 , \
			                &thisDisplay->defaultMessage->textMessage1->Text1 , \
			                     sizeof(nvmemory->nvram->DM1Text1 ), TRUE  );

	//Write Default Message 2 Information into the Flash memory
	flashc_memcpy((void *)&nvmemory->nvram->DM2CharSet , \
			                &thisDisplay->defaultMessage->textMessage2->charSet , \
			                     sizeof(nvmemory->nvram->DM2CharSet ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM2PrePend , \
			                &thisDisplay->defaultMessage->textMessage2->prePend , \
			                     sizeof(nvmemory->nvram->DM2PrePend ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM2ScrollEnable , \
			                &thisDisplay->defaultMessage->textMessage2->modeDisplayLine , \
			                     sizeof(nvmemory->nvram->DM2ScrollEnable ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM2Text2 , \
			                &thisDisplay->defaultMessage->textMessage2->Text2 , \
			                     sizeof(nvmemory->nvram->DM2Text2 ), TRUE  );

	//Write Default Message 3 Information into the Flash memory
	flashc_memcpy((void *)&nvmemory->nvram->DM3CharSet , \
			                &thisDisplay->defaultMessage->textMessage3->charSet , \
			                     sizeof(nvmemory->nvram->DM3CharSet ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM3PrePend , \
			                &thisDisplay->defaultMessage->textMessage3->prePend , \
			                     sizeof(nvmemory->nvram->DM3PrePend ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM3ScrollEnable , \
			                &thisDisplay->defaultMessage->textMessage3->modeDisplayLine , \
			                     sizeof(nvmemory->nvram->DM3ScrollEnable ), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM3Text3 , \
			                &thisDisplay->defaultMessage->textMessage3->Text3 , \
			                     sizeof(nvmemory->nvram->DM3Text3 ), TRUE  );

	thisDisplay->defaultMessage->textMessage1->storedInFlash = 0x01;
	thisDisplay->defaultMessage->textMessage2->storedInFlash = 0x01;
	thisDisplay->defaultMessage->textMessage3->storedInFlash = 0x01;

	flashc_memcpy((void *)&nvmemory->nvram->DM1storedInFlash , \
			                &thisDisplay->defaultMessage->textMessage1->storedInFlash , \
			                     sizeof(nvmemory->nvram->DM1storedInFlash), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM2storedInFlash , \
			                &thisDisplay->defaultMessage->textMessage2->storedInFlash , \
			                     sizeof(nvmemory->nvram->DM2storedInFlash), TRUE  );

	flashc_memcpy((void *)&nvmemory->nvram->DM3storedInFlash , \
			                &thisDisplay->defaultMessage->textMessage3->storedInFlash , \
			                     sizeof(nvmemory->nvram->DM3storedInFlash), TRUE  );


	NVmemory_displayNeuronData( displayLine, nvmemory );
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);



	//Call function CRC_calcCrc16Block to calculate the CRC
	//of the Neuron data to be placed into NVM.
	//Place the result of the CRC calculation in to nvram_data->CRC16.
	//CRC block includes :
	//////////////////////////////////////////////////////////////////////////////////////////////
	//					6 bytes		Neuron ID data		       unsigned char   id[6];
	//					8 bytes		Neuron Program ID data	   unsigned char   prog_id[8];
	//					1 byte 	    Neuron Data found flag     unsigned char   idFound;
	//////////////////////////////////////////////////////////////////////////////////////////////
    //	                1 byte      Charset Default Msg1       unsigned char   DM1CharSet;
    //	                1 byte      PrePend Default Msg1       unsigned char   DM1PrePend;
    //	                1 byte      ScrollEnable Default Msg1  unsigned char   DM1ScrollEnable;
	//                100 byte      Default Msg1                        char   DM1Text1[100];
	//	                1 byte      StoredInFlash Default MSg1 unsigned char   DM1storedInFlash;
	//////////////////////////////////////////////////////////////////////////////////////////////
    //	                1 byte      Charset Default Msg1       unsigned char   DM1CharSet;
    //	                1 byte      PrePend Default Msg1       unsigned char   DM1PrePend;
    //	                1 byte      ScrollEnable Default Msg1  unsigned char   DM1ScrollEnable;
	//                100 byte      Default Msg1                        char   DM1Text1[100];
	//	                1 byte      StoredInFlash Default MSg1 unsigned char   DM1storedInFlash;
	//////////////////////////////////////////////////////////////////////////////////////////////
    //	                1 byte      Charset Default Msg1       unsigned char   DM1CharSet;
    //	                1 byte      PrePend Default Msg1       unsigned char   DM1PrePend;
    //	                1 byte      ScrollEnable Default Msg1  unsigned char   DM1ScrollEnable;
	//                100 byte      Default Msg1                        char   DM1Text1[100];
	//	                1 byte      StoredInFlash Default MSg1 unsigned char   DM1storedInFlash;
	/////////////////////////////////////////////////////////////////////////////////////////////
    //         Total: 15 + 104 + 104 + 104 = 15 + 312 = 327
	/////////////////////////////////////////////////////////////////////////////////////////////
	CRC_calcCrc16Block((void *)nvmemory->nvram, 327, &calcCRC16);

	flashc_memcpy((void *)&nvmemory->nvram->CRC16, &calcCRC16, sizeof(nvmemory->nvram->CRC16), TRUE  );

	//Check the CRC of the stored data in NVM
	NVmemory_corruptTest(displayLine, nvmemory);
	if ( nvmemory->CRC_checkPassed == 1 )
	{
		neuron->storedInFlash = 1;
		thisDisplay->defaultMessage->textMessage1->storedInFlash = 0x01;
		thisDisplay->defaultMessage->textMessage2->storedInFlash = 0x01;
		thisDisplay->defaultMessage->textMessage3->storedInFlash = 0x01;

#if  DISPLAY_TYPE == SALOON_DISPLAY
		Displayline_set_messageBuffer(displayLine, "NV Memory Storage completed successfully");
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
		Displayline_set_messageBuffer(displayLine, "NV Memory Storage completed successfully");
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
		Displayline_set_reversedMessageBuffer(displayLine, "NV Memory Storage completed successfully");
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;

		//Visualtest_loopCount( thisDisplay->displayLine1 );

		//NVmemory_displayMessage1( thisDisplay->displayLine1, nvmemory  );
		//NVmemory_displayMessage2( thisDisplay->displayLine2, nvmemory  );

#endif
	}
	else
	{
		neuron->storedInFlash = 0;
		thisDisplay->defaultMessage->textMessage1->storedInFlash = 0x00;
		thisDisplay->defaultMessage->textMessage2->storedInFlash = 0x00;
		thisDisplay->defaultMessage->textMessage3->storedInFlash = 0x00;
		neuron->idFound = 0;
#if  DISPLAY_TYPE == SALOON_DISPLAY
		Displayline_set_messageBuffer(displayLine, "NV Memory Storage corrupted");
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
		displayLine->displayLineDotsCreated = 0;
		Displayline_set_messageBuffer(displayLine, "NV Memory Storage corrupted");
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
		Displayline_set_reversedMessageBuffer(displayLine, "NV Memory Storage corrupted");;
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;

#endif
	}
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
	NVmemory_delay(999999);
}

void NVmemory_corruptNVMData(NVmemory nvmemory)
{
	unsigned char testData[6] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	// Corrupt the NVM by writing over the Neuron ID with Zeros.
	flashc_memcpy((void *)&nvmemory->nvram->id, &testData, sizeof( nvmemory->nvram->id), TRUE  );

}

void NVmemory_corruptTest( Displayline displayLine, NVmemory nvmemory )
{
	nvmemory->CRC_checkPassed = 0;
	short crcCalculated;

	//Calculate the CRC for the current data in the NVM
	CRC_calcCrc16Block((void *)nvmemory->nvram, 327, &crcCalculated);

	//Check to see if the calculated CRC value differs from the NVM CRC value
	if ( nvmemory->nvram->CRC16 == crcCalculated )
	{
		//CRC_checkPassed = 1;
		nvmemory->CRC_checkPassed = 1;
	}
	else
	{
		NVmemory_displayCorruptTest( displayLine, nvmemory );
	}
}


void NVmemory_displayCorruptTest(Displayline displayLine, NVmemory nvmemory)
{
	static volatile int messageChanged = 0;
	char val[224];
	displayLine->charSet = 1;
	displayLine->prePend = 112;
	displayLine->scrollEnable = 1;

	//If CRC check fails display message upon the display.
	if ( nvmemory->CRC_checkPassed == 0 && (messageChanged == 0) )
	{
		sprintf( val, "CRC Failed:%x Learning ", nvmemory->NV_crcCalculated );
		displayLine->messageComplete = 0x00;
		messageChanged = 1;
	}

	if (messageChanged)
	{

		Displayline_set_messageBuffer(displayLine, val);
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;
	}
	messageChanged = 0;
}
void NVmemory_readNeuronData( Neuron neuron, NVmemory nvmemory )
{
	neuron->id[0] = nvmemory->nvram->id[0];
	neuron->id[1] = nvmemory->nvram->id[1];
	neuron->id[2] = nvmemory->nvram->id[2];
	neuron->id[3] = nvmemory->nvram->id[3];
	neuron->id[4] = nvmemory->nvram->id[4];
	neuron->id[5] = nvmemory->nvram->id[5];

	neuron->prog_id[0] = nvmemory->nvram->prog_id[0];
	neuron->prog_id[1] = nvmemory->nvram->prog_id[1];
	neuron->prog_id[2] = nvmemory->nvram->prog_id[2];
	neuron->prog_id[3] = nvmemory->nvram->prog_id[3];
	neuron->prog_id[4] = nvmemory->nvram->prog_id[4];
	neuron->prog_id[5] = nvmemory->nvram->prog_id[5];
	neuron->prog_id[6] = nvmemory->nvram->prog_id[6];
	neuron->prog_id[7] = nvmemory->nvram->prog_id[7];

	neuron->idFound = nvmemory->nvram->idFound;
}

void NVmemory_readDataTable( Displayline displayLine, Neuron neuron, NVmemory nvmemory )
{
	//Carry out pre-check to assert that information is stored in the NVM memory correctly.
	//Check the CRC of the stored data in NVM
	NVmemory_corruptTest(displayLine, nvmemory);
	if ( nvmemory->CRC_checkPassed == 1 )
	{
		neuron->storedInFlash = 1;
		neuron->idFound = 1;
		thisDisplay->defaultMessage->textMessage1->storedInFlash = 0x01;
		thisDisplay->defaultMessage->textMessage2->storedInFlash = 0x01;
		thisDisplay->defaultMessage->textMessage3->storedInFlash = 0x01;
	}
	else
	{
		neuron->storedInFlash = 0;
		thisDisplay->defaultMessage->textMessage1->storedInFlash = 0x00;
		thisDisplay->defaultMessage->textMessage2->storedInFlash = 0x00;
		thisDisplay->defaultMessage->textMessage3->storedInFlash = 0x00;
		neuron->idFound = 0;
		return;
	}

	thisDisplay->defaultMessage->textMessage1->charSet = nvmemory->nvram->DM1CharSet;
	thisDisplay->defaultMessage->textMessage1->prePend = nvmemory->nvram->DM1PrePend;
	thisDisplay->defaultMessage->textMessage1->modeDisplayLine = \
			                                        nvmemory->nvram->DM1ScrollEnable;


	thisDisplay->defaultMessage->textMessage2->charSet = nvmemory->nvram->DM2CharSet;
	thisDisplay->defaultMessage->textMessage2->prePend = nvmemory->nvram->DM2PrePend;
	thisDisplay->defaultMessage->textMessage2->modeDisplayLine = \
			                                        nvmemory->nvram->DM2ScrollEnable;

	thisDisplay->defaultMessage->textMessage3->charSet = nvmemory->nvram->DM3CharSet;
	thisDisplay->defaultMessage->textMessage3->prePend = nvmemory->nvram->DM3PrePend;
	thisDisplay->defaultMessage->textMessage3->modeDisplayLine = \
			                                        nvmemory->nvram->DM3ScrollEnable;


	int index = 0;
	for ( index = 0; index < defaultMessageSize; index++)
	{
	  thisDisplay->defaultMessage->textMessage1->Text1[index] = \
			                       nvmemory->nvram->DM1Text1[index];
	  thisDisplay->defaultMessage->textMessage2->Text2[index] = \
			                       nvmemory->nvram->DM2Text2[index];
	  thisDisplay->defaultMessage->textMessage3->Text3[index] = \
			                       nvmemory->nvram->DM3Text3[index];
	}

	neuron->id[0] = nvmemory->nvram->id[0];
	neuron->id[1] = nvmemory->nvram->id[1];
	neuron->id[2] = nvmemory->nvram->id[2];
	neuron->id[3] = nvmemory->nvram->id[3];
	neuron->id[4] = nvmemory->nvram->id[4];
	neuron->id[5] = nvmemory->nvram->id[5];

	neuron->prog_id[0] = nvmemory->nvram->prog_id[0];
	neuron->prog_id[1] = nvmemory->nvram->prog_id[1];
	neuron->prog_id[2] = nvmemory->nvram->prog_id[2];
	neuron->prog_id[3] = nvmemory->nvram->prog_id[3];
	neuron->prog_id[4] = nvmemory->nvram->prog_id[4];
	neuron->prog_id[5] = nvmemory->nvram->prog_id[5];
	neuron->prog_id[6] = nvmemory->nvram->prog_id[6];
	neuron->prog_id[7] = nvmemory->nvram->prog_id[7];

	neuron->idFound = nvmemory->nvram->idFound;

	Displayline_set_messageBuffer(displayLine, nvmemory->nvram->DM1Text1);
	Displayline_createDots(displayLine);
	displayLine->messageChanged = 1;

}

char NVMemory_getNeuronIDStatus()
{
	return (flash_nvram_data.idFound);
}


void NVMemory_readNeuron(Displayline displayLine, Network network, NVmemory nvmemory)
{

//Check to assert if Neuron ID has been stored in flash memory.
if ( nvmemory->readNeuronState == 0 )
{
	//Is the Neuron ID stored in Flash Memory?
	if ( NVMemory_getNeuronIDStatus() == 1 )
	{
		//Neuron ID found in flash
		nvmemory->readNeuronState = 1;
	}
	else
	{
		//Neuron ID not present in flash
		nvmemory->readNeuronState = 2;
	}
}
if ( nvmemory->readNeuronState == 1 )
{
	//Check the CRC of the stored data in NVM
	NVmemory_corruptTest( displayLine, nvmemory );
	if ( nvmemory->CRC_checkPassed == 1 )
	{
		//NVM data stored correctly in
		network->neuron->storedInFlash = 1;
		nvmemory->readNeuronState = 4;
	}
	else
	{
		//NVM data failed CRC check
		network->neuron->storedInFlash = 0;
		network->neuron->idFound = 0;
		nvmemory->readNeuronState = 2;
	}
}

//If we don't have Neuron ID stored in flash memory and we have not been able to
//read it from the Echelon yet, try to read the Neuron ID from the Echelon.
if (nvmemory->readNeuronState == 2)
{

	  Network_readNeuronID( network );
	  if ( network->neuron->idFound == 1 )
	  {
		  nvmemory->readNeuronState = 3;
	  }
}

//Store the retrieved Neuron ID into NVM.
if (nvmemory->readNeuronState == 3)
{
	NVmemory_writeDataTable(displayLine, network->neuron, nvmemory);

	//Was the Neuron ID stored successfully in flash ?
	if ( network->neuron->storedInFlash == 1)
	{
		nvmemory->readNeuronState = 4;
	}
}

//If we have the Neuron ID stored in flash memory read the Neuron ID
//stored in flash and placed it into the Display Structure in SRAM.
if (nvmemory->readNeuronState == 4)
{
	NVmemory_readNeuronData( network->neuron, nvmemory );
	nvmemory->readNeuronState = 5;
}

}
