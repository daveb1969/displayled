/*
 * neuron.h
 *
 *  Created on: 1 Sep 2010
 *      Author: DAVE
 */
#ifndef NEURON_H_
#define NEURON_H_

//unsigned char neuron_id[8];
//unsigned char prog_id[6];


struct neuron_str {
	unsigned char	id[6];
	unsigned char	prog_id[8];
	unsigned char   idFound;
	unsigned char   storedInFlash;
};

typedef struct neuron_str* Neuron;

// Constructor
//
//
Neuron Neuron_construct();

// Destructor.
//
//
void Neuron_destroy(Neuron neuron);

#endif /* NEURON_H_ */
