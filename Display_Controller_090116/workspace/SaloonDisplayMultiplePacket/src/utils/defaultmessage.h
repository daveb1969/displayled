/*
 * defaultmessage.h
 *
 *  Created on: 1 Sep 2010
 *      Author: DAVE
 */
#ifndef DEFAULTMESSAGE_H_
#define DEFAULTMESSAGE_H_

#define defaultMessageSize 100

struct defaultMessage1_str{
	unsigned char charSet;
	unsigned char prePend;
	unsigned char modeDisplayLine;
	char Text1[defaultMessageSize];
	unsigned char storedInFlash;
};


typedef struct defaultMessage1_str* TextMessage1;


struct defaultMessage2_str{
	unsigned char charSet;
	unsigned char prePend;
	unsigned char modeDisplayLine;
	char Text2[defaultMessageSize];
	unsigned char storedInFlash;
};



typedef struct defaultMessage2_str* TextMessage2;

struct defaultMessage3_str{
	unsigned char charSet;
	unsigned char prePend;
	unsigned char modeDisplayLine;
	char Text3[defaultMessageSize];
	unsigned char storedInFlash;
};



typedef struct defaultMessage3_str* TextMessage3;



struct defaultmessage_str {
	TextMessage1 textMessage1;
	TextMessage2 textMessage2;
	TextMessage3 textMessage3;
};

typedef struct defaultmessage_str* DefaultMessage;

// Constructor
//
//
DefaultMessage DefaultMessage_construct();

// Destructor.
//
//
void DefaultMessage_destroy(TextMessage1 defaultmessage);


void DefaultMessage_setText(char *textMessage, char* messageToStore );




#endif /* DEFAULTMESSAGE_H_ */
