/*! \file ******************************************************************
 *
 * \brief crc.h 		Cyclic Redundancy Check.
 *
 * This file creates and provides services for Cyclic Redundancy Checking.
 * .
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The service provided allow the calculation of a 16 bit CRC value from a
 *	buffer of data.
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#ifndef CRC_H_
#define CRC_H_

#define MAX_CRC_DIGITS       	256
#define BYTE_WIDTH            	8
#define CRC_WIDTH             	16
#define CRC_MASK              	0xFF

//! \brief Calculate CRC 16 bit value from Buffer of data.
//!
//! \param[in] pBlock	- pointer to buffer of char data
//! \param[in] Number	- number of char's in buffer of data
//! \param[in] pCrc     - pointer to SHORT into which the calculated 16 bit CRC value will be placed
//!
void CRC_calcCrc16Block(
char *pBlock,				// Pointer to start of block
short Number, 		        // Number of bytes i block
short *pCrc);

unsigned char CRC_test1();

unsigned char CRC_test2();

unsigned char CRC_test3();

unsigned char CRC_test4();

unsigned char findEndian();

#endif /* CRC_H_ */
