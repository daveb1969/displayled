// Assertion mechanism.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------


#include "wdt.h"
#define DEBUG2

#if defined(DEBUG2)

  #include "tfx_assert.h"

  // Standard C library includes.
  #include "stdio.h"
  #include "stdlib.h"

  // Assertion handler.
  void tfx_assert(const char* message_text) {
    TFX_ASSERT(message_text, "Expected a non-null pointer.");
    fprintf(stderr, "ASSERTION FAILURE: %s\n", message_text);
    exit(EXIT_FAILURE);
  }

#endif // DEBUG
