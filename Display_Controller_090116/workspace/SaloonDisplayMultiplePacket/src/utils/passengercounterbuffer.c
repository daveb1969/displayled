/*! \file ******************************************************************
 *
 * \brief SNPserialport.c 		SNP Serial Port.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carriages on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

// Standard C library includes.
#include "stdlib.h"
#include "string.h"

// Project includes.
#include "../utils/tfx_assert.h"
#include "passengercounterbuffer.h"
#include "usart.h"
#include "gpio.h"
#include "user_board.h"


//! \brief Passenger Counter Buffer constructor
//!
//! Create Passenger Counter data structure off the heap.
//!
//! \return Return PassengerCounterBuffer*
//!
PassengerCounterBuffer PassengerCounterBuffer_construct() {
	PassengerCounterBuffer passengerCounterBuffer = ( struct passengercounterbuffer_str* ) malloc(sizeof(struct passengercounterbuffer_str));
	TFX_ASSERT(passengerCounterBuffer, "Failed to allocate memory.");

	passengerCounterBuffer->countIn = 0;
	passengerCounterBuffer->countOut = 0;
	passengerCounterBuffer->crcFails = 0;
	passengerCounterBuffer->crcPasses = 0;

	return passengerCounterBuffer;
}

//! \brief Passenger Counter Buffer destructor
//!
//!
//! Remove Passenger Counter data structure off the heap.
//!
void PassengerCounterBuffer_destroy(PassengerCounterBuffer passengerCounterBuffer) {
	TFX_ASSERT(passengerCounterBuffer, "Expected a non-null pointer.");
	free(passengerCounterBuffer);
}



