/*
 * optoinputs.h
 *
 *  Created on: 12 Oct 2010
 *      Author: DAVE
 */

#ifndef OPTOINPUTS_H_
#define OPTOINPUTS_H_
#include "opto.h"

struct optoinputs_str
{
    volatile Opto opto_Input1;
    volatile Opto opto_Input2;
    volatile Opto opto_Input3;
    volatile unsigned char changedState;
};

typedef volatile struct optoinputs_str* Optoinputs;

// Constructor.
//
//
Optoinputs Optoinputs_construct();

// Destructor.
//
//
void Optoinputs_destroy(Optoinputs optoinputs);

#endif /* OPTOINPUTS_H_ */
