/*
 * slaveMasterSM.c
 *
 *  Created on: 2 Sep 2010
 *      Author: DAVE
 */

#include "TFX-SPI.h"
#include "network.h"
#include "visualtest.h"

void slaveMasterSM( Network network )
{
	TFX_ASSERT(network, "Expected a non-null pointer.");
	static unsigned char stateCount = 0;
	char character = 65;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[MAX_SPI_LEN];

	if (stateCount == 0 && network->msState == 0 && network->smState == 0)
	{
	  network->sendMessageBuffer->transLen = 0x00;
	  network->sendMessageBuffer->transType = 0x00;
	  unsigned char messageCount;
	  for (messageCount = 0; messageCount < (messageLength-2); messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  stateCount = 1;
	}

	if (stateCount == 1)
	{
	  Network_spiSlaveMasterTransfer( network, messageBuffer, messageLength );

	  if ( network->smState == 9 )
	  {
		  network->smState = 0;
		  stateCount = 2;
	  }
	}

	if (stateCount == 2)
	{
      Network_processMessages( network );
	  character++;
	  if (character >= 127)
		  character = 32;
	  stateCount = 0;
	}
}
