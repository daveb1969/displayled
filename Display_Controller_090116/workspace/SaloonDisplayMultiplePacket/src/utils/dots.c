// A "class" to represent the dot representation of a message.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#include "dots.h"

// Standard C library includes.
#include "stdlib.h"
#include "string.h"
#include "board.h"

// Project includes.
#include "tfx_assert.h"

enum CharSet{
SmallText = 1,
LargeText,
SmallNumber
} characterSet;

#if DISPLAY_TYPE == SALOON_DISPLAY

//! \brief Dots data structure definition
//!
//! Size of the bitmap buffer used to store the bitmap
//! data that will be rendered onto the display for each
//! individual display line.
#define DOTS_BUFFER_SIZE 20000
#define WORKING_BUFFER_SIZE 30
struct dots_str {
  char m_dots[DOTS_BUFFER_SIZE];
  volatile int m_length;
  char m_working_dots[WORKING_BUFFER_SIZE];
  volatile int m_working_dots_length;
};

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY

//! \brief Dots data structure definition
//!
//! Size of the bitmap buffer used to store the bitmap
//! data that will be rendered onto the display for each
//! individual display line.
#define DOTS_BUFFER_SIZE 4000
#define WORKING_BUFFER_SIZE 50
struct dots_str {
  char m_dots[DOTS_BUFFER_SIZE];
  volatile int m_length;
  char m_working_dots[WORKING_BUFFER_SIZE];
  volatile int m_working_dots_length;
};

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY

//! \brief Dots data structure definition
//!
//! Size of the bitmap buffer used to store the bitmap
//! data that will be rendered onto the display for each
//! individual display line.
//DB Changed #define DOTS_BUFFER_SIZE 4096
#define DOTS_BUFFER_SIZE 4000
#define WORKING_BUFFER_SIZE 50
struct dots_str {
  char m_dots[DOTS_BUFFER_SIZE];
  volatile int m_length;
  char m_working_dots[WORKING_BUFFER_SIZE];
  volatile int m_working_dots_length;
};
#else
  #error No DISPLAY_TYPE defined
#endif

//! \brief Dots data structure constructor
//!
//! Create Dots data structure off the heap and zeros its contents.
//!
//! The Dots data structure is used to store the bitmap data
//! that will be rendered onto the LED display.
//!
//! A Dots data structure is created for each display line found upon
//! the LED display.
//!
//! \return Return Dots*
//!
Dots Dots_construct() {
  Dots dots = ( struct dots_str* ) malloc(sizeof(struct dots_str));
  TFX_ASSERT(dots, "Failed to allocate memory for Dots Buffers.");
  Dots_clear(dots);
  Dots_working_clear(dots);
  return dots;
}

//! \brief Dots data structure destructor
//!
//! Removes Dots data structure off the heap.
//!
//! \param[in] dots address of Dots structure to destroy
void Dots_destroy(Dots dots) {
  TFX_ASSERT(dots, "Expected a non-null pointer34.");
  free(dots);
}


//! \brief Get length of dots structure
//!
//! \param[in] dots address of Dots structure
//!
//! \return m_length - length of dots structure in bytes

size_t Dots_get_length(const Dots dots)
{
  TFX_ASSERT(dots, "Expected a non-null pointer35.");
  return dots->m_length;
}

//! \brief Get length of dots structure
//!
//! \param[in] dots address of Dots structure
//!
//! \return m_working_dots_length - length of working dots structure in bytes

size_t Dots_get_working_length(const Dots dots)
{
  TFX_ASSERT(dots, "Expected a non-null pointer36.");
  return dots->m_working_dots_length;
}
//! \brief Get bitmap byte from Dots structure at specified position in structure.
//!
//! \param[in] dots      - address of Dots structure
//! \param[in] position  - byte position to retrieve from Dots structure
//!
//! \return char - data found at byte position or if position out of range return 0
// Get the value in the specified position.
char Dots_get_value(const Dots dots, int position) {
  TFX_ASSERT(dots, "Expected a non-null pointer37.");
  return (position < dots->m_length) ? dots->m_dots[position] : 0;
}


//! \brief Get bitmap byte from Dots structure at specified position in structure.
//!
//! \param[in] dots      - address of Dots structure
//! \param[in] position  - byte position to retrieve from Dots structure
//!
//! \return char - data found at byte position or if position out of range return 0
// Get the value in the specified position.
char Dots_working_get_value(const Dots dots, int position) {
  TFX_ASSERT(dots, "Expected a non-null pointer38.");
  return (position < dots->m_working_dots_length ) ? dots->m_working_dots[position] : 0;
}



//! \brief Clear the Dots structure of any data..
//!
//! \param[in] dots      - address of Dots structure
void Dots_clear(Dots dots) {
  TFX_ASSERT(dots, "Expected a non-null pointer39.");
  //memset(dots->m_dots, 0, DOTS_BUFFER_SIZE);
  // Don't really need to clear the memory --- we can just anull the length.
  dots->m_length = 0;
}



//! \brief Clear the Dots structure of any data..
//!
//! \param[in] dots      - address of Dots structure
void Dots_working_clear(Dots dots) {
  TFX_ASSERT(dots, "Expected a non-null pointer40.");
  //memset(dots->m_dots, 0, DOTS_BUFFER_SIZE);
  // Don't really need to clear the memory --- we can just anull the length.
  dots->m_working_dots_length = 0;
}

//! \brief Populate the Dots data structure with bitmap data.
//!
//! \param[in] dots      - address of Dots structure.
//! \param[in] raw_dots  - address of raw dot buffer used to populate Dots structure.
//! \param[in] length    - number of bytes present in the raw dot buffer.
void Dots_set(Dots dots, const unsigned char* raw_dots, size_t length)
{
  TFX_ASSERT(dots, "Expected a non-null pointer41.");
  TFX_ASSERT(raw_dots, "Expected a non-null pointer42.");
  TFX_ASSERT(length < DOTS_BUFFER_SIZE, "Insufficient space in DOT buffer.");
  if ( length < DOTS_BUFFER_SIZE)
  {
  memcpy(dots->m_dots, raw_dots, sizeof(char) * length);
  dots->m_length = length;
  }
}


//! \brief Populate the Dots data structure with bitmap data.
//!
//! \param[in] dots      - address of Dots structure.
//! \param[in] raw_dots  - address of raw dot buffer used to populate Dots structure.
//! \param[in] length    - number of bytes present in the raw dot buffer.
void Dots_working_set(Dots dots, const unsigned char* raw_dots, size_t length)
{
  TFX_ASSERT(dots, "Expected a non-null pointer43.");
  TFX_ASSERT(raw_dots, "Expected a non-null pointer44.");
  TFX_ASSERT(length < WORKING_BUFFER_SIZE, "Insufficient space in DOT buffer.");
  if ( length < WORKING_BUFFER_SIZE)
  {
  memcpy(dots->m_working_dots, raw_dots, sizeof(char) * length);
  dots->m_working_dots_length = length;
  }
}


//! \brief Append one byte of bitmap data onto the end of the Dot Structure.
//!
//! \param[in] dots      - address of Dots structure
//! \param[in] value     - byte of bitmap data to append.//!

// Append the specified value to the list.
void Dots_append_value(Dots dots, char value)
{
  TFX_ASSERT(dots, "Expected a non-null pointer.");
  TFX_ASSERT(dots->m_length < DOTS_BUFFER_SIZE,
             "Insufficient space in DOT buffer - APPENDING.");
  if ( dots->m_length < DOTS_BUFFER_SIZE)
  {
  dots->m_dots[dots->m_length++] = value;
  }
}


//! \brief Append to end of one Dots structure a second Dots structure.
//!
//! \param[in] dots       - address of Dots structure
//! \param[in] more_dots  - address of Dots Structure to append
//!
//! TFX_ASSERT is used to range the append operation.
//!
//! If there is not enough space in the Dots structure 'dots'
//! to append Dots structure 'more_dots' an error message is asserted
//! and the code exits.
void Dots_append_dots(Dots dots )
{
  TFX_ASSERT(dots, "Expected a non-null pointer.");
  const size_t more_dots_length = Dots_get_working_length(dots);
  TFX_ASSERT(dots->m_length + more_dots_length < DOTS_BUFFER_SIZE,
             "Insufficient space in buffer - APPENDING.");

  if ( dots->m_length + more_dots_length < DOTS_BUFFER_SIZE )
  {
	    int i = 0;
	    for (i = 0; i < more_dots_length; i++)
	    {
	      const char value_i = dots->m_working_dots[i];
	      Dots_append_value(dots, value_i);
	    }
  }
}



//void Dots_append_dots(Dots dots, const Dots more_dots)
//{
//  TFX_ASSERT(dots, "Expected a non-null pointer.");
//  TFX_ASSERT(more_dots, "Expected a non-null pointer.");
//  const size_t more_dots_length = Dots_get_length(more_dots);
//  TFX_ASSERT(dots->m_length + more_dots_length < DOTS_BUFFER_SIZE,
//             "Insufficient space in buffer - APPENDING.");
//  {
//    int i = 0;
//    for (i = 0; i < more_dots_length; i++) {
//      const char value_i = more_dots->m_dots[i];
//      Dots_append_value(dots, value_i);
//    }
//  }
//}
