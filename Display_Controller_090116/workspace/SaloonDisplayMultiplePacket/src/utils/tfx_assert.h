// Assertion mechanism.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef TFX_ASSERT_H_
#define TFX_ASSERT_H_
#define DEBUG2


#if defined(DEBUG2)

  // Handy assertion macro.
  #define TFX_ASSERT(EXPR, MSG) \
    if (!(EXPR)) { \
      tfx_assert(MSG); \
    }

  // Assertion handler.
  void tfx_assert(const char* message_text);

#else // DEBUG

  // Assertion macro is defined, but a no-op.
  #define TFX_ASSERT(EXPR, MSG)

#endif // DEBUG

#endif /* TFX_ASSERT_H_ */
