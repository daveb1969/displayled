/*! \file ******************************************************************
 *
 * \brief passengercounterbuffer.h 		Passenger Counter.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage..
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carraiges on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#ifndef PASSENGERCOUNTERBUFFER_H_
#define PASSENGERCOUNTERBUFFER_H_


struct passengercounterbuffer_str
{
	volatile unsigned short  countIn;			     		       // Passenger Counter In Count Value
	volatile unsigned short  countOut;         		      	   // Passenger Counter Out Count Value
    volatile unsigned short  crcPasses;						   // Passenger Counter crc Passes Count Value
    volatile unsigned short  crcFails;                            // Passenger Counter crc Fails Count Value
};

typedef struct passengercounterbuffer_str* PassengerCounterBuffer;

// Constructor.
//
//
PassengerCounterBuffer PassengerCounterBuffer_construct();

// Destructor.
//
//
void PassengerCounterBuffer_destroy(PassengerCounterBuffer passengerCounterBuffer);


#endif /* PASSENGERCOUNTER_H_ */
