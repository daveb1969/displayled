// A generic list "class" creation mechanism.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

// Example usage.
//
// This will create a list struct called MyStructList, which is a list of
// MyStruct structs.
//
// MyStructList.h:
// ---------------
//
// #include "MyStruct.h"
//
// #include "genericlist.h"
//
// GENERIC_LIST_DECLARE(MyStruct);
//
//
// MyStructList.c:
// ---------------
//
// #include "MyStructList.h"
//
// // N.B. The maximum list size is specified as an implementation parameter
// //      only.
// GENERIC_LIST_IMPLEMENT(MyStruct, 32);

#ifndef GENERICLIST_H_
#define GENERICLIST_H_

// Standard C library includes.
#include "stdlib.h"

// Project includes.
#include "tfx_assert.h"

// Definition macro.
#define GENERIC_LIST_DECLARE(TYPE) \
  typedef struct TYPE##List_str* TYPE##List; \
  \
  /* Constructor. */ \
  TYPE##List TYPE##List_construct(void); \
  \
  /* Destructor. */ \
  void TYPE##List_destroy(TYPE##List list); \
  \
  /* Get the length of the list. */ \
  size_t TYPE##List_get_length(const TYPE##List list); \
  \
  /* Get the item at the specified position in the list. */ \
  TYPE TYPE##List_get_item(const TYPE##List list, int position); \
  \
  /* Append the specified item to the list. */ \
  /* */ \
  /* N.B. The list assumes ownership of the item. */ \
  void TYPE##List_append(TYPE##List list, const TYPE item);


// Implementation macro.
#define GENERIC_LIST_IMPLEMENT(TYPE, MAX_SIZE) \
  /* "Object" structure definition. */ \
  struct TYPE##List_str { \
    TYPE m_items[MAX_SIZE]; \
    size_t m_length; \
  }; \
  \
  /* Constructor. */ \
  TYPE##List TYPE##List_construct(void) { \
    TYPE##List list = ( TYPE##List )malloc(sizeof(struct TYPE##List_str)); \
    TFX_ASSERT(list, "Failed to allocate memory."); \
    list->m_length = 0; \
    return list; \
  } \
  \
  /* Destructor. */ \
  void TYPE##List_destroy(TYPE##List list) { \
    TFX_ASSERT(list, "Expected a non-null pointer."); \
    /* Delete our items. */ \
    { \
      int i = 0; \
      for (i = 0; i < MAX_SIZE; i++) { \
        TYPE item_i = TYPE##List_get_item(list, i); \
        if (item_i != NULL) { \
          TYPE##_destroy(item_i); \
          list->m_items[i] = NULL; \
        } \
      } \
    } \
    \
    /* Delete ourself. */ \
    list->m_length = 0; \
    free(list); \
  } \
  \
  /* Get the length of the list. */ \
  size_t TYPE##List_get_length(const TYPE##List list) \
  { \
    TFX_ASSERT(list, "Expected a non-null pointer."); \
    return list->m_length; \
  } \
  \
  /* Get the item at the specified position in the list. */ \
  TYPE TYPE##List_get_item(const TYPE##List list, int position) { \
    TFX_ASSERT(list, "Expected a non-null pointer."); \
    return (0 <= position && position < list->m_length) ? \
      list->m_items[position] : NULL; \
  } \
  \
  /* Append the specified item to the list. */ \
  /* */ \
  /* N.B. The list assumes ownership of the item. */ \
  void TYPE##List_append(TYPE##List list, const TYPE item) \
  { \
    TFX_ASSERT(list, "Expected a non-null pointer."); \
    list->m_items[list->m_length++] = item; \
    TFX_ASSERT(list->m_length < MAX_SIZE, "Insufficient space in buffer."); \
  }


#endif /* GENERICLIST_H_ */
