/*! \file ******************************************************************
 *
 * \brief passengercount.c 		Passeneger Count .
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the reading of count values from a
 *	a particular PCN-1001 Passenger Counter mounted above the doorway
 *	in the saloon carriages on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/


#include "passengercount.h"


// Project includes.
#include "tfx_assert.h"
#include "compiler.h"
#include "malloc.h"
#include "circularbuffer.h"
#include "visualtest.h"
#include "network.h"
#include "delay.h"
#include "user_board.h"
#include "displayExtern.h"
#include "slaveMasterSM.h"
#include "board.h"

extern char clkTick;



// Constructor.
//
//
PassengerCount PassengerCount_construct()
{
	PassengerCount passengerCount = ( struct passengercount_str* ) malloc(sizeof(struct passengercount_str));
	TFX_ASSERT(passengerCount, "Failed to allocate memory.");
	passengerCount->passengerCounter1 = PassengerCounter_construct();
	passengerCount->passengerCounter2 = PassengerCounter_construct();
	passengerCount->passengerCounter3 = PassengerCounter_construct();
	passengerCount->passengerCounter4 = PassengerCounter_construct();
	passengerCount->passengerCountComms = PassengerCountComms_construct();

	passengerCount->passengerCounterDataSet1 = PassengerCounterDataSet_construct();
	passengerCount->passengerCounterDataSet2 = PassengerCounterDataSet_construct();
	passengerCount->passengerCounterDataSet3 = PassengerCounterDataSet_construct();
	passengerCount->passengerCounterDataSet4 = PassengerCounterDataSet_construct();
	passengerCount->passengerCounterDataSet5 = PassengerCounterDataSet_construct();

	passengerCount->passengerCounter1->SNP_passengerCounterAddress = 0x02;
	passengerCount->passengerCounter2->SNP_passengerCounterAddress = 0x03;
	passengerCount->passengerCounter3->SNP_passengerCounterAddress = 0x04;
	passengerCount->passengerCounter4->SNP_passengerCounterAddress = 0x05;
	passengerCount->passengerCountComms->SNP_displayControllerAddress = 0x01;

	passengerCount->sendStartPassengerCounters = 0;
	passengerCount->sendStopPassengerCounters = 0;
	passengerCount->resetPassengerCounterMessageFeedback = 0;

	passengerCount->CRCFails = 0;
	passengerCount->CRCPasses = 0;

	passengerCount->startMessageSent = 0;

	passengerCount->numberPassengerCounterPacketsSent = 0;

	passengerCount->doorClosedCounter = 0;

	passengerCount->closedDoorID = 0;

    passengerCount->numberPassengerCounterAcknowledgesReceived = 0;
    passengerCount->totalNumberPassengerCounterReadCycles = 0;
    passengerCount->totalNumberAPISRebootMessages = 0;
    passengerCount->pcEOLMessageModeEnabled = 0;

	return passengerCount;

}

// Destructor.
//
//
void PassengerCount_destroy(PassengerCount passengerCount)
{

	PassengerCounter_destroy(passengerCount->passengerCounter1 );
	PassengerCounter_destroy(passengerCount->passengerCounter2 );
	PassengerCounter_destroy(passengerCount->passengerCounter3 );
	PassengerCounter_destroy(passengerCount->passengerCounter4 );
	PassengerCountComms_destroy(passengerCount->passengerCountComms);
	free(passengerCount);
}

void PassengerCount_delay(unsigned long count){

	long delay1 = 0;
	long delay2 = 0;
	long delay3 = 0;
	while (delay1 < count){
		delay1++;
		while (delay2 < count){
				delay2++;
				while (delay3 < count){
						delay3++;
					}
			}
	}
}

void PassengerCount_readCounters(PassengerCount passengerCount)
{
	signed char readOperation = 0;
	unsigned char passengerCounter = 0;
	unsigned char readCounterValueRetries = 4;

	//Cycle through all four passenger counters and read their count values.
	//The do loop continues until the readCountValue state machine returns:
	//	readOperation = -1 The read operation failed to complete after readCounterValueRetries have been attempted.
	//  readOperation = 1  The read operation completed successfully

	for (passengerCounter = 1; passengerCounter < 5; passengerCounter++)
	{
		do
		{
		readOperation = PassengerCount_readCountValue( passengerCount, passengerCounter, readCounterValueRetries);
		}
		while ( (readOperation != -1) && (readOperation != 1)  );
	}
}



unsigned char PassengerCount_sendReadCounterMessage(PassengerCount passengerCount, int passengerCounter)
{
    unsigned char gcountReady = 0;

    switch (passengerCounter)
    {
    case 1:
        gcountReady = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter1->SNP_passengerCounterAddress,
        		                    "gcounters", NULL, 0 );
    break;
    case 2:
        gcountReady = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter2->SNP_passengerCounterAddress,
        		                    "gcounters", NULL, 0 );
    break;
    case 3:
        gcountReady = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter3->SNP_passengerCounterAddress,
        		                    "gcounters", NULL, 0 );
    break;
    case 4:
        gcountReady = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter4->SNP_passengerCounterAddress,
        		                    "gcounters", NULL, 0 );
    break;
    default:
    	gcountReady = 0;
    break;
    }
    return gcountReady;

}


unsigned char PassengerCount_sendResetCounterMessage(PassengerCount passengerCount, int passengerCounter)
{
    unsigned char messageSent = 0;

    switch (passengerCounter)
    {
    case 1:
    	messageSent = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter1->SNP_passengerCounterAddress,
        		                    "reset", NULL, 0 );
    break;
    case 2:
    	messageSent = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter2->SNP_passengerCounterAddress,
        		                    "reset", NULL, 0 );
    break;
    case 3:
    	messageSent =PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter3->SNP_passengerCounterAddress,
        		                    "reset", NULL, 0 );
    break;
    case 4:
    	messageSent = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
        		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
        		                    passengerCount->passengerCounter4->SNP_passengerCounterAddress,
        		                    "reset", NULL, 0 );
    break;
    default:
    	messageSent = 0;
    break;
    }
    return messageSent;

}


unsigned char PassengerCount_resetCounters( PassengerCount passengerCount )
{
	unsigned char messageSentCounter1 = 0;
	unsigned char messageSentCounter2 = 0;
	unsigned char messageSentCounter3 = 0;
	unsigned char messageSentCounter4 = 0;

#if PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter1 = PassengerCount_sendResetCounterMessage( passengerCount, 1 );

#if PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter2 = PassengerCount_sendResetCounterMessage( passengerCount, 2 );

	#if PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif
	messageSentCounter3 = PassengerCount_sendResetCounterMessage( passengerCount, 3 );
#if PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter4 = PassengerCount_sendResetCounterMessage( passengerCount, 4 );

#if PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION == PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif
	if ( messageSentCounter1 && messageSentCounter2 && messageSentCounter3 && messageSentCounter4 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

unsigned char PassengerCount_enableCounters(PassengerCount passengerCount)
{
	unsigned char messageSentCounter1 = 0;
	unsigned char messageSentCounter2 = 0;
	unsigned char messageSentCounter3 = 0;
	unsigned char messageSentCounter4 = 0;

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter1 = PassengerCountComms_enableCounter( passengerCount->passengerCountComms, \
	  		                              passengerCount->passengerCounter1 );

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter2 = PassengerCountComms_enableCounter( passengerCount->passengerCountComms, \
	  		                              passengerCount->passengerCounter2 );

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter3 = PassengerCountComms_enableCounter( passengerCount->passengerCountComms, \
			                              passengerCount->passengerCounter3 );

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter4 = PassengerCountComms_enableCounter( passengerCount->passengerCountComms, \
	 		                              passengerCount->passengerCounter4 );

	#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif
	if ( messageSentCounter1 && messageSentCounter2 && messageSentCounter3 && messageSentCounter4 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


unsigned char PassengerCount_disableCounters(PassengerCount passengerCount)
{
	unsigned char messageSentCounter1 = 0;
	unsigned char messageSentCounter2 = 0;
	unsigned char messageSentCounter3 = 0;
	unsigned char messageSentCounter4 = 0;

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter1 = PassengerCountComms_disableCounter( passengerCount->passengerCountComms,
									   passengerCount->passengerCounter1 );
#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter2 = PassengerCountComms_disableCounter( passengerCount->passengerCountComms,
									   passengerCount->passengerCounter2 );

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter3 = PassengerCountComms_disableCounter( passengerCount->passengerCountComms,
									   passengerCount->passengerCounter3 );

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif

	messageSentCounter4 = PassengerCountComms_disableCounter( passengerCount->passengerCountComms,
									   passengerCount->passengerCounter4 );

#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif
	if ( messageSentCounter1 && messageSentCounter2 && messageSentCounter3 && messageSentCounter4 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}



unsigned char PassengerCount_setLedBrightness(PassengerCount passengerCount,
											PassengerCounter passengerCounter,
												char ledBrightnessLevel )
{
	unsigned char messageSent = 0;
	messageSent = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
    		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
    		                    passengerCounter->SNP_passengerCounterAddress,
    		                    "sled", &ledBrightnessLevel, sizeof(ledBrightnessLevel));

	return messageSent;
}


unsigned char PassengerCount_setAutoLedBrightness(PassengerCount passengerCount,
											PassengerCounter passengerCounter,
												char enableAutoLed )
{
	unsigned char messageSent = 0;
	messageSent = PassengerCountComms_send(passengerCount->passengerCountComms->buffer, \
    		                   passengerCount->passengerCountComms->SNP_displayControllerAddress, \
    		                    passengerCounter->SNP_passengerCounterAddress,
    		                    "autoled", &enableAutoLed, sizeof(enableAutoLed));

    return messageSent;
}


signed char PassengerCount_readCountValue( PassengerCount passengerCount,
		                                     unsigned char passengerCounter,
		                                       unsigned char numberRetries)
{
	static unsigned char step = 0;
	static unsigned char stepOneShot = 0;
	static unsigned char counter = 0;
	static signed char success = 0;
	static unsigned char tryCounter = 0;
	char temp;

	if (step == 0)
	{
		tryCounter = 0;
		step++;
	}

	if (step == 1)
	{
		counter = 0;
		//Empty the Passenger Count Comms object circular Receive Buffer.
		while ( passengerCount->passengerCountComms->receiveCircularBuffer->count > 0 )
		{
			temp = cb_pop_front(passengerCount->passengerCountComms->receiveCircularBuffer, &temp);
			counter++;
		}
		step++;
	}

	//Send the Read Counter Message to the Passenger Counter
	//This will enable the USART Rx Interrupt service routine
	if (step == 2)
	{
		PassengerCount_sendReadCounterMessage( passengerCount, passengerCounter );
		step++;
	}

	//Wait 100 ms to allow time for the
	//reply message to be received from the Passenger Counter
	//Stop the Rx interrupt routine
	if (step == 3 && stepOneShot == 0)
	{
#if PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_ENABLED
	{
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
		slaveMasterSM( thisDisplay->network );
	}
#elif PC_SLAVE_MASTER_FUNCTION==PC_SLAVE_MASTER_DISABLED
	{

	}
#else
#error No PC_SLAVE_MASTER_FUNCTION defined
#endif
		step++;
	}


	if (step == 4)
	{
		//Has the gcounters message been received back from the passenger counter?
		//If the gcounters message has been received correctly the receiveCircular Buffer will contain
		//at least the number of characters required for a gcounter message
		if ( passengerCount->passengerCountComms->receiveCircularBuffer->count > 10)
		{
		  step++;
		}
		//Have we tried the designated number of times to read the current passenger counter count values?
		//If we have not then place the state machine in step 1 else move onto step 6
		//placing the sentinel value into the passenger counter count values and increment the failed CRC counter.
		else if (tryCounter < numberRetries)
		{
			tryCounter++;
			step = 1;
			return 2;
		}
		else
		{
			success = -1;
			step = 6;
		}
	}

	//Push the data from the Passenger Counter Circular Receive Buffer
	//into the Passenger Counter Comms' object Buffer
	if (step == 5)
	{
		success = PassengerCountComms_receiveCircular(passengerCount->passengerCountComms);
		step++;
	}

	//Decode the message found in the Passenger Counter Comms' object receive buffer.
	if (step == 6)
	{
		//If the data has been received successfully across the RS485/RS232 channel and placed into the
		//Passenger Counter Comms' object Receive Buffer then decode the message in the
		//Passenger Counter Comms' object for passenger counter data.
		if ( success == 1 )
		{
			passengerCount->CRCPasses++;
			switch (passengerCounter)
			{
				case 1:
					PassengerCountComms_messageDecode(passengerCount->passengerCountComms,
														passengerCount->passengerCounter1 );
					passengerCount->passengerCounter1->crcPasses++;
					break;
				case 2:
					PassengerCountComms_messageDecode(passengerCount->passengerCountComms,
														passengerCount->passengerCounter2 );
					passengerCount->passengerCounter2->crcPasses++;
					break;
				case 3:
					PassengerCountComms_messageDecode(passengerCount->passengerCountComms,
														passengerCount->passengerCounter3 );
					passengerCount->passengerCounter3->crcPasses++;
					break;
				case 4:
					PassengerCountComms_messageDecode(passengerCount->passengerCountComms,
														passengerCount->passengerCounter4 );
					passengerCount->passengerCounter4->crcPasses++;
					break;
				default:
					break;
			}
		step = 0;
		return 1;
		}
		//If the data has NOT been received successfully across the RS485/RS232 channel and placed into the
		//Passenger Counter Comms' object Receive Buffer then return indicating the failure to receive the
		//passener counter data.
		if ( success == -1 )
		{
			passengerCount->CRCFails++;
			switch (passengerCounter)
			{
				case 1:
					passengerCount->passengerCounter1->countIn = 65535;
					passengerCount->passengerCounter1->countOut = 65535;
					passengerCount->passengerCounter1->crcFails++;
					break;
				case 2:
					passengerCount->passengerCounter2->countIn = 65535;
					passengerCount->passengerCounter2->countOut = 65535;
					passengerCount->passengerCounter2->crcFails++;
					break;
				case 3:
					passengerCount->passengerCounter3->countIn = 65535;
					passengerCount->passengerCounter3->countOut = 65535;
					passengerCount->passengerCounter3->crcFails++;
					break;
				case 4:
					passengerCount->passengerCounter4->countIn = 65535;
					passengerCount->passengerCounter4->countOut = 65535;
					passengerCount->passengerCounter4->crcFails++;
					break;
				default:
					break;
			}
			step = 0;
			return -1;
		}
	}
	return -1;

}

void PassengerCount_disableAutoLEDBrightness(PassengerCount passengerCount)
{
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter1,
    		                               0 );
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter2,
    		                               0 );
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter3,
    		                               0 );
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter4,
    		                               0 );
}

void PassengerCount_enableAutoLEDBrightness(PassengerCount passengerCount)
{
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter1,
    		                               1 );
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter2,
    		                               1 );
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter3,
    		                               1 );
	PassengerCount_setAutoLedBrightness( passengerCount,
    		                             passengerCount->passengerCounter4,
    		                               1 );
}



void PassengerCounter_ledsON( PassengerCount passengerCount, PassengerCounter passengerCounter )
{
    PassengerCount_setLedBrightness( passengerCount,
    		                             passengerCounter,
    		                               255 );

}

void PassengerCounter_ledsOFF( PassengerCount passengerCount, PassengerCounter passengerCounter )
{

    PassengerCount_setLedBrightness( passengerCount,
										passengerCounter,
    		                               0 );

}


void PassengerCount_updateCounterVisualTest( Displayline displayLine, PassengerCount passengerCount )
{
	static unsigned int lastCount1In = 0;
	static unsigned int lastCount1Out = 0;
	static unsigned int lastCount2In = 0;
	static unsigned int lastCount2Out = 0;
	static unsigned int lastCount3In = 0;
	static unsigned int lastCount3Out = 0;
	static unsigned int lastCount4In = 0;
	static unsigned int lastCount4Out = 0;

    if ( lastCount1In != passengerCount->passengerCounter1->countIn || \
    	 lastCount1Out != passengerCount->passengerCounter1->countOut || \
    	 lastCount2In != passengerCount->passengerCounter2->countIn || \
    	 lastCount2Out != passengerCount->passengerCounter2->countOut || \
    	 lastCount3In != passengerCount->passengerCounter3->countIn || \
    	 lastCount3Out != passengerCount->passengerCounter3->countOut || \
    	 lastCount4In != passengerCount->passengerCounter4->countIn || \
    	 lastCount4Out != passengerCount->passengerCounter4->countOut ||
    	 thisDisplay->visualtest->updateDisplayAtleastOnce == 1
    )
    {
        Visualtest_passengerCounter( displayLine, passengerCount );

        lastCount1In = passengerCount->passengerCounter1->countIn;
        lastCount1Out = passengerCount->passengerCounter1->countOut;
        lastCount2In = passengerCount->passengerCounter2->countIn;
        lastCount2Out = passengerCount->passengerCounter2->countOut;
        lastCount3In = passengerCount->passengerCounter3->countIn;
        lastCount3Out = passengerCount->passengerCounter3->countOut;
        lastCount4In = passengerCount->passengerCounter4->countIn;
        lastCount4Out = passengerCount->passengerCounter4->countOut;
    }
    thisDisplay->visualtest->updateDisplayAtleastOnce = 0;
}

void PassengerCount_startCounters(PassengerCount passengerCount)
{
	//Enable passenger counter(s)
	PassengerCount_enableCounters( passengerCount );

	//Reset the passenger counter(s) count values
	PassengerCount_resetCounters( passengerCount );
}

void PassengerCount_stopCounters(PassengerCount passengerCount)
{
	PassengerCount_readCounters( passengerCount);

	//DB Changed

	//Push Set of passenger counter values into Data Set storage before
	//creation of Passenger counter feedback message.

	PassengerCount_UpdateCounterBuffers( passengerCount);

	//Dave Added 13/09/2011
	//-----------------------------------------------------------
	//Enable passenger counter(s)
	PassengerCount_enableCounters( passengerCount );

	//Reset the passenger counter(s) count values
	PassengerCount_resetCounters( passengerCount );
	//-----------------------------------------------------------

}

void PassengerCount_UpdateCounterBuffers(PassengerCount passengerCount)
{

	switch (passengerCount->doorClosedCounter)
	{
	case 1:
		passengerCount->passengerCounterDataSet1->passengerCounter1->countIn = (unsigned short) (passengerCount->passengerCounter1->countIn);
		passengerCount->passengerCounterDataSet1->passengerCounter2->countIn = (unsigned short) (passengerCount->passengerCounter2->countIn);
		passengerCount->passengerCounterDataSet1->passengerCounter3->countIn = (unsigned short) (passengerCount->passengerCounter3->countIn);
		passengerCount->passengerCounterDataSet1->passengerCounter4->countIn = (unsigned short) (passengerCount->passengerCounter4->countIn);

		passengerCount->passengerCounterDataSet1->passengerCounter1->countOut = (unsigned short) (passengerCount->passengerCounter1->countOut);
		passengerCount->passengerCounterDataSet1->passengerCounter2->countOut = (unsigned short) (passengerCount->passengerCounter2->countOut);
		passengerCount->passengerCounterDataSet1->passengerCounter3->countOut = (unsigned short) (passengerCount->passengerCounter3->countOut);
		passengerCount->passengerCounterDataSet1->passengerCounter4->countOut = (unsigned short) (passengerCount->passengerCounter4->countOut);


		passengerCount->passengerCounterDataSet1->passengerCounter1->crcFails = passengerCount->passengerCounter1->crcFails;
		passengerCount->passengerCounterDataSet1->passengerCounter2->crcFails = passengerCount->passengerCounter2->crcFails;
		passengerCount->passengerCounterDataSet1->passengerCounter3->crcFails = passengerCount->passengerCounter3->crcFails;
		passengerCount->passengerCounterDataSet1->passengerCounter4->crcFails = passengerCount->passengerCounter4->crcFails;

		passengerCount->passengerCounterDataSet1->passengerCounter1->crcPasses = passengerCount->passengerCounter1->crcPasses;
		passengerCount->passengerCounterDataSet1->passengerCounter2->crcPasses = passengerCount->passengerCounter2->crcPasses;
		passengerCount->passengerCounterDataSet1->passengerCounter3->crcPasses = passengerCount->passengerCounter3->crcPasses;
		passengerCount->passengerCounterDataSet1->passengerCounter4->crcPasses = passengerCount->passengerCounter4->crcPasses;

		passengerCount->passengerCounterDataSet1->closedDoorID = passengerCount->closedDoorID;
	break;
	case 2:
		passengerCount->passengerCounterDataSet2->passengerCounter1->countIn = (unsigned short) (passengerCount->passengerCounter1->countIn);
		passengerCount->passengerCounterDataSet2->passengerCounter2->countIn = (unsigned short) (passengerCount->passengerCounter2->countIn);
		passengerCount->passengerCounterDataSet2->passengerCounter3->countIn = (unsigned short) (passengerCount->passengerCounter3->countIn);
		passengerCount->passengerCounterDataSet2->passengerCounter4->countIn = (unsigned short) (passengerCount->passengerCounter4->countIn);

		passengerCount->passengerCounterDataSet2->passengerCounter1->countOut = (unsigned short) (passengerCount->passengerCounter1->countOut);
		passengerCount->passengerCounterDataSet2->passengerCounter2->countOut = (unsigned short) (passengerCount->passengerCounter2->countOut);
		passengerCount->passengerCounterDataSet2->passengerCounter3->countOut = (unsigned short) (passengerCount->passengerCounter3->countOut);
		passengerCount->passengerCounterDataSet2->passengerCounter4->countOut = (unsigned short) (passengerCount->passengerCounter4->countOut);

		passengerCount->passengerCounterDataSet2->passengerCounter1->crcFails = passengerCount->passengerCounter1->crcFails;
		passengerCount->passengerCounterDataSet2->passengerCounter2->crcFails = passengerCount->passengerCounter2->crcFails;
		passengerCount->passengerCounterDataSet2->passengerCounter3->crcFails = passengerCount->passengerCounter3->crcFails;
		passengerCount->passengerCounterDataSet2->passengerCounter4->crcFails = passengerCount->passengerCounter4->crcFails;

		passengerCount->passengerCounterDataSet2->passengerCounter1->crcPasses = passengerCount->passengerCounter1->crcPasses;
		passengerCount->passengerCounterDataSet2->passengerCounter2->crcPasses = passengerCount->passengerCounter2->crcPasses;
		passengerCount->passengerCounterDataSet2->passengerCounter3->crcPasses = passengerCount->passengerCounter3->crcPasses;
		passengerCount->passengerCounterDataSet2->passengerCounter4->crcPasses = passengerCount->passengerCounter4->crcPasses;

		passengerCount->passengerCounterDataSet2->closedDoorID = passengerCount->closedDoorID;
	break;
	case 3:
		passengerCount->passengerCounterDataSet3->passengerCounter1->countIn = (unsigned short) (passengerCount->passengerCounter1->countIn);
		passengerCount->passengerCounterDataSet3->passengerCounter2->countIn = (unsigned short) (passengerCount->passengerCounter2->countIn);
		passengerCount->passengerCounterDataSet3->passengerCounter3->countIn = (unsigned short) (passengerCount->passengerCounter3->countIn);
		passengerCount->passengerCounterDataSet3->passengerCounter4->countIn = (unsigned short) (passengerCount->passengerCounter4->countIn);

		passengerCount->passengerCounterDataSet3->passengerCounter1->countOut = (unsigned short) (passengerCount->passengerCounter1->countOut);
		passengerCount->passengerCounterDataSet3->passengerCounter2->countOut = (unsigned short) (passengerCount->passengerCounter2->countOut);
		passengerCount->passengerCounterDataSet3->passengerCounter3->countOut = (unsigned short) (passengerCount->passengerCounter3->countOut);
		passengerCount->passengerCounterDataSet3->passengerCounter4->countOut = (unsigned short) (passengerCount->passengerCounter4->countOut);

		passengerCount->passengerCounterDataSet3->passengerCounter1->crcFails = passengerCount->passengerCounter1->crcFails;
		passengerCount->passengerCounterDataSet3->passengerCounter2->crcFails = passengerCount->passengerCounter2->crcFails;
		passengerCount->passengerCounterDataSet3->passengerCounter3->crcFails = passengerCount->passengerCounter3->crcFails;
		passengerCount->passengerCounterDataSet3->passengerCounter4->crcFails = passengerCount->passengerCounter4->crcFails;

		passengerCount->passengerCounterDataSet3->passengerCounter1->crcPasses = passengerCount->passengerCounter1->crcPasses;
		passengerCount->passengerCounterDataSet3->passengerCounter2->crcPasses = passengerCount->passengerCounter2->crcPasses;
		passengerCount->passengerCounterDataSet3->passengerCounter3->crcPasses = passengerCount->passengerCounter3->crcPasses;
		passengerCount->passengerCounterDataSet3->passengerCounter4->crcPasses = passengerCount->passengerCounter4->crcPasses;

		passengerCount->passengerCounterDataSet3->closedDoorID = passengerCount->closedDoorID;

	break;

	case 4:
		passengerCount->passengerCounterDataSet4->passengerCounter1->countIn = (unsigned short) (passengerCount->passengerCounter1->countIn);
		passengerCount->passengerCounterDataSet4->passengerCounter2->countIn = (unsigned short) (passengerCount->passengerCounter2->countIn);
		passengerCount->passengerCounterDataSet4->passengerCounter3->countIn = (unsigned short) (passengerCount->passengerCounter3->countIn);
		passengerCount->passengerCounterDataSet4->passengerCounter4->countIn = (unsigned short) (passengerCount->passengerCounter4->countIn);

		passengerCount->passengerCounterDataSet4->passengerCounter1->countOut = (unsigned short) (passengerCount->passengerCounter1->countOut);
		passengerCount->passengerCounterDataSet4->passengerCounter2->countOut = (unsigned short) (passengerCount->passengerCounter2->countOut);
		passengerCount->passengerCounterDataSet4->passengerCounter3->countOut = (unsigned short) (passengerCount->passengerCounter3->countOut);
		passengerCount->passengerCounterDataSet4->passengerCounter4->countOut = (unsigned short) (passengerCount->passengerCounter4->countOut);

		passengerCount->passengerCounterDataSet4->passengerCounter1->crcFails = passengerCount->passengerCounter1->crcFails;
		passengerCount->passengerCounterDataSet4->passengerCounter2->crcFails = passengerCount->passengerCounter2->crcFails;
		passengerCount->passengerCounterDataSet4->passengerCounter3->crcFails = passengerCount->passengerCounter3->crcFails;
		passengerCount->passengerCounterDataSet4->passengerCounter4->crcFails = passengerCount->passengerCounter4->crcFails;

		passengerCount->passengerCounterDataSet4->passengerCounter1->crcPasses = passengerCount->passengerCounter1->crcPasses;
		passengerCount->passengerCounterDataSet4->passengerCounter2->crcPasses = passengerCount->passengerCounter2->crcPasses;
		passengerCount->passengerCounterDataSet4->passengerCounter3->crcPasses = passengerCount->passengerCounter3->crcPasses;
		passengerCount->passengerCounterDataSet4->passengerCounter4->crcPasses = passengerCount->passengerCounter4->crcPasses;

		passengerCount->passengerCounterDataSet4->closedDoorID = passengerCount->closedDoorID;

	break;
	case 5:
		passengerCount->passengerCounterDataSet5->passengerCounter1->countIn = (unsigned short) (passengerCount->passengerCounter1->countIn);
		passengerCount->passengerCounterDataSet5->passengerCounter2->countIn = (unsigned short) (passengerCount->passengerCounter2->countIn);
		passengerCount->passengerCounterDataSet5->passengerCounter3->countIn = (unsigned short) (passengerCount->passengerCounter3->countIn);
		passengerCount->passengerCounterDataSet5->passengerCounter4->countIn = (unsigned short) (passengerCount->passengerCounter4->countIn);

		passengerCount->passengerCounterDataSet5->passengerCounter1->countOut = (unsigned short) (passengerCount->passengerCounter1->countOut);
		passengerCount->passengerCounterDataSet5->passengerCounter2->countOut = (unsigned short) (passengerCount->passengerCounter2->countOut);
		passengerCount->passengerCounterDataSet5->passengerCounter3->countOut = (unsigned short) (passengerCount->passengerCounter3->countOut);
		passengerCount->passengerCounterDataSet5->passengerCounter4->countOut = (unsigned short) (passengerCount->passengerCounter4->countOut);

		passengerCount->passengerCounterDataSet5->passengerCounter1->crcFails = passengerCount->passengerCounter1->crcFails;
		passengerCount->passengerCounterDataSet5->passengerCounter2->crcFails = passengerCount->passengerCounter2->crcFails;
		passengerCount->passengerCounterDataSet5->passengerCounter3->crcFails = passengerCount->passengerCounter3->crcFails;
		passengerCount->passengerCounterDataSet5->passengerCounter4->crcFails = passengerCount->passengerCounter4->crcFails;

		passengerCount->passengerCounterDataSet5->passengerCounter1->crcPasses = passengerCount->passengerCounter1->crcPasses;
		passengerCount->passengerCounterDataSet5->passengerCounter2->crcPasses = passengerCount->passengerCounter2->crcPasses;
		passengerCount->passengerCounterDataSet5->passengerCounter3->crcPasses = passengerCount->passengerCounter3->crcPasses;
		passengerCount->passengerCounterDataSet5->passengerCounter4->crcPasses = passengerCount->passengerCounter4->crcPasses;

		passengerCount->passengerCounterDataSet5->closedDoorID = passengerCount->closedDoorID;

	break;
	default:
	break;
	}
}


