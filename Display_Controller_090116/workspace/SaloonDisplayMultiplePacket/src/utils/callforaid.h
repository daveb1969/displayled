/*
 * callforaid.h
 *
 *  Created on: 8 Sep 2010
 *      Author: DAVE
 */

#ifndef CALLFORAID_H_
#define CALLFORAID_H_

#include "opto.h"

struct callforaid_str
{
    Opto opto_CFA1;
    Opto opto_CFA2;
    Opto opto_RESET;
};

typedef volatile struct callforaid_str* Callforaid;

// Constructor.
//
//
Callforaid Callforaid_construct();

// Destructor.
//
//
void Callforaid_destroy(Callforaid callforaid);


#endif /* CALLFORAID_H_ */
