// A "class" to represent the dot representation of a message.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef DOTS_H_
#define DOTS_H_

// Standard C library includes.
#include "stddef.h"

typedef struct dots_str* Dots;

// Constructor.
Dots Dots_construct();

// Destructor.
void Dots_destroy(Dots dots);

// Get the length of the list.
size_t Dots_get_length(const Dots dots);

// Get the value in the specified position.
char Dots_get_value(const Dots dots, int position);

// Clear the list.
void Dots_clear(Dots dots);

// Set the list to the specified array of values.
void Dots_set(Dots dots, const unsigned char* raw_dots, size_t length);

// Append the specified value to the list.
void Dots_append_value(Dots dots, char value);

// Append the specified list to the list.
void Dots_append_dots(Dots dots, const Dots more_dots);

#endif /* DOTS_H_ */
