/*! \file ******************************************************************
 *
 * \brief callforaid.c 		Call For Aid.
 *
 * This file creates the data structure used within the call for aid service.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#include "callforaid.h"
#include "tfx_assert.h"
#include "malloc.h"
#include "board.h"
#include "gpio.h"

extern char clkTick;

//! \brief Callforaid data structure constructor
//!
//! Create Callforaid data structure off the heap.
//!
//! The Callforaid data structure is used to store the GPIO information
//! retrieved from the de-bounced call for aid buttons, found within the train
//! carriage, wired as inputs to the display using opto-isolated inputs.
//!
//!
//! \return Return Callforaid*
//!
Callforaid Callforaid_construct() {
	Callforaid callforaid = ( struct callforaid_str* ) malloc(sizeof(struct callforaid_str));
	TFX_ASSERT( callforaid, "Failed to allocate memory.");
	callforaid->opto_CFA1 = Opto_construct();
    callforaid->opto_CFA2 = Opto_construct();
    callforaid->opto_RESET = Opto_construct();
	return callforaid;
}

//! \brief Callforaid data structure destructor
//!
//! Callforaid data structure off the heap.
//!
//! \param[in] callforaid address of Callforaid data structure to destroy
void Callforaid_destroy(Callforaid callforaid ) {
	TFX_ASSERT( callforaid, "Expected a non-null pointer.");
	free( callforaid );
}










