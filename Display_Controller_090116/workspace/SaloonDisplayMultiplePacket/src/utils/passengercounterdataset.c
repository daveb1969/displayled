/*! \file ******************************************************************
 *
 * \brief SNPserialport.c 		SNP Serial Port.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage.
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carriages on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

// Standard C library includes.
#include "stdlib.h"
#include "string.h"

// Project includes.
#include "../utils/tfx_assert.h"
#include "passengercounterdataset.h"
#include "usart.h"
#include "gpio.h"
#include "user_board.h"


//! \brief Passenger Counter Data Set constructor
//!
//! Create Passenger Counter Data Set data structure off the heap.
//!
//! \return Return PassengerCounterDataSet*
//!
PassengerCounterDataSet PassengerCounterDataSet_construct() {
	PassengerCounterDataSet passengerCounterDataSet = ( struct passengercounterdataset_str* ) malloc(sizeof(struct passengercounterdataset_str));
	TFX_ASSERT( passengerCounterDataSet , "Failed to allocate memory.");
	passengerCounterDataSet->passengerCounter1 = PassengerCounterBuffer_construct();
	passengerCounterDataSet->passengerCounter2 = PassengerCounterBuffer_construct();
	passengerCounterDataSet->passengerCounter3 = PassengerCounterBuffer_construct();
	passengerCounterDataSet->passengerCounter4 = PassengerCounterBuffer_construct();
	passengerCounterDataSet->closedDoorID = 0;
	return passengerCounterDataSet;
}

//! \brief Passenger Counter Data Set destructor
//!
//!
//! Remove Passenger Counter Data Set data structure off the heap.
//!
void PassengerCounterDataSet_destroy(PassengerCounterDataSet passengerCounterDataSet) {
	TFX_ASSERT( passengerCounterDataSet , "Expected a non-null pointer.");
	free(passengerCounterDataSet);
}



