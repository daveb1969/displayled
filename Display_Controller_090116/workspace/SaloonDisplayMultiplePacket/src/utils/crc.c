/*! \file ******************************************************************
 *
 * \brief crc.c 		Cyclic Redundancy Check.
 *
 * This file creates and provides services for Cyclic Redundancy Checking.
 * .
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The service provided allow the calculation of a CRC value from a
 *	buffer of data.
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/


#include "crc.h"
#include <string.h>

//! \brief Static CRC table used to calculate CRC values.
//!
//!
static const unsigned short CRC_crcTable[MAX_CRC_DIGITS] =
{
        0,32773,32783,   10,32795,   30,   20,32785,32819,   54,
       60,32825,   40,32813,32807,   34,32867,  102,  108,32873,
      120,32893,32887,  114,   80,32853,32863,   90,32843,   78,
       68,32833,32963,  198,  204,32969,  216,32989,32983,  210,
      240,33013,33023,  250,33003,  238,  228,32993,  160,32933,
    32943,  170,32955,  190,  180,32945,32915,  150,  156,32921,
      136,32909,32903,  130,33155,  390,  396,33161,  408,33181,
    33175,  402,  432,33205,33215,  442,33195,  430,  420,33185,
      480,33253,33263,  490,33275,  510,  500,33265,33235,  470,
      476,33241,  456,33229,33223,  450,  320,33093,33103,  330,
    33115,  350,  340,33105,33139,  374,  380,33145,  360,33133,
    33127,  354,33059,  294,  300,33065,  312,33085,33079,  306,
      272,33045,33055,  282,33035,  270,  260,33025,33539,  774,
      780,33545,  792,33565,33559,  786,  816,33589,33599,  826,
    33579,  814,  804,33569,  864,33637,33647,  874,33659,  894,
      884,33649,33619,  854,  860,33625,  840,33613,33607,  834,
      960,33733,33743,  970,33755,  990,  980,33745,33779, 1014,
     1020,33785, 1000,33773,33767,  994,33699,  934,  940,33705,
      952,33725,33719,  946,  912,33685,33695,  922,33675,  910,
      900,33665,  640,33413,33423,  650,33435,  670,  660,33425,
    33459,  694,  700,33465,  680,33453,33447,  674,33507,  742,
      748,33513,  760,33533,33527,  754,  720,33493,33503,  730,
    33483,  718,  708,33473,33347,  582,  588,33353,  600,33373,
    33367,  594,  624,33397,33407,  634,33387,  622,  612,33377,
      544,33317,33327,  554,33339,  574,  564,33329,33299,  534,
      540,33305,  520,33293,33287,  514
};


static char testData[12] =
{
  0x01, 0x02, 0x01, 0x01, 0x06, 0x00,
  0x73, 0x6C, 0x65, 0x64, 0x00, 0x64
};


//! \brief Calculate CRC 16 bit value from Buffer of data.
//!
//! \param[in] pBlock	- pointer to buffer of char data
//! \param[in] Number	- number of char's in buffer of data
//! \param[in] pCrc     - pointer to SHORT into which the calculated 16 bit CRC value will be placed
//!
void CRC_calcCrc16Block(
char *pBlock,				// Pointer to start of block
short Number, 		        // Number of bytes i block
short *pCrc)				// Will be updated with CRC16
{
    *pCrc = -1;
    while ( Number > 0 )
    {
        *pCrc = CRC_crcTable[((*pCrc >> (CRC_WIDTH - BYTE_WIDTH)) ^ *pBlock++) &
                        CRC_MASK] ^ (*pCrc << BYTE_WIDTH);
        Number--;
    }
}


unsigned char CRC_test1()
{
   static short crcResult = 0;
   char Block[] = {"abcdefghijk"};

   CRC_calcCrc16Block( Block, strlen(Block), &crcResult);
   if ( crcResult == 0x6c4d )
   {
	     //CRC calculated correctly
	    return 1;
   }
   else
   {
	    //CRC Calulation Failed
	    return 0;
   }

}

unsigned char CRC_test2()
{
   static short crcResult;

   CRC_calcCrc16Block ( testData, 12, &crcResult );

   //if ( crcResult == 0x9310 )
   if ( crcResult == -27888 )
   {
	   //CRC calculated correctly
	    return 1;
   }
	    //CRC Calculation Failed
	    return 0;

}


unsigned char CRC_test3()
{
   static short crcResult;
   char Block[] = {"\\T015\\X1\\F0\\S1\\C1\\P112\\M11\\J00Would customers please remain seated until the attachment process has been completed. Thank You."};

   CRC_calcCrc16Block( Block, strlen(Block), &crcResult);
   //if ( crcResult == 0x6c4d )
   if ( crcResult == 3366 )
   {
	     //CRC calculated correctly
	    return 1;
   }
	    //CRC Calulation Failed
	    return 0;
}

unsigned char CRC_test4()
{
   static short crcResult;
   char Block[] = {0x05, 0x01, 0xFC, 0x42, 0xB2, 0x00, 0x9F, 0xFF, 0x00, 0x00, 0x00, 0x11, 0x08,0x01, 0x00};

   CRC_calcCrc16Block( Block, 15, &crcResult);

   //if ( crcResult == 0x1318 )
   if ( crcResult == 4888 )

   {
	     //CRC calculated correctly
	    return 1;
   }
	    //CRC Calulation Failed
	    return 0;
}


unsigned char findEndian()
{
	static short i = 1;
	static unsigned char *pc = (unsigned char *)&i;

	if ( *pc == 1 )
	{
		//Little Endian
	    return 0;
	}
	else
	{
		//Big Endian
	    return 1;
	}

}





unsigned char CRC_test5()
{
   short crcResult = -1;
   char Block[] = {0x05, 0x01, 0xFC, 0x42, 0xB2, 0x00, 0x9F, 0xFF, 0x00, 0x00, 0x00, 0x11, 0x08,0x01, 0x00};

   CRC_calcCrc16Block( Block, 15, &crcResult);

   //if ( crcResult == 0x1318 )
   if ( crcResult == 4888 )

   {
	     //CRC calculated correctly
	    return 1;
   }
	    //CRC Calulation Failed
	    return 0;
}
