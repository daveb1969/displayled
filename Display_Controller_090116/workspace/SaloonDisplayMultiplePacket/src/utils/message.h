// A "class" to represent messages.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "dots.h"
//#include "display.h"
#include "arraylist.h"
#include "board.h"


#define ESCAPE_HEADER_LENGTH 11


#if DISPLAY_TYPE == SALOON_DISPLAY
// "Object" structure definition.
//#define MESSAGE_BUFFER_SIZE 720
//DB CHANGED START
#define MESSAGE_BUFFER_SIZE 800
//DB CHANGED END
struct message_str {
  volatile char m_message_text[MESSAGE_BUFFER_SIZE];
  volatile size_t m_length;
};

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
// "Object" structure definition.
#define MESSAGE_BUFFER_SIZE 150
struct message_str {
  volatile char m_message_text[MESSAGE_BUFFER_SIZE];
  volatile size_t m_length;
};
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
// "Object" structure definition.
#define MESSAGE_BUFFER_SIZE 150
struct message_str {
  volatile char m_message_text[MESSAGE_BUFFER_SIZE];
  volatile size_t m_length;
};
#else
  #error No DISPLAY_TYPE defined
#endif

typedef struct message_str* Message;

// Constructor, taking message text as input.
//
// If message_text is NULL, then an empty message is created.
Message Message_construct(const char* message_text);

// Destructor.
void Message_destroy(Message message);

// Get the text of the message.
const char* Message_get_message_text(const Message message);

// Get the length of the message, in characters.
size_t Message_get_length(const Message message);

// Clear the message.
void Message_clear(Message message);

// Set the message to the specified text.
void Message_set(Message message, const char* message_text);

// Append the specified character to the message.
void Message_append_char(Message message, char c);

// Append the specified text to the message.
void Message_append_text(Message message, const char* more_text);


long getHeapFreeSize();

#endif /* MESSAGE_H_ */
