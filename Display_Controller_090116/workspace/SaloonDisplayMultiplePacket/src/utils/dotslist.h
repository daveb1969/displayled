// A list of dot representations of messages.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

#ifndef DOTSLIST_H_
#define DOTSLIST_H_

// Project includes.
#include "dots.h"
#include "genericlist.h"

GENERIC_LIST_DECLARE(Dots);
//typedef struct DotsList_str* DotsList;


#endif /* DOTSLIST_H_ */
