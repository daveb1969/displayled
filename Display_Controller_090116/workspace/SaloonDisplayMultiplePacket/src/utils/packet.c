/*
 * packet.c
 *
 *  Created on: 26 Aug 2010
 *      Author: DAVE
 */


// Standard C library includes.
#include "stdlib.h"
#include "string.h"

// Project includes.
#include "tfx_assert.h"
#include "compiler.h"
#include "malloc.h"
#include "packet.h"


// Constructor.
//
// If message_text is NULL, then an empty message is created.
Datagram Datagram_construct() {
  Datagram datagram = ( struct datagram_str* ) malloc(sizeof(struct datagram_str));
  TFX_ASSERT( datagram, "Failed to allocate memory.");
  Datagram_clear( datagram );
  return datagram;
}

// Destructor.
void Datagram_destroy(Datagram datagram) {
  TFX_ASSERT( datagram, "Expected a non-null pointer.");
  free(datagram);
}


// Get the text of the message.
const char* Datagram_get_payload(const Datagram datagram) {
  TFX_ASSERT( datagram, "Expected a non-null pointer.");
  return datagram->dataContent;
}

// Get the transactionID.
unsigned char  Datagram_get_transactionID(const Datagram datagram) {
  TFX_ASSERT( datagram, "Expected a non-null pointer.");
  return datagram->transactionID;
}



// Get the transactionID.
unsigned char  Datagram_get_messageSubType(const Datagram datagram) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  return datagram->messageSubType;
}


// Get the numberOfFrames.
unsigned char  Datagram_get_numberOfFrames(const Datagram datagram) {
  TFX_ASSERT( datagram, "Expected a non-null pointer.");
  return datagram->numberOfFrames;
}


// Get the frameNumber.
unsigned char Datagram_get_frameNumber(const Datagram datagram) {
  TFX_ASSERT( datagram, "Expected a non-null pointer.");
  return datagram->frameNumber;
}


// Set the Datagram payload to the specified text.
void Datagram_set_payload(Datagram datagram, const char* payload, unsigned char length)
{
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  if (datagram != NULL) {
    TFX_ASSERT( ( length <= MAX_SPI_LEN ) , "Insufficient space in buffer - SET SPI BUFFER.");
    memcpy( datagram->dataContent, payload, length);
  }
}

// Set the transactionID.
void Datagram_set_messageSubType(Datagram datagram, unsigned char messageSubType) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  datagram->messageSubType = messageSubType;
}

// Set the transactionID.
void Datagram_set_transactionID(Datagram datagram, unsigned char transactionID) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  datagram->transactionID = transactionID;
}


// Set the numberOfFrames.
void Datagram_set_numberOfFrames(Datagram datagram, unsigned char numberOfFrames) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  datagram->numberOfFrames = numberOfFrames;
}


// Set the frameNumber.
void Datagram_set_frameNumber(Datagram datagram, unsigned char frameNumber) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  datagram->frameNumber = frameNumber;
}



// Set the messageLength.
void Datagram_set_messageLength(Datagram datagram, unsigned char messageLength) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  datagram->messageLength = messageLength;
}


// Set the messageType.
void Datagram_set_messageType(Datagram datagram, unsigned char messageType) {
  TFX_ASSERT(datagram, "Expected a non-null pointer.");
  datagram->messageType = messageType;
}


// Clear Datagram
void Datagram_clear( Datagram datagram )
{
	int index;
	TFX_ASSERT(datagram, "Expected a non-null pointer.");
	datagram->frameNumber = 0xFF;
	datagram->messageLength = 0x00;
	datagram->messageSubType = 0xFF;
	datagram->numberOfFrames = 0xFF;
	datagram->transactionID = 0xFF;
	datagram->crc = 0x00;
	index = 0;
	while (index < MAX_SPI_LEN)
	{
		datagram->dataContent[index] = 0x00;
		index++;
	}
}


// Copies Datagram2 into Datagram1
void Datagram_copy( Datagram datagram1, Datagram datagram2,
		                       unsigned char transactionIDIndex, unsigned char crcEnabledPacket)
{
  TFX_ASSERT(datagram1, "Expected a non-null pointer.");
  TFX_ASSERT(datagram2, "Expected a non-null pointer.");

  int usefulPayLoadLength = 0;
  int startOfUsefulPayloadIndex = 0;
  int index1;
  int index2;
  unsigned char packetMessageTypeLength = 1;
  unsigned char packetMessageSubTypeFieldLength = 1;
  unsigned char transactionIDFieldLength = 5;
  unsigned char numberFramesFieldLength = 3;
  unsigned char frameNumberFieldLength = 3;
  unsigned char crcFieldLength = 2;
  index1 = 0;
  index2 = 0;

  if ( crcEnabledPacket == 1 )
  {
	  usefulPayLoadLength = ( datagram2->messageLength - ( packetMessageTypeLength + packetMessageSubTypeFieldLength \
			                                               + transactionIDFieldLength + numberFramesFieldLength \
			                                               + frameNumberFieldLength  + crcFieldLength));
  }
  else
  {
	  usefulPayLoadLength = ( datagram2->messageLength - ( packetMessageTypeLength + packetMessageSubTypeFieldLength \
			                                               + transactionIDFieldLength + numberFramesFieldLength \
			                                               + frameNumberFieldLength ));
  }

  // Carry out a range check on the payload length
  // We can only possibly get 224 chars in a packet.
  // Something must be going wrong if we do - so return.
  if ( usefulPayLoadLength > 224)
  {
	  return;
  }

  startOfUsefulPayloadIndex = transactionIDIndex +
		                     ( transactionIDFieldLength + numberFramesFieldLength + frameNumberFieldLength);

  index2 = startOfUsefulPayloadIndex;

  while (index1 < usefulPayLoadLength )
  {
    datagram1->dataContent[index1] = datagram2->dataContent[index2];
	index1++;
	index2++;
  }

  datagram1->frameNumber = (datagram2->frameNumber);
  datagram1->messageLength = (datagram2->messageLength);
  datagram1->messageLength = usefulPayLoadLength;
  datagram1->messageSubType = (datagram2->messageSubType);
  datagram1->messageType = (datagram2->messageType);
  datagram1->transactionID = (datagram2->transactionID);
  datagram1->numberOfFrames = (datagram2->numberOfFrames);
  datagram1->crc = (datagram2->crc);

}
