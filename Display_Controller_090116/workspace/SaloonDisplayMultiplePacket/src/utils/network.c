/*
 * network.c
 *
 *  Created on: 27 Aug 2010
 *      Author: DAVE
 */
// Standard C library includes.
#include "stdio.h"
#include "stdlib.h"
#include <string.h>


#include "network.h"
#include "TFX-SPI.h"
#include "displayExtern.h"
#include "pdca.h"
#include "conf_echelon.h"
#include "user_board.h"
#include "delay.h"
#include "callforaid.h"
#include "optoinputs.h"
#include "board.h"
#include "passengercount.h"
#include "tfxDisplayTestMode.h"
#include "crc.h"
#include "displayline.h"
#include "nvmemory.h"




volatile Datagram receivedPacket0;
volatile Datagram receivedPacket1;
volatile Datagram receivedPacket2;
volatile Datagram receivedPacket3;
volatile Datagram receivedPacket4;
/* DB CHANGED
volatile Datagram receivedPacket5;
volatile Datagram receivedPacket6;
volatile Datagram receivedPacket7;
volatile Datagram receivedPacket8;
volatile Datagram receivedPacket9;
*/
volatile Datagram workPacket;

// Constructor.
//
//
Network Network_construct()
{
	Network network = ( struct network_str* ) malloc(sizeof(struct network_str));
	TFX_ASSERT(network, "Failed to allocate memory.");
	network->numberBytesInTransfer = 224;
	network->readBuffer = NULL;
	network->writeBuffer = NULL;
	network->readBuffer = cb_construct(network->numberBytesInTransfer, 1);
	network->writeBuffer = cb_construct(network->numberBytesInTransfer, 2);
	network->smState = 0;
	network->msState = 0;
	network->disableAutoTestMode = 0;
	network->numberCRCFailures = 0;
	network->numberCRCPasses = 0;
	network->numberStateMCCalls = 200;
	network->crcEnabledNetworkTransactions = 0;
	network->feedbackFeatureSet = 0;
	network->feedbackFeatureSet1 = 0;
	network->feedbackFeatureSet2 = 0;
	network->feedbackFeatureSet3 = 0;
	network->neuron = Neuron_construct();
	network->neuronACK = Neuron_construct();
	network->neuronReply = Neuron_construct();
	network->sendMessageBuffer = SPI_construct();
	network->tryDMATransfer = 0;
	network->failedDMATransfer = 0;
	network->numberFailedDMATransfers = 0;
	network->numberSuccessfulDMATransfers = 0;
	network->acknowledgePCResetReceived = 0;
	network->acknowledgeFeatureResetReceived = 0;

	network->messageAcknowledgeType = 0;
	network->numberECARequests = 0;
	network->numberECAAck = 0;
	network->ECARequested = 0;
	network->numberServiceECAPacketsSent = 0;
	network->acknowledgeEchoResetReceived = 0;

	network->EchoMessageRequested = 0;
	network->numberServiceEchoPacketsSent = 0;
	network->echoModeEnabled = 0;
	network->completedPowerOnCycle = 0;

	receivedPacket0 = Datagram_construct();
	receivedPacket1 = Datagram_construct();
	receivedPacket2 = Datagram_construct();
	receivedPacket3 = Datagram_construct();
	receivedPacket4 = Datagram_construct();
	/* DB CHANGED
	receivedPacket5 = Datagram_construct();
	receivedPacket6 = Datagram_construct();
	receivedPacket7 = Datagram_construct();
	receivedPacket8 = Datagram_construct();
	receivedPacket9 = Datagram_construct();
	*/
	workPacket      = Datagram_construct();

	network->receivedPacket0 = receivedPacket0;
	network->receivedPacket1 = receivedPacket1;
	network->receivedPacket2 = receivedPacket2;
	network->receivedPacket3 = receivedPacket3;
	network->receivedPacket4 = receivedPacket4;
	/* DB CHANGED
	network->receivedPacket5 = receivedPacket5;
	network->receivedPacket6 = receivedPacket6;
	network->receivedPacket7 = receivedPacket7;
	network->receivedPacket8 = receivedPacket8;
	network->receivedPacket9 = receivedPacket9;
	*/
	network->workPacket = workPacket;

	return network;
}

// Destructor.
void Network_destroy(Network network) {
	TFX_ASSERT(network, "Expected a non-null pointer.");

	//DatagramList_destroy(network->datagramList);
	Datagram_destroy(workPacket);
	/* DB CHANGED
	Datagram_destroy(receivedPacket9);
	Datagram_destroy(receivedPacket8);
	Datagram_destroy(receivedPacket7);
	Datagram_destroy(receivedPacket6);
	Datagram_destroy(receivedPacket5);
	*/
	Datagram_destroy(receivedPacket4);
	Datagram_destroy(receivedPacket3);
	Datagram_destroy(receivedPacket2);
	Datagram_destroy(receivedPacket1);
	Datagram_destroy(receivedPacket0);
	SPI_destroy(network->sendMessageBuffer);
	Neuron_destroy( network->neuron );
	cb_free( network->writeBuffer);
	cb_free( network->readBuffer );
	free(network);
}


//void Network_decodePacketInformation(Network network)
void Network_processMessages( Network network )
{
	unsigned char message_length = MAX_SPI_LEN;
	volatile unsigned char frame_message_length = 0;
	int i = 0;
	char readCharacter;
    char readMessageSubType = 0;
	static char temp[MAX_SPI_LEN];
	static unsigned char frameCounter = 0;
	static unsigned char lastTransactionID = 0;
	static unsigned char frameList[20];
	unsigned char saloonDisplaySubTypeIndex = 0;
	unsigned char destinationDisplaySubTypeIndex = 0;
	unsigned char displayNeuronSubTypeIndex = 0;
	unsigned char maintenanceModeIndex = 0;
	unsigned char temp2 = 0;
	unsigned char transactionIDIndex = 255;

	static volatile char crcUpper = 0;
	static volatile char crcLower = 0;

    unsigned char ReplyType = 0;
	unsigned char ReplyIndexPosition = 0;


	Datagram_clear( network->workPacket );

	// Is there anything in the Ring Buffer?
	// If so then process the packet present.
	if ( network->readBuffer->count > 0  )
	{
		//Set message_length to the number of bytes of data that are present in the ring buffer
		//received across the SPI channel connected to the Echelon
		message_length = network->readBuffer->count;
		i = 0;
		int counter = 1;
		while (network->readBuffer->count > 0 )
		{
			cb_pop_front(network->readBuffer, &readCharacter);
			temp[i] = readCharacter;
			i++;
			counter++;
		}

		//Place into the workpacket the contents of the ring buffer of data received across the SPI channel connected
		//to the Echelon
		Datagram_set_payload( network->workPacket, temp, message_length );


		for(i = 0; i < message_length; i++)
		  {
			  readCharacter = Datagram_get_payload( network->workPacket)[i];
			  //Is packet for current display?
			  if ( readCharacter == '\\' ){
				  if (  i < (message_length - 2) &&  ( Datagram_get_payload(network->workPacket)[i+1] == 'T') )
				  {
					  readMessageSubType = Datagram_get_payload(network->workPacket)[i-1];

					  if ( thisDisplay->displaytype == SUB_TT_SALOON && \
							  readMessageSubType == SUB_TT_SALOON && saloonDisplaySubTypeIndex == 0)
					  {
						  Datagram_set_messageSubType(network->workPacket, SUB_TT_SALOON);
						  Datagram_set_messageType(network->workPacket, Datagram_get_payload(network->workPacket)[i-2] );
						  Datagram_set_messageLength(network->workPacket, Datagram_get_payload(network->workPacket)[i-3] );
						  saloonDisplaySubTypeIndex = i;
					  }
					  if ( thisDisplay->displaytype == SUB_TT_DESTINATION && \
							  readMessageSubType == SUB_TT_DESTINATION && destinationDisplaySubTypeIndex ==0)
					  {
						  Datagram_set_messageSubType(network->workPacket, SUB_TT_DESTINATION);
						  Datagram_set_messageType(network->workPacket, Datagram_get_payload(network->workPacket)[i-2] );
						  Datagram_set_messageLength(network->workPacket, Datagram_get_payload(network->workPacket)[i-3] );
						  destinationDisplaySubTypeIndex = i;
					  }

					  if ( readMessageSubType == SUB_TT_DISPLAY_NEURON && displayNeuronSubTypeIndex ==0)
					  {
						  Datagram_set_messageSubType(network->workPacket, SUB_TT_DISPLAY_NEURON);
						  Datagram_set_messageType(network->workPacket, Datagram_get_payload(network->workPacket)[i-2] );
						  Datagram_set_messageLength(network->workPacket, Datagram_get_payload(network->workPacket)[i-3] );
						  displayNeuronSubTypeIndex = i;
					  }

					  if ( readMessageSubType == SUB_TT_GLOBAL )
					  {
						  Datagram_set_messageSubType(network->workPacket, SUB_TT_GLOBAL);
						  Datagram_set_messageType(network->workPacket, Datagram_get_payload(network->workPacket)[i-2] );
						  Datagram_set_messageLength(network->workPacket, Datagram_get_payload(network->workPacket)[i-3] );
					  }

				  }
			  }

			  //Find Transacion ID, Number of Frames, Frame Number, Data Content, CRC
			  if ( readCharacter == '\\' )
			  {
				  if (  i < (message_length - 2) )
				  {
						  switch ( Datagram_get_payload(network->workPacket)[i+1])
						  {
						  case 'T':
							  temp2  = ( Datagram_get_payload(network->workPacket)[i+2] - 48) * 100;
							  temp2 += ( Datagram_get_payload(network->workPacket)[i+3] - 48) * 10;
							  temp2 += ( Datagram_get_payload(network->workPacket)[i+4] - 48);
							  Datagram_set_transactionID(network->workPacket,  temp2 );
							  transactionIDIndex = i;
						  break;
						  case 'X':
							  Datagram_set_numberOfFrames(network->workPacket, ( Datagram_get_payload(network->workPacket)[i+2] - 48) );
						  break;
						  case 'F':
							  Datagram_set_frameNumber(network->workPacket, ( Datagram_get_payload(network->workPacket)[i+2] - 48) );
						  break;
						  //Logic has been added to the code at this point to see if the Network Packet
						  //has present within its payload an \Jxx escape character sequence.
						  //If it does the APIS controller sending messages to the Display Controller
						  //is derived from the new source branch and has CRC and Operational Mode Select
						  //code present.
						  //APIS sending CRC within network packet
						  case 'J':
							  network->crcEnabledNetworkTransactions = 1;
						  break;


						  //Does the Work Packet contain an APIS reply packet?
						  //Reply Message Types:
						  //0x01 - N/A Used within Saloon RX Amp with Comms
						  //0x02 - Passenger Counter Passenger Counter Feedback Message Acknowledge from APIS controller
						  //0x03 - Passenger Counter Closed Door Active from APIS controller
						  //0x04 - Passenger Counter Start Passenger Counters from APIS controller
						  //If the work packet does contain APIS replay Message then process
						  //the message here and not in the Process Long message function.
						  //This allows for out of order multiple packet transactions to arrive
						  //with a APIS reply message in between packets without
						  //throwing away the multiple packet transaction due to differing transaction ID
						  case 'R':
								ReplyIndexPosition = i;
								ReplyType = (Datagram_get_payload(network->workPacket)[i+3] - 48);
								ReplyType += (Datagram_get_payload(network->workPacket)[i+2] - 48) * 10;
						  break;
						  default:
					      break;
						  }
			      }
		      }
	        }

		//Do we have a Display Neuron Packet?
		//If we do then we process them differently
		if (readMessageSubType ==  SUB_TT_DISPLAY_NEURON )
		{
			//Range check the received message length
			if ( network->workPacket->messageLength > MAX_SPI_LEN )
			{
				return;
			}
			int index = network->workPacket->messageLength + 1;
			int messageLength = network->workPacket->messageLength;
			//Place a null terminator at the end of the Display Neuron Packet
			network->workPacket->dataContent[index] = 0x00;
			//Process the Display Neuron Packet.
		 	Network_processLongMessages(network, network->workPacket->dataContent, &messageLength);
			return;
		}


		//If the subtype of the packet is not from the display neuron check for the display sub type
		if ( readMessageSubType !=  SUB_TT_GLOBAL)
		{
				// Is the received packet for this particular display - if not then return from packet decode now.
				if ( (thisDisplay->displaytype == SaloonDisplay) && (saloonDisplaySubTypeIndex == 0) )
				{
				  return;
				}

				if ( (thisDisplay->displaytype == FrontDestinationDisplay) && (destinationDisplaySubTypeIndex == 0) )
				{
				  return;
				}
		}


		//CRC Enabled Transactions being used for display messages?
		//If they are - retrieve the CRC from the received packet
		if ( network->crcEnabledNetworkTransactions  == 1 )
		{

			frame_message_length = Datagram_get_payload(network->workPacket)[transactionIDIndex - 3];
			//Retrieve the CRC from the packet
			crcUpper = ( Datagram_get_payload(network->workPacket)[transactionIDIndex + frame_message_length - 4]);
			crcLower = ( Datagram_get_payload(network->workPacket)[transactionIDIndex + frame_message_length - 3]);

			//Calculate the CRC for the packet
			CRC_calcCrc16Block( &(Datagram_get_payload(network->workPacket)[transactionIDIndex]),
					                                            (frame_message_length - 4) , &network->CRCResult_Calculated);

			//Check that the CRC sent in the network packet matches
			//that calculated from the received packet.
			//If they match place the CRC value into the Packet structure for storage.
			network->CRCResult_Received = ( (short)crcUpper * 256 + (short) crcLower );;

			if ( network->CRCResult_Received == network->CRCResult_Calculated)
			{
				network->workPacket->crc = network->CRCResult_Received;
				network->numberCRCPasses++;
			}
			else
			{
				network->workPacket->crc = 0x00;
				network->numberCRCFailures++;
				return;
			}
		}

        // Has the transactionID, numberOfFrames and frameNumber been decoded from the packet?
		// If not then return from packet decode now.
		if ( Datagram_get_transactionID( network->workPacket ) == 0xFF || \
				 Datagram_get_numberOfFrames( network->workPacket ) == 0xFF || \
						 Datagram_get_frameNumber( network->workPacket ) == 0xFF )
		{
			return;
		}

		if ( readMessageSubType ==  SUB_TT_GLOBAL)
		{
			//Has an APIS reply message been found ?
			//This type of message applies to all sub types and thus is placed
			//before the sub type check for the message.
			if ( ReplyIndexPosition > 0)
			{
				//Has an APIS reply re-boot message been found?
				//If it has then we need to reset the passenger counters
				//if this is a passenger counter enabled display controller.
#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
				//Is there a Passenger Counter Feedback Acknowledge Message from APIS controller
				if ( ReplyType == 0x02)
				{
					//Place the Neuron ID in the reply message into the Neuron Structure
					i=ReplyIndexPosition + 2 + 2;
					int i2 = 0;
					while ( i < ( ReplyIndexPosition + 2 + 2 + 6) )
					{
					   (network->neuronReply->id)[i2] = Datagram_get_payload(network->workPacket)[i];
					   i++;
					   i2++;
					}
					//Is the reply message for all Neuron Id's?
					if ( Network_globalCompare( network->neuronReply ) == 1)
					{
						thisDisplay->passengerCount->sendStartPassengerCounters = 1;
						thisDisplay->passengerCount->resetPassengerCounterMessageFeedback = 1;
						thisDisplay->passengerCount->sendStopPassengerCounters = 0;
						thisDisplay->passengerCount->totalNumberAPISRebootMessages++;
					}
				}
#endif
				return;
			}
		return;
		}

		if ( readMessageSubType ==  SUB_TT_SALOON)
		{
			//Has an APIS reply message been found ?
			if ( ReplyIndexPosition > 0)
			{

#if PASSENGER_COUNTER_FUNCTION == PC_FUNCTION_ON
				//Has a Closed Door Active message been found?
				//If it has then we need to reset the passenger counters
				//if this is a passenger counter enabled display controller.
				//Is there a Closed Door Active Reply Message from the APIS controller?
				if ( ReplyType == 0x03)
				{
					//Place the Neuron ID in the reply message into the Neuron Structure
					i=ReplyIndexPosition + 2 + 2;
					int i2 = 0;
					while ( i < ( ReplyIndexPosition + 2 + 2 + 6) )
					{
					   (network->neuronReply->id)[i2] = Datagram_get_payload(network->workPacket)[i];
					   i++;
					   i2++;
					}
					//Is the reply message for all Neuron Id's?
					if ( Network_globalCompare( network->neuronReply ) == 1)
					{
						thisDisplay->passengerCount->doorClosedCounter++;
						if (thisDisplay->passengerCount->doorClosedCounter > 5)
						{
							thisDisplay->passengerCount->doorClosedCounter = 1;
						}
						thisDisplay->passengerCount->closedDoorID = (Datagram_get_payload(network->workPacket)[i]) * 256 +
																			  (Datagram_get_payload(network->workPacket)[i+1]);
						thisDisplay->passengerCount->sendStartPassengerCounters = 0;
						thisDisplay->passengerCount->resetPassengerCounterMessageFeedback = 1;
						thisDisplay->passengerCount->sendStopPassengerCounters = 1;
					}
				}
				//Has a Start Passenger Counters message been found?
				//If it has then we need to reset the passenger counters
				//and stop sending back Passenger Counter Feedback Messages to APIS controller
				//if they are currently being sent.
				if ( ReplyType == 0x04)
				{
					//Place the Neuron ID in the reply message into the Neuron Structure
					i=ReplyIndexPosition + 2 + 2;
					int i2 = 0;
					while ( i < ( ReplyIndexPosition + 2 + 2 + 6) )
					{
					   (network->neuronReply->id)[i2] = Datagram_get_payload(network->workPacket)[i];
					   i++;
					   i2++;
					}
					//Is the reply message for all Neuron Id's?
					if ( Network_globalCompare( network->neuronReply ) == 1)
					{
						thisDisplay->passengerCount->sendStartPassengerCounters = 1;
						thisDisplay->passengerCount->resetPassengerCounterMessageFeedback = 1;
						thisDisplay->passengerCount->sendStopPassengerCounters = 0;
					}
				}
#endif
				return;
			}
		}

		//Carry out the following if we have received a packet for a Saloon display or
		//Destination display and it has passed CRC checks if CRC present in packet.

		//Do we have a new transaction taking place?
		// If so, then the receivedPacket.transactionID will be different to the lastTransactionID?
		// If it is then set the lastTransactionID to the transactionID in the current received packet,
		// reset the frameCounter to zero and clear the array list of frame numbers of packets received.
		if (lastTransactionID != Datagram_get_transactionID( network->workPacket ))
		{
			lastTransactionID = Datagram_get_transactionID( network->workPacket );
			frameCounter = 0;
			//Clear the frameNumberList of frame numbers of packets received
			i=0;
			while( i < 20)
			{
				frameList[i] = 0;
				i++;
			}
			Datagram_clear( network->receivedPacket0 );
			Datagram_clear( network->receivedPacket1 );
			Datagram_clear( network->receivedPacket2 );
			Datagram_clear( network->receivedPacket3 );
			Datagram_clear( network->receivedPacket4 );
			/* DB CHANGED
			Datagram_clear( network->receivedPacket5 );
			Datagram_clear( network->receivedPacket6 );
			Datagram_clear( network->receivedPacket7 );
			Datagram_clear( network->receivedPacket8 );
			Datagram_clear( network->receivedPacket9 );
			*/
			maintenanceModeIndex = 0;
		}

		//Has the frame already been received before?
		//Should the current frame number be placed into the frameNumberList?
		//If the frame number does not already exist in the frameNumberList then add it.
		//As we have added a new frame to the frameNumberList we have received a new packet -
		//so increment the frameCounter.

		if ( !frameList[Datagram_get_frameNumber( network->workPacket )] )
		{
			if (Datagram_get_frameNumber( network->workPacket ) < 20){
				frameList[Datagram_get_frameNumber( network->workPacket )] = 1;
			}
			frameCounter++;
			// Make a copy of the current received packet
			switch ( Datagram_get_frameNumber( network->workPacket) )
			{
			case 0:
				Datagram_copy( network->receivedPacket0, network->workPacket,
														transactionIDIndex,
														network->crcEnabledNetworkTransactions );
			break;
			case 1:
				Datagram_copy( network->receivedPacket1, network->workPacket,
						                                transactionIDIndex,
						                                network->crcEnabledNetworkTransactions );
			break;
			case 2:
				Datagram_copy( network->receivedPacket2, network->workPacket,
						                                transactionIDIndex,
						                                network->crcEnabledNetworkTransactions );
			break;
			case 3:
				Datagram_copy( network->receivedPacket3, network->workPacket,
						                                transactionIDIndex,
						                                network->crcEnabledNetworkTransactions );
			break;
			case 4:
				Datagram_copy( network->receivedPacket4, network->workPacket,
						                                transactionIDIndex,
						                                network->crcEnabledNetworkTransactions );
			break;
			/*  DB CHANGED
			case 5:
				Datagram_copy( network->receivedPacket5, network->workPacket,
						                                transactionIDIndex,
						                                network->crcEnabledNetworkTransactions );
			break;
			case 6:
				Datagram_copy( network->receivedPacket6, network->workPacket,
						                               transactionIDIndex,
						                               network->crcEnabledNetworkTransactions );
			break;
			case 7:
				Datagram_copy( network->receivedPacket7, network->workPacket,
						                               transactionIDIndex,
						                               network->crcEnabledNetworkTransactions );
			break;
			case 8:
				Datagram_copy( network->receivedPacket8, network->workPacket,
						                               transactionIDIndex,
						                               network->crcEnabledNetworkTransactions );
			break;
			case 9:
				Datagram_copy( network->receivedPacket9, network->workPacket,
						                               transactionIDIndex,
						                               network->crcEnabledNetworkTransactions );
			break;
			*/
			}

		}

		//Has the whole of the transaction taken place?
		//A whole transaction has taken place when the receivedPacket.numberOfFrames = frameCounter.
		//At this point all packet frames will have been received.
		//As a second check - the frameNumberList should contain 1's at indexes 0 through to (frameCounter-1)

		if ( frameCounter >= Datagram_get_numberOfFrames( network->workPacket) )
		{
			int counter = 0;
			unsigned char frameFilled;
			while ( counter < Datagram_get_numberOfFrames( network->workPacket) )
			{

				frameFilled = frameList[ counter ];
				if (!frameFilled)
				{
					return;
				}
				counter++;
			}
			Network_mergeReceivedPacketFrames( network );
			network->disableAutoTestMode = 1;
		}
   }

}

int Network_convertCharBufferToShortBuffer( const char* charMessageBuffer, const int charBufferSize, unsigned short* ushortMessageBuffer)
{
  int charIndex;
  int ushortIndex;
  char even = 0;
  unsigned short hexNo;
  int lengthOfShortBuffer = 0;

  // Convert the char message array to a unsigned short message array for sending over the
  // SPI channel.

  even = charBufferSize % 2;

  if ( even ){
		for (charIndex=0, ushortIndex=0; charIndex<charBufferSize; charIndex+=2, ushortIndex++)
		{
		unsigned short hexNo=charMessageBuffer[charIndex]<<8; //Shift Lower Byte to Higher Byte
		hexNo|=charMessageBuffer[charIndex+1]; // Or'd next byte at lower byte position.
		ushortMessageBuffer[ushortIndex] = hexNo;
		}
		lengthOfShortBuffer = charBufferSize/2;
  }
  // Are we sending an odd number of bytes?
  // If so we need to send an extra byte at the end of the transfer to make it even.
  else
  {
		for (charIndex=0, ushortIndex=0; charIndex<(charBufferSize - 1); charIndex+=2, ushortIndex++)
		{
		hexNo=charMessageBuffer[charIndex]<<8; //Shift Lower Byte to Higher Byte
		hexNo|=charMessageBuffer[charIndex+1]; // Or'd next byte at lower byte position.
		ushortMessageBuffer[ushortIndex] = hexNo;
		}
		hexNo = charMessageBuffer[charIndex+1]<<8;
		hexNo|=0x00;
		ushortMessageBuffer[ushortIndex+1] = hexNo;
		lengthOfShortBuffer = (charBufferSize+1) / 2;
  }
  return lengthOfShortBuffer;
}

void Network_storeReceivedMessagePacket(Network network)
{
  int counter = 0;
  unsigned short tempShort;
  unsigned char  tempChar1;
  unsigned char  tempChar2;

  unsigned char messageType;
  unsigned char messageLength;
  unsigned char messageIndex = 0;

  char even = 0;

  tempShort = network->receivedMessageArraySPI[0];
  messageLength = (unsigned short) tempShort >> 8;
  messageType =   (unsigned short) tempShort;

  messageLength = 224;
  even = ( (messageLength-1) % 2) + 1;

  //if ( messageType == TT_UP_MSG )
  {
	  //print_dbg("Message Received for Destination Display\n");
	  cb_push_back( network->readBuffer, &messageLength );

	  messageIndex = 0;
	  counter = 0;
	  if ( even == 1 )
	  {
		  while ( counter < ( (messageLength - 1)/2 )  )
		  {
			  tempShort = network->receivedMessageArraySPI[messageIndex];
			  // Convert Short into two char's
			  tempChar1 = (unsigned short) tempShort >> 8;
			  tempChar2 = (unsigned short) tempShort;
			  //Push Two chars into ring buffer;
			  cb_push_back(network->readBuffer, &tempChar1 );
			  cb_push_back(network->readBuffer, &tempChar2 );
			  messageIndex++;
			  counter++;
		  }
	  }

      if ( even != 1 && messageLength > 2 )
      {
		  //Push pairs of chars present in readArray
		  while ( counter < ( (messageLength - 1)/2 ) )
		  {
			  tempShort = network->receivedMessageArraySPI[messageIndex];
			  // Convert Short into two char's
			  tempChar1 = (unsigned short) tempShort >> 8;
			  tempChar2 = (unsigned short) tempShort;
			  //Push Two chars into ring buffer;
			  cb_push_back(network->readBuffer, &tempChar1 );
			  cb_push_back(network->readBuffer, &tempChar2 );
			  messageIndex++;
			  counter++;
		  }
		  // Push last char present in readArray
		  tempShort = network->receivedMessageArraySPI[messageIndex];
		  // Convert Short into two char's
		  tempChar1 = (unsigned short) tempShort >> 8;
		  //Push Two chars into ring buffer;
		  cb_push_back(network->readBuffer, &tempChar1 );
		  }
	  }
	  if ( messageLength == 2)
	  {
		  // Push last char present in readArray
		  tempShort = network->receivedMessageArraySPI[messageIndex];
		  // Convert Short into two char's
		  tempChar1 = (unsigned short) tempShort >> 8;
		  //Push Two chars into ring buffer;
		  cb_push_back(network->readBuffer, &tempChar1 );
	  }

  }

int Network_enableMessageTransferOverSPI( Network network, char* charSendMessageBuffer, const int charBufferSize )
{
  int transferLength;

  if (charSendMessageBuffer == NULL )
  {
	 return 0;
  }

  //Convert the Char Message Buffer to Unsigned Short Buffer before sending over SPI
  //Conversion function returns the length of the Unsigned Short Buffer created by conversion process.
  transferLength = Network_convertCharBufferToShortBuffer( charSendMessageBuffer, charBufferSize, network->sendMessageArraySPI );

  //Clear the Received Message Array into which the received message packet will be placed
  int counter = 0;
  while (counter < 224)
  {
	  network->receivedMessageArraySPI[counter] = 0x00;
	  counter++;
  }

  //Send Message packet in the Unsigned Short sendMessageArray over the SPI channel using DMA
  //Receive Message packet from the Echelon into the Unsigned Short receiveMessageArraySPI over SPI using DMA
  {
  spi_selectChip(ECHELON_SPI, ECHELON_SPI_CHANNEL);

  //Enable the transfer NOW.
  //Receive Message Packet from Echelon
  pdca_load_channel( PDCA_CHANNEL_SPI_RX,
							 (void *)&network->receivedMessageArraySPI,
							  transferLength  );
  //Send Message Packet to Echelon.
  pdca_load_channel( PDCA_CHANNEL_SPI_TX,
							 (void *)&network->sendMessageArraySPI,
							  transferLength  );

  thisDisplay->network->tryDMATransfer = 1;

  //Dave changed 22/06/2011
  pdca_enable(PDCA_CHANNEL_SPI_RX);
  pdca_enable(PDCA_CHANNEL_SPI_TX);

  //Enable interrupts for DMA transfer complete and DMA transfer in error
  pdca_enable_interrupt_transfer_complete( PDCA_CHANNEL_SPI_RX );
  pdca_enable_interrupt_transfer_error( PDCA_CHANNEL_SPI_RX );

  //Dave changed 22/06/2011
  //pdca_enable_interrupt_transfer_complete( PDCA_CHANNEL_SPI_TX );
  //pdca_enable_interrupt_transfer_error( PDCA_CHANNEL_SPI_TX );

  //Wait until transfer is complete.
  //while( (pdca_channeltx->tcr & AVR32_PDCA_TCR_TCV_MASK)
  //		     && (pdca_channelrx->tcr & AVR32_PDCA_TCR_TCV_MASK));

  //The current while loop is a busy while loop.
  //Maybe try a non busy while loop.
  while( ( thisDisplay->network->tryDMATransfer == 1) );

  // Disable the transfer now.
  pdca_disable(PDCA_CHANNEL_SPI_TX);
  pdca_disable(PDCA_CHANNEL_SPI_RX);
  spi_unselectChip(ECHELON_SPI, ECHELON_SPI_CHANNEL);
  }
  if ( thisDisplay->network->failedDMATransfer == 1)
  {
	  return 0;
  }
  return 1;

}

void Network_spiSlaveMasterTransfer(Network network, char *message, int numberBytesInTransfer)
{
	int transferComplete;
	int spiStatus;
    int spiEnabled;

	//Wait for HREQ to be asserted from Echelon A
	if ( network->smState == 0 )
	{

	  if ( gpio_get_pin_value(HREQA_GPIO) == 0)
	  {
	  //print_dbg("State 0 : Host Request received\n");
	   network->smState = 1;
	  }
	}
	// Assert SREQ to Echelon A
	if ( network->smState == 1)
	{
	  //print_dbg("State 1 : Slave Request Asserted\n");
	  gpio_clr_gpio_pin(SREQA_GPIO);
	  gpio_clr_gpio_pin(SREQB_GPIO);
	  delay_ms(20);
	  network->smState = 2;
	}
	// Wait for SRDY from Echelon A to be Asserted
	if ( network->smState == 2 )
	{
	  if ( gpio_get_pin_value(SRDYA_GPIO) == 0 ){
	  //print_dbg("State 2 : Slave Ready received \n");
		 network->smState = 3;
	  }
	}

	// Deassert SREQ for Echelon A
	if ( network->smState == 3 )
	{
	  gpio_set_gpio_pin(SREQA_GPIO);
	  gpio_set_gpio_pin(SREQB_GPIO);
	  delay_ms(3);
	  //print_dbg("State 3 : Slave Request Deasserted\n");
	  network->smState = 4;
	}

	// Wait for SRDY being Deasserted from Echelon A
	if ( network->smState == 4 )
	{
	  if ( gpio_get_pin_value(SRDYA_GPIO) == 1 ){
	  //print_dbg("State 4 : Slave Ready Deasserted\n");
	  spiStatus = spi_getStatus(ECHELON_SPI);
	  spiEnabled = spi_is_enabled(ECHELON_SPI);
	  network->smState = 5;
	  }
	}
	  spiStatus = spi_getStatus(ECHELON_SPI);
	  spiEnabled = spi_is_enabled(ECHELON_SPI);
	// If status of the SPI channel is OK and the SPI channel has be enabled
	// then enable the Chip Select line to Echelon to enable the
	// Echelon SPI channel so as to allow communication to take place.
	if ( network->smState == 5 && spiStatus == SPI_OK && spiEnabled == TRUE )
	{
	  //print_dbg("State 5 : SPI OK and Enabled\n");
		transferComplete = 0;
		network->smState = 6;
	}

	// Enable Message Transfer over SPI using DMA Transfer
	if ( network->smState == 6)
	{
	  //print_dbg("State 6 : Send Buffer over SPI using DMA Transfer\n");
	  transferComplete = Network_enableMessageTransferOverSPI( network, message, numberBytesInTransfer);
	  if (transferComplete == 1)
	  {
		  network->smState = 7;
	  }
	}
	// Store received Message Packet
	if (  network->smState == 7 )
	{
		transferComplete = 0;
		{
		  //Store Received Message Packet in Ring Buffer
		  Network_storeReceivedMessagePacket(network);
		}
		 network->smState = 8;
	}
	// Wait for writeBuffer to become empty.
	if ( network->smState == 8 )
	{
		network->smState = 9;
	}

	if ( network->smState == 9 )
	{
	}
}


void Network_spiMasterSlaveTransfer(Network network, char *message, int numberBytesInTransfer)
{
	int transferComplete;
	int spiStatus;
    int spiEnabled;


	// Assert SREQ to Echelon A
	if ( network->msState == 0)
	{
	  //print_dbg("State 1 : Slave Request Asserted\n");
	  gpio_clr_gpio_pin(SREQA_GPIO);
	  gpio_clr_gpio_pin(SREQB_GPIO);
	  delay_ms(20);
	  network->msState = 1;
	}
	// Wait for SRDY from Echelon A to be Asserted
	if ( network->msState == 1 )
	{
	  if ( gpio_get_pin_value(SRDYA_GPIO) == 0 ){
	  //print_dbg("State 2 : Slave Ready received \n");
		  network->msState = 2;
	  }
	}

	// Deassert SREQ for Echelon A
	if ( network->msState == 2 )
	{
	  gpio_set_gpio_pin(SREQA_GPIO);
	  gpio_set_gpio_pin(SREQB_GPIO);
	  //print_dbg("State 3 : Slave Request Deasserted\n");
	  delay_ms(3);
	  network->msState = 3;
	}

	// Wait for SRDY being Deasserted from Echelon A
	if ( network->msState == 3 )
	{
	  if ( gpio_get_pin_value(SRDYA_GPIO) == 1 ){
	  //print_dbg("State 4 : Slave Ready Deasserted\n");
	  spiStatus = spi_getStatus(ECHELON_SPI);
	  spiEnabled = spi_is_enabled(ECHELON_SPI);
	  network->msState = 4;
	  }
	}
	  spiStatus = spi_getStatus(ECHELON_SPI);
	  spiEnabled = spi_is_enabled(ECHELON_SPI);
	// If status of the SPI channel is OK and the SPI channel has be enabled
	// then enable the Chip Select line to Echelon to enable the
	// Echelon SPI channel so as to allow communication to take place.
	if ( network->msState == 4 && spiStatus == SPI_OK && spiEnabled == TRUE )
	{
	  //print_dbg("State 5 : SPI OK and Enabled\n");
		transferComplete = 0;
		network->msState = 5;
	}

	// Enable Message Transfer over SPI using DMA Transfer
	if ( network->msState == 5)
	{
	  //print_dbg("State 6 : Send Buffer over SPI using DMA Transfer\n");
	  transferComplete = Network_enableMessageTransferOverSPI( network, message, numberBytesInTransfer);
	  if (transferComplete == 1)
	  {
		  network->msState = 6;
	  }
	}
	// Store received Message Packet
	if ( network->msState == 6 )
	{
		  //Store Received Message Packet in Ring Buffer
		  Network_storeReceivedMessagePacket( network );
		  Network_processMessages( network );
		  network->msState = 7;
	 }
	// Wait for writeBuffer to become empty.
	// Does another SPI transfer need to take place ?
	if ( network->msState == 7 )
	{
		network->msState = 8;
	}

	if ( network->msState == 8 )
	{
      //delay(1);
      //SMState = 0;
	}
}


void Network_mergeReceivedPacketFrames(Network network)
{
	int message_length = 0;
	int i = 0;
	int i2 = 0;
	char completeMessageContent[1200];

	unsigned char numberPacketFrames = network->receivedPacket0->numberOfFrames;

	//Before the start of a new set of packet frames being received,
	//the frame length for frames in the frame store for all packets is zeroed.
	//It follows that a received stored packet in the frame store will have a
	//messageLength > 0 and that the numberPacketFrames count will include the frame
	//in the frame list.
        if (numberPacketFrames >= 1)
        {
			// Copy all the packet data into one big buffer for processing
			i2 = 0;
			i = 0;
			while (i < network->receivedPacket0->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket0->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket0->messageLength;
        }
        if (numberPacketFrames >= 1)
        {
			i = 0;
			while (i < network->receivedPacket1->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket1->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket1->messageLength;
        }
        if (numberPacketFrames >= 2)
        {

			i = 0;
			while (i < network->receivedPacket2->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket2->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket2->messageLength;
        }
        if (numberPacketFrames >= 3)
        {
			i = 0;
			while (i < network->receivedPacket3->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket3->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket3->messageLength;
        }
        if (numberPacketFrames >= 4)
        {
			i = 0;
			while (i < network->receivedPacket4->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket4->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket4->messageLength;
        }
        /* DB CHANGED
        if (numberPacketFrames >= 5)
        {
			i = 0;
			while (i < network->receivedPacket5->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket5->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket5->messageLength;
        }
        if (numberPacketFrames >= 6)
        {
			i = 0;
			while (i < network->receivedPacket6->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket6->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket6->messageLength;
        }
        if (numberPacketFrames >= 7)
        {
			i = 0;
			while (i < network->receivedPacket7->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket7->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket7->messageLength;
        }
        if (numberPacketFrames >= 8)
        {
			i = 0;
			while (i < network->receivedPacket8->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket8->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket8->messageLength;
        }
        if (numberPacketFrames >= 9)
        {
			i = 0;
			while (i < network->receivedPacket9->messageLength )
			{
				completeMessageContent[i2] = network->receivedPacket9->dataContent[i];
				i++;
				i2++;
			}
			message_length += network->receivedPacket9->messageLength;
        }
        */
        //Add a null to the end of the message content
        //completeMessageContent[i2] = 0x00;

        Network_processLongMessages(network, completeMessageContent, &message_length);

}


void Network_decodeRingBufferMessages(Network network)
{
	//Read from the message:
	// \Sx 	where x = 1,2,3,4 - Line upon display to render
	// \Cx	where x = 1,2,3,4 - Characterset to use
	// \Pxxx	where xx = 001, 002, ...., 112 - Number of columns to prepend onto text message.

	//Parse the message character by character to find Line number and
	//characterset information.
	unsigned char message_length = 0;
	int i = 0;
	int i2 = 0;
	char readCharacter;
	char temp[224];
	char temp2[224];

	unsigned char displayLinecharSet =0;
	unsigned char displayLinelineNumber = 0;
	unsigned char displayLineprePend = 0;

	//Dots custom_message_dots;

	// Is there anything in the Ring Buffer?
	// If so then process the message present.
	if ( network->readBuffer->count > 0  )
	{
	    // Pop the first item from the ring buffer.
		// First item will be message length.
		//cb_pop_front(&readBuffer, &message_length);

		message_length = network->readBuffer->count;
		i = 0;
		int counter = 1;
		while (network->readBuffer->count > 0 )
		{
			cb_pop_front(network->readBuffer, &readCharacter);
			//if (readCharacter > 32 && readCharacter < 127)
			{
			temp[i] = readCharacter;
			}
			i++;
			counter++;
		}


		unsigned char lineParamIndexPosition = 0;
		unsigned char foundCharSet = 0;
		unsigned char foundPrePend = 0;
		unsigned char neuronIDParamIndexPosition = 0;

		for(i = 0; i < message_length; i++)
		  {
			  readCharacter = temp[i];
			  //Find Line number, Character set and Pre-pend information
			  if ( readCharacter == '\\' )
			  {
				  if (  i < (message_length - 2) )
				  {
						  switch (temp[i+1])
						  {
						  case 'S':
							  displayLinelineNumber = (temp[i+2]) - 48;
							  lineParamIndexPosition = i;
						  case 'D':
							  displayLinelineNumber = (temp[i+2]) - 48;
							  lineParamIndexPosition = i;
						  break;
						  case 'C':
							  displayLinecharSet = (temp[i+2]) - 48;
							  foundCharSet = i;
						  break;
						  case 'P':
							  displayLineprePend =  ( temp[i+2] - 48) * 100 ;
							  displayLineprePend +=  ( temp[i+3] - 48) * 10 ;
							  displayLineprePend += ( temp[i+4] - 48);
							  foundPrePend = i;
						  break;
						  case 'N':
							  neuronIDParamIndexPosition = i+2;
						  break;
						  default:
					      break;
						  }
			      }
		      }
	        }

		//Has a neuronID been found ?
		if ( neuronIDParamIndexPosition > 0)
		{
			//Place the Neuron ID into the Neuron Structure
			i=neuronIDParamIndexPosition;
	        i2 = 0;
			while ( i < (neuronIDParamIndexPosition + 6) )
			{
			   (network->neuron->id)[i2] = temp[i];
			   i++;
			   i2++;
			}
			i2=0;
			while ( i < (neuronIDParamIndexPosition + 6 + 7) )
			{
			   network->neuron->prog_id[i2] = temp[i];
			   i++;
			   i2++;
			}
			network->neuron->idFound = 1;
			return;
		}


		//Has line data been found in the current data in ring buffer.
		if (lineParamIndexPosition == 0 || foundCharSet == 0 || foundPrePend == 0  )
		{
			return;
		}

		message_length = temp[lineParamIndexPosition - 2];

		int EscapeSeqLength = 11;
		int lengthOfHeader = (lineParamIndexPosition + EscapeSeqLength);
		int addionalBytes = (lengthOfHeader - 12);
		int startIndexOfTextContent = (lineParamIndexPosition + EscapeSeqLength);

		//message length does not include the TTYPE byte but does include the message_length
		//byte and the escape sequence in front of the text content of the packet received.
		//To calculate the endIndexOfTextContent we need the number of additional
		//bytes in front of the message - that is, the number of random bytes in front
		// of the message received plus one extra byte - the TTYPE byte.
		//Thus addionalBytes is the length of the header of information less the
		// ( escape sequence length + one byte) = 11 + 1 = 12. The message_length byte is
		// included in the message_length
		//additionalBytes = lengthOfHeader - 12
		int endIndexOfTextContent = (message_length + addionalBytes);


		//Insert a zero at the end of the string obtained from the message packet received.
		int i2;
		for(i = startIndexOfTextContent, i2 = 0 ; i < (endIndexOfTextContent) ; i++, i2++)
		{
			temp2[i2] = temp[i];
		}
		temp2[i2] = 0x00;

		if ( ( displayLinecharSet > 0 && displayLinecharSet < 5 ) &&
				(displayLinelineNumber > 0 && displayLinelineNumber < 4 ) &&
						( displayLineprePend < 130 ) )
		{
			switch (displayLinelineNumber)
			{
			case 1:
				thisDisplay->displayLine1->prePend = displayLineprePend;
				thisDisplay->displayLine1->charSet = displayLinecharSet;
				thisDisplay->displayLine1->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine1, temp2);
				thisDisplay->displayLine1->displayLineDotsCreated = 0;
				//Displayline_createDots(thisDisplay->displayLine1);
				thisDisplay->displayLine1->messageChanged = 1;
			break;
			case 2:
				thisDisplay->displayLine2->prePend = displayLineprePend;
				thisDisplay->displayLine2->charSet = displayLinecharSet;
				thisDisplay->displayLine2->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine2, temp2);
				//Displayline_createDots(thisDisplay->displayLine2);
				thisDisplay->displayLine2->messageChanged = 1;

			break;
			case 3:
				thisDisplay->displayLine3->prePend = displayLineprePend;
				thisDisplay->displayLine3->charSet = displayLinecharSet;
				thisDisplay->displayLine3->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine3, temp2);
				//Displayline_createDots(thisDisplay->displayLine3);
				thisDisplay->displayLine3->messageChanged = 1;
			break;
			case 7:
				thisDisplay->displayLine1->prePend = displayLineprePend;
				thisDisplay->displayLine1->charSet = displayLinecharSet;
				thisDisplay->displayLine1->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine1, temp2);
				//Displayline_createDots(thisDisplay->displayLine3);
				thisDisplay->displayLine1->messageChanged = 1;
			case 8:
				thisDisplay->displayLine2->prePend = displayLineprePend;
				thisDisplay->displayLine2->charSet = displayLinecharSet;
				thisDisplay->displayLine2->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine2, temp2);
				//Displayline_createDots(thisDisplay->displayLine3);
				thisDisplay->displayLine2->messageChanged = 1;
			case 9:
				thisDisplay->displayLine3->prePend = displayLineprePend;
				thisDisplay->displayLine3->charSet = displayLinecharSet;
				thisDisplay->displayLine3->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine3, temp2);
				//Displayline_createDots(thisDisplay->displayLine3);
				thisDisplay->displayLine3->messageChanged = 1;
			break;


			default:
			break;
			}
		return;
		}
   }
return;
}


void Network_createMessageBuffer(Network network, char* messageBuffer, unsigned char messageLength )
{

	TFX_ASSERT(network, "Expected a non-null pointer.");
	TFX_ASSERT(messageBuffer, "Expected a non-null pointer.");
	int messageCount = 0;
	int index = 0;


	Network_clearBuffer(MAX_SPI_LEN, messageBuffer);

	messageBuffer[0] = network->sendMessageBuffer->transLen;
	messageBuffer[1] = network->sendMessageBuffer->transType;

	for (messageCount = 2; messageCount < (messageLength-2); messageCount++)
	{
	  messageBuffer[messageCount] = network->sendMessageBuffer->data[index];
	  index++;
	}
}


void Network_createMessageBufferSaloon(Network network, char* messageBuffer, unsigned char messageLength )
{

	TFX_ASSERT(network, "Expected a non-null pointer.");
	TFX_ASSERT(messageBuffer, "Expected a non-null pointer.");
	int messageCount = 0;
	int index = 0;


	Network_clearBuffer(MAX_SPI_LEN, messageBuffer);

	messageBuffer[0] = network->sendMessageBuffer->transLen;
	messageBuffer[1] = network->sendMessageBuffer->transType;
	messageBuffer[2] = SUB_TT_SALOON;

	for (messageCount = 2; messageCount < (messageLength-2); messageCount++)
	{
	  messageBuffer[messageCount] = network->sendMessageBuffer->data[index];
	  index++;
	}
}


void Network_createMessageBufferDestination(Network network, char* messageBuffer, unsigned char messageLength )
{

	TFX_ASSERT(network, "Expected a non-null pointer.");
	TFX_ASSERT(messageBuffer, "Expected a non-null pointer.");
	int messageCount = 0;
	int index = 0;


	Network_clearBuffer(MAX_SPI_LEN, messageBuffer);

	messageBuffer[0] = network->sendMessageBuffer->transLen;
	messageBuffer[1] = network->sendMessageBuffer->transType;
	messageBuffer[2] = SUB_TT_DESTINATION;

	for (messageCount = 2; messageCount < (messageLength-2); messageCount++)
	{
	  messageBuffer[messageCount] = network->sendMessageBuffer->data[index];
	  index++;
	}
}

void Network_clearBuffer(int bufferSize, char * buffer)
{
	TFX_ASSERT(buffer, "Expected a non-null pointer.");
	memset(buffer, 0, bufferSize * sizeof  (*buffer) );
}

void Network_readNeuronID(Network network)
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];


	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 0x01;
	  (network->sendMessageBuffer->transType) = TT_DOWN_IDENT;
	  int messageCount;
	  for (messageCount = 0; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  delay_ms(85);
	  //Send the message buffer to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 2;
		  tryCounter = 0;
	  }


	  tryCounter++;
	  if (tryCounter > 10)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

	if (stateCount == 2 )
	{
	  Network_processMessages( network );
	  network->sendMessageBuffer->transLen = 0x00;
	  network->sendMessageBuffer->transType = 0x00;
	  int messageCount;
	  for (messageCount = 0; messageCount < (messageLength-2); messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  stateCount = 3;
	  tryCounter = 0;
	}

	while (stateCount == 3)
	{
	  network->sendMessageBuffer->transLen = 0x00;
	  network->sendMessageBuffer->transType = 0x00;
	  int messageCount;
	  for (messageCount = 0; messageCount < (messageLength-2); messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  Network_spiSlaveMasterTransfer( network, messageBuffer, messageLength );

	  if ( network->smState == 9 )
	  {
		  network->smState = 0;
		  stateCount = 4;
		  tryCounter = 0;
	  }
	  tryCounter++;

	  if (tryCounter > 100)
	  {
	  	  tryCounter = 0;
	  	  stateCount = 0;
	  }
	  delay_ms(85);
	}

	if (stateCount == 4)
	{
	  delay_ms(85);
	  Network_processMessages( network );
	  stateCount = 0;
	  return;
	}
}


void Network_storeNeuronID(Network network)
{
	unsigned char message_length = 0;
	unsigned char i = 0;
	unsigned char i2 = 0;
	char readCharacter;
	char temp[224];

	// Is there anything in the Ring Buffer?
	// If so then process the message present.
	if ( network->readBuffer->count > 0  )
	{

		message_length = network->readBuffer->count;
		i = 0;
		int counter = 1;
		while (network->readBuffer->count > 0 )
		{
			cb_pop_front(network->readBuffer, &readCharacter);
			temp[i] = readCharacter;
			i++;
			counter++;
		}
	 }

	unsigned char neuronIDParamIndexPosition = 99;

	for(i = 0; i < message_length; i++)
	  {
		  readCharacter = temp[i];
		  //Find Line number, Character set and Pre-pend information
		  if ( readCharacter == '\\' )
		  {
			  if (  i < (message_length - 2) )
			  {
					  switch (temp[i+1])
					  {
					  case 'N':
						  neuronIDParamIndexPosition = i+2;
					  break;
					  default:
				      break;
					  }
		      }
	      }
        }
	if ( neuronIDParamIndexPosition < 20)
	{
		//Place the Neuron ID into the Neuron Structure
		i=neuronIDParamIndexPosition;
        i2 = 0;
		while ( i < (neuronIDParamIndexPosition + 6) )
		{
		   (network->neuron->id)[i2] = temp[i];
		   i++;
		   i2++;
		}
		i2=0;
		while ( i < (neuronIDParamIndexPosition + 6 + 7) )
		{
		   network->neuron->prog_id[i2] = temp[i];
		   i++;
		   i2++;
		}
	}
}


void Network_CFAPressed(Network network, Callforaid callforaid)
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;


	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 0x14;
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 'E';
      messageCount++;
	  index = 0;
	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;
	  messageCount++;
	  }
	  index = 0;
	  while (index < 8)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->prog_id[index];
	  index++;
	  messageCount++;
	  }
	  network->sendMessageBuffer->data[messageCount] = callforaid->opto_CFA1->optoInput;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = callforaid->opto_CFA2->optoInput;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = callforaid->opto_RESET->optoInput;
      messageCount++;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(500);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}

void Network_OptoMessage(Network network, Optoinputs optoinputs )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 0x18+2;
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 'E';
      messageCount++;
	  index = 0;
	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;
	  messageCount++;
	  }
	  index = 0;
	  while (index < 8)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->prog_id[index];
	  index++;
	  messageCount++;
	  }

	  network->sendMessageBuffer->data[messageCount] = optoinputs->opto_Input1->edge_detect;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = optoinputs->opto_Input2->edge_detect;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = optoinputs->opto_Input3->edge_detect;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = optoinputs->opto_Input1->optoInput;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = optoinputs->opto_Input2->optoInput;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = optoinputs->opto_Input3->optoInput;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->functionality;
      messageCount++;

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;


	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 150ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  //DB Changed
	  delay_ms(85);


	  //Send the message buffer
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called  network->numberStateMCCalls,
	  // preset to 10, times without the successful completion of the MasterSlave transfer,
	  // drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}


void Network_PassengerCounterMessage(Network network, PassengerCount passengerCount )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;


	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 204;
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 'F';
      messageCount++;
	  index = 0;

	  // Feedback Message Type \F01
	  network->sendMessageBuffer->data[messageCount] = 0x00;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 0x01;
	  messageCount++;

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;
	  messageCount++;
	  }

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet1->closedDoorID >> 8 ;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet1->closedDoorID;
	  messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter1->SNP_passengerCounterAddress;
	  messageCount++;

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter1->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter1->countIn;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter1->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter1->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter1->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter1->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter1->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter1->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter2->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter2->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter2->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter2->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter2->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter2->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter2->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter2->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter2->crcFails;
      messageCount++;


	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter3->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter3->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter3->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter3->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter3->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter3->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter3->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter3->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter3->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter4->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter4->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter4->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter4->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter4->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter4->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter4->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet1->passengerCounter4->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet1->passengerCounter4->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet2->closedDoorID >> 8 ;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet2->closedDoorID ;
	  messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter1->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter1->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter1->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter1->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter1->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter1->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter1->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter1->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter1->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter2->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter2->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter2->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter2->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter2->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter2->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter2->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter2->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter2->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter3->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter3->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter3->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter3->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter3->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter3->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter3->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter3->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter3->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter4->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter4->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter4->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter4->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter4->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter4->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter4->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet2->passengerCounter4->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet2->passengerCounter4->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet3->closedDoorID >> 8 ;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet3->closedDoorID ;
	  messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter1->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter1->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter1->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter1->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter1->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter1->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter1->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter1->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter1->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter2->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter2->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter2->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter2->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter2->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter2->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter2->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter2->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter2->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter3->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter3->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter3->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter3->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter3->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter3->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter3->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter3->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter3->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter4->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter4->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter4->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter4->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter4->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter4->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter4->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet3->passengerCounter4->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet3->passengerCounter4->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet4->closedDoorID >> 8 ;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet4->closedDoorID ;
	  messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter1->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter1->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter1->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter1->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter1->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter1->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter1->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter1->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter1->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter2->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter2->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter2->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter2->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter2->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter2->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter2->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter2->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter2->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter3->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter3->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter3->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter3->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter3->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter3->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter3->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter3->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter3->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter4->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter4->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter4->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter4->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter4->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter4->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter4->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet4->passengerCounter4->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet4->passengerCounter4->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet5->closedDoorID >> 8 ;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounterDataSet5->closedDoorID ;
	  messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter1->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter1->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter1->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter1->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter1->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter1->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter1->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter1->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter1->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter2->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter2->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter2->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter2->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter2->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter2->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter2->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter2->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter2->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter3->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter3->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter3->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter3->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter3->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter3->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter3->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter3->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter3->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = passengerCount->passengerCounter4->SNP_passengerCounterAddress;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter4->countIn >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter4->countIn;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter4->countOut >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter4->countOut;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter4->crcPasses >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter4->crcPasses;
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (passengerCount->passengerCounterDataSet5->passengerCounter4->crcFails >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->passengerCounterDataSet5->passengerCounter4->crcFails;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) passengerCount->numberPassengerCounterPacketsSent;
      messageCount++;

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;
	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 85ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}

unsigned char Network_neuronCompare( Network network )
{
	unsigned char index;
	index = 0;
	while (index < 6)
	{
		if ( network->neuron->id[index] != network->neuronACK->id[index])
		{
			return 0;
		}
		index++;
	}
	return 1;
}


Neuron Network_getNeuron()
{
  return thisDisplay->network->neuron;
}

unsigned char * Network_getNeuronID()
{
	return thisDisplay->network->neuron->id;
}



void Network_processLongMessages(Network network, char *longMessageBuffer, int* messageLength)
{
	int message_length = 0;
	int i = 0;
	int i2 = 0;
	char readCharacter;
	//DB Changed from static char temp[1200]
	volatile char temp[800];
	unsigned char displayLinecharSet =0;
	unsigned char displayLinelineNumber = 0;
	unsigned char displayLineprePend = 0;
    unsigned char scrollDisplayLineEnable = 0;
    unsigned char scrollDisplayLineParamIndexPosition = 0;
    unsigned char scrollDisplayLineNumber = 0;

    unsigned char modeDisplayLine = 0;
    unsigned char modeDisplayLineParamIndexPosition = 0;
    unsigned char modeDisplayLineNumber = 0;

    unsigned char displayTestMode = 0;
	unsigned char lineParamIndexPosition = 0;
	unsigned char foundCharSet = 0;
	unsigned char foundPrePend = 0;
	unsigned char foundScrollEnable = 0;
	unsigned char neuronIDParamIndexPosition = 0;
	unsigned char acknowledgeIndexPosition = 0;
	unsigned char displayTestModeIndexPosition = 0;
	unsigned char lastIndexPosition = -1;
	int EscapeSeqLength = 0;
	int lengthOfHeader = 0;
	int addionalBytes = 0;
	int startIndexOfTextContent = 0;
	int endIndexOfTextContent = 0;

	for(; i < *messageLength; i++)
	  {
		  readCharacter = longMessageBuffer[i];
		  //Find Line number, Character set and Pre-pend information
		  if ( readCharacter == '\\' )
		  {
			  if (  i < (*messageLength - 2) )
			  {
					  switch (longMessageBuffer[i+1])
					  {
					  case 'S': case 'D':
					  {
						  displayLinelineNumber = (longMessageBuffer[i+2]) - 48;
						  lineParamIndexPosition = i;
						  EscapeSeqLength += 3;
					  break;
					  }
					  case 'C':
					  {
						  displayLinecharSet = (longMessageBuffer[i+2]) - 48;
						  foundCharSet = i;
						  EscapeSeqLength += 3;
					  break;
					  }
					  case 'P':
					  {
#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY && CENTER_ENABLE == DISABLED
						  displayLineprePend +=  ( longMessageBuffer[i+2] - 48) * 10 ;
						  displayLineprePend += ( longMessageBuffer[i+3] - 48);
						  foundPrePend = i;
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY && CENTER_ENABLE == DISABLED
						  displayLineprePend +=  ( longMessageBuffer[i+2] - 48) * 10 ;
						  displayLineprePend += ( longMessageBuffer[i+3] - 48);
						  foundPrePend = i;
#elif DISPLAY_TYPE == SALOON_DISPLAY && CENTER_ENABLE == DISABLED
						  displayLineprePend =  ( longMessageBuffer[i+2] - 48) * 100 ;
						  displayLineprePend +=  ( longMessageBuffer[i+3] - 48) * 10 ;
						  displayLineprePend += ( longMessageBuffer[i+4] - 48);
						  foundPrePend = i;
#endif

#if CENTER_ENABLE == ENABLED
						 displayLineprePend = 0;
						 foundPrePend = i;
#endif

#if DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
						  EscapeSeqLength += 5;
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
						  EscapeSeqLength +=5;
#elif DISPLAY_TYPE == SALOON_DISPLAY
						  EscapeSeqLength +=6;
#else
  #error No DISPLAY_TYPE defined
#endif
						  break;
					  }
					  case 'N':
					  {
						  neuronIDParamIndexPosition = i+2;
						  EscapeSeqLength += 2;
					  break;
					  }
					  case 'M':
					  {
#if CENTER_ENABLE == DISABLED
						  scrollDisplayLineNumber = (longMessageBuffer[i+2] - 48);
						  scrollDisplayLineEnable = (longMessageBuffer[i+3] - 48);
						  scrollDisplayLineParamIndexPosition = i;

						  modeDisplayLineNumber = (longMessageBuffer[i+2] - 48);
						  modeDisplayLine = (longMessageBuffer[i+3] - 48);
						  modeDisplayLineParamIndexPosition = i;
#endif
						  EscapeSeqLength += 4;

					  break;
					  }
					  case 'A':
					  {
						  acknowledgeIndexPosition = i+2;
						  EscapeSeqLength += 15;
					  break;
					  }
					  case 'J':
					  {
						  displayTestModeIndexPosition = i+2;
						  displayTestMode = (longMessageBuffer[i+2] - 48)*10;
						  displayTestMode += (longMessageBuffer[i+3] - 48);
						  EscapeSeqLength += 4;
					  break;
					  }
					  default:
					  break;
					  }
			  }
		  }
		}

	//Has a neuronID been found ?
	if ( neuronIDParamIndexPosition > 0)
	{
		//Place the Neuron ID into the Neuron Structure
		i=neuronIDParamIndexPosition;
		i2 = 0;
		while ( i < (neuronIDParamIndexPosition + 6) )
		{
		   (network->neuron->id)[i2] = longMessageBuffer[i];
		   i++;
		   i2++;
		}
		i2=0;
		while ( i < (neuronIDParamIndexPosition + 6 + 8) )
		{
		   network->neuron->prog_id[i2] = longMessageBuffer[i];
		   i++;
		   i2++;
		}
		network->neuron->idFound = 1;
		return;
	}

	//Has an acknowledge been found ?
	if ( acknowledgeIndexPosition > 0)
	{
		//Place the Neuron ID into the Neuron Structure
		i=acknowledgeIndexPosition;
		i2 = 0;
		while ( i < ( acknowledgeIndexPosition + 6) )
		{
		   (network->neuronACK->id)[i2] = longMessageBuffer[i];
		   i++;
		   i2++;
		}
		//Is the Acknowledge message for this particular Neuron Id?
	    if ( Network_neuronCompare( network ) == 1  )
		{
			network->messageAcknowledgeType = longMessageBuffer[i];
			if ( network->messageAcknowledgeType == AckPassengerCount )
			{
				network->acknowledgePCResetReceived = 1;
			}
			if ( network->messageAcknowledgeType == AckECA )
			{
				network->acknowledgeECAResetReceived = 1;
			}
			if ( network->messageAcknowledgeType == AckEcho )
			{
				network->acknowledgeEchoResetReceived = 1;
			}
			if ( network->messageAcknowledgeType == AckReportFeatureSet )
			{
				network->acknowledgeFeatureResetReceived = 1;
			}
			return;
		}

	    //Is the Acknowledge message a global Acknowledge?
	    if ( Network_globalCompare(network->neuronACK ) == 1)
	    {
			network->messageAcknowledgeType = longMessageBuffer[i];
			if ( network->messageAcknowledgeType == AckReportFeatureSet )
			{
				network->acknowledgeFeatureResetReceived = 1;
			}
			return;
	    }
	    return;
	}

	//Has a displayTestMode been found ?
	if ( displayTestModeIndexPosition )
	{
		if ( displayLinelineNumber == 1)
		{
			{
				thisDisplay->numberMaintenanceMessages++;
				//Activate the test mode requested.
				Display_activateTest( displayTestMode );
			}
		}

		//If a Test Mode has been requested by the APIS controller
		//drop out of the Process Long Message Method now -
		//preventing the received message from being rendered onto the display.
		if (displayTestMode != tfxDISPLAY_TEST_MODE_OFF )
		{
		return;
		}
	}



	if ( lineParamIndexPosition > lastIndexPosition)
	{
		lastIndexPosition = lineParamIndexPosition;
	}
	if ( foundCharSet > lastIndexPosition)
	{
		lastIndexPosition = foundCharSet;
	}
	if ( foundPrePend > lastIndexPosition)
	{
		lastIndexPosition = foundPrePend;
	}
	if ( scrollDisplayLineParamIndexPosition > lastIndexPosition)
	{
		lastIndexPosition = scrollDisplayLineParamIndexPosition;
	}
	if ( acknowledgeIndexPosition > lastIndexPosition)
	{
		lastIndexPosition = acknowledgeIndexPosition;
	}

	//Has a scroll enable be found?

	//Is scroll enable for the current display line to be displayed?
    if ( displayLinelineNumber < 4 )
    {
	//Has line scroll enable been found for render line?
#if DISPLAY_TYPE == SALOON_DISPLAY
	if ( scrollDisplayLineParamIndexPosition > 0 )
	{
		switch (scrollDisplayLineNumber)
		{
		case 1:
			Displayline_set_scrollEnable(thisDisplay->displayLine1, \
															&scrollDisplayLineEnable);
		break;
		default:
		break;
		}
	}
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY || DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	if ( scrollDisplayLineParamIndexPosition > 0 )
	{

		switch (scrollDisplayLineNumber)
		{
		case 1:
			Displayline_set_scrollEnable(thisDisplay->displayLine1, \
															&scrollDisplayLineEnable);
		break;
		case 2:
			Displayline_set_scrollEnable(thisDisplay->displayLine2, \
															&scrollDisplayLineEnable);
		break;
		case 3:
			Displayline_set_scrollEnable(thisDisplay->displayLine3, \
															&scrollDisplayLineEnable);
		break;
		default:
		break;
		}

	}
#endif
    }

	//Has line scroll enable been found for default line?
	if ( scrollDisplayLineParamIndexPosition > 0 && displayLinelineNumber > 3)
	{
		foundScrollEnable = 1;
	}



	//Has line data been found in the current data in ring buffer.
	if (displayLinelineNumber == 0 || foundCharSet == 0 || foundPrePend == 0  )
	{
		return;
	}

	message_length = *messageLength;

	lengthOfHeader = (lineParamIndexPosition + EscapeSeqLength );
	addionalBytes = (lengthOfHeader - EscapeSeqLength );

	startIndexOfTextContent = (lineParamIndexPosition + (EscapeSeqLength - 1) );
	endIndexOfTextContent = (message_length + addionalBytes);


#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY

	//If this message is to be placed onto the display then reverse the text message
	if ( displayLinelineNumber < 3)
	{
		//Reversing the text message before sending for the dot information
		//Insert a zero at the end of the string obtained from the message packet received.
		i = (endIndexOfTextContent-1);
		i2 = 0;
		while ( i >= (startIndexOfTextContent))
		{
			temp[i2] = longMessageBuffer[i];
			i--;
			i2++;
		}
		temp[i2] = 0x00;
	}

	if ( (displayLinelineNumber > 3) && (displayLinelineNumber < 7) )
	{
		//If this message is to be stored in the Flash Memory Store then do not reverse the message
		//Insert a zero at the end of the string obtained from the message packet received.
		for(i = startIndexOfTextContent, i2 = 0 ; i < (endIndexOfTextContent) ; i++, i2++)
		{
			temp[i2] = longMessageBuffer[i];
		}
		temp[i2] = 0x00;
	}

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
	//Insert a zero at the end of the string obtained from the message packet received.
	for(i = startIndexOfTextContent, i2 = 0 ; i < (endIndexOfTextContent) ; i++, i2++)
	{
		temp[i2] = longMessageBuffer[i];
	}
	temp[i2] = 0x00;
#elif DISPLAY_TYPE == SALOON_DISPLAY
	//Insert a zero at the end of the string obtained from the message packet received.
	for(i = startIndexOfTextContent, i2 = 0 ; i < (endIndexOfTextContent) ; i++, i2++)
	{
		temp[i2] = longMessageBuffer[i];
	}
	temp[i2] = 0x00;
#else
  #error No DISPLAY_TYPE defined
#endif

#if DISPLAY_TYPE == SALOON_DISPLAY
	if ( ( displayLinecharSet > 0 && displayLinecharSet < 6 ) &&
			(displayLinelineNumber > 0 && displayLinelineNumber < 9 ) &&
					( displayLineprePend < 130 ) )
	{
		switch (displayLinelineNumber)
		{
		case 1:
			thisDisplay->displayLine1->prePend = displayLineprePend;
            //DB CHANGED FOR CLASS319 PROJECT as APIS uses the incorrect
			//characterset currently.
			//thisDisplay->displayLine1->charSet = displayLinecharSet;
			thisDisplay->displayLine1->charSet = 5;
			thisDisplay->displayLine1->lineNumber = displayLinelineNumber;
			Displayline_set_messageBuffer(thisDisplay->displayLine1, temp);
			//DB Added
			if ( thisDisplay->network->echoModeEnabled == 1)
			{
			  //network->EchoMessageRequested = 1;
			  network->EchoMessageRequested++;

			}
			thisDisplay->displayLine1->numberTextMessages++;
			thisDisplay->displayLine1->displayLineDotsCreated = 0;
			thisDisplay->displayLine1->messageChanged = 1;
		break;

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
		if ( ( displayLinecharSet > 0 && displayLinecharSet < 6 ) &&
				(displayLinelineNumber > 0 && displayLinelineNumber < 9 ) &&
						( displayLineprePend < 130 ) )
		{
			switch (displayLinelineNumber)
			{
			case 1:
				thisDisplay->displayLine1->prePend = displayLineprePend;
				thisDisplay->displayLine1->charSet = displayLinecharSet;
				thisDisplay->displayLine1->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine1, temp);
				//DB Added
				if ( thisDisplay->network->echoModeEnabled == 1)
				{
				  //network->EchoMessageRequested = 1;
				  network->EchoMessageRequested++;

				}
				thisDisplay->displayLine1->numberTextMessages++;
				thisDisplay->displayLine1->displayLineDotsCreated = 0;
				thisDisplay->displayLine1->messageChanged = 1;
			break;
			case 2:
				thisDisplay->displayLine2->prePend = displayLineprePend;
				thisDisplay->displayLine2->charSet = displayLinecharSet;
				thisDisplay->displayLine2->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine2, temp);
				//DB Added
				thisDisplay->displayLine2->numberTextMessages++;
				thisDisplay->displayLine2->displayLineDotsCreated = 0;
				thisDisplay->displayLine2->messageChanged = 1;
			break;
			case 3:
				thisDisplay->displayLine3->prePend = displayLineprePend;
				thisDisplay->displayLine3->charSet = displayLinecharSet;
				thisDisplay->displayLine3->lineNumber = displayLinelineNumber;
				Displayline_set_messageBuffer(thisDisplay->displayLine3, temp);
				//DB Added
			    thisDisplay->displayLine3->numberTextMessages++;
				thisDisplay->displayLine3->displayLineDotsCreated = 0;
				thisDisplay->displayLine3->messageChanged = 1;
			break;
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
			if ( ( displayLinecharSet > 0 && displayLinecharSet < 6 ) &&
					(displayLinelineNumber > 0 && displayLinelineNumber < 9 ) &&
							( displayLineprePend < 130 ) )
			{
				switch (displayLinelineNumber)
				{
				case 1:
					thisDisplay->displayLine1->prePend = displayLineprePend;
					thisDisplay->displayLine1->charSet = displayLinecharSet;
					thisDisplay->displayLine1->lineNumber = displayLinelineNumber;
					Displayline_set_messageBuffer(thisDisplay->displayLine1, temp);
					//DB Added
					if ( thisDisplay->network->echoModeEnabled == 1)
					{
					  //network->EchoMessageRequested = 1;
					  network->EchoMessageRequested++;

					}
					thisDisplay->displayLine1->numberTextMessages++;
					thisDisplay->displayLine1->displayLineDotsCreated = 0;
					thisDisplay->displayLine1->messageChanged = 1;
				break;
				case 2:
					thisDisplay->displayLine2->prePend = displayLineprePend;
					thisDisplay->displayLine2->charSet = displayLinecharSet;
					thisDisplay->displayLine2->lineNumber = displayLinelineNumber;
					Displayline_set_messageBuffer(thisDisplay->displayLine2, temp);
					//DB Added
					thisDisplay->displayLine2->numberTextMessages++;
					thisDisplay->displayLine2->displayLineDotsCreated = 0;
					thisDisplay->displayLine2->messageChanged = 1;
				break;
				case 3:
					thisDisplay->displayLine3->prePend = displayLineprePend;
					thisDisplay->displayLine3->charSet = displayLinecharSet;
					thisDisplay->displayLine3->lineNumber = displayLinelineNumber;
					Displayline_set_messageBuffer(thisDisplay->displayLine3, temp);
					//DB Added
				    thisDisplay->displayLine3->numberTextMessages++;
					thisDisplay->displayLine3->displayLineDotsCreated = 0;
					thisDisplay->displayLine3->messageChanged = 1;
				break;
#endif

#if TEST_MODE != ENABLE_DEFAULT_PL_MESSAGE_SENDER || TEST_MODE == ENABLE_DEFAULT_PL_MESSAGE_READER && TEST_MODE != NOT_IN_USE || TEST_MODE == ENABLE_DEFAULT_PL_MESSAGE_READER
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Decoded display line relates to default message to store in flash storage:
		//
		//Line 4 decoded: Message received stored as default startup message for Line 1 of display.
		//Line 5 decoded: Message received stored as default startup message for Line 2 of display.
		//Line 6 decoded: Message received stored as default startup message for Line 2 of display.
		//
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Default message for Line1 of display stored in Flash memory
		case 4:
#if DISPLAY_TYPE != SALOON_DISPLAY && END_CLIENT != CLASS150
			thisDisplay->defaultMessage->textMessage1->charSet = displayLinecharSet;
			thisDisplay->defaultMessage->textMessage1->prePend = displayLineprePend;
			thisDisplay->defaultMessage->textMessage1->modeDisplayLine = modeDisplayLine;
			DefaultMessage_setText(thisDisplay->defaultMessage->textMessage1->Text1, temp);
			NVmemory_writeDataTable(thisDisplay->displayLine1, thisDisplay->network->neuron, thisDisplay->nvmemory  );
#endif
#if DISPLAY_TYPE != SALOON_DISPLAY && END_CLIENT == CLASS150
			thisDisplay->defaultMessage->textMessage1->charSet = displayLinecharSet;
			thisDisplay->defaultMessage->textMessage1->prePend = displayLineprePend;
			thisDisplay->defaultMessage->textMessage1->modeDisplayLine = modeDisplayLine;
			DefaultMessage_setText(thisDisplay->defaultMessage->textMessage1->Text1, temp);
			NVmemory_writeDataTable(thisDisplay->displayLine1, thisDisplay->network->neuron, thisDisplay->nvmemory  );
#endif
		break;

		//Default message for Line2 of display stored in Flash memory
		//Class156 Project:
		// The current maintenance tool only sends default messages to line 2 of the display.
		// The Class156 project only displays text upon Display Line 1 of the display.
		// The code for the Class156 project thus takes the information sent from the APIS
		// maintenance tool that was originally destined for line 2 of the display and
		// makes it the the default message for line 1.

		case 5:
#if DISPLAY_TYPE != SALOON_DISPLAY && END_CLIENT != CLASS150
			thisDisplay->defaultMessage->textMessage2->charSet = displayLinecharSet;
			thisDisplay->defaultMessage->textMessage2->prePend = displayLineprePend;
			thisDisplay->defaultMessage->textMessage2->modeDisplayLine = modeDisplayLine;
			DefaultMessage_setText(thisDisplay->defaultMessage->textMessage2->Text2, temp);
			NVmemory_writeDataTable(thisDisplay->displayLine2, thisDisplay->network->neuron, thisDisplay->nvmemory  );
#endif
#if DISPLAY_TYPE != SALOON_DISPLAY && END_CLIENT == CLASS150
			thisDisplay->defaultMessage->textMessage1->charSet = displayLinecharSet;
			thisDisplay->defaultMessage->textMessage1->prePend = displayLineprePend;
			thisDisplay->defaultMessage->textMessage1->modeDisplayLine = modeDisplayLine;
			DefaultMessage_setText(thisDisplay->defaultMessage->textMessage1->Text1, temp);
			NVmemory_writeDataTable(thisDisplay->displayLine1, thisDisplay->network->neuron, thisDisplay->nvmemory  );
#endif
		break;

		//Default message for Line3 of display stored in Flash memory
		case 6:
			//thisDisplay->defaultMessage->textMessage3->charSet = displayLinecharSet;
			//thisDisplay->defaultMessage->textMessage3->prePend = displayLineprePend;
			//thisDisplay->defaultMessage->textMessage3->modeDisplayLine = modeDisplayLine;
			//DefaultMessage_setText(thisDisplay->defaultMessage->textMessage3->Text3, temp);
			//NVmemory_writeDataTable(thisDisplay->displayLine3, thisDisplay->network->neuron, thisDisplay->nvmemory  );
		break;
#endif
		default:
		break;
		} // switch (displayLinelineNumber)
	return;
	} //		if ( ( displayLinecharSet > 0 && displayLinecharSet < 4 ) &&
	  //		(displayLinelineNumber > 0 && displayLinelineNumber < 4 ) &&
	  //				( displayLineprePend < 130 ) )
return;
}

void Network_ReportFeatureSetMessageLP( Network network )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	static char counter = 0;
	if (counter > 250)
	{
		counter = 0;
	}
	else
	{
		counter++;
	}

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_TFXMSG;		//1
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'F';
      messageCount++;												//3
	  index = 0;

	  // Feedback Message Type \F02
	  network->sendMessageBuffer->data[messageCount] = 'L';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = counter;
	  messageCount++;												//5

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;														//11
	  messageCount++;
	  }
	  index = 0;
	  while (index < 8)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->prog_id[index];
	  index++;
	  messageCount++;												//19
	  }
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[0];
      messageCount++;												//20
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[1];
      messageCount++;												//21
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[2];
      messageCount++;												//22
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[3];
      messageCount++;												//23
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->displaytype;
      messageCount++;												//24
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->functionality;
      messageCount++;												//25
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->client;
      messageCount++;												//26

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 24);
      messageCount++;												//27
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 16);
      messageCount++;												//28
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 8);
      messageCount++;												//29
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberCRCPasses;
      messageCount++;												//30

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 24);
      messageCount++;												//31
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 16);
      messageCount++;												//32
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 8);
      messageCount++;												//33
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberCRCFailures;
      messageCount++;												//34

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 24);
      messageCount++;												//35
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 16);
      messageCount++;												//36
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 8);
      messageCount++;												//37
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages);
      messageCount++;												//38


      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}

unsigned char Network_globalCompare( Neuron neuron )
{
	unsigned char index;
	index = 0;
	while (index < 6)
	{
		if ( neuron->id[index] != 0xFF )
		{
			return 0;
		}
		index++;
	}
	return 1;
}


void Network_ECAMessage( Network network )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 20+2;
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 'F';
      messageCount++;
	  index = 0;

	  // Feedback Message Type \F04
	  network->sendMessageBuffer->data[messageCount] = 0x00;
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 0x04;
	  messageCount++;

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;
	  messageCount++;
	  }

	  network->sendMessageBuffer->data[messageCount] = network->numberServiceECAPacketsSent;
	  messageCount++;


	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberECARequests >> 24);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberECARequests >> 16);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberECARequests >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberECARequests;
      messageCount++;

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberECAAck >> 24);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberECAAck >> 16);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberECAAck >> 8);
      messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberECAAck;
      messageCount++;

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}



void Network_EchoMessage( Network network )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	unsigned char message_length;

	if ( thisDisplay->displayLine1->TextMessageBuffer->m_length > 100 )
	{
		message_length = 100;
	}
	else
	{
		message_length = thisDisplay->displayLine1->TextMessageBuffer->m_length;
	}

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 14 + (message_length);  // Total 14 + m_length
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;		//1
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'F';
      messageCount++;												//3
	  index = 0;

	  // Feedback Message Type \F05
	  network->sendMessageBuffer->data[messageCount] = 0x00;
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = 0x05;
	  messageCount++;												//5

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;
	  messageCount++;												//11
	  }

	  network->sendMessageBuffer->data[messageCount] = thisDisplay->displaytype;;
	  index++;
	  messageCount++;
	  //12
	  index = 0;
	  while (index < message_length )
	  {
		  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->displayLine1->TextMessageBuffer->m_message_text[index]);
		  messageCount++;											// 12 + m_length
		  index++;
	  }

      //Calculate the CRC for the Echo Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												// 12 + m_length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												// 13 + m_length

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}

void Network_PowerCycleMessage( Network network )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 14;  // Total 14 + m_length
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;		//1
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'F';
      messageCount++;												//3
	  index = 0;

	  // Feedback Message Type \F07
	  network->sendMessageBuffer->data[messageCount] = 0x00;
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = 0x07;
	  messageCount++;												//5

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;
	  messageCount++;												//11
	  }

	  network->sendMessageBuffer->data[messageCount] = thisDisplay->displaytype;;
	  index++;
	  messageCount++;
	  //12

      //Calculate the CRC for the Echo Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												// 13
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												// 14

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}
}

void Network_ReportFeatureSetMessageHP( Network network )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;

	static char counter = 64;
	if (counter == 90)
	{
		counter = 65;
	}
	else
	{
		counter++;
	}
	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;		//1
	  network->sendMessageBuffer->data[messageCount] = 'H';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'H';
      messageCount++;												//3
	  index = 0;

	  // Feedback Message Type \F02
	  network->sendMessageBuffer->data[messageCount] = 'H';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = 'H';
	  messageCount++;												//5

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = counter;
	  index++;														//11
	  messageCount++;
	  }
	  index = 0;
	  while (index < 8)
	  {
	  network->sendMessageBuffer->data[messageCount] = counter;
	  index++;
	  messageCount++;												//19
	  }
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//20
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//21
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//22
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//23
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//24
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//25
	  network->sendMessageBuffer->data[messageCount] = counter;
      messageCount++;												//26

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 24);
      messageCount++;												//27
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 16);
      messageCount++;												//28
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 8);
      messageCount++;												//29
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberCRCPasses;
      messageCount++;												//30

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 24);
      messageCount++;												//31
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 16);
      messageCount++;												//32
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 8);
      messageCount++;												//33
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberCRCFailures;
      messageCount++;												//34

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 24);
      messageCount++;												//35
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 16);
      messageCount++;												//36
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 8);
      messageCount++;												//37
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages);
      messageCount++;												//38


      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  //tryCounter++;
	  //if (tryCounter > network->numberStateMCCalls)
	  //{
	  //	  tryCounter = 0;
	//	  stateCount = 0;
	 // }
	}

}



void Network_TriggerDiagnosticMessage( Network network )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;

	static char counter = 0;
	if (counter > 250)
	{
		counter = 0;
	}
	else
	{
		counter++;
	}
	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  (network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_TRIGGER_DIAG;		//1
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'T';
      messageCount++;												//3
	  index = 0;

	  // Feedback Message Type \F02
	  network->sendMessageBuffer->data[messageCount] = 'G';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = counter;
	  messageCount++;												//5

	  while (index < 6)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->id[index];
	  index++;														//11
	  messageCount++;
	  }
	  index = 0;
	  while (index < 8)
	  {
	  network->sendMessageBuffer->data[messageCount] = network->neuron->prog_id[index];
	  index++;
	  messageCount++;												//19
	  }
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[0];
      messageCount++;												//20
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[1];
      messageCount++;												//21
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[2];
      messageCount++;												//22
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->serialNumber[3];
      messageCount++;												//23
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->displaytype;
      messageCount++;												//24
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->functionality;
      messageCount++;												//25
	  network->sendMessageBuffer->data[messageCount] = thisDisplay->client;
      messageCount++;												//26

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 24);
      messageCount++;												//27
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 16);
      messageCount++;												//28
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCPasses >> 8);
      messageCount++;												//29
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberCRCPasses;
      messageCount++;												//30

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 24);
      messageCount++;												//31
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 16);
      messageCount++;												//32
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (network->numberCRCFailures >> 8);
      messageCount++;												//33
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) network->numberCRCFailures;
      messageCount++;												//34

	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 24);
      messageCount++;												//35
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 16);
      messageCount++;												//36
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages >> 8);
      messageCount++;												//37
	  network->sendMessageBuffer->data[messageCount] = (unsigned char) (thisDisplay->numberTextMessages);
      messageCount++;												//38


      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBuffer( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }

      // If we have not completed a MasterSlave transfer update the tryCounter.
	  // The tryCounter holds the number of times the MasterSlave statemachine code has been called.
	  // If the MasterSlave state machine code has been called 100 times without the
	  // successful completion of the MasterSlave transfer, drop out of the while loop.
	  tryCounter++;
	  if (tryCounter > network->numberStateMCCalls)
	  {
		  tryCounter = 0;
		  stateCount = 0;
	  }
	}

}


static volatile int transactionID = 0;


//! \brief Send a Default Display Message for a designated Line to be stored in Flash Memory.
//! \brief Used to Test Flash Storage in Displays present upon the current power line network.
//! \brief The pay load present in the Default Message will be received and flashed into the
//! \brief permanent flash store of all Flash Message Store enabled Displays.
//!
//! \param[in] Network  - Network object data structure
//! \param[in] messageToSend - character buffer to be included in the Default Message
//! \param[in] displayLine - the Display Line the Default Message will be stored at [ 1, 2 or 3].
//! \param[in] displayType - the Display Type the Default Message will be stored within [ S, D].
//!
//! \return Returns 1 if successfully sent Default Message, else return 0.
unsigned char  Network_DefaultDisplayLineMessage( Network network, char* messageToSend, char displayType, char displayLine  )
{
	char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	char temp[30];
    char foundDisplay = 0;

	//Create a new TransactionID to be used with the Default Message that will be created and
	//subsequently sent over the power line comms' channel.
	if (transactionID > 100)
	{
		transactionID = 0;
	}
	transactionID++;

	//Place into a temp buffer the currently created transactionID, Number of frames, Frame Number
	//to be used with the default message.
	//The transactionID changes upon every Default Message that is sent from this device.
	sprintf(temp, "\\T%03u\\X1\\F0", transactionID );

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  //(network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;	    //1

	  /*Insert the TransactionID, Number of Frames and Frame Number into the sendMessageBuffer
	   Message Type TXXX\X1\X0\
      */

	  if ( displayType == 'S' )
	  {
		  network->sendMessageBuffer->data[messageCount] = SUB_TT_SALOON;
		  messageCount++;
	  }
	  if ( displayType == 'D' )
	  {
		  network->sendMessageBuffer->data[messageCount] = SUB_TT_DESTINATION;
		  messageCount++;
	  }

	  index = 0;
	  while (index < (strlen( temp )))
	  {
	  network->sendMessageBuffer->data[messageCount] = temp[index];
	  index++;
	  messageCount++;
	  }

	  if ( displayType == 'S' && displayLine == '1')
	  {
	  //Insert the Display Line, CharacterSet and Prepend to be used in the
	  //default message to be stored in the Displays' Flash Memory
	  //and placed into the sendMessageBuffer.
	  //Display Type : Saloon Display
	  //Display Line : 1
	  //CharacterSet : 5
	  //Scroll Enable: 1 [enabled]
	  //Pre-pend : 112 columns
	  // \S4\C5\P112\M11
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'S';
      messageCount++;												//3
	  network->sendMessageBuffer->data[messageCount] = '4';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '5';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//12
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'M';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;
	  foundDisplay = 1;
	  }
	  else if ( displayType == 'S' && displayLine == '2')
	  {
	  //Insert the Display Line, CharacterSet and Prepend to be used in the
	  //default message to be stored in the Displays' Flash Memory
	  //and placed into the sendMessageBuffer.
	  //Display Type : Saloon Display
	  //Display Line : 2
	  //CharacterSet : 5
	  //Scroll Enable: 1 [enabled]
	  //Pre-pend : 112 columns
	  // Message Type \S5\C5\P112\M21
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'S';
      messageCount++;												//3
	  network->sendMessageBuffer->data[messageCount] = '5';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '5';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//12
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'M';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;
	  foundDisplay = 1;
	  }
	  else if ( displayType == 'S' && displayLine == '3')
	  {
	  //Insert the Display Line, CharacterSet and Prepend to be used in the
	  //default message to be stored in the Displays' Flash Memory
	  //and placed into the sendMessageBuffer.
	  //Display Type : Saloon Display
	  //Display Line : 3
	  //CharacterSet : 5
	  //Scroll Enable: 1 [enabled]
	  //Pre-pend : 112 columns
	  // Message Type \S6\C5\P112\M31
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'S';
      messageCount++;												//3
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '5';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//12
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'M';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '3';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;
	  foundDisplay = 1;
	  }
	  else if ( displayType == 'D' && displayLine == '1')
	  {
	  //Insert the Display Line, CharacterSet and Prepend to be used in the
      //default message to be stored in the Displays' Flash Memory
      //and placed into the sendMessageBuffer.
      //Display Type : Destination Display
	  //Display Line : 1
      //CharacterSet : 2
	  //Pre-pend     : 96 led columns
	  //Scroll Enable: 1 [enabled]
      //Message Type \D4\C5\P96\M11

	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'D';
      messageCount++;												//3
	  network->sendMessageBuffer->data[messageCount] = '4';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '9';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'M';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;

	  foundDisplay = 1;
	  }
	  else if ( displayType == 'D' && displayLine == '2')
	  {
	  //Insert the Display Line, CharacterSet and Prepend to be used in the
	  //default message to be stored in the Displays' Flash Memory
	  //and placed into the sendMessageBuffer.
	  //Display Type : Destination Display
      //Display Line : 2
	  //CharacterSet : 2
	  //Pre-pend     : 96 led columns
	  //Scroll Enable: 1 [enabled]
	  //Message Type \D5\C5\P96\M21
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'D';
      messageCount++;												//3
	  network->sendMessageBuffer->data[messageCount] = '5';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '9';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'M';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;
	  foundDisplay = 1;
	  }
	  else if ( displayType == 'D' && displayLine == '3')
	  {
	  //Insert the Display Line, CharacterSet and Prepend to be used in the
	  //default message to be stored in the Displays' Flash Memory
	  //and placed into the sendMessageBuffer.
	  //Display Type : Destination Display
	  //Display Line : 3
	  //CharacterSet : 2
	  //Pre-pend     : 96 led columns
	  //Scroll Enable: 1 [enabled]
	  //Message Type \D6\C5\P96\M31
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'D';
      messageCount++;												//3
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '9';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'M';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '3';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;
	  foundDisplay = 1;
	  }

	  //If we cannot find the display type and line number return failed.
	  if ( foundDisplay == 0 )
	  {
		  return 0;
	  }

	  //Check that the current messageToSend buffer will fit in the
	  //defaultMessageSize used to store Default Messages - currently set at 100 characters.
	  //If the message will not fit inside the defaultMessageSize limit return a fail = ;
	  int messageToSendLength = strlen( messageToSend );
	  if ( messageToSendLength > defaultMessageSize )
	  {
		  return 0;
	  }
	  else
	  {
		  // Place into the sendMessageBuffer the contents of the messageToSendBuffer
		  index = 0;
		  while (index <= defaultMessageSize)
		  {
		  network->sendMessageBuffer->data[messageCount] = messageToSend[index];
		  index++;
		  messageCount++;
		  }

		  //Calculate the CRC for the Passenger Counter Message.
		  CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
		  network->CRCResult = crcResult;

		  //Place unsigned short CRC value into two unsigned char's of message buffer
		  //and add two more bytes to the overall message length
		  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
		  messageCount++;												//39
		  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
		  messageCount++;												//40

		  (network->sendMessageBuffer->transLen) = messageCount;

		  for (; messageCount < messageLength; messageCount++)
		  {
			  network->sendMessageBuffer->data[messageCount] = character;
		  }

		  Network_createMessageBuffer( network, messageBuffer, messageLength );

		  tryCounter = 0;
		  stateCount = 1;

		}

		while (stateCount == 1)
		{

		  // Delay the while loop for 500ms
		  // This has been placed into the loop to allow the Echelon Neuron
		  // Time to react to the MasterSlave message request.
		  delay_ms(85);

		  //Send the message buffer asking for the NeuronID to the Echelon Neuron
		  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

		  // Has the MasterSlave transfer completed?
		  // Completion takes place when the MasterSlave state machine reaches State 8.
		  // If the MasterSlave transfer is complete place the stateCount = 0
		  // - dropping us out of the current while loop.
		  if ( network->msState == 8 )
		  {
			  network->msState = 0;
			  stateCount = 0;
			  tryCounter = 0;
			  return 1;
		  }
		}
	}
return 0;
}

void Network_SendSaloonLine1TextMessage( Network network, char* messageToSend  )
{
	static char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	volatile char temp[224];
	char messageToSendLength = 0;

	if (transactionID > 100)
	{
		transactionID = 0;
	}

	transactionID++;
	messageCount = 0;

	sprintf(temp, "\\T%03u\\X3\\F0", transactionID );

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  //(network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;	    //1

	  {
		  network->sendMessageBuffer->data[messageCount] = SUB_TT_SALOON;
		  messageCount++;
	  }
	  // Message Type TXXX\X3\F0\S4\C4\P112
	  index = 0;
	  while (index < (strlen( temp )))
	  {
	  network->sendMessageBuffer->data[messageCount] = temp[index];
	  index++;
	  messageCount++;
	  }

	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'S';
      messageCount++;												//3

	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//12

	  //Place the message that has been sent in to function into a temp store
      //Range check the message to ensure that it is under the 200 character threshold.
      messageToSendLength = strlen( messageToSend );
	  if ( messageToSendLength <= 200 )
	  {
		  sprintf( temp , messageToSend );
	  }
	  else
	  {
		  sprintf( temp, "Message frame 0 cannot fit into buffer" );
		  messageToSendLength = strlen( temp );
	  }

	  //Move the contents of the temp buffer into the sendMessageBuffer
	  index = 0;
	  while (index < (messageToSendLength - 1) )
	  {
	  network->sendMessageBuffer->data[messageCount] = messageToSend[index];
	  index++;
	  messageCount++;
	  }

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  (network->sendMessageBuffer->transLen) = messageCount;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBufferSaloon( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }
	}

	Network_sendMessageFrame( network, messageToSend, 1, 4  );
	Network_sendMessageFrame( network, messageToSend, 2, 4  );
	Network_sendMessageFrame( network, messageToSend, 3, 4  );

}


void Network_SendDestinationLine1TextMessage( Network network, char* messageToSend  )
{
	static char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	volatile char temp[224];
	char messageToSendLength = 0;

	if (transactionID > 100)
	{
		transactionID = 0;
	}

	transactionID++;
	messageCount = 0;

	sprintf(temp, "\\T%03u\\X1\\F0", transactionID );

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  //(network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;	    //1

	  {
		  network->sendMessageBuffer->data[messageCount] = SUB_TT_DESTINATION;
		  messageCount++;
	  }
	  // Message Type TXXX\X1\F0\D1\C5\P96
	  index = 0;
	  while (index < (strlen( temp )))
	  {
	  network->sendMessageBuffer->data[messageCount] = temp[index];
	  index++;
	  messageCount++;
	  }

	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'D';
      messageCount++;												//3

	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '9';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 'M';
	  messageCount++;												//12
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//13
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//14

	  //Place the message that has been sent in to function into a temp store
      //Range check the message to ensure that it is under the 200 character threshold.
      messageToSendLength = strlen( messageToSend );
	  if ( messageToSendLength <= 200 )
	  {
		  sprintf( temp , messageToSend );
	  }
	  else
	  {
		  sprintf( temp, "Message frame 0 cannot fit into buffer" );
		  messageToSendLength = strlen( temp );
	  }

	  //Move the contents of the temp buffer into the sendMessageBuffer
	  index = 0;
	  while (index < (messageToSendLength) )
	  {
	  network->sendMessageBuffer->data[messageCount] = messageToSend[index];
	  index++;
	  messageCount++;
	  }

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  (network->sendMessageBuffer->transLen) = messageCount;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBufferSaloon( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }
	}
}


void Network_SendDestinationLine2TextMessage( Network network, char* messageToSend  )
{
	static char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	volatile char temp[224];
	char messageToSendLength = 0;

	if (transactionID > 100)
	{
		transactionID = 0;
	}

	transactionID++;
	messageCount = 0;

	sprintf(temp, "\\T%03u\\X1\\F0", transactionID );

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  //(network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;	    //1

	  {
		  network->sendMessageBuffer->data[messageCount] = SUB_TT_DESTINATION;
		  messageCount++;
	  }
	  // Message Type TXXX\X1\F0\D1\C5\P96
	  index = 0;
	  while (index < (strlen( temp )))
	  {
	  network->sendMessageBuffer->data[messageCount] = temp[index];
	  index++;
	  messageCount++;
	  }

	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//2
	  network->sendMessageBuffer->data[messageCount] = 'D';
      messageCount++;												//3

	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//4
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//5
	  network->sendMessageBuffer->data[messageCount] = 'C';
	  messageCount++;												//6
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;	                                            //7
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;												//8
	  network->sendMessageBuffer->data[messageCount] = 'P';
      messageCount++;												//9
	  network->sendMessageBuffer->data[messageCount] = '9';
	  messageCount++;												//10
	  network->sendMessageBuffer->data[messageCount] = '6';
	  messageCount++;												//11
	  network->sendMessageBuffer->data[messageCount] = '\\';
	  messageCount++;
	  network->sendMessageBuffer->data[messageCount] = 'M';
	  messageCount++;												//12
	  network->sendMessageBuffer->data[messageCount] = '2';
	  messageCount++;												//13
	  network->sendMessageBuffer->data[messageCount] = '1';
	  messageCount++;												//14
	  //Place the message that has been sent in to function into a temp store
      //Range check the message to ensure that it is under the 200 character threshold.
      messageToSendLength = strlen( messageToSend );
	  if ( messageToSendLength <= 200 )
	  {
		  sprintf( temp , messageToSend );
	  }
	  else
	  {
		  sprintf( temp, "Message frame 0 cannot fit into buffer" );
		  messageToSendLength = strlen( temp );
	  }

	  //Move the contents of the temp buffer into the sendMessageBuffer
	  index = 0;
	  while (index < (messageToSendLength) )
	  {
	  network->sendMessageBuffer->data[messageCount] = messageToSend[index];
	  index++;
	  messageCount++;
	  }

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  (network->sendMessageBuffer->transLen) = messageCount;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBufferSaloon( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }
	}
}

void Network_sendMessageFrame( Network network, char* messageToSend, int frameNumber, int totalFrameNumber  )
{

	static char character = 65;
	static int tryCounter = 0;
	static unsigned char stateCount = 0;
	unsigned char messageLength = MAX_SPI_LEN;
	char messageBuffer[224];
	unsigned char index;
	int messageCount = 0;
	static short crcResult = 0;
	volatile char temp[224];
	char messageToSendLength = 0;

    // Send second part of multiple frame message with new transactionID and frame number
	messageCount = 0;

	if (stateCount == 0 && network->smState == 0 && network->msState == 0 )
	{
	  //(network->sendMessageBuffer->transLen) = 26+8+4+2;			//Total 40 bytes
	  (network->sendMessageBuffer->transType) = TT_DOWN_MSG;	    //1

	  {
		  network->sendMessageBuffer->data[messageCount] = SUB_TT_SALOON;
		  messageCount++;
	  }

	  /*Place escape sequence into the start of the message frame to be sent
	   Message Type TXXX\X3\F2\
      */

	  sprintf(temp, "\\T%03u\\X%01u\\F%01u", transactionID, totalFrameNumber, frameNumber );

	  //Copy the escape sequence for the frame to be sent into the sendMessageBuffer[]
	  index = 0;
	  while (index < (strlen( temp )))
	  {
	  network->sendMessageBuffer->data[messageCount] = temp[index];
	  index++;
	  messageCount++;
	  }

	  //Place the message that has been sent in to function into a temp store
      //Range check the message to ensure that it is under the 200 character threshold.
      messageToSendLength = strlen( messageToSend );
	  if ( messageToSendLength <= 200 )
	  {
		  sprintf( temp , messageToSend );
	  }
	  else
	  {
		  sprintf( temp, "Message frame %03u cannot fit into buffer", frameNumber );
		  messageToSendLength = strlen( temp );
	  }

	  //If we are NOT sending the last message frame in the transaction we need to removed the NULL at the end of the string.
	  //We thus populate the message buffer with the message less the last character
      if ( (frameNumber + 1) < totalFrameNumber )
      {
		  //Move the contents of the temp buffer into the sendMessageBuffer
		  index = 0;
		  while (index < (messageToSendLength - 1) )
		  {
		  network->sendMessageBuffer->data[messageCount] = temp[index];
		  index++;
		  messageCount++;
		  }
      }
      //If we are sending the last message frame in the transaction we need to send the message with the NULL at the end of the string
      else
      {
		  //Move the contents of the temp buffer into the sendMessageBuffer
		  index = 0;
		  while (index < messageToSendLength)
		  {
		  network->sendMessageBuffer->data[messageCount] = temp[index];
		  index++;
		  messageCount++;
		  }
      }

      //Calculate the CRC for the Passenger Counter Message.
      CRC_calcCrc16Block( network->sendMessageBuffer->data, (messageCount) , &crcResult);
      network->CRCResult = crcResult;

	  //Place unsigned short CRC value into two unsigned char's of message buffer
	  //and add two more bytes to the overall message length
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)(crcResult >> 8);
	  messageCount++;												//39
	  network->sendMessageBuffer->data[messageCount] = (unsigned char)crcResult;
	  messageCount++;												//40


	  (network->sendMessageBuffer->transLen) = messageCount;

	  for (; messageCount < messageLength; messageCount++)
	  {
		  network->sendMessageBuffer->data[messageCount] = character;
	  }
	  //Create the message buffer to send to Neuron to request ID.
	  Network_createMessageBufferSaloon( network, messageBuffer, messageLength );
	  tryCounter = 0;
	  stateCount = 1;

	}

	while (stateCount == 1)
	{

	  // Delay the while loop for 500ms
	  // This has been placed into the loop to allow the Echelon Neuron
	  // Time to react to the MasterSlave message request.
	  delay_ms(85);

	  //Send the message buffer asking for the NeuronID to the Echelon Neuron
	  Network_spiMasterSlaveTransfer( network, messageBuffer, messageLength );

	  // Has the MasterSlave transfer completed?
	  // Completion takes place when the MasterSlave state machine reaches State 8.
	  // If the MasterSlave transfer is complete place the stateCount = 0
	  // - dropping us out of the current while loop.
	  if ( network->msState == 8 )
	  {
		  network->msState = 0;
		  stateCount = 0;
		  tryCounter = 0;
	  }
	}
}

