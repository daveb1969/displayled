/*
 * defaultmessage.c
 *
 *  Created on: 1 Sep 2010
 *      Author: DAVE
 */

// Standard C library includes.
#include "stdlib.h"
#include "string.h"
#include "compiler.h"
#include "malloc.h"
#include "stdio.h"
#include "board.h"


// Project includes.
#include "defaultmessage.h"
#include "tfx_assert.h"



// Constructor
//
//
DefaultMessage DefaultMessage_construct() {
	TextMessage1 textMessage1 = ( struct defaultMessage1_str* ) malloc(sizeof(struct defaultMessage1_str));
	TFX_ASSERT(textMessage1, "Expected a non-null pointer1.");
	TextMessage2 textMessage2 = ( struct defaultMessage2_str* ) malloc(sizeof(struct defaultMessage2_str));
	TFX_ASSERT(textMessage2, "Expected a non-null pointer2.");
	TextMessage3 textMessage3 = ( struct defaultMessage3_str* ) malloc(sizeof(struct defaultMessage3_str));
	TFX_ASSERT(textMessage3, "Expected a non-null pointer3.");
	DefaultMessage defaultMessage = ( struct defaultmessage_str* ) malloc(sizeof(struct defaultmessage_str));
	TFX_ASSERT(defaultMessage, "Expected a non-null pointer4.");

	defaultMessage->textMessage1 = textMessage1;
	defaultMessage->textMessage2 = textMessage2;
	defaultMessage->textMessage3 = textMessage3;

#if DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT == CLASS319
	defaultMessage->textMessage1->charSet = 5;
	defaultMessage->textMessage1->prePend = 112;
	defaultMessage->textMessage1->modeDisplayLine = 1;

	defaultMessage->textMessage2->charSet = 5;
	defaultMessage->textMessage2->prePend = 112;
	defaultMessage->textMessage2->modeDisplayLine = 1;

	defaultMessage->textMessage3->charSet = 5;
	defaultMessage->textMessage3->prePend = 112;
	defaultMessage->textMessage3->modeDisplayLine = 1;

	defaultMessage->textMessage1->storedInFlash = 0x00;
	defaultMessage->textMessage2->storedInFlash = 0x00;
	defaultMessage->textMessage3->storedInFlash = 0x00;

	if ( defaultMessage->textMessage1->Text1 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage1->Text1, "Welcome aboard this service.");
	}
	if ( defaultMessage->textMessage2->Text2 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage2->Text2, ".");
	}
	if ( defaultMessage->textMessage3->Text3 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage3->Text3, ".");
	}

#elif DISPLAY_TYPE == SALOON_DISPLAY && END_CLIENT == CLASS150
	defaultMessage->textMessage1->charSet = 5;
	defaultMessage->textMessage1->prePend = 112;
	defaultMessage->textMessage1->modeDisplayLine = 1;

	defaultMessage->textMessage2->charSet = 5;
	defaultMessage->textMessage2->prePend = 112;
	defaultMessage->textMessage2->modeDisplayLine = 1;

	defaultMessage->textMessage3->charSet = 5;
	defaultMessage->textMessage3->prePend = 112;
	defaultMessage->textMessage3->modeDisplayLine = 1;

	defaultMessage->textMessage1->storedInFlash = 0x00;
	defaultMessage->textMessage2->storedInFlash = 0x00;
	defaultMessage->textMessage3->storedInFlash = 0x00;

	if ( defaultMessage->textMessage1->Text1 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage1->Text1, "Welcome aboard this service.");
	}
	if ( defaultMessage->textMessage2->Text2 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage2->Text2, ".");
	}
	if ( defaultMessage->textMessage3->Text3 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage3->Text3, ".");
	}

#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY && END_CLIENT != CLASS150
	defaultMessage->textMessage1->charSet = 2;
	defaultMessage->textMessage1->prePend = 96;
	defaultMessage->textMessage1->modeDisplayLine = 1;

	defaultMessage->textMessage2->charSet = 2;
	defaultMessage->textMessage2->prePend = 96;
	defaultMessage->textMessage2->modeDisplayLine = 1;

	defaultMessage->textMessage3->charSet = 2;
	defaultMessage->textMessage3->prePend = 96;
	defaultMessage->textMessage3->modeDisplayLine = 1;

	defaultMessage->textMessage1->storedInFlash = 0x00;
	defaultMessage->textMessage2->storedInFlash = 0x00;
	defaultMessage->textMessage3->storedInFlash = 0x00;

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY && END_CLIENT == CLASS319
	defaultMessage->textMessage1->charSet = 2;
	defaultMessage->textMessage1->prePend = 99;
	defaultMessage->textMessage1->modeDisplayLine = 1;

	defaultMessage->textMessage2->charSet = 2;
	defaultMessage->textMessage2->prePend = 99;
	defaultMessage->textMessage2->modeDisplayLine = 1;

	defaultMessage->textMessage3->charSet = 2;
	defaultMessage->textMessage3->prePend = 0;
	defaultMessage->textMessage3->modeDisplayLine = 0;

	defaultMessage->textMessage1->storedInFlash = 0x00;
	defaultMessage->textMessage2->storedInFlash = 0x00;
	defaultMessage->textMessage3->storedInFlash = 0x00;

	if ( defaultMessage->textMessage1->Text1 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage1->Text1, " ");
	}
	if ( defaultMessage->textMessage2->Text2 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage2->Text2, "Welcome aboard this service.");
	}
	if ( defaultMessage->textMessage3->Text3 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage3->Text3, " ");
	}
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY && END_CLIENT == CLASS150
	defaultMessage->textMessage1->charSet = 2;
	defaultMessage->textMessage1->prePend = 0;
	defaultMessage->textMessage1->modeDisplayLine = 1;

	defaultMessage->textMessage2->charSet = 2;
	defaultMessage->textMessage2->prePend = 0;
	defaultMessage->textMessage2->modeDisplayLine = 1;

	defaultMessage->textMessage3->charSet = 2;
	defaultMessage->textMessage3->prePend = 0;
	defaultMessage->textMessage3->modeDisplayLine = 0;

	defaultMessage->textMessage1->storedInFlash = 0x00;
	defaultMessage->textMessage2->storedInFlash = 0x00;
	defaultMessage->textMessage3->storedInFlash = 0x00;

	if ( defaultMessage->textMessage1->Text1 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage1->Text1, "Welcome aboard");
	}
	if ( defaultMessage->textMessage2->Text2 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage2->Text2, " ");
	}
	if ( defaultMessage->textMessage3->Text3 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage3->Text3, " ");
	}

#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY && END_CLIENT == CLASS150
	defaultMessage->textMessage1->charSet = 2;
	defaultMessage->textMessage1->prePend = 0;
	defaultMessage->textMessage1->modeDisplayLine = 1;

	defaultMessage->textMessage2->charSet = 2;
	defaultMessage->textMessage2->prePend = 0;
	defaultMessage->textMessage2->modeDisplayLine = 1;

	defaultMessage->textMessage3->charSet = 2;
	defaultMessage->textMessage3->prePend = 0;
	defaultMessage->textMessage3->modeDisplayLine = 0;

	defaultMessage->textMessage1->storedInFlash = 0x00;
	defaultMessage->textMessage2->storedInFlash = 0x00;
	defaultMessage->textMessage3->storedInFlash = 0x00;

	if ( defaultMessage->textMessage1->Text1 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage1->Text1, "Welcome aboard");
	}
	if ( defaultMessage->textMessage2->Text2 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage2->Text2, " ");
	}
	if ( defaultMessage->textMessage3->Text3 != NULL )
	{
	DefaultMessage_setText(defaultMessage->textMessage3->Text3, " ");
	}
#else
  #error No DISPLAY_TYPE defined
#endif

  return defaultMessage;
}

// Destructor.
//
//
void DefaultMessage_destroy(TextMessage1 defaultmessage) {
  TFX_ASSERT( defaultmessage , "Expected a non-null pointer5.");
  free(defaultmessage);
}


void DefaultMessage_setText(char *textMessage, char* messageToStore )
{
	if ( strlen( messageToStore ) <= defaultMessageSize )
	{
		sprintf( textMessage , messageToStore );
	}
	else
	{
		sprintf( textMessage, "Message too long for storage" );
	}
}


