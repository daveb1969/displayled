/*! \file ******************************************************************
 *
 * \brief passengercounterdataset.h 		Passenger Counter.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage..
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carraiges on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#ifndef PASSENGERCOUNTERDATASET_H_
#define PASSENGERCOUNTERDATASET_H_

#include "passengercounterbuffer.h"

struct passengercounterdataset_str
{
	volatile PassengerCounterBuffer passengerCounter1;
	volatile PassengerCounterBuffer passengerCounter2;
	volatile PassengerCounterBuffer passengerCounter3;
	volatile PassengerCounterBuffer passengerCounter4;
    volatile unsigned short closedDoorID;						   // Closed Door ID
};

typedef struct passengercounterdataset_str* PassengerCounterDataSet;

// Constructor.
//
//
PassengerCounterDataSet PassengerCounterDataSet_construct();

// Destructor.
//
//
void PassengerCounterDataSet_destroy(PassengerCounterDataSet passengerCounterDataSet);


#endif /* PASSENGERCOUNTER_H_ */
