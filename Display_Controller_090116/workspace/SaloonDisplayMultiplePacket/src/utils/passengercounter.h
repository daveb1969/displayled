/*! \file ******************************************************************
 *
 * \brief passengercounter.h 		Passenger Counter.
 *
 * This file creates and provides services for the RS485 link to the
 * Passenger Counter found above each doorway in the saloon carriage..
 *
 * - Compiler:           GNU GCC for AVR32
 * - Supported devices:  All AVR32UC3B0256.
 * - AppNote:
 *
 *	The services provided allow the sending and receiving of commands and
 *	data from the PCN-1001 Passenger Counter mounted above each doorway
 *	in the saloon carraiges on the train
 *
 *
 *
 * \author               	TrainFX Limited: http://www.trainfx.com\n
 *                       	Unit 15,
 *							Melbourne Business Court, Millennium Way,
 *							Pride Park,
 *							Derby, DE24 8HZ,
 *							United Kingdom.
 *							Tel: 01332 366 175
 *
 *
 ***************************************************************************/

#ifndef PASSENGERCOUNTER_H_
#define PASSENGERCOUNTER_H_


#define MAX_DATA_LEN		50
#define MAX_PACKET_LEN		5+1+1+1+1+1+2+MAX_DATA_LEN+2+1
#define MAX_CRC_DIGITS       	256
#define BYTE_WIDTH            	8
#define CRC_WIDTH             	16
#define CRC_MASK              	0xFF

struct passengercounter_str
{
	volatile unsigned char SNP_passengerCounterAddress;         // SNP Address of Passenger Counter
	volatile unsigned int  countIn;			     		       // Passenger Counter In Count Value
	volatile unsigned int  countOut;         		      	   // Passenger Counter Out Count Value
    volatile unsigned char passengerCounterEnabled;			   // Passenger Counter Enabled status
    volatile unsigned short  crcPasses;						   // Passenger Counter crc Passes Count Value
    volatile unsigned short  crcFails;                          // Passenger Counter crc Fails Count Value
};

typedef struct passengercounter_str* PassengerCounter;

// Constructor.
//
//
PassengerCounter PassengerCounter_construct();

// Destructor.
//
//
void PassengerCounter_destroy(PassengerCounter passengerCounter);


#endif /* PASSENGERCOUNTER_H_ */
