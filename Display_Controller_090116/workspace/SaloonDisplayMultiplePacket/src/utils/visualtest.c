/*
 * visualtest.c
 *
 *  Created on: 6 Sep 2010
 *      Author: DAVE
 */

#include "visualtest.h"
#include "gpio.h"
#include "board.h"
#include "usart.h"
#include "displayExtern.h"
#include "visualtest.h"
#include "nvmemory.h"

extern void NVmemory_displayMessage1( Displayline displayLine, NVmemory nvmemory  );
extern void NVmemory_displayMessage2( Displayline displayLine, NVmemory nvmemory  );
extern void NVmemory_displayMessage3( Displayline displayLine, NVmemory nvmemory  );


extern char clkTick;

//! \brief Visualtest constructor
//!
//! Create Visualtest data structure off the heap.
//!
//! \return Return Visualtest*
//!
Visualtest Visualtest_construct() {
	Visualtest visualtest = ( struct visualtest_str* ) malloc(sizeof(struct visualtest_str));
	TFX_ASSERT(visualtest, "Failed to allocate memory.");
	visualtest->updateDisplayAtleastOnce = 0;
	visualtest->disableTestCycle = 0;
	return visualtest;
}

//! \brief Visualtest destructor
//!
//!
//! Destroy visualtest structure
//!
void Visualtest_destroy( Visualtest visualtest ) {
	free(visualtest);
}

void Visualtest_delay(unsigned long count){

	long delay1 = 0;
	long delay2 = 0;
	long delay3 = 0;
	while (delay1 < count){
		delay1++;
		while (delay2 < count){
				delay2++;
				while (delay3 < count){
						delay3++;
					}
			}
	}
}

void Visualtest_idCodes( Displayline displayLine, Neuron neuron )
{
  char characterBuffer[224];
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;

  int index;

  index  = sprintf( characterBuffer, "Neuron: %02x:", neuron->id[0]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->id[1]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->id[2]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->id[3]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->id[4]);
  index += sprintf( characterBuffer + index, "%02x", neuron->id[5]);
  index += sprintf( characterBuffer + index, " ProgID: %02x:", neuron->prog_id[0]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->prog_id[1]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->prog_id[2]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->prog_id[3]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->prog_id[4]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->prog_id[5]);
  index += sprintf( characterBuffer + index, "%02x:", neuron->prog_id[6]);
  index += sprintf( characterBuffer + index, "%02x", neuron->prog_id[7]);
  index += sprintf( characterBuffer + index, " Firmware: %02d:", thisDisplay->serialNumber[0]);
  index += sprintf( characterBuffer + index, "%02d:", thisDisplay->serialNumber[1]);
  index += sprintf( characterBuffer + index, "%02d:", thisDisplay->serialNumber[2]);
  index += sprintf( characterBuffer + index, "%02d ", thisDisplay->serialNumber[3]);
  switch (thisDisplay->functionality)
  {
  case Standard:
  index += sprintf( characterBuffer + index, " Type: Standard ");
  break;
  case Crew_Comms:
  index += sprintf( characterBuffer + index, " Type: Crew Comms");
  break;
  case ECAon_PCoff:
  index += sprintf( characterBuffer + index, " Type: ECA:ON PC:OFF ");
  break;
  case ECAon_PCon:
  index += sprintf( characterBuffer + index, " Type: ECA:ON PC:ON");
  break;
  case ECAoff_PCon:
  index += sprintf( characterBuffer + index, " Type: ECA:OFF PC:ON");
  break;
  default:
  index += sprintf( characterBuffer + index, " Type: %02x", thisDisplay->functionality);
  break;
  }

  if (thisDisplay->network->echoModeEnabled == 1)
  {
	  index += sprintf( characterBuffer + index, " ECHO: ON ");
  }
  else
  {
	  index += sprintf( characterBuffer + index, " ECHO: OFF ");
  }

#if PASSENGER_COUNTER_FUNCTION==PC_FUNCTION_ON
  if (thisDisplay->passengerCount->pcEOLMessageModeEnabled)
  {
	  index += sprintf( characterBuffer + index, " EOL: ON ");
  }
  else
  {
	  index += sprintf( characterBuffer + index, " EOL: OFF ");
  }
#endif

  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
}



void Visualtest_passengerCounter( Displayline displayLine, PassengerCount passengerCount )
{
  char characterBuffer[224];
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;
  static unsigned int passengerLoopCount = 0;

  int index;

  index  = sprintf( characterBuffer, "Counter 1 In:%u ", passengerCount->passengerCounter1->countIn);
  index += sprintf( characterBuffer + index, "Counter 1 Out:%u ", passengerCount->passengerCounter1->countOut);

  index += sprintf( characterBuffer + index, "Counter 2 In:%u ", passengerCount->passengerCounter2->countIn);
  index += sprintf( characterBuffer + index, "Counter 2 Out:%u ", passengerCount->passengerCounter2->countOut);

  index += sprintf( characterBuffer + index, "Counter 3 In:%u ", passengerCount->passengerCounter3->countIn);
  index += sprintf( characterBuffer + index, "Counter 3 Out:%u ", passengerCount->passengerCounter3->countOut);

  index += sprintf( characterBuffer + index, "Counter 4 In:%u ", passengerCount->passengerCounter4->countIn);
  index += sprintf( characterBuffer + index, "Counter 4 Out:%u ", passengerCount->passengerCounter4->countOut);

  passengerLoopCount++;

  index += sprintf( characterBuffer + index, "Message Count:%u ", passengerLoopCount );

  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
}


void Visualtest_movingBlock( Displayline displayLine )
{
	char characterBuffer[224];
#if  DISPLAY_TYPE == SALOON_DISPLAY
	characterBuffer[0] = 0x7F;
	characterBuffer[1] = 0x00;
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
	characterBuffer[0] = 0x7F;
	characterBuffer[1] = 0x00;
#elif DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	characterBuffer[0] = 0x7F;
	characterBuffer[1] = 0x00;
#endif

  displayLine->messageComplete = 0x00;
  Displayline_set_messageBuffer(displayLine, characterBuffer);
  displayLine->displayLineDotsCreated = 0;
  displayLine->messageChanged = 1;

}


void Visualtest_opto( Displayline displayLine, Optoinputs optoinputs, Visualtest visualtest  )
{

  static volatile unsigned int opto_01_gpio_last = 0;
  static volatile unsigned int opto_02_gpio_last = 0;
  static volatile unsigned int opto_03_gpio_last = 0;
  int index;
  char characterBuffer[224];

  unsigned int opto_01_gpio = optoinputs->opto_Input1->optoInput;
  unsigned int opto_02_gpio = optoinputs->opto_Input2->optoInput;
  unsigned int opto_03_gpio = optoinputs->opto_Input3->optoInput;


  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

  if ( opto_01_gpio_last == opto_01_gpio && \
		  opto_02_gpio_last == opto_02_gpio && \
		          opto_03_gpio_last == opto_03_gpio && \
		        		  visualtest->updateDisplayAtleastOnce == 0 )
  {
	  return;
  }

  index  = sprintf( characterBuffer, "Op1: %u", opto_01_gpio);
  index += sprintf( characterBuffer + index, " Op2: %u", opto_02_gpio);
  index += sprintf( characterBuffer + index, " Op3: %u", opto_03_gpio);

  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;

  opto_01_gpio_last = optoinputs->opto_Input1->optoInput;
  opto_02_gpio_last = optoinputs->opto_Input2->optoInput;
  opto_03_gpio_last = optoinputs->opto_Input3->optoInput;

  visualtest->updateDisplayAtleastOnce = 0;
}



void Visualtest_heap( Displayline displayLine, Visualtest visualtest )
{
  static int step = 0;
  static int messageChanged = 0;
  unsigned int heapSize;
  static unsigned int lastHeapSize = 0;
  char characterBuffer[224];

  heapSize = getHeapFreeSize();

  if ( visualtest->updateDisplayAtleastOnce == 1)
  {
	  step = 0;
	  visualtest->updateDisplayAtleastOnce = 0;
  }

  if ( step == 0 )
  {
	  sprintf( characterBuffer, "heapfree:%u", heapSize );
	  displayLine->messageComplete = 0x00;
	  messageChanged = 1;
	  lastHeapSize = heapSize;
	  step++;
  }

  if ( step == 1 && lastHeapSize != heapSize )
  {
	  step = 0;
  }

  if (messageChanged)
  {
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
		displayLine->displayLineDotsCreated = 0;
		displayLine->messageChanged = 1;
  }
  messageChanged = 0;

}


void Visualtest_rs485( Displayline displayLine )
{

  static int character = 65;
  static char character2 = 65;
  static volatile int messageChanged = 0;
  char val[224];
  int index;

  character = usart_getchar(RS485_USART);
  character2 = character;

  if (character == -1)
  {
	  return;
  }

  index  = sprintf( val, "Character: %c", character);
  displayLine->messageComplete = 0x00;
  messageChanged = 1;

  if (messageChanged)
  {
	    displayLine->prePend = 0;
	    displayLine->scrollEnable =0;
		Displayline_set_messageBuffer(displayLine, val);
		Displayline_createDots(displayLine);
		displayLine->messageChanged = 1;
  }
  messageChanged = 0;

}



void Visualtest_loopCount( Displayline displayLine )
{
	char characterBuffer[224];
	static unsigned int loopCount = 0;
	displayLine->prePend = 0;
	displayLine->scrollEnable = 0;

	  int index;

	  index  = sprintf( characterBuffer, "Loop Count :%u ", loopCount );

	  displayLine->messageComplete = 0x00;

	#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
		  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
	#else
		  Displayline_set_messageBuffer(displayLine, characterBuffer);
	#endif
		  displayLine->displayLineDotsCreated = 0;
		  displayLine->messageChanged = 1;
		  loopCount++;

}

void Visualtest_optoCount( Displayline displayLine, Optoinputs optoinputs, Visualtest visualtest )
{
  int index = 0;
  static int opto1_count_last = -1;
  static int opto2_count_last = -1;
  static int opto3_count_last = -1;
  char characterBuffer[224];

  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

  int opto1_count = optoinputs->opto_Input1->optoChangedCounter;
  int opto2_count = optoinputs->opto_Input2->optoChangedCounter;
  int opto3_count = optoinputs->opto_Input3->optoChangedCounter;

  if ( opto1_count == opto1_count_last && \
		  opto2_count == opto2_count_last && \
		      opto3_count == opto3_count_last && \
		    		  visualtest->updateDisplayAtleastOnce == 0 )
  {
	  return;
  }


  index  = sprintf( characterBuffer, "Op1:%d", opto1_count);
  index += sprintf( characterBuffer + index, " Op2:%d", opto2_count);
  index += sprintf( characterBuffer + index, " Op3:%d", opto3_count );
  displayLine->messageComplete = 0x00;

#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
  displayLine->displayLineDotsCreated = 0;
  displayLine->messageChanged = 1;

  opto1_count_last = optoinputs->opto_Input1->optoChangedCounter;
  opto2_count_last = optoinputs->opto_Input2->optoChangedCounter;
  opto3_count_last = optoinputs->opto_Input3->optoChangedCounter;

  visualtest->updateDisplayAtleastOnce = 0;
}
/*
void Visualtest_rs485( Displayline displayLine )
{

  static int character = 65;
  static char character2 = 65;
  static volatile int messageChanged = 0;
  int index;

  gpio_clr_gpio_pin(RS485_USART_CTS_PIN);
  gpio_clr_gpio_pin(RS485_USART_RTS_PIN);
  character = usart_getchar(RS485_USART);
  gpio_set_gpio_pin(RS485_USART_CTS_PIN);
  gpio_set_gpio_pin(RS485_USART_RTS_PIN);

  character2 = character;

  if (character == -1)
  {
	  return;
  }

  index  = sprintf( gcharacterBuffer, "Char %c", character);
  displayLine->messageComplete = 0x00;
  messageChanged = 1;

  if (messageChanged)
  {
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, gcharacterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, gcharacterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
  }
  messageChanged = 0;

}
*/


void Visualtest_RXTriggerCounter( Displayline displayLine, PassengerCount passengerCount )
{

  static long lastCountValue = 0;
  int index;
  char characterBuffer[224];
  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

  if ( lastCountValue != passengerCount->passengerCountComms->triggerRXInterruptCounter ||
		  thisDisplay->visualtest->updateDisplayAtleastOnce == 0 )
  {
  index = sprintf( characterBuffer, "RX Counter:%u ", passengerCount->passengerCountComms->triggerRXInterruptCounter);

  displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
	  lastCountValue = passengerCount->passengerCountComms->triggerRXInterruptCounter;
	  thisDisplay->visualtest->updateDisplayAtleastOnce = 1;

  }
}


void Visualtest_DMACounter( Displayline displayLine )
{
  static unsigned int lastCountValue = 0;
  int index;
  char characterBuffer[224];
  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

  if ( lastCountValue != thisDisplay->network->numberSuccessfulDMATransfers )
  {
  index = sprintf( characterBuffer, "DMAG:%u ", thisDisplay->network->numberSuccessfulDMATransfers );
  index += sprintf( characterBuffer + index, " DMAB:%u ", thisDisplay->network->numberFailedDMATransfers );

  displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
	  lastCountValue = thisDisplay->network->numberSuccessfulDMATransfers;

  }
}

void Visualtest_CRCCounter( Displayline displayLine )
{
  static unsigned int lastCountValue = 0;
  int index;
  char characterBuffer[224];
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;

  if ( lastCountValue != (thisDisplay->passengerCount->CRCFails
		                             + thisDisplay->passengerCount->CRCPasses ))
  {
  index = sprintf( characterBuffer, "CRCG:%u ", thisDisplay->passengerCount->CRCPasses );
  index += sprintf( characterBuffer + index, " CRCB:%u ", thisDisplay->passengerCount->CRCFails );

  //index += sprintf( characterBuffer + index, " DoorCycle:%u ", thisDisplay->passengerCount->doorCycleSuccess );

  index += sprintf( characterBuffer + index, " CRCG1:%u ",
		                 thisDisplay->passengerCount->passengerCounter1->crcPasses );
  index += sprintf( characterBuffer + index, " CRCG2:%u ",
		                 thisDisplay->passengerCount->passengerCounter2->crcPasses );
  index += sprintf( characterBuffer + index, " CRCG3:%u ",
		                 thisDisplay->passengerCount->passengerCounter3->crcPasses );
  index += sprintf( characterBuffer + index, " CRCG4:%u ",
		                 thisDisplay->passengerCount->passengerCounter4->crcPasses );

  index += sprintf( characterBuffer + index, " CRCB1:%u ",
		                 thisDisplay->passengerCount->passengerCounter1->crcFails );
  index += sprintf( characterBuffer + index, " CRCB2:%u ",
		                 thisDisplay->passengerCount->passengerCounter2->crcFails );
  index += sprintf( characterBuffer + index, " CRCB3:%u ",
		                 thisDisplay->passengerCount->passengerCounter3->crcFails );
  index += sprintf( characterBuffer + index, " CRCB4:%u ",
		                 thisDisplay->passengerCount->passengerCounter4->crcFails );

  displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
	  lastCountValue = (thisDisplay->passengerCount->CRCFails
			                                  + thisDisplay->passengerCount->CRCPasses);
  }
}


void Visual_TestPassengerCounterMessage( Displayline displayLine,
												unsigned char passengerCounter)
{
  int index;
  char characterBuffer[224];
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;

  switch ( passengerCounter )
  {
  case 1:
	  index = sprintf( characterBuffer, "Passenger Counter 1 Under Test LEDS On" );
  break;
  case 2:
	  index = sprintf( characterBuffer, "Passenger Counter 2 Under Test LEDS On" );
  break;
  case 3:
	  index = sprintf( characterBuffer, "Passenger Counter 3 Under Test LEDS On" );
  break;
  case 4:
	  index = sprintf( characterBuffer, "Passenger Counter 4 Under Test LEDS On" );
  break;

  case 5:
	  index = sprintf( characterBuffer, "Passenger Counter 1 Under Test LEDS Off" );
  break;
  case 6:
	  index = sprintf( characterBuffer, "Passenger Counter 2 Under Test LEDS Off" );
  break;
  case 7:
	  index = sprintf( characterBuffer, "Passenger Counter 3 Under Test LEDS Off" );
  break;
  case 8:
	  index = sprintf( characterBuffer, "Passenger Counter 4 Under Test LEDS Off" );
  break;

  default:
	  index = sprintf( characterBuffer, "Passenger Counter Under Test" );
  break;
  }
	  displayLine->messageComplete = 0x00;
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
}


void Visualtest_TestPassengerCounterComms( Displayline displayLine, PassengerCount passengerCount )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;

	if (counter == 20)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 1);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter1 );
	Display_enablePanelScan();

	}

	if (counter == 40)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 5);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter1 );
	Display_enablePanelScan();
	}

	if (counter == 60)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 1);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter1 );
	Display_enablePanelScan();
	}

	if (counter == 80)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 5);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter1 );
	Display_enablePanelScan();
	}

	if (counter == 100)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 2);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter2 );
	Display_enablePanelScan();
	}

	if (counter == 120)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 6);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter2 );
	Display_enablePanelScan();
	}

	if (counter == 140)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 2);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter2 );
	Display_enablePanelScan();
	}

	if (counter == 160)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 6);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter2 );
	Display_enablePanelScan();
	}


	if (counter == 180)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 3);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter3 );
	Display_enablePanelScan();
	}

	if (counter == 200)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 7);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter3 );
	Display_enablePanelScan();
	}

	if (counter == 220)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 3);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter3 );
	Display_enablePanelScan();
	}

	if (counter == 240)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 7);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter3 );
	Display_enablePanelScan();

	}

	if (counter == 260)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 4);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter4 );
	Display_enablePanelScan();
	}

	if (counter == 280)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 8);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter4 );
	Display_enablePanelScan();
	}

	if (counter == 300)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 4);
	PassengerCounter_ledsON( passengerCount, passengerCount->passengerCounter4 );
	Display_enablePanelScan();
	}

	if (counter == 320)
	{
	counter++;
	Display_disablePanelScan();
	Visual_TestPassengerCounterMessage( displayLine, 8);
	PassengerCounter_ledsOFF( passengerCount, passengerCount->passengerCounter4 );
	Display_enablePanelScan();
	counter = 0;
	}

	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}


void Visualtest_TestPassengerCounterCRCTest( Displayline displayLine, PassengerCount passengerCount )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;

	if (counter == 30)
	{
	counter++;
	Display_disablePanelScan();
	PassengerCount_startCounters( passengerCount );
	Display_enablePanelScan();

	}

	if (counter == 60)
	{
	counter++;
	Display_disablePanelScan();
	PassengerCount_stopCounters( passengerCount );
	Display_enablePanelScan();
	}

	if (counter == 120)
	{
	counter++;
	counter = 0;
	}

	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}



void Visualtest_WriteNVMemory( Displayline displayLine )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 20;
	static unsigned int messageNumber = 0;

	char characterBuffer[defaultMessageSize];
	sprintf( characterBuffer, "This is a default message stored in flash number: %u ", messageNumber);

	if (counter == 20)
	{
	counter++;
	NVmemory_displayMessage1( displayLine, thisDisplay->nvmemory  );
	}

	if (counter == 40)
	{
	counter++;
	NVmemory_displayMessage2( displayLine, thisDisplay->nvmemory  );
	}

	if (counter == 60)
	{
	counter++;
	NVmemory_displayMessage3( displayLine, thisDisplay->nvmemory  );
	}

	if (counter == 80)
	{
	counter++;
	thisDisplay->defaultMessage->textMessage1->charSet = 2;
	thisDisplay->defaultMessage->textMessage1->prePend = 96;
	thisDisplay->defaultMessage->textMessage1->modeDisplayLine = 1;
	sprintf( characterBuffer, "This is a default message stored in flash number: %u ", messageNumber);
	DefaultMessage_setText(thisDisplay->defaultMessage->textMessage1->Text1, characterBuffer);
	sprintf( characterBuffer, "This is a default message stored in flash number: %u ", ++messageNumber );
	DefaultMessage_setText(thisDisplay->defaultMessage->textMessage2->Text2, characterBuffer);
	sprintf( characterBuffer, "This is a default message stored in flash number: %u ", ++messageNumber );
	DefaultMessage_setText(thisDisplay->defaultMessage->textMessage3->Text3, characterBuffer);
	NVmemory_writeDataTable(displayLine, thisDisplay->network->neuron, thisDisplay->nvmemory  );

	messageNumber++;
	}
	if (counter == 400)
	{
	  counter = 0;
	}

	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}


void Visualtest_TestNVMemory( Displayline displayLine )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 100;

	if (counter == 100)
	{
	counter++;
	NVmemory_displayMessage1( displayLine, thisDisplay->nvmemory  );
	}

	if (counter == 200)
	{
	counter++;
	NVmemory_displayMessage2( displayLine, thisDisplay->nvmemory  );
	}

	if (counter == 300)
	{
	counter++;
	NVmemory_displayMessage3( displayLine, thisDisplay->nvmemory  );
	}

	if (counter == 400)
	{
		counter = 0;
	}


	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}


void Visualtest_TestDefaultMessagePacket( Displayline displayLine )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;
	char characterBuffer[224];
	static unsigned int messageNumber = 0;

	unsigned char index = 0;
	static unsigned char character = 48;



	if (counter == 0)
	{

		if ( character < 64)
		{
			character++;
		}
		else
		{
			character = 48;
		}

		index = 0;
		while (index < 50 )
		{
			characterBuffer[index] = character;
			index++;
		}
		//Place a null at the end of the string of chracters in the buffer
		//characterBuffer that we wish to send as a text message to the saloon display
		characterBuffer[index] = '\0';
		Network_SendSaloonLine1TextMessage( thisDisplay->network, characterBuffer );
		displayLine->messageComplete = 0x00;
		Displayline_set_messageBuffer(displayLine, characterBuffer);
		displayLine->displayLineDotsCreated = 0;
		displayLine->messageChanged = 1;
		counter++;
		messageNumber++;
	}

	if (counter == 100)
	{
	sprintf( characterBuffer, "Message received over power line for Saloon Display Line 2 and stored in flash. Message number: %u ", messageNumber );
	Network_DefaultSaloonLine2Message( thisDisplay->network, characterBuffer );
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 2 -  Message number: %u ", messageNumber );;
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 200)
	{
	sprintf( characterBuffer, "Message received over power line for Saloon Display Line 3 and stored in flash. Message number: %u ", messageNumber );
	Network_DefaultSaloonLine3Message( thisDisplay->network, characterBuffer );
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 3 -  Message number: %u ", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter > 90 )
	{
		counter = 0;
	}


	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}

void Visualtest_PowerLineCRC( Displayline displayLine )
{
  static unsigned int lastValue = 0;
  int index;
  char characterBuffer[224];
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;

  if ( lastValue != (thisDisplay->network->numberCRCFailures
		                             + thisDisplay->network->numberCRCPasses ))
  {
  index = sprintf( characterBuffer, "NumberCRCPasses:%u ", thisDisplay->network->numberCRCPasses );
  index += sprintf( characterBuffer + index, "NumberCRCFails :%u ", thisDisplay->network->numberCRCFailures );

  displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
	  lastValue = (thisDisplay->network->numberCRCFailures
										  + thisDisplay->network->numberCRCPasses);
  }
}
void Visualtest_PowerLineCRCResult( Displayline displayLine )
{
  static unsigned int lastValue = 0;
  int index;
  char characterBuffer[224];
  displayLine->prePend = 112;
  displayLine->scrollEnable = 1;

  if ( lastValue != thisDisplay->network->CRCResult)
  {
  index = sprintf( characterBuffer, "CRCResult:%x", thisDisplay->network->CRCResult );

  index += sprintf( characterBuffer + index, " :%d ", thisDisplay->network->CRCResult);

  index += sprintf( characterBuffer + index, " :%d", (unsigned char) (thisDisplay->network->CRCResult >> 8) );
  index += sprintf( characterBuffer + index, " :%d", (unsigned char) (thisDisplay->network->CRCResult) );


  displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
	  lastValue = (thisDisplay->network->CRCResult);
  }
}

void Visualtest_PowerLineCRCApisCompare( Displayline displayLine )
{
  static unsigned int lastValue = 0;
  int index;
  char characterBuffer[224];
  displayLine->prePend = 0;
  displayLine->scrollEnable = 0;

  if ( lastValue != thisDisplay->network->CRCResult_Received + thisDisplay->network->CRCResult_Calculated )
  {
  index = sprintf( characterBuffer, "Rx:%x", thisDisplay->network->CRCResult_Received );

  index += sprintf( characterBuffer+ index, " C:%x", thisDisplay->network->CRCResult_Calculated );

  index += sprintf( characterBuffer + index, " Rx2:%d", (unsigned char) (thisDisplay->network->CRCResult_Received >> 8 ) );
  index += sprintf( characterBuffer + index, " Rx1:%d ", (unsigned char) (thisDisplay->network->CRCResult_Received) );

  index += sprintf( characterBuffer + index, " C1:%d", (unsigned char) (thisDisplay->network->CRCResult_Calculated >> 8 ) );
  index += sprintf( characterBuffer + index, " C2:%d", (unsigned char) (thisDisplay->network->CRCResult_Calculated) );
  displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	  Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#else
	  Displayline_set_messageBuffer(displayLine, characterBuffer);
#endif
	  displayLine->displayLineDotsCreated = 0;
	  displayLine->messageChanged = 1;
	  lastValue = (thisDisplay->network->CRCResult_Received + thisDisplay->network->CRCResult_Calculated);
  }
}


void Visualtest_SendDefaultMessagePacket( Displayline displayLine )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;
	char characterBuffer[224];
	static unsigned int messageNumber = 0;

	if (counter == 0)
	{
	//Send a default message over the power line for line 1 of the display:
	sprintf( characterBuffer, "Saloon Display Line 1 and stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'S', '1'  );
	//Indicate on the current display that the default message has been sent for line 1 of the display:
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 1 -  Message:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 30)
	{
	//Send default message over the power line for line 2 of the display:
	sprintf( characterBuffer, "Saloon Display Line 2 and stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'S', '2'  );
	//Indicate on the current display that the default message has been sent for line 2 of the display:
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 2 - Message number:%u", messageNumber );;
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 60)
	{
	//Send a default message over the power line for line 3 of the display:
	sprintf( characterBuffer, "Saloon Display Line 3 and stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'S', '3'  );
	//Indicate on the current display that the default message has been sent for line 3 of the display:
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 3 - Message number:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 90)
	{
	//Send a default message over the power line for line 1 of the display:
	sprintf( characterBuffer, "Destination Display Line 1 stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'D', '1');
	//Indicate on the current display that the default message has been sent for line 1 of the display:
	sprintf( characterBuffer, "Sent Default message for Destination Display Line 1 - Message:%u ", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 120 )
	{
	//Send a default message over the power line for line 2 of the display:
	sprintf( characterBuffer, "Destination Display Line 2 stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'D', '2'  );
	//Indicate on the current display that the default message has been sent for line 2 of the display:
	sprintf( characterBuffer, "Sent Default message for Destination Display Line 2 - Message:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 150 )
	{
	//Send a default message over the power line for line 3 of the display:
	sprintf( characterBuffer, "Destination Display Line 3 stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'D', '3'  );
	//Indicate on the current display that the default message has been sent for line 3 of the display:
	sprintf( characterBuffer, "Sent Default message for Destination Display Line 3 - Message:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter > 200 )
	{
		counter = 0;
	}

	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}

void Visualtest_SendTextMessage()
{

	static unsigned char lastTick = 0;
	static unsigned int counter = 0;
	static int frameLength = 20;
	static char character = 65;
	char sendBuffer[200];
	int loopCount;


	//Initialise the frameLength for the message to be sent
	if ( counter == 0 )
	{
		if (frameLength > 190 )
		{
			frameLength = 20;
		}
		else
		{
			frameLength++;
		}

		if (character > 50 )
		{
			character = 48;
		}
		counter++;
	}

	//Create message frame the will be used multiple times
	//to make up the final text message to be sent.
	//In the current code the frame is placed into the text message to be sent
	//4 times.
	if ( counter == 5)
	{

		sprintf(sendBuffer, "%03u", (frameLength*4) );
		loopCount = 3;
		while ( loopCount < frameLength )
		{
			sendBuffer[loopCount] = character;
			loopCount++;
		}
		sendBuffer[loopCount++] = 'E';
		sendBuffer[loopCount++] = 'N';
		sendBuffer[loopCount++] = 'D';
		Network_SendSaloonLine1TextMessage( thisDisplay->network , sendBuffer );
		counter++;

	}


	if ( counter == 10)
	{

		sprintf(sendBuffer, "%03u", frameLength );
		loopCount = 3;
		while ( loopCount < frameLength )
		{
			sendBuffer[loopCount] = character;
			loopCount++;
		}
		sendBuffer[loopCount++] = 'E';
		sendBuffer[loopCount++] = 'N';
		sendBuffer[loopCount++] = 'D';
		Network_SendDestinationLine1TextMessage( thisDisplay->network , sendBuffer  );
		counter++;
	}

	if ( counter == 15)
	{

		sprintf(sendBuffer, "%03u", frameLength );
		loopCount = 3;
		while ( loopCount < frameLength )
		{
			sendBuffer[loopCount] = character;
			loopCount++;
		}
		sendBuffer[loopCount++] = 'E';
		sendBuffer[loopCount++] = 'N';
		sendBuffer[loopCount++] = 'D';
		Network_SendDestinationLine2TextMessage( thisDisplay->network , sendBuffer  );
		counter++;
	}
	if (counter > 30 )
	{
		counter = 0;
		character++;
	}


	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}


void Visualtest_Class319TestSuite( Displayline displayLine )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;
	char characterBuffer[224];
	static unsigned int messageNumber = 0;

	if (counter == 0)
	{
	//Send a default message over the power line for line 1 of the display:
	sprintf( characterBuffer, "Saloon Display Line 1 and stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'S', '1'  );
	//Indicate on the current display that the default message has been sent for line 1 of the display:
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 1 -  Message:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 30)
	{
	//Send default message over the power line for line 2 of the display:
	sprintf( characterBuffer, "Saloon Display Line 2 and stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'S', '2'  );
	//Indicate on the current display that the default message has been sent for line 2 of the display:
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 2 - Message number:%u", messageNumber );;
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 60)
	{
	//Send a default message over the power line for line 3 of the display:
	sprintf( characterBuffer, "Saloon Display Line 3 and stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'S', '3'  );
	//Indicate on the current display that the default message has been sent for line 3 of the display:
	sprintf( characterBuffer, "Sent Default message for Saloon Display Line 3 - Message number:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 90)
	{
	//Send a default message over the power line for line 1 of the display:
	sprintf( characterBuffer, "Destination Display Line 1 stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'D', '1');
	//Indicate on the current display that the default message has been sent for line 1 of the display:
	sprintf( characterBuffer, "Sent Default message for Destination Display Line 1 - Message:%u ", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 120 )
	{
	//Send a default message over the power line for line 2 of the display:
	sprintf( characterBuffer, "Destination Display Line 2 stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'D', '2'  );
	//Indicate on the current display that the default message has been sent for line 2 of the display:
	sprintf( characterBuffer, "Sent Default message for Destination Display Line 2 - Message:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter == 150 )
	{
	//Send a default message over the power line for line 3 of the display:
	sprintf( characterBuffer, "Destination Display Line 3 stored in flash. Message:%u", messageNumber );
	Network_DefaultDisplayLineMessage( thisDisplay->network, characterBuffer, 'D', '3'  );
	//Indicate on the current display that the default message has been sent for line 3 of the display:
	sprintf( characterBuffer, "Sent Default message for Destination Display Line 3 - Message:%u", messageNumber );
	displayLine->messageComplete = 0x00;
	Displayline_set_messageBuffer(displayLine, characterBuffer);
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter > 200 )
	{
		Visualtest_SendTextMessage();
	}

	if (counter > 1800 )
	{
		counter = 0;
	}
	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}



void Visualtest_counterDisplay( Displayline displayLine )
{
	static unsigned char lastTick = 0;
	static unsigned int counter = 0;
	char characterBuffer[224];
	static unsigned int messageNumber = 0;

	if (counter == 0)
	{
	//Indicate on the current display that the default message has been sent for line 1 of the display:
	sprintf( characterBuffer, "Message----:%u", messageNumber );
	displayLine->messageComplete = 0x00;
#if DISPLAY_TYPE == REVERSED_FRONT_DESTINATION_DISPLAY
	Displayline_set_reversedMessageBuffer(displayLine, characterBuffer);
#elif DISPLAY_TYPE == STANDARD_FRONT_DESTINATION_DISPLAY
	Displayline_set_messageBuffer(displayLine, characterBuffer);
#else
#error DISPLAY_TYPE not specified
#endif
	displayLine->displayLineDotsCreated = 0;
	displayLine->messageChanged = 1;
	counter++;
	messageNumber++;
	}

	if (counter > 1 )
	{
		counter = 0;
	}

	if (lastTick !=clkTick)
	{
	 counter++;
	 lastTick = clkTick;
	}
}




