//// Utilities for working with characters and their dot representations.
////
////-----------------------------------------------------------------------------
//// Copyright (C) 2009 TrainFX. All rights reserved.
////-----------------------------------------------------------------------------
//
//#include "characterutils.h"
//
//// Standard C library includes.
//#include "ctype.h"
//
//// Project includes.
//#include "tfx_assert.h"
//
//extern enum CharSet characterSet;
//
//// Trim leading and trailing "blanks" from the specified dot representation.
//void CharUtils_trim_dots( Dots char_dots, enum CharSet characterSet) {
//  // Create a working buffer --- storage for the trimmed dot representation
//  // as we build it.
//  Dots working_space = NULL;
//  if (working_space == NULL) {
//    working_space = Dots_construct();
//  } else {
//    Dots_clear(working_space);
//  }
//
//  TFX_ASSERT(char_dots, "Expected a non-null pointer.");
//
//  // Copy the non-blank dots into the working buffer.
//  const int initial_length = Dots_get_length(char_dots);
//  {
//	// If we have a small text, or small number character we need to
//	// trim off a column only if we see 16 zeros in a column -
//	// that is two chars of zeros.
//	// If we have a small text, or small number character we parse the dot
//	// file one char at a time - the character set is 12 rows high.
//
//	if (characterSet == SmallText || characterSet == SmallNumber)
//	{
//		int i = 0;
//		for (i = 0; i < initial_length; i +=1) {
//		  char item_i1 = Dots_get_value(char_dots, i);
//		  if (item_i1 != 0 ) {
//			Dots_append_value(working_space, item_i1);
//		  }
//		}
//	  // We want to have some inter-character spacing, so insert a single zero.
//	  Dots_append_value(working_space, 0x00);
//	}
//
//  // Copy from the working buffer back into the output variable.
//  Dots_clear(char_dots);
//  Dots_append_dots(char_dots, working_space);
//  }
//}
