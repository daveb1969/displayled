// Application entry point.
//
//-----------------------------------------------------------------------------
// Copyright (C) 2009 TrainFX. All rights reserved.
//-----------------------------------------------------------------------------

// Standard C library includes.
#include "stdlib.h"

// Project includes.
#include "app/fwork.h"

//-----------------------------------------------------------------------------
// Main application entry point.
int main(void) {

  // Initialise the application framework.
  frameWorkInit();

  // Run the application framework.
  frameWorkRun();

  // Clean up.
  frameWorkCleanUp();

  return EXIT_SUCCESS;
}
