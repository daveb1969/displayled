/*
 * TFX-SPI.c
 *
 *  Created on: 7 Sep 2010
 *      Author: DAVE
 */

#include "TFX-SPI.H"
#include "../utils/tfx_assert.h"
#include "malloc.h"


// Constructor.
//
//
Spibuf SPI_construct()
{
	Spibuf spibuf = ( struct spibuf_str* ) malloc(sizeof(struct spibuf_str));
	TFX_ASSERT(spibuf, "Failed to allocate memory.");
	return spibuf;
}

// Destructor.
void SPI_destroy(Spibuf spibuf) {
	TFX_ASSERT(spibuf, "Expected a non-null pointer.");
	free(spibuf);
}
