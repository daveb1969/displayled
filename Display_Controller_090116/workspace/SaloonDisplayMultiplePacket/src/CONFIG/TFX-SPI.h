
#ifndef TFX_SPI_H
#define TFX_SPI_H


/* ======================  SPI Declarations =======================  */
/* Route  SPI Declarations =======================  */
/* Common definitions for both sides of the link                     */

/* The first SPI transfer needs to have exactly this size.
   This is an arbytrary number but it needs to be agreed by the
   Master and the Slave. In our case, this number can not exceed 31 
   bytes because the Master's Neurowire object model can only
   transmit/receive 255 bits at once.                                */
#define MIN_SPI_LEN     224

/* The current protocol has a requirment that 224 bytes be passed in
   each SPI transfer that takes place. */
#define MAX_SPI_LEN		224

/* The full transaction buffer has three pieces, the length byte, the
   type-of-transaction byte and the data itself, this is why the data
   length is 2 bytes smaller than the MAX_SPI_LEN value.             */
#define MAX_DATA_SIZE   (MAX_SPI_LEN - 2)

/* The Transaction Type enum values are to be defined depending on 
   the application needs. The following types of messages are just 
   examples.                                                         */
typedef enum {
	TT_NULL = 0,  /* Null value (Only mandatory element of the enum) */
	TT_DOWN_IDENT, /* Downlink message asking for Neuron ID          */
	TT_UP_NID,    /* Uplink message containing the Neuron & Prog ID  */
	TT_UP_MSG,    /* Generic Uplink (Slave to Master) message        */
	TT_DOWN_MSG,  /* Generic Downlink (Master to Slave) message      */
	TT_UP_TFXMSG,  /* Low Priority Uplink (Slave to Master) message */
	TT_DOWN_TFXMSG, /* Low Priority Downlink (Master to Slave) message */
	TT_TRIGGER_DIAG
} TTYPE;

#define SUB_TT_DESTINATION 	1
#define SUB_TT_SALOON		2
#define SUB_TT_DISPLAY_NEURON 3
#define SUB_TT_SIDE 4
#define SUB_TT_KEYPAD 5
#define SUB_TT_AUDIO 8
#define SUB_TT_SALOON_AMP_COMMS 9
#define SUB_TT_GLOBAL 10



/* All transactions are contained in this structure.  The length of
   the transaction (transLen) will include transType and the length
   of the data, it won't include transLen.                           */
struct spibuf_str{
	unsigned char transLen;
    unsigned char transType;	/* enum TTYPE */
	unsigned char data[MAX_SPI_LEN];
};


typedef struct spibuf_str* Spibuf;


// Constructor.
//
//
Spibuf SPI_construct();

// Destructor.
void SPI_destroy(Spibuf spibuf);

#endif  /* TFX_SPI_H */
