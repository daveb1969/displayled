/* This source file is part of the ATMEL AVR32-SoftwareFramework-1.2.2ES-AT32UC3B Release */

/*This file is prepared for Doxygen automatic documentation generation.*/
/*! \file *********************************************************************
 *
 * \brief Management of the AT45DBX data flash controller through SPI.
 *
 * This file manages the accesses to the AT45DBX data flash components.
 *
 * - Compiler:           IAR EWAVR32 and GNU GCC for AVR32
 * - Supported devices:  All AVR32 devices with an SPI module can be used.
 * - AppNote:
 *
 * \author               Atmel Corporation: http://www.atmel.com \n
 *                       Support and FAQ: http://support.atmel.no/
 *
 ******************************************************************************/

/* Copyright (c) 2007, Atmel Corporation All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of ATMEL may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
 * SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


//_____  I N C L U D E S ___________________________________________________

#include "compiler.h"
#include "board.h"
#include "gpio.h"
#include "spi.h"
#include "echelon.h"
#include "conf_echelon.h"


/*! \brief Sends a dummy byte through SPI.
 */
#define echelon_spi_write_dummy()   spi_write(ECHELON_SPI, 0xFF)

/*! \name Control Functions
 */
//! @{


Bool echelon_init(spi_options_t spiOptions, unsigned int pba_hz)
{
  if (spi_setupChipReg(ECHELON_SPI, &spiOptions, pba_hz) != SPI_OK) return KO;
  return OK;
}


/*! \brief Selects or unselects echelon SPI channel
 *
 */
static void echelon_chipselect(Bool bSelect)
{
  if (bSelect)
  {
    // Select SPI chip.
    spi_selectChip(ECHELON_SPI, ECHELON_SPI_CHANNEL);
  }
  else
  {
    // Unselect SPI chip.
    spi_unselectChip(ECHELON_SPI, ECHELON_SPI_CHANNEL);
  }
}

