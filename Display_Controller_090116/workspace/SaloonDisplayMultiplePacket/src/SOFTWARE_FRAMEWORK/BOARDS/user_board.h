/* This header file is part of the ATMEL AVR32-UC3-SoftwareFramework-1.6.0 Release */

/*This file is prepared for Doxygen automatic documentation generation.*/
/*! \file *********************************************************************
 *
 * \brief User board header file.
 *
 * This file includes the appropriate board header file according to the
 * defined board.
 *
 * - Compiler:           IAR EWAVR32 and GNU GCC for AVR32
 * - Supported devices:  All AVR32 devices can be used.
 * - AppNote:
 *
 * \author				 TrainFX Limited
 *
 *
 *
 ******************************************************************************/

#ifndef USER_BOARD_H_
#define USER_BOARD_H_

#include "compiler.h"

#ifdef __AVR32_ABI_COMPILER__ // Automatically defined when compiling for AVR32, not when assembling.
//#  include "led.h"
#endif  // __AVR32_ABI_COMPILER__


/*! \name Oscillator Definitions
 */
//! @{

// RCOsc has no custom calibration by default. Set the following definition to
// the appropriate value if a custom RCOsc calibration has been applied to your
// part.
//#define FRCOSC          AVR32_PM_RCOSC_FREQUENCY              //!< RCOsc frequency: Hz.

#define FOSC32          32768                                 //!< Osc32 frequency: Hz.
#define OSC32_STARTUP   AVR32_PM_OSCCTRL32_STARTUP_8192_RCOSC //!< Osc32 startup time: RCOsc periods.

#define FOSC0           12000000                              //!< Osc0 frequency: Hz.
#define OSC0_STARTUP    AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC  //!< Osc0 startup time: RCOsc periods.

// Osc1 crystal is not mounted by default. Set the following definitions to the
// appropriate values if a custom Osc1 crystal is mounted on your board.
//#define FOSC1           12000000                              //!< Osc1 frequency: Hz.
//#define OSC1_STARTUP    AVR32_PM_OSCCTRL1_STARTUP_2048_RCOSC  //!< Osc1 startup time: RCOsc periods.

//! @}


/*! \name USB Definitions
 */
//! @{

//! Multiplexed pin used for USB_ID: AVR32_USBB_USB_ID_x_x.
//! To be selected according to the AVR32_USBB_USB_ID_x_x_PIN and
//! AVR32_USBB_USB_ID_x_x_FUNCTION definitions from <avr32/uc3bxxxx.h>.
#define USB_ID                      AVR32_USBB_USB_ID_0_0

//! Multiplexed pin used for USB_VBOF: AVR32_USBB_USB_VBOF_x_x.
//! To be selected according to the AVR32_USBB_USB_VBOF_x_x_PIN and
//! AVR32_USBB_USB_VBOF_x_x_FUNCTION definitions from <avr32/uc3bxxxx.h>.
#define USB_VBOF                    AVR32_USBB_USB_VBOF_0_0

//! Active level of the USB_VBOF output pin.
#define USB_VBOF_ACTIVE_LEVEL       LOW

//! USB overcurrent detection pin.
#define USB_OVERCURRENT_DETECT_PIN  AVR32_PIN_PA20

//! @}


//! Number of LEDs.
#define LED_COUNT   4

/*! \name GPIO Connections of LEDs
 */
//! @{
#define LED0_GPIO   AVR32_PIN_PA07
#define LED1_GPIO   AVR32_PIN_PA08
#define LED2_GPIO   AVR32_PIN_PA21
#define LED3_GPIO   AVR32_PIN_PA22
//! @}

/*! \name PWM Channels of LEDs
 */
//! @{
#define LED0_PWM    0
#define LED1_PWM    1
#define LED2_PWM    2
#define LED3_PWM    6
//! @}

/*! \name PWM Functions of LEDs
 */
//! @{
#define LED0_PWM_FUNCTION   AVR32_PWM_0_0_FUNCTION
#define LED1_PWM_FUNCTION   AVR32_PWM_1_0_FUNCTION
#define LED2_PWM_FUNCTION   AVR32_PWM_2_0_FUNCTION
#define LED3_PWM_FUNCTION   AVR32_PWM_6_0_FUNCTION
//! @}

/*! \name Color Identifiers of LEDs to Use with LED Functions
 */
//! @{
#define LED_MONO0_GREEN   LED0
#define LED_MONO1_GREEN   LED1
#define LED_MONO2_GREEN   LED2
#define LED_MONO3_GREEN   LED3
//! @}


/*! \name GPIO Connections of Push Buttons
 */
//! @{
#define GPIO_PUSH_BUTTON_0            AVR32_PIN_PB02
#define GPIO_PUSH_BUTTON_0_PRESSED    0
#define GPIO_PUSH_BUTTON_1            AVR32_PIN_PB03
#define GPIO_PUSH_BUTTON_1_PRESSED    0
//! @}


/*! \name GPIO Connections of the Joystick
 */
//! @{
#define GPIO_JOYSTICK_PUSH            AVR32_PIN_PA13
#define GPIO_JOYSTICK_PUSH_PRESSED    0
#define GPIO_JOYSTICK_LEFT            AVR32_PIN_PB06
#define GPIO_JOYSTICK_LEFT_PRESSED    0
#define GPIO_JOYSTICK_RIGHT           AVR32_PIN_PB09
#define GPIO_JOYSTICK_RIGHT_PRESSED   0
#define GPIO_JOYSTICK_UP              AVR32_PIN_PB07
#define GPIO_JOYSTICK_UP_PRESSED      0
#define GPIO_JOYSTICK_DOWN            AVR32_PIN_PB08
#define GPIO_JOYSTICK_DOWN_PRESSED    0
//! @}


/*! \name ADC Connection of the Temperature Sensor
 */
//! @{
#define ADC_TEMPERATURE_CHANNEL     1
#define ADC_TEMPERATURE_PIN         AVR32_ADC_AD_1_PIN
#define ADC_TEMPERATURE_FUNCTION    AVR32_ADC_AD_1_FUNCTION
//! @}


/*! \name ADC Connection of the Light Sensor
 */
//! @{
#define ADC_LIGHT_CHANNEL           0
#define ADC_LIGHT_PIN               AVR32_ADC_AD_0_PIN
#define ADC_LIGHT_FUNCTION          AVR32_ADC_AD_0_FUNCTION
//! @}



/*! \name ADC Connections of the Accelerometer
 */
//! @{
#define ADC_ACC_X_CHANNEL           1
#define ADC_ACC_X_PIN               AVR32_ADC_AD_1_PIN
#define ADC_ACC_X_FUNCTION          AVR32_ADC_AD_1_FUNCTION
#define ADC_ACC_Y_CHANNEL           2
#define ADC_ACC_Y_PIN               AVR32_ADC_AD_2_PIN
#define ADC_ACC_Y_FUNCTION          AVR32_ADC_AD_2_FUNCTION
#define ADC_ACC_Z_CHANNEL           3
#define ADC_ACC_Z_PIN               AVR32_ADC_AD_3_PIN
#define ADC_ACC_Z_FUNCTION          AVR32_ADC_AD_3_FUNCTION
//! @}


/*! \name PWM Connections of Audio
 */
//! @{
#define AUDIO_LOW_PWM_CHANNEL       5
#define AUDIO_LOW_PWM_PIN           AVR32_PWM_5_0_PIN
#define AUDIO_LOW_PWM_FUNCTION      AVR32_PWM_5_0_FUNCTION
#define AUDIO_HIGH_PWM_CHANNEL      6
#define AUDIO_HIGH_PWM_PIN          AVR32_PWM_6_1_PIN
#define AUDIO_HIGH_PWM_FUNCTION     AVR32_PWM_6_1_FUNCTION
//! @}


/*! \name SPI Connections of the AT45DBX Data Flash Memory
 */
//! @{
#define AT45DBX_SPI                 (&AVR32_SPI)
#define AT45DBX_SPI_NPCS            0
#define AT45DBX_SPI_SCK_PIN         AVR32_SPI_SCK_0_0_PIN
#define AT45DBX_SPI_SCK_FUNCTION    AVR32_SPI_SCK_0_0_FUNCTION
#define AT45DBX_SPI_MISO_PIN        AVR32_SPI_MISO_0_0_PIN
#define AT45DBX_SPI_MISO_FUNCTION   AVR32_SPI_MISO_0_0_FUNCTION
#define AT45DBX_SPI_MOSI_PIN        AVR32_SPI_MOSI_0_0_PIN
#define AT45DBX_SPI_MOSI_FUNCTION   AVR32_SPI_MOSI_0_0_FUNCTION
#define AT45DBX_SPI_NPCS0_PIN       AVR32_SPI_NPCS_0_0_PIN
#define AT45DBX_SPI_NPCS0_FUNCTION  AVR32_SPI_NPCS_0_0_FUNCTION
//! @}


/*! \name GPIO and SPI Connections of the SD/MMC Connector
 */
//! @{
#define SD_MMC_CARD_DETECT_PIN      AVR32_PIN_PB00
#define SD_MMC_WRITE_PROTECT_PIN    AVR32_PIN_PB01
#define SD_MMC_SPI                  (&AVR32_SPI)
#define SD_MMC_SPI_NPCS             1
#define SD_MMC_SPI_SCK_PIN          AVR32_SPI_SCK_0_0_PIN
#define SD_MMC_SPI_SCK_FUNCTION     AVR32_SPI_SCK_0_0_FUNCTION
#define SD_MMC_SPI_MISO_PIN         AVR32_SPI_MISO_0_0_PIN
#define SD_MMC_SPI_MISO_FUNCTION    AVR32_SPI_MISO_0_0_FUNCTION
#define SD_MMC_SPI_MOSI_PIN         AVR32_SPI_MOSI_0_0_PIN
#define SD_MMC_SPI_MOSI_FUNCTION    AVR32_SPI_MOSI_0_0_FUNCTION
#define SD_MMC_SPI_NPCS_PIN         AVR32_SPI_NPCS_1_0_PIN
#define SD_MMC_SPI_NPCS_FUNCTION    AVR32_SPI_NPCS_1_0_FUNCTION
//! @}


/*! \name TWI Connections of the Spare TWI Connector
 */
//! @{
//#define SPARE_TWI                   (&AVR32_TWI)
//#define SPARE_TWI_SCL_PIN           AVR32_TWI_SCL_0_0_PIN
//#define SPARE_TWI_SCL_FUNCTION      AVR32_TWI_SCL_0_0_FUNCTION
//#define SPARE_TWI_SDA_PIN           AVR32_TWI_SDA_0_0_PIN
//#define SPARE_TWI_SDA_FUNCTION      AVR32_TWI_SDA_0_0_FUNCTION
//! @}

/*! \name SPI Connections of the Spare SPI Connector
 */
//! @{
#define ECHELON_SPI                   (&AVR32_SPI)
#define ECHELON_SPI_NPCS              3
#define ECHELON_SPI_SCK_PIN           AVR32_SPI_SCK_0_0_PIN
#define ECHELON_SPI_SCK_FUNCTION      AVR32_SPI_SCK_0_0_FUNCTION
#define ECHELON_SPI_MISO_PIN          AVR32_SPI_MISO_0_0_PIN
#define ECHELON_SPI_MISO_FUNCTION     AVR32_SPI_MISO_0_0_FUNCTION
#define ECHELON_SPI_MOSI_PIN          AVR32_SPI_MOSI_0_0_PIN
#define ECHELON_SPI_MOSI_FUNCTION     AVR32_SPI_MOSI_0_0_FUNCTION
#define ECHELON_SPI_NPCS_PIN          AVR32_SPI_NPCS_3_1_PIN
#define ECHELON_SPI_NPCS_FUNCTION     AVR32_SPI_NPCS_3_1_FUNCTION
//! @}

//! @}
/*! \name PDCA Connections of the Spare SPI Connector
 */
//! @{
#define PDCA_PID_SPI_TX		  AVR32_PDCA_PID_SPI_TX
#define PDCA_PID_SPI_RX       AVR32_PDCA_PID_SPI_RX
//! @}



/*! \name GPIO Connections of Echelon A Handshake Lines
 */
//! @{
#define HREQA_GPIO   AVR32_PIN_PA30
#define SREQA_GPIO   AVR32_PIN_PA31
#define SRDYA_GPIO   AVR32_PIN_PA20
//! @}

/*! \name GPIO Connections of Echelon B Handshake Lines
 */
//! @{
#define HREQB_GPIO   AVR32_PIN_PA26
#define SREQB_GPIO   AVR32_PIN_PA27
#define SRDYB_GPIO   AVR32_PIN_PA28
//! @}

/*! \name GPIO Connections of Opto Isolated Call For Aid Lines
 */
//! @{
#define OPTO_01_GPIO   AVR32_PIN_PA08
#define OPTO_02_GPIO   AVR32_PIN_PA21
#define OPTO_03_GPIO   AVR32_PIN_PA22
//! @}


#  define RS485_USART               (&AVR32_USART1)
#  define RS485_USART_RX_PIN        AVR32_USART1_RXD_0_0_PIN
#  define RS485_USART_RX_FUNCTION   AVR32_USART1_RXD_0_0_FUNCTION
#  define RS485_USART_TX_PIN        AVR32_USART1_TXD_0_0_PIN
#  define RS485_USART_TX_FUNCTION   AVR32_USART1_TXD_0_0_FUNCTION
#  define RS485_USART_RTS_PIN       AVR32_USART1_RTS_0_1_PIN
#  define RS485_USART_RTS_FUNCTION  AVR32_USART1_RTS_0_1_FUNCTION
#  define RS485_USART_CTS_PIN       AVR32_USART1_CTS_0_1_PIN
#  define RS485_USART_CTS_FUNCTION  AVR32_USART1_CTS_0_1_FUNCTION
#  define RS485_USART_IRQ           AVR32_USART1_IRQ

//#  define RS485_USART_CLOCK_MASK    AVR32_USART1_CLK_PBA
//#  define RS485_PDCA_CLOCK_HSB      AVR32_PDCA_CLK_HSB
//#  define RS485_PDCA_CLOCK_PB       AVR32_PDCA_CLK_PBA



#endif /* USER_BOARD_H_ */
